import json
import time
from datetime import date, datetime
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
from django.test import TestCase
from django.shortcuts import reverse
from django.contrib.auth.models import User, Group
from django.core.management import call_command
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from authorization.models import City, Profile, School, ClassGroup
from authorization.utils import (
    new_city_data,
    basic_user_data,
    new_user_data,
    SeleniumViewHelper
)
from chat.models import Message
from dashboard.utils import DashboardTestMixin
from management.models import Invitation
from management.utils import principal_data, teacher_data, student_data
from .views import HomeView
from .utils import MainSeleniumViewHelper, InfoBlockHelper


class HomeViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(**basic_user_data)
        login_url = reverse("authorization:login")
        self.exp_url = f"{login_url}?next={reverse('main:home')}"

    def test_view_url_exists_by_location_without_user(self):
        """Tests visiting home view by location without user logged in."""
        res = self.client.get("/main/")
        # Redirect to login view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, self.exp_url)

    def test_view_url_exists_by_name_without_user(self):
        """Tests visiting home view by name without user logged in."""
        res = self.client.get(reverse("main:home"))
        # Redirect to login view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, self.exp_url)

    def test_view_url_exists_by_location_with_user(self):
        """Tests visiting home view by location with user logged in."""
        self.client.force_login(self.user)
        # Redirect to main view
        res = self.client.get("/main/")
        self.assertEqual(res.status_code, 200)

    def test_view_url_exists_by_name_with_user(self):
        """Tests visiting home view by name with user logged in."""
        self.client.force_login(self.user)
        # Redirect to main view
        res = self.client.get(reverse("main:home"))
        self.assertEqual(res.status_code, 200)

    def test_view_uses_correct_template(self):
        """Tests if home view uses proper template."""
        self.client.force_login(self.user)
        res = self.client.get(reverse("main:home"))
        self.assertTemplateUsed(res, "main/home.html")

    def test_view_template_content(self):
        """Tests if home view contains expected content."""
        self.client.force_login(self.user)
        res = self.client.get(reverse("main:home"))
        self.assertContains(res, '<div id="app">')


class HeaderSeleniumTest(
        MainSeleniumViewHelper,
        InfoBlockHelper,
        StaticLiveServerTestCase):
    def test_text_content(self):
        """
        Tests main heading content. Hello text with first and last name is
        expected.
        """
        self.login_user()

        header_part = self.browser.find_element_by_class_name(
            "main-header__part"
        )

        exp_text = f"Hello, {self.user.first_name} {self.user.last_name}"
        self.assertEqual(header_part.text, exp_text)

    def test_search_input_underline(self):
        """Tests search input underline."""
        self.login_user()

        input = self.browser.find_element_by_css_selector("[type=search]")
        input_parent = self.browser.execute_script(
            "return arguments[0].parentNode", input
        )

        active_class = "form-field__wrapper--active"

        # Search input is not active
        body = self.browser.find_element_by_tag_name("body")
        self.assertNotIn(active_class, input_parent.get_attribute("class"))

        # Search input should be active
        input.click()
        self.assertIn(active_class, input_parent.get_attribute("class"))

        # Search input is not active again
        body.click()
        self.assertNotIn(active_class, input_parent.get_attribute("class"))

    def test_header_media_queries(self):
        """
        Tests header media queries. Visibility of elements should be changed.
        """
        self.login_user()

        header = self.browser.find_element_by_class_name("main-header")
        info, search = self.browser.find_elements_by_class_name(
            "main-header__part"
        )
        heading = info.find_element_by_tag_name("h1")

        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=1000, height=600)

        # Logo, heading and search input in one line
        self.assertEqual(heading.value_of_css_property("display"), "block")
        self.assertEqual(
            header.value_of_css_property("flex-direction"), "row"
        )
        self.assertEqual(
            info.value_of_css_property("justify-content"), "normal"
        )

        # Logo and heading in one line, search input in another
        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=720, height=1000)

        self.assertEqual(
            header.value_of_css_property("flex-direction"), "column"
        )
        self.assertEqual(
            info.value_of_css_property("justify-content"), "center"
        )

        # Heading hidden, logo and search input in one line
        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=500, height=1000)

        self.assertEqual(heading.value_of_css_property("display"), "none")
        self.assertEqual(
            header.value_of_css_property("flex-direction"), "row"
        )

    def test_search_input_one_word(self):
        """
        Tests search input autocomplete results with only one word in phrase.
        """
        self.login_user()

        query = "tester"
        exp_results = []

        for index in range(1, 6):
            new_user = User.objects.create(username=f"{query}_{index}")
            exp_results.append(new_user.username)

        school = School.objects.create(
            name=f"{query} university",
            city=City.objects.create(**new_city_data)
        )
        exp_results.append(school.name)

        input = self.browser.find_element_by_css_selector("[type=search]")
        input.send_keys(query[:2])

        time.sleep(2)

        autocomplete = "class-group__autocomplete"
        results = self.browser.find_elements_by_class_name(autocomplete)

        self.assertJSONEqual(
            json.dumps([result.text for result in results]), exp_results
        )

    def test_search_input_more_words(self):
        """
        Tests search input autocomplete results with more than one word in
        phrase.
        """
        self.login_user()

        first_name = "Tester"
        last_name = "Smith"
        query = f"{first_name} {last_name}"
        exp_result = [query]

        for index in range(1, 6):
            User.objects.create(
                username=f"tester_{index}",
                first_name=first_name,
                last_name=last_name
            )

        input = self.browser.find_element_by_css_selector("[type=search]")

        for char in query:
            input.send_keys(char)
            time.sleep(1)

        autocomplete = "class-group__autocomplete"
        results = self.browser.find_elements_by_class_name(autocomplete)

        # There are no duplicates in autocomplete results
        self.assertJSONEqual(
            json.dumps([result.text for result in results]), exp_result
        )

    def test_search_input_click(self):
        """
        Tests search input autocomplete result click. Input value should be
        changed.
        """
        self.login_user()

        query = "tester"
        user = User.objects.create(username=query)

        input = self.browser.find_element_by_css_selector("[type=search]")
        input.send_keys(query[:2])
        before_click_value = input.get_attribute("value")

        time.sleep(2)

        autocomplete = "class-group__autocomplete"
        result = self.browser.find_element_by_class_name(autocomplete)
        hover = ActionChains(self.browser).move_to_element(result)
        hover.perform()
        result.click()

        self.assertEqual(input.get_attribute("value"), user.username)

    def test_search_input_redirect(self):
        """
        Tests search input autocomplete result click. Input value should be
        changed.
        """
        self.activate_user()
        self.login_user()

        query = "tester"
        user = User.objects.create(username=query)
        Profile.objects.create(user=user)

        current_url = self.browser.current_url

        input = self.browser.find_element_by_css_selector("[type=search]")
        input.send_keys(query[:2])

        self.browser.find_element_by_class_name("search-submit").click()

        time.sleep(5)

        self.assertNotEqual(current_url, self.browser.current_url)
        self.assertIn("search", self.browser.current_url)

    def test_nav_show_and_hide(self):
        """Tests showing and hiding navigation section."""
        self.activate_user()
        self.login_user()

        logo = self.browser.find_element_by_class_name("logo-owl")
        home_nav = self.browser.find_element_by_class_name("home-nav")

        # Navigation section is invisible by default
        self.assertNotIn("visible", home_nav.get_attribute("class"))

        logo.click()
        time.sleep(2)

        # After clicking owl logo, navigation section is visible
        self.assertIn("visible", home_nav.get_attribute("class"))

        close_nav = self.browser.find_element_by_class_name(
            "home-nav__close-heading"
        )
        self.assertEqual(close_nav.text, "CLOSE")
        close_nav.click()

        time.sleep(2)

        # After clicking closing button, navigation section is invisible
        self.assertNotIn("visible", home_nav.get_attribute("class"))

    def test_nav_option_nav_close(self):
        """Tests hiding navigation section after clicking option block."""
        self.activate_user()
        self.login_user()

        logo = self.browser.find_element_by_class_name("logo-owl")
        logo.click()

        home_nav = self.browser.find_element_by_class_name("home-nav")

        time.sleep(2)
        self.assertIn("visible", home_nav.get_attribute("class"))

        option = self.browser.find_element_by_class_name("option-block")
        option.click()

        time.sleep(2)

        # After clicking option block, navigation section is invisible
        self.assertNotIn("visible", home_nav.get_attribute("class"))

    def test_nav_content_as_principal(self):
        """Tests navigation section content as principal user."""
        exp_options = ["HOME", "SETTINGS", "SCHOOL", "SIGN OUT"]
        self.check_nav_content("principals", exp_options)

    def test_nav_content_as_teacher_without_class_group(self):
        """
        Tests navigation section content as teacher user that is not related
        with any class group.
        """
        exp_options = ["HOME", "SETTINGS", "GAME", "SIGN OUT"]
        self.check_nav_content("teachers", exp_options)

    def test_nav_content_as_student_without_class_group(self):
        """
        Tests navigation section content as student user that is not related
        with any class group.
        """
        exp_options = ["HOME", "SETTINGS", "PLAY", "SIGN OUT"]
        self.check_nav_content("students", exp_options)

    def test_nav_content_as_teacher_with_class_group(self):
        """
        Tests navigation section content as teacher user that is related with
        class group.
        """
        exp_options = ["HOME", "SETTINGS", "DASHBOARD", "GAME", "SIGN OUT"]
        self.check_nav_content("teachers", exp_options, True)

    def test_nav_content_as_student_with_class_group(self):
        """
        Tests navigation section content as student user that is related with
        class group.
        """
        exp_options = ["HOME", "SETTINGS", "DASHBOARD", "PLAY", "SIGN OUT"]
        self.check_nav_content("students", exp_options, True)

    def check_nav_content(self, role_name, exp_options, class_group=False):
        """Checks if navigation section has proper option blocks.

        Args:
            role_name: name of role object of current user
            exp_options: expected displayed option blocks
            class_group: boolean that indicates if current user needs to be
            related with class group
        """
        self.activate_user(role_name)

        if class_group:
            school = School.objects.create(
                name="University", city=self.user.profile.city,
                principal=User.objects.create_user(username="prince")
            )

            class_group = ClassGroup.objects.create(
                name="test", school=school
            )

            if self.user.profile.role.name == "teachers":
                class_group.teacher = self.user
                class_group.save()
            else:
                class_group.students.add(self.user)

        self.login_user()

        self.browser.find_element_by_class_name("logo-owl").click()
        time.sleep(2)

        nav_options = self.browser.find_elements_by_css_selector(
            "a.option-block h4"
        )

        self.assertEqual([option.text for option in nav_options], exp_options)


class CustomizationSeleniumTest(
        InfoBlockHelper,
        MainSeleniumViewHelper,
        StaticLiveServerTestCase):
    def test_activated_user(self):
        """Tests visiting home view as activated user."""
        self.activate_user()
        self.login_user()

        check = lambda: self.assertNotIn("customize", self.browser.current_url)
        check()

        # User should be redirected from customization page if current user
        # already has customized own profile
        custom_url = f"{self.live_server_url}{reverse('main:home')}#customize"
        self.browser.get(custom_url)
        time.sleep(3)

        check()

    def test_unactivated_user(self):
        """Tests visiting home view as unactivated user."""
        self.login_user()

        # User is automatically redirected to customization page
        check = lambda: self.assertIn("customize", self.browser.current_url)
        check()

        # User can't view other sections until account will be activated
        self.browser.get(f"{self.live_server_url}{reverse('main:home')}")
        time.sleep(3)

        check()

    def test_customization_title(self):
        """Tests document title after visiting customization page."""
        self.login_user()

        # User is automatically redirected to customization page
        self.assertEqual(self.browser.title, "Customize your profile - Esqola")

    def test_customize_header_headings_content(self):
        """Tests if customize headings content is proper."""
        self.login_user()

        heading_class = "rules__heading--customize"
        headings = self.browser.find_elements_by_class_name(heading_class)

        text = " ".join([h.text for h in headings])
        expected_text = "BEFORE WE BEGIN CONFIGURE YOUR PROFILE."
        self.assertEqual(text, expected_text)

    def test_customize_header_style(self):
        """Tests if customize header alignment is changing after resize."""
        self.login_user()

        header_class = "customize-section__header"
        header = self.browser.find_element_by_class_name(header_class)

        self.assertEqual(header.value_of_css_property("text-align"), "start")

        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=400, height=1000)

        # Alignment mode of header has changed
        self.assertEqual(header.value_of_css_property("text-align"), "center")

    def test_customize_header_animations(self):
        """Tests if customization header animation are changing after resize."""
        self.login_user()

        animation_class = "rules__header--customize"
        red, gray = self.browser.find_elements_by_class_name(animation_class)

        self.assertEqual(
            red.value_of_css_property("animation-name"),
            "slide-up, fade-in, slide-right"
        )
        self.assertEqual(
            gray.value_of_css_property("animation-name"),
            "slide-down, fade-in, slide-left"
        )

        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=400, height=1000)

        exp_anim = "slide-up, fade-in"

        # After resize two headings have the same animation
        self.assertEqual(
            red.value_of_css_property("animation-name"), exp_anim
        )
        self.assertEqual(
            gray.value_of_css_property("animation-name"), exp_anim
        )

    def test_customize_user_card_content(self):
        """Tests initial user card content."""
        self.login_user()

        nickname_class = "customize-user-card__nickname"
        nickname = self.browser.find_element_by_class_name(nickname_class)

        self.assertEqual(nickname.text, f"@{self.user.username}")

        info_el_class = "customize-user-card__list_el"
        name, points = self.browser.find_elements_by_class_name(info_el_class)

        # Full name of user and amount of points have been displayed
        fullname = f"{self.user.first_name} {self.user.last_name}"
        self.assertEqual(name.text, f"face {fullname}")
        self.assertEqual(points.text, "star 0 points")

    def test_customize_user_card_visibility(self):
        """Tests user card visibility after resizing."""
        self.login_user()

        # User card is visible now
        user_card_class = "customize-user-card"
        user_card = self.browser.find_element_by_class_name(user_card_class)
        self.assertEqual(user_card.value_of_css_property("display"), "block")

        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=800, height=1000)

        # After changing window size, user card is no longer visible
        self.assertEqual(user_card.value_of_css_property("display"), "none")

    def test_successful_customization(self):
        """Tests successful customization process."""
        submit_button = self.prepare_test_customization_process()

        # Info about city should appear on user card
        self.fill_city_field()
        time.sleep(5)
        exp_text = "location_on Gorlice, PL"
        self.assertEqual(self.get_last_user_info_item_text(), exp_text)

        # Info about birthday (age) should appear on user card
        age = self.fill_birthday_fields(submit_button)
        time.sleep(5)
        exp_text = f"date_range {age} y.o"
        self.assertEqual(self.get_last_user_info_item_text(), exp_text)

        # Info about role (Esqoal group) should appear on user card
        exp_text = self.fill_role_field(submit_button)
        time.sleep(5)
        role_class = "customize-user-card__role"
        role_el = self.browser.find_element_by_class_name(role_class)
        self.assertEqual(role_el.text, exp_text)

        # Testing drag'n'drop or file picker is impossible
        photo_button = self.browser.find_element_by_class_name("photo-block")
        self.assertEqual(
            "add_a_photo\nClick here or drop image here", photo_button.text
        )

        submit_button.click()
        time.sleep(5)

        # Info block with info about successful customization process should
        # be displayed
        info_block = self.get_info_block_data()
        self.assertEqual(info_block["heading"], "DONE")
        exp_msg = "Your account has been successfully configured. " \
                    "Now, close this dialog and click the red owl " \
                    "icon to open a menu"
        self.assertEqual(info_block["text"], exp_msg)
        self.assertIn(
            "info-block--logo",
            info_block["el"].get_attribute("class")
        )

    def test_successful_customization_button_text(self):
        """
        Tests if submit customization form button text is changing after
        filling all required fields.
        """
        submit_button = self.prepare_test_customization_process()
        self.assertEqual(submit_button.text, "PROCEED")

        self.fill_all_fields(submit_button)

        self.assertEqual(submit_button.text, "SAVE")

    def test_city_field_error(self):
        """
        Tests if after filling city field with invalid data, error about it
        has been displayed
        """
        submit_button = self.prepare_test_customization_process()
        self.fill_all_fields(submit_button)

        # This place id value is invalid
        place_data = "[name='profile.city.place_id']"
        place_id_field = self.browser.find_element_by_css_selector(place_data)
        self.browser.execute_script(
            "return arguments[0].setAttribute('value', 'pernambuco')",
            place_id_field
        )

        submit_button.click()
        time.sleep(5)

        self.check_error_info_block("City doesn't exist.")

    def test_birthday_field_too_big_error(self):
        """
        Tests if after filling birthday field with data of date bigger than
        today's date, error about it has been displayed.
        """
        submit_button = self.prepare_test_customization_process()
        self.fill_all_fields(submit_button)

        # Year is bigger than current year
        today = datetime.now()
        birthday_fields = self.browser.find_elements_by_tag_name("select")
        birthday_data = {
            "day": today.day,
            "month": today.month,
            "year": today.year + 1
        }

        for field in birthday_fields[:-1]:
            select = Select(field)
            option = select.first_selected_option
            value_to_set = birthday_data[field.get_attribute("name")]
            self.browser.execute_script(
                f"return arguments[0].setAttribute('value', {value_to_set})",
                option
            )

        submit_button.click()
        time.sleep(5)

        self.check_error_info_block("Invalid birthday.")

    def test_birthday_field_invalid_date_part_error(self):
        """
        Tests if after filling birthday field with data that contains too big
        day value, error about it has been displayed.
        """
        submit_button = self.prepare_test_customization_process()
        self.fill_all_fields(submit_button)

        # Day value of birthday is invalid
        birthday_data = {"day": 40, "month": 12, "year": 1989}
        self.fill_invalid_birthday_fields(birthday_data)

        submit_button.click()
        time.sleep(5)

        msg = "Date has wrong format. Use one of these formats instead: " \
                                                        "YYYY[-MM[-DD]]."
        self.check_error_info_block(msg)

    def test_role_field_invalid_id(self):
        """
        Tests if after filling role field with invalid data, error about it has
        been displayed.
        """
        submit_button = self.prepare_test_customization_process()
        self.fill_all_fields(submit_button)

        role_selector = "[name='profile.role.id']"
        role_field = self.browser.find_element_by_css_selector(role_selector)
        select = Select(role_field)

        # Role with that id doesn't exist
        self.browser.execute_script(
            "return arguments[0].setAttribute('value', 123456789)",
            select.first_selected_option
        )

        submit_button.click()
        time.sleep(5)

        self.check_error_info_block("Role doesn't exist.")

    def prepare_test_customization_process(self):
        """Invokes methods responsible for creating objects necessary to
        test customization proccess.

        Returns:
            Submit customization form button element
        """
        call_command("creategroups")
        self.login_user()

        submit_class = "index-forms__button"
        return self.browser.find_element_by_class_name(submit_class)

    def get_last_user_info_item_text(self):
        """Gets last added user info element to user card.

        Returns:
            Text of last user info item
        """
        css_selector = ".customize-user-card__list_el:last-child"
        return self.browser.find_element_by_css_selector(css_selector).text

    def fill_all_fields(self, submit_button):
        """Invokes methods responsible for filling all required fields of
        customization form.

        Arg:
            submit_button: submit customization form button element
        """
        self.fill_city_field()
        time.sleep(2)
        self.fill_birthday_fields(submit_button)
        self.fill_role_field(submit_button)

    def fill_city_field(self):
        """Fills city field with example city data."""
        city_selector = "[name=autocomplete]"
        city_field = self.browser.find_element_by_css_selector(city_selector)

        # Choosing city with Google Places autocomplete
        city_field.send_keys("Gorlice")
        time.sleep(2)
        city_field.send_keys(Keys.ARROW_DOWN)
        city_field.send_keys(Keys.ENTER)
        time.sleep(2)
        city_field.send_keys(Keys.ENTER)

    def fill_birthday_fields(self, submit_button):
        """Fills birthday field with example birthday data.

        Arg:
            submit_button: submit customization form button element
        Returns:
            Age based on example birthday data
        """
        birthday_fields = self.browser.find_elements_by_tag_name("select")
        birthday_info = {}

        for field in birthday_fields:
            select = Select(field)
            select.select_by_index(3)

            key = field.get_attribute("name")
            value = select.first_selected_option.get_attribute("value")

            birthday_info[key] = int(value)

        submit_button.click()

        return int(((datetime.now().date() - date(**birthday_info)).days)/365)

    def fill_invalid_birthday_fields(self, birthday_data):
        """Fills birthday field with invalid birthday data.

        Arg:
            birthday_data: dictionary with invalid day, month and year
        """
        birthday_fields = self.browser.find_elements_by_tag_name("select")[:-1]

        for field in birthday_fields:
            select = Select(field)
            option = select.first_selected_option
            value_to_set = birthday_data[field.get_attribute("name")]
            self.browser.execute_script(
                f"return arguments[0].setAttribute('value', {value_to_set})",
                option
            )

    def fill_role_field(self, submit_button):
        """Fills role field with proper role id.

        Arg:
            submit_button: submit customization form button element
        """
        role_selector = "[name='profile.role.id']"
        role_field = self.browser.find_element_by_css_selector(role_selector)
        select = Select(role_field)
        select.select_by_index(1)

        submit_button.click()

        return select.first_selected_option.text.upper()


class SchoolSeleniumTest(
        SeleniumViewHelper,
        InfoBlockHelper,
        StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.school_url = f"{self.live_server_url}{reverse('main:home')}#school"

        call_command("creategroups")
        self.city = City.objects.create(**new_city_data)

        self.school_data = {
            "name": "My university",
            "city_name": f"{self.city.name}, {self.city.country_short}"
        }

        self.class_group_names = ["Spanish", "French", "German", "Russian"]

        self.principal_data = dict(new_user_data)
        self.principal_data.pop("password_repeat")
        self.principal = User.objects.create_user(**self.principal_data)

        self.teacher_data = dict(teacher_data)
        self.teacher = User.objects.create_user(**self.teacher_data)

        self.student_data = dict(student_data)
        self.student = User.objects.create_user(**self.student_data)

        account_data = [
            (self.principal, Group.objects.filter(name="principals").first()),
            (self.teacher, Group.objects.filter(name="teachers").first()),
            (self.student, Group.objects.filter(name="students").first()),
        ]

        for user, group in account_data:
            Profile.objects.create(
                user=user, city=self.city, birthday=date(1992, 10, 10)
            )
            user.groups.add(group)
            user.save()

    def test_school_without_query_params_as_teacher(self):
        """
        Tests if visiting school page as teacher without school id, redirects
        user to cockpit.
        """
        self.check_school_without_query_params_as_non_principal(
            self.teacher_data
        )

    def test_school_without_query_params_as_student(self):
        """
        Tests if visiting school page as student without school id, redirects
        user to cockpit.
        """
        self.check_school_without_query_params_as_non_principal(
            self.student_data
        )

    def test_school_without_query_params_as_principal(self):
        """
        Tests if visiting school page as principal without school id, displays
        school form.
        """
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        # New school form should be displayed
        school_heading = self.browser.find_element_by_class_name(
            "school-help-heading"
        )
        self.assertEqual(school_heading.text, "HOW TO CREATE A SCHOOL?")

        tip1 = "Enter a name of new school (needs to be unique for " \
                                                    "particular city)."
        tip2 = "Select a city where school is placed."
        tip3 = "(optional) Upload logo school by file picker " \
                        "(click on school icon) or drop image."

        tips = [tip1, tip2, tip3]

        # All school creating tips should be displayed
        school_create_tips = self.browser.find_elements_by_tag_name("li")
        expected_tips = [tip.text for tip in school_create_tips]

        self.assertJSONEqual(json.dumps(tips), expected_tips)

    def test_school_media_queries(self):
        """Tests if school page with new school form media queries working."""
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        school_info = self.browser.find_element_by_class_name(
            "school-article__info"
        )
        self.assertEqual(
            school_info.value_of_css_property("justify-content"),
            "space-between"
        )

        school_fields = school_info.find_element_by_class_name(
            "school-article__info_fields"
        )
        self.assertEqual(
            school_fields.value_of_css_property("align-items"), "normal"
        )
        self.assertEqual(
            school_fields.value_of_css_property("flex-direction"), "row"
        )

        exp_logo_width = self.browser.get_window_size()["width"]*0.35
        school_logo = school_info.find_element_by_class_name("school-logo")
        self.assertNotEqual(
            school_logo.value_of_css_property("width"), exp_logo_width
        )
        self.assertNotEqual(
            school_logo.value_of_css_property("height"), exp_logo_width
        )

        school_fields_wrapper = school_info.find_element_by_class_name(
            "school-article__info_fields__wrapper"
        )
        self.assertNotEqual(
            school_fields_wrapper.value_of_css_property("padding"), "0"
        )

        school_button_wrapper = school_info.find_element_by_class_name(
            "school-article__button_wrapper"
        )
        self.assertEqual(
            school_button_wrapper.value_of_css_property("justify-content"),
            "normal"
        )

        self.browser.set_window_size(width=500, height=1000)

        # School logo, fields and buttons should be in one column now
        self.assertEqual(
            school_info.value_of_css_property("justify-content"), "center"
        )

        self.assertEqual(
            school_fields.value_of_css_property("align-items"), "center"
        )
        self.assertEqual(
            school_fields.value_of_css_property("flex-direction"), "column"
        )

        self.assertEqual(
            school_fields_wrapper.value_of_css_property("padding"), ""
        )

    def test_school_create_form_success(self):
        """
        Tests successful creating school via school form. School should be
        created and info about it should be displayed.
        """
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        submit_button = self.browser.find_element_by_css_selector(
            ".school-article__info button"
        )
        self.assertEqual(submit_button.text, "CREATE NEW SCHOOL")

        self.fill_school_form_fields()
        time.sleep(5)

        # Info with success message should be displayed
        info_block = self.get_info_block_data()
        info_block_name = f"{self.school_data['name']} school has been created"

        self.assertEqual(info_block["heading"], info_block_name.upper())
        exp_msg = "Now you can create class groups for students."
        self.assertEqual(info_block["text"], exp_msg)
        self.assertIn(
            "info-block--success", info_block["el"].get_attribute("class")
        )

        # School page displays info about created school now (there is no
        # school form anymore)
        self.assertEqual(
            self.browser.find_element_by_class_name("school-name").text,
            self.school_data['name']
        )

    def test_school_create_form_with_city_that_doesnt_exist(self):
        """
        Tests invalid creating school via school form. Info about that city
        doesn't exist should be displayed.
        """
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        self.fill_school_form_fields("Maracana")
        time.sleep(5)

        # Info with city error message should be displayed
        self.check_error_info_block("City doesn't exist.")

        # School object hasn't been created
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name("school-name")
        )

    def test_school_create_form_with_school_that_already_exists_in_city(self):
        """
        Tests invalid creating school via school form. Info about that city
        name is already used should be displayed.
        """
        error_msg = "School with that name already exists in that city."
        self.school_failed_create_form(self.teacher, error_msg)

    def test_school_create_form_with_principal_that_has_school(self):
        """
        Tests invalid creating school via school form. Info about that user is
        already principal should be displayed.
        """
        error_msg = "Current user is already a principal."
        self.school_failed_create_form(self.principal, error_msg)

    def test_school_detail_as_principal(self):
        """Tests school detail page content logged in as principal."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        # Checking school name content
        school_name_heading = self.browser.find_element_by_class_name(
            "school-name"
        )
        self.assertEqual(school_name_heading.text, school_obj.name)

        # Checking city name content
        city = school_obj.city
        school_city_heading = self.browser.find_element_by_class_name(
            "school-city"
        )
        self.assertEqual(
            school_city_heading.text,
            f"location_on\n{city.name}, {city.country}"
        )

        # Buttons responsible for enabling edit mode and deleting school should
        # be displayed
        buttons = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )
        self.assertJSONEqual(
            json.dumps(["MODIFY", "DELETE"]), [b.text for b in buttons]
        )

    def test_school_detail_as_principal_without_class_groups(self):
        """Tests school detail page content logged in as principal."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        # Buttons responsible for enabling edit mode and deleting school should
        # be displayed
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name(
                "class-group__order"
            )
        )

        self.assertIsNotNone(
            self.browser.find_element_by_class_name("class-group")
        )

    def test_school_detail_as_non_principal(self):
        """Tests school detail page content logged in as non principal."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.school_url}/{school_obj.id}/")
        time.sleep(5)

        # Checking school name content
        school_name_heading = self.browser.find_element_by_class_name(
            "school-name"
        )
        self.assertEqual(school_name_heading.text, school_obj.name)

        # Checking city name content
        city = school_obj.city
        school_city_heading = self.browser.find_element_by_class_name(
            "school-city"
        )
        self.assertEqual(
            school_city_heading.text,
            f"location_on\n{city.name}, {city.country}"
        )

        principal = school_obj.principal
        full_name = f"{principal.first_name} {principal.last_name}".upper()
        school_principal_heading = self.browser.find_element_by_class_name(
            "school-principal__text"
        )
        self.assertEqual(school_principal_heading.text, full_name)

        # New class group form and school principal buttons shouldn't be
        # displayed
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name("class-group")
        )
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name(
                "index-forms__button--school"
            )
        )

    def test_school_detail_as_non_principal_without_class_groups(self):
        """
        Tests school detail page content logged in as non principal without
        class groups related with school.
        """
        school_obj = self.create_school(self.principal)
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.school_url}/{school_obj.id}/")
        time.sleep(5)

        no_class_groups_heading = self.browser.find_element_by_class_name(
            "cockpit-class-group__heading"
        )

        self.assertEqual(
            no_class_groups_heading.text, "No class groups created yet"
        )

        # Class group order field and class group detail shouldn't be displayed
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name(
                "class-group__order"
            )
        )
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name(
                "class-group-detail"
            )
        )

    def test_school_detail_as_non_principal_with_class_group(self):
        """Tests school detail page content logged in as non principal."""
        school_obj = self.create_school(self.principal)
        class_group_obj  = ClassGroup.objects.create(
            name="Test", school=school_obj
        )

        self.login_user(self.teacher_data)
        self.browser.get(f"{self.school_url}/{school_obj.id}/")
        time.sleep(5)

        # Class group order field and class group detail should be displayed
        self.assertIsNotNone(
            self.browser.find_element_by_class_name("class-group__order")
        )
        self.assertIsNotNone(
            self.browser.find_element_by_class_name("class-group-detail")
        )

    def test_school_detail_different_schools_as_principal(self):
        """Tests visiting different school logged in as principal."""
        school_obj_1 = self.create_school(self.principal)
        self.school_data["name"] = "Super new university"
        school_obj_2 = self.create_school(self.teacher)

        self.login_user(self.principal_data)
        self.browser.get(f"{self.school_url}/{school_obj_1.id}/")
        time.sleep(5)

        school_city_name = self.browser.find_element_by_class_name(
            "school-name"
        ).text

        self.assertIsNotNone(school_city_name)

        self.browser.get(f"{self.school_url}/{school_obj_2.id}/")
        time.sleep(5)

        new_school_city_name = self.browser.find_element_by_class_name(
            "school-name"
        ).text

        # School detail page has changed after redirecting to another school
        # page
        self.assertIsNotNone(new_school_city_name)
        self.assertNotEqual(school_city_name, new_school_city_name)

        self.browser.get(self.school_url)
        time.sleep(5)

        # School page without school id query param as principal displays info
        # about school related with principal
        new_school_city_name = self.browser.find_element_by_class_name(
            "school-name"
        ).text

        self.assertEqual(new_school_city_name, school_city_name)

    def test_school_detail_different_schools_as_non_principal(self):
        """Tests visiting different school logged in as non principal."""
        school_obj_1 = self.create_school(self.principal)
        self.school_data["name"] = "Super new university"
        school_obj_2 = self.create_school(self.teacher)

        self.login_user(self.student_data)
        self.browser.get(f"{self.school_url}/{school_obj_1.id}/")
        time.sleep(5)

        school_city_name = self.browser.find_element_by_class_name(
            "school-name"
        ).text

        self.assertIsNotNone(school_city_name)

        self.browser.get(f"{self.school_url}/{school_obj_2.id}/")
        time.sleep(5)

        new_school_city_name = self.browser.find_element_by_class_name(
            "school-name"
        ).text

        # School detail page has changed after redirecting to another school
        # page
        self.assertIsNotNone(new_school_city_name)
        self.assertNotEqual(school_city_name, new_school_city_name)

        self.browser.get(self.school_url)
        time.sleep(5)

        # School page without school id query param as non principal should
        # redirect to cockpit
        self.assertNotIn("school", self.browser.current_url)

    def test_school_edit_mode_detail(self):
        """Tests school edit mode content."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        modify_button = self.browser.find_elements_by_css_selector(
        ".school-article__button_wrapper button"
        )[0]
        modify_button.click()
        time.sleep(2)

        school_heading = self.browser.find_element_by_class_name(
            "school-help-heading"
        )
        self.assertEqual(school_heading.text, "HOW TO EDIT A SCHOOL?")


        # School name field should be filled with school name
        name_field = self.browser.find_element_by_css_selector("[name=name]")
        self.assertEqual(name_field.get_attribute("value"), school_obj.name)

        # School city field should be filled with city name
        city = school_obj.city
        city_name_field = self.browser.find_element_by_css_selector(
            f"[name=city_name]"
        )
        self.assertEqual(
            city_name_field.get_attribute("value"),
            f"{city.name}, {city.country}"
        )

        # School edit buttons should be displayed
        buttons = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )
        self.assertJSONEqual(
            json.dumps(["SAVE", "CANCEL"]), [b.text for b in buttons]
        )

    def test_school_edit_mode_save_without_any_changes(self):
        """Tests school edit mode send form without any changes."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        modify_button = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )[0]
        modify_button.click()
        time.sleep(2)

        save_button = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )[0]
        save_button.click()

        # School form hasn't been sent and school edit form is still visible
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name("school-name")
        )

    def test_school_edit_mode_cancel(self):
        """Tests school edit mode form cancel."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        modify_button = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )[0]
        modify_button.click()
        time.sleep(2)

        cancel_button = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )[1]
        cancel_button.click()
        time.sleep(2)

        # School form has been canceled and school form is no longer visible
        self.assertEqual(
            self.browser.find_element_by_class_name("school-name").text,
            school_obj.name,
        )

    def test_school_edit_mode_success(self):
        """Tests school edit form successful."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        modify_button = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )[0]
        modify_button.click()
        time.sleep(2)

        new_school_name = "The best school"

        name_field = self.browser.find_element_by_css_selector("[name=name]")
        name_field.clear()
        name_field.send_keys(new_school_name)
        name_field.send_keys(Keys.ENTER)
        time.sleep(2)

        # School has been edited and edit mode has been closed (school edit
        # form is not visible anymore)
        school_name_heading = self.browser.find_element_by_class_name(
            "school-name"
        )
        self.assertEqual(school_name_heading.text, new_school_name)

    def test_school_delete_success(self):
        """Tests school delete successful."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        delete_button = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )[1]
        delete_button.click()
        time.sleep(2)

        # New school form should have been displayed
        school_heading = self.browser.find_element_by_class_name(
            "school-help-heading"
        )
        self.assertEqual(school_heading.text, "HOW TO CREATE A SCHOOL?")

        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name("school-name")
        )

    def test_school_delete_school_doesnt_exist(self):
        """Tests school delete unsuccessful - school doesn't exist."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        school_obj.delete()

        delete_button = self.browser.find_elements_by_css_selector(
            ".school-article__button_wrapper button"
        )[1]
        delete_button.click()
        time.sleep(2)

        # Info about that school doesn't exist should be displayed
        self.check_error_info_block("School doesn't exist.")
        self.assertEqual(
            self.browser.find_element_by_class_name("school-name").text,
            school_obj.name,
        )

    def test_school_class_group_form_media_queries(self):
        """Tests if school page with class group form media queries working."""
        self.create_school(self.principal)
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        class_group_part = self.browser.find_element_by_class_name(
            "class-group__part_field"
        )
        self.assertEqual(
            class_group_part.value_of_css_property("justify-content"), "normal"
        )
        self.assertEqual(
            class_group_part.value_of_css_property("flex-direction"), "row"
        )
        self.assertEqual(
            class_group_part.value_of_css_property("align-items"), "center"
        )

        self.browser.set_window_size(width=750, height=1000)

        self.assertEqual(
            class_group_part.value_of_css_property("justify-content"),
            "space-between"
        )

        self.browser.set_window_size(width=500, height=1000)

        self.assertEqual(
            class_group_part.value_of_css_property("flex-direction"), "column"
        )
        self.assertEqual(
            class_group_part.value_of_css_property("align-items"), "baseline"
        )

    def test_school_class_group_form_content(self):
        """Tests school class group form content."""
        self.create_school(self.principal)
        self.login_user(self.principal_data)

        class_group_form = self.browser.find_element_by_class_name(
            "class-group"
        )

        # Name and teacher fields are expected to be displayed in class group
        # form
        class_group_labels = class_group_form.find_elements_by_tag_name(
            "label"
        )
        self.assertJSONEqual(
            json.dumps(["Name", "Teacher"]),
            [l.text for l in class_group_labels]
        )

        class_group_button = class_group_form.find_element_by_tag_name(
            "button"
        )
        self.assertEqual("CREATE", class_group_button.text)

    def test_school_class_group_form_success_without_a_teacher(self):
        """Tests school class group form successful without teacher."""
        self.create_school(self.principal)
        self.login_user(self.principal_data)

        new_class_group_name = "Spanish class"

        class_group_form = self.browser.find_element_by_class_name(
            "class-group"
        )

        name_field = class_group_form.find_element_by_css_selector(
            "[name=name]"
        )
        name_field.send_keys(new_class_group_name)
        name_field.send_keys(Keys.ENTER)

        time.sleep(5)

        # Class group object has been created and class group block has been
        # displayed
        class_group_block = self.browser.find_element_by_class_name(
            "class-group-detail"
        )

        self.assertEqual(
            class_group_block.find_element_by_tag_name("h3").text,
            new_class_group_name
        )

    def test_school_class_group_form_success_clear_form(self):
        """Tests if school class group form after successful send is clear."""
        self.create_school(self.principal)
        self.login_user(self.principal_data)

        class_group_form = self.browser.find_element_by_class_name(
            "class-group"
        )
        class_group_name = "Spanish class"

        name_field = class_group_form.find_element_by_css_selector(
            "[name=name]"
        )
        name_field.send_keys(class_group_name)
        name_field.send_keys(Keys.ENTER)

        time.sleep(5)

        # Name field is clear
        self.assertEqual(len(name_field.get_attribute("value")), 0)

    def test_school_class_group_form_success_with_a_teacher_username(self):
        """
        Tests school class group form successful send with choosing teacher
        with username.
        """
        self.check_school_with_teacher(
            self.teacher.username[:2], self.teacher.username, "French class"
        )

    def test_school_class_group_form_success_with_a_teacher_full_name(self):
        """
        Tests school class group form successful send with choosing teacher
        with full name.
        """
        self.check_school_with_teacher(
            f"{self.teacher.first_name} {self.teacher.last_name[0]}",
            f"{self.teacher.first_name} {self.teacher.last_name}",
            "German class"
        )

    def test_school_class_group_delete_teacher_button(self):
        """Tests school class group form delete teacher button."""
        self.create_school(self.principal)
        self.login_user(self.principal_data)

        teacher_id_field = self.browser.find_element_by_css_selector(
            "[name='teacher.id']"
        )
        self.assertEqual(teacher_id_field.get_attribute("value"), "")

        # Checking class group form buttons
        class_group_button_class = "index-forms__button--class-group-form"
        class_group_form_buttons = self.browser.find_elements_by_class_name(
            class_group_button_class
        )

        self.assertJSONEqual(
            json.dumps(["CREATE"]),
            [button.text for button in class_group_form_buttons]
        )

        # Fill teacher name field
        teacher_field = self.browser.find_element_by_css_selector(
            "[name=teacher]"
        )
        teacher_field.send_keys(self.teacher.username[:2])
        time.sleep(2)

        teacher_suggestion = self.browser.find_element_by_class_name(
            "class-group__autocomplete"
        )

        # Choose teacher autocomplete
        hover = ActionChains(self.browser).move_to_element(teacher_suggestion)
        hover.perform()
        teacher_suggestion.click()

        time.sleep(2)

        class_group_form_buttons = self.browser.find_elements_by_class_name(
            class_group_button_class
        )

        self.assertJSONEqual(
            json.dumps(["DELETE TEACHER", "CREATE"]),
            [button.text for button in class_group_form_buttons]
        )

        # Clicking button responsible for clearing teacher fields
        class_group_form_buttons[0].click()
        time.sleep(2)

        # Teacher fields have been cleaned
        self.assertEqual(teacher_field.get_attribute("value"), "")
        self.assertEqual(teacher_id_field.get_attribute("value"), "")

    def test_school_class_group_failed_with_the_same_name(self):
        """Tests school class group form with used class group name."""
        school_obj = self.create_school(self.principal)
        self.login_user(self.principal_data)

        class_group_obj = ClassGroup.objects.create(
            school=school_obj,
            name="Foreign students"
        )
        class_group_form = self.browser.find_element_by_class_name(
            "class-group"
        )

        # Fill class group name field with unavailable data
        name_field = class_group_form.find_element_by_css_selector(
            "[name=name]"
        )
        name_field.send_keys(class_group_obj.name)
        name_field.send_keys(Keys.ENTER)

        time.sleep(2)

        # Info with message about that class group name is already used in
        # this school should be displayed
        self.check_error_info_block(
            "Class group with that name already exists in that school."
        )

    def test_school_class_group_failed_with_invalid_teacher(self):
        """Tests school class group form with invalid class group teacher."""
        self.create_school(self.principal)
        self.login_user(self.principal_data)

        class_group_form = self.browser.find_element_by_class_name(
            "class-group"
        )

        # Fill class group name field
        name_field = class_group_form.find_element_by_css_selector(
            "[name=name]"
        )
        name_field.send_keys("test")

        # Fill class group teacher id field with invalid data
        teacher_id_field = class_group_form.find_element_by_css_selector(
            "[name='teacher.id']"
        )
        self.browser.execute_script(
            "arguments[0].value = '12345';", teacher_id_field
        )

        name_field.send_keys(Keys.ENTER)
        time.sleep(5)

        # Info with message about that teacher user doesn't exist should be
        # displayed
        self.check_error_info_block("Teacher user with that id doesn't exist.")

    def test_school_class_group_order_by_id(self):
        """Tests school class groups ordering by id field."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        class_groups_el = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )

        # Class groups are ordered ascendingly by id field by default
        self.assertEqual(
            [c.name for c in class_groups],
            [c.find_element_by_tag_name("h3").text for c in class_groups_el]
        )

        # Change mode of ordering to descending
        order_button = self.browser.find_element_by_class_name(
            "class-group__order_button"
        )
        order_button.click()
        time.sleep(2)

        # Order of class groups has changed
        class_groups.reverse()
        class_groups_el = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )

        self.assertEqual(
            [c.name for c in class_groups],
            [c.find_element_by_tag_name("h3").text for c in class_groups_el]
        )

    def test_school_class_group_order_by_class_name(self):
        """Tests school class groups ordering by name field."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        # Change field of ordering to class name
        select = Select(
            self.browser.find_element_by_class_name("custom-select--order")
        )
        select.select_by_index(1)

        time.sleep(2)

        # Prepare expected order of class groups and check it
        class_groups.sort(key=lambda x: x.name)
        class_groups_el = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )

        self.assertEqual(
            [c.name for c in class_groups],
            [c.find_element_by_tag_name("h3").text for c in class_groups_el]
        )

        # Change mode of ordering to descending
        order_button = self.browser.find_element_by_class_name(
            "class-group__order_button"
        )
        order_button.click()
        time.sleep(2)

        # Order of class groups has changed
        class_groups.reverse()
        class_groups_el = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )

        self.assertEqual(
            [c.name for c in class_groups],
            [c.find_element_by_tag_name("h3").text for c in class_groups_el]
        )

    def test_school_class_group_order_by_button_text(self):
        """Tests school class groups order by button behaviour."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        order_button = self.browser.find_element_by_class_name(
            "class-group__order_button"
        )
        self.assertEqual(order_button.text, "ascending")

        # After click order class groups button, text of button have changed
        order_button.click()
        time.sleep(2)

        self.assertEqual(order_button.text, "descending")

    def test_school_class_group_order_by_teacher_name(self):
        """Tests school class groups ordering by teacher name field."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        # Change field of ordering to class teacher name
        select = Select(
            self.browser.find_element_by_class_name("custom-select--order")
        )
        select.select_by_index(2)

        time.sleep(2)

        # Prepare expected order of class groups and check it
        class_groups.sort(key=SchoolSeleniumTest.order_by_teacher_name)
        class_groups_el = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )

        self.assertEqual(
            [c.name for c in class_groups],
            [c.find_element_by_tag_name("h3").text for c in class_groups_el]
        )

        # Change mode of ordering to descending
        order_button = self.browser.find_element_by_class_name(
            "class-group__order_button"
        )
        order_button.click()
        time.sleep(2)

        # Order of class groups has changed
        class_groups.sort(
            key=SchoolSeleniumTest.order_by_teacher_name, reverse=True
        )
        class_groups_el = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )

        self.assertEqual(
            [c.name for c in class_groups],
            [c.find_element_by_tag_name("h3").text for c in class_groups_el]
        )

    def test_school_class_group_basic_as_principal(self):
        """Tests school class group basic block as principal of school."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )

        # Class group basic block should contain 3 buttons if current user is
        # principal of this school
        self.assertEqual(
            len(school_detail.find_elements_by_tag_name("button")), 3
        )

    def test_school_class_group_basic_as_teacher(self):
        """Tests school class group basic block as teacher."""
        class_groups = self.create_class_groups()
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.school_url}/{class_groups[0].school.id}/")

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )

        # Class group basic block should contain only one button if current
        # user is teacher (button responsible for expanding class data)
        self.assertEqual(
            len(school_detail.find_elements_by_tag_name("button")), 1
        )

    def test_school_class_group_basic_as_student(self):
        """Tests school class group basic block as principal of school."""
        class_groups = self.create_class_groups()
        self.login_user(self.student_data)
        self.browser.get(f"{self.school_url}/{class_groups[0].school.id}/")

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )

        # Class group basic block should contain only one button if current
        # user is student (button responsible for expanding class data)
        self.assertEqual(
            len(school_detail.find_elements_by_tag_name("button")), 1
        )

    def test_school_class_group_edit_content(self):
        """Tests school class group edit form content."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        # Enable class group data edit mode
        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )
        school_detail.find_element_by_tag_name("button").click()

        time.sleep(2)

        # Class group name field is filled with current class group name
        name_field = school_detail.find_element_by_css_selector("[name=name]")
        self.assertEqual(
            name_field.get_attribute("value"), class_groups[0].name
        )

        # Class group teacher fields is filled with current class group teacher
        # data
        teacher_field = school_detail.find_element_by_css_selector(
            "[name=teacher]"
        )
        self.assertEqual(
            teacher_field.get_attribute("value"),
            class_groups[0].teacher.username
        )

        teacher_id_field = school_detail.find_element_by_css_selector(
            "[name='teacher.id']"
        )
        self.assertEqual(
            teacher_id_field.get_attribute("value"),
            str(class_groups[0].teacher.id)
        )

        # Delete teacher, save class group and cancel edit buttons are expected
        # to be displayed
        self.assertEqual(
            [b.text for b in school_detail.find_elements_by_tag_name("button")],
            ["DELETE TEACHER", "SAVE", "CANCEL"]
        )

    def test_school_class_group_edit_success(self):
        """
        Tests successful school edit form send. Class group should be updated.
        """
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        # Enable class group data edit mode
        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )
        school_detail.find_element_by_tag_name("button").click()

        time.sleep(2)

        # After clicking save button nothing has happenned because no data of
        # class group has been changed
        save_button = school_detail.find_elements_by_tag_name("button")[1]
        save_button.click()

        time.sleep(2)

        # Fill class group name field with new data
        new_class_group_name = "Alunos"
        name_field = school_detail.find_element_by_css_selector("[name=name]")
        name_field.clear()
        name_field.send_keys(new_class_group_name)

        save_button.click()

        time.sleep(2)

        # Class group data has changed
        self.assertEqual(
            school_detail.find_element_by_class_name(
                "class-group__label--detail"
            ).text,
            new_class_group_name
        )

    def test_school_class_group_edit_cancel(self):
        """Tests school class group edit mode form cancel."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )

        # Enable class group edit mode
        school_detail.find_elements_by_tag_name("button")[0].click()
        time.sleep(2)

        # Cancel class group edit mode
        school_detail.find_elements_by_tag_name("button")[-1].click()
        time.sleep(2)

        # Class group edit form is no longer visible
        self.assertRaises(
            NoSuchElementException,
            lambda: school_detail.find_element_by_class_name(
                "class-group-form"
            )
        )

    def test_school_class_group_edit_delete_teacher_button(self):
        """Tests school class group edit mode form delete teacher button."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )

        # Enable class group edit mode
        school_detail.find_elements_by_tag_name("button")[0].click()
        time.sleep(2)

        # Delete class group teacher
        school_detail.find_elements_by_tag_name("button")[0].click()
        time.sleep(2)

        # Class group teacher fields are empty now
        teacher_field = school_detail.find_element_by_css_selector(
            "[name=teacher]"
        )
        self.assertEqual(teacher_field.get_attribute("value"), "")

        teacher_id_field = school_detail.find_element_by_css_selector(
            "[name='teacher.id']"
        )
        self.assertEqual(teacher_id_field.get_attribute("value"), "")

    def test_school_class_group_delete_success(self):
        """Tests school class group delete successful."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_details = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )
        before_delete_school_details = len(school_details)

        # Click delete class group button
        school_details[0].find_elements_by_tag_name("button")[1].click()
        time.sleep(2)

        # Class group should be deleted and class group block should removed
        self.assertGreater(
            before_delete_school_details,
            len(self.browser.find_elements_by_class_name("class-group-detail"))
        )

    def test_school_class_delete_invalid(self):
        """Tests school class group delete that doesn't exist."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_details = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )
        before_delete_school_details = len(school_details)

        # Click delete class group button
        class_groups[0].delete()
        school_details[0].find_elements_by_tag_name("button")[1].click()
        time.sleep(2)

        # Info with message that class group doesn't exist should be displayed
        self.check_error_info_block("Class group doesn't exist.")

        self.assertEqual(
            before_delete_school_details,
            len(self.browser.find_elements_by_class_name("class-group-detail"))
        )

    def test_school_class_group_detail_show_content(self):
        """Tests school class group detail show content and behaviour."""
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )
        school_detail_content = school_detail.find_element_by_class_name(
        "class-group-content"
        )
        school_detail_show = school_detail.find_elements_by_tag_name(
            "button"
        )[-1]

        self.assertEqual(
            school_detail_show.find_element_by_tag_name("i").text,
            "keyboard_arrow_down"
        )
        self.assertEqual(
            school_detail_content.value_of_css_property("display"), "none"
        )
        self.assertNotIn(
            "class-group-content--visible",
            school_detail_content.get_attribute("class")
        )

        # After clicking school show detail button, icon of button have changed
        # and class group details are visible
        school_detail_show.click()
        time.sleep(2)

        self.assertEqual(
            school_detail_show.find_element_by_tag_name("i").text,
            "keyboard_arrow_up"
        )
        self.assertEqual(
            school_detail_content.value_of_css_property("display"), "block"
        )
        self.assertIn(
            "class-group-content--visible",
            school_detail_content.get_attribute("class")
        )
        # After clicking school show detail button again, class group details
        # should be hidden again
        school_detail_show.click()
        time.sleep(2)

        self.assertEqual(
            school_detail_show.find_element_by_tag_name("i").text,
            "keyboard_arrow_down"
        )
        self.assertEqual(
            school_detail_content.value_of_css_property("display"), "none"
        )
        self.assertNotIn(
            "class-group-content--visible",
            school_detail_content.get_attribute("class")
        )

    def test_school_class_group_detail_table_as_principal(self):
        """
        Tests school class group detail table headings as principal of school.
        """
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )
        school_detail_show = school_detail.find_elements_by_tag_name(
            "button"
        )[-1]
        school_detail_content = school_detail.find_element_by_class_name(
            "class-group-content"
        )

        school_detail_show.click()
        time.sleep(2)

        exp_headings = [
            "USERNAME", "FIRST NAME", "LAST NAME", "BIRTHDAY", "CITY", "POINTS"
        ]

        table_headings = school_detail_content.find_elements_by_tag_name("th")
        self.assertEqual([th.text for th in table_headings], exp_headings)

    def test_school_class_group_detail_table_students_content(self):
        """Tests school class group detail table students content."""
        class_group = self.load_class_group_detail_as_teacher()
        student_rows = self.browser.find_elements_by_css_selector("thead tr")
        student_rows.pop(0)

        # Prepare list with displayed students
        cell = "class-group__table__cell"
        rows = []

        for row in student_rows:
            rows.append([
                info.text for info in row.find_elements_by_class_name(cell)
            ])

        exp_rows = []

        # Prepare list with students expected to be displayed
        for s in class_group.students.all():
            exp_rows.append([
                s.username,
                s.first_name,
                s.last_name,
                s.profile.birthday.strftime("%d.%m.%Y"),
                f"{s.profile.city.name}, {s.profile.city.country}",
                str(s.profile.points)
            ])

        self.assertEqual(rows, exp_rows)

    def test_school_class_group_detail_table_as_teacher(self):
        """
        Tests school class group detail table headings as teacher of class
        group.
        """
        self.load_class_group_detail_as_teacher()

        exp_headings = [
            "", "USERNAME", "FIRST NAME", "LAST NAME", "BIRTHDAY", "CITY",
            "POINTS"
        ]

        table_headings = self.browser.find_elements_by_css_selector("table th")
        self.assertEqual([th.text for th in table_headings], exp_headings)

    def test_school_class_group_detail_as_principal_of_school(self):
        """
        Tests school class group detail table content as principal of school.
        """
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )
        school_detail_show = school_detail.find_elements_by_tag_name(
            "button"
        )[-1]
        school_detail_content = school_detail.find_element_by_class_name(
            "class-group-content"
        )

        # Class group details should be visible after button click
        school_detail_show.click()
        time.sleep(2)

        class_group_heading = self.browser.find_element_by_class_name(
            "class-group__heading"
        )
        self.assertEqual(class_group_heading.text, "LIST OF STUDENTS")

        teacher_obj = class_groups[0].teacher

        class_group_teacher = self.browser.find_element_by_class_name(
            "class-group__teacher__heading"
        )
        full_name = f"{teacher_obj.first_name} {teacher_obj.last_name}".upper()
        self.assertEqual(class_group_teacher.text, f"Teacher {full_name}")

        # Only info about students and teacher are displayed if current user
        # is principal of school related with class group
        self.assertRaises(
            NoSuchElementException,
            lambda: school_detail_content.find_element_by_tag_name("input")
        )

        self.assertRaises(
            NoSuchElementException,
            lambda: school_detail_content.find_element_by_tag_name("button")
        )

    def test_school_class_group_details_as_related_student(self):
        """
        Tests school class group detail table content as student of class group.
        """
        class_groups = self.create_class_groups()
        self.login_user(self.principal_data)

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )
        school_detail_show = school_detail.find_elements_by_tag_name(
            "button"
        )[-1]
        school_detail_content = school_detail.find_element_by_class_name(
            "class-group-content"
        )

        # Class group details should be visible after button click
        school_detail_show.click()
        time.sleep(2)

        class_group_heading = self.browser.find_element_by_class_name(
            "class-group__heading"
        )
        self.assertEqual(class_group_heading.text, "LIST OF STUDENTS")

        teacher_obj = class_groups[0].teacher

        class_group_teacher = self.browser.find_element_by_class_name(
            "class-group__teacher__heading"
        )
        full_name = f"{teacher_obj.first_name} {teacher_obj.last_name}".upper()
        self.assertEqual(class_group_teacher.text, f"Teacher {full_name}")

        # Only info about students and teacher are displayed if current user
        # is student related with class group
        self.assertRaises(
            NoSuchElementException,
            lambda: school_detail_content.find_element_by_tag_name("input")
        )

        self.assertRaises(
            NoSuchElementException,
            lambda: school_detail_content.find_element_by_tag_name("button")
        )

    def test_school_class_group_details_as_unrelated_user(self):
        """
        Tests school class group detail table content as user unrelated with
        class group.
        """
        class_groups = self.create_class_groups()
        class_groups[0].students.remove(self.student)
        self.login_user(self.student_data)
        self.browser.get(f"{self.school_url}/{class_groups[0].school.id}/")

        time.sleep(2)

        school_detail = self.browser.find_element_by_class_name(
            "class-group-detail"
        )
        school_detail_show = school_detail.find_elements_by_tag_name(
            "button"
        )[-1]
        school_detail_content = school_detail.find_element_by_class_name(
            "class-group-content"
        )

        school_detail_show.click()
        time.sleep(2)

        # Only info about teacher is displayed if current user is unrelated
        # with this class group
        teacher_obj = class_groups[0].teacher
        class_group_teacher = self.browser.find_element_by_class_name(
            "class-group__teacher__heading"
        )
        full_name = f"{teacher_obj.first_name} {teacher_obj.last_name}".upper()
        self.assertEqual(class_group_teacher.text, f"Teacher {full_name}")

        self.assertRaises(
            NoSuchElementException,
            lambda: school_detail.find_element_by_class_name(
                "class-group__heading"
            )
        )

        self.assertRaises(
            NoSuchElementException,
            lambda: school_detail_content.find_element_by_tag_name("input")
        )

        self.assertRaises(
            NoSuchElementException,
            lambda: school_detail_content.find_element_by_tag_name("button")
        )

    def test_school_class_group_detail_as_teacher_of_class_group(self):
        """
        Tests school group headings and buttons content as teacher of class
        group.
        """
        self.load_class_group_detail_as_teacher()

        # Teacher of class group is allowed to remove students from class group
        # and invite of students to class group
        class_group_headings = self.browser.find_elements_by_class_name(
            "class-group__heading"
        )
        self.assertEqual(class_group_headings[0].text, "LIST OF STUDENTS")
        self.assertEqual(class_group_headings[1].text, "INVITED USERS")

        table_buttons = self.browser.find_elements_by_css_selector(
            ".class-group__table__buttons button"
        )

        self.assertEqual(
            [button.text for button in table_buttons],
            ["MARK/UNMARK STUDENTS", "REMOVE STUDENTS"]
        )

    def test_school_class_group_detail_mark_unmark_students_all(self):
        """Tests school class group mark/unmark all students button."""
        self.load_class_group_detail_as_teacher(True)

        mark_unmark_button = self.browser.find_element_by_class_name(
            "index-forms__button--students-table"
        )
        student_buttons = self.browser.find_elements_by_class_name(
            "class-group__checkbox_button"
        )

        # All students are unmarked now
        student_icons = [
            btn.find_element_by_tag_name("i") for btn in student_buttons
        ]
        self.assertTrue(
            all([
                icon.text == "check_box_outline_blank" for icon in student_icons
            ])
        )

        get_student_input = lambda btn: self.browser.execute_script(
            "return arguments[0].nextElementSibling", btn
        )

        student_inputs = [get_student_input(btn) for btn in student_buttons]
        self.assertTrue(all(
            [input.get_attribute("checked") is None for input in student_inputs]
        ))

        # After mark/unmark button click, all students are marked now
        mark_unmark_button.click()
        time.sleep(2)

        self.assertTrue(
            all([icon.text == "check_box" for icon in student_icons])
        )

        self.assertTrue(all(
            [inp.get_attribute("checked") == "true" for inp in student_inputs]
        ))

        # After mark/unmark button click again, all students are unmarked now
        mark_unmark_button.click()
        time.sleep(2)

        self.assertTrue(
            all([
                icon.text == "check_box_outline_blank" for icon in student_icons
            ])
        )

        self.assertTrue(all(
            [input.get_attribute("checked") is None for input in student_inputs]
        ))

    def test_school_class_group_detail_unmark_students_some(self):
        """Tests school class group unmark some students button."""
        self.load_class_group_detail_as_teacher(True)

        mark_unmark_button = self.browser.find_element_by_class_name(
            "index-forms__button--students-table"
        )
        student_buttons = self.browser.find_elements_by_class_name(
            "class-group__checkbox_button"
        )
        student_icons = [
            btn.find_element_by_tag_name("i") for btn in student_buttons
        ]
        get_student_input = lambda btn: self.browser.execute_script(
            "return arguments[0].nextElementSibling", btn
        )
        student_inputs = [get_student_input(btn) for btn in student_buttons]

        # Mark only one student
        student_buttons[0].click()

        # After mark/unmark button click, all students are unmarked now
        mark_unmark_button.click()
        time.sleep(2)

        self.assertTrue(
            all([
                icon.text == "check_box_outline_blank" for icon in student_icons
            ])
        )

        self.assertTrue(all(
            [input.get_attribute("checked") is None for input in student_inputs]
        ))

    def test_school_class_group_detail_mark_students_some(self):
        """Tests school class group mark some students button."""
        self.load_class_group_detail_as_teacher(True)

        mark_unmark_button = self.browser.find_element_by_class_name(
            "index-forms__button--students-table"
        )
        student_buttons = self.browser.find_elements_by_class_name(
            "class-group__checkbox_button"
        )
        student_icons = [
            btn.find_element_by_tag_name("i") for btn in student_buttons
        ]
        get_student_input = lambda btn: self.browser.execute_script(
            "return arguments[0].nextElementSibling", btn
        )
        student_inputs = [get_student_input(btn) for btn in student_buttons]

        # Mark more than 50% of students
        for i in range(int(len(student_buttons)/2) + 1):
            student_buttons[i].click()

        # After mark/unmark button click, all students are marked now
        mark_unmark_button.click()
        time.sleep(2)

        self.assertTrue(
            all([icon.text == "check_box" for icon in student_icons])
        )

        self.assertTrue(all(
            [inp.get_attribute("checked") == "true" for inp in student_inputs]
        ))

    def test_school_class_group_delete_user(self):
        """Tests school class group delete students button."""
        self.load_class_group_detail_as_teacher(True)

        class_buttons = self.browser.find_elements_by_class_name(
            "index-forms__button--students-table"
        )

        # Mark all students
        class_buttons[0].click()
        time.sleep(2)

        # Remove all students from class_group
        class_buttons[1].click()
        time.sleep(2)

        # No students related with this class group should be displayed
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name(
                "class-group__table__cell"
            )
        )

    def test_school_class_invitation_success(self):
        """Tests school class group invitation form send successful."""
        self.load_class_group_detail_as_teacher()
        self.create_additional_users()

        self.browser.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);"
        )

        student_name_field = self.browser.find_element_by_css_selector(
            "[name='student_name']"
        )
        self.assertEqual(student_name_field.get_attribute("value"), "")

        student_id_field = self.browser.find_element_by_css_selector(
            "[name='student.id']"
        )
        self.assertEqual(student_id_field.get_attribute("value"), "")

        student_name_field.send_keys("tester")
        time.sleep(2)

        # Choose invitation student autocomplete suggestion
        student_suggestion = self.browser.find_element_by_class_name(
            "class-group__autocomplete"
        )
        hover = ActionChains(self.browser).move_to_element(student_suggestion)
        hover.perform()
        student_suggestion.click()

        time.sleep(5)

        student_id = student_id_field.get_attribute("value")

        # Inviation student fields are filled with data of student from
        # autocomplete suggestion
        self.assertIsNotNone(student_name_field.get_attribute("value"))
        self.assertIsNotNone(student_id)
        student_name_field.send_keys(Keys.ENTER)

        time.sleep(2)

        # New inviatation has been created
        invited_user_obj = User.objects.get(id=student_id)
        self.assertEqual(
            self.browser.find_element_by_class_name(
                "class-group__invitation_username"
            ).text,
            f"@{invited_user_obj.username}"
        )

    def test_school_class_invitation_already_user(self):
        """
        Tests school class group invitation form send with invalid data. User
        is already a student of class group.
        """
        self.check_invitation_already_user_or_invited(False)

    def test_school_class_invitation_already_invited(self):
        """
        Tests school class group invitation form send with invalid data. User
        is already a student of class group.
        """
        self.check_invitation_already_user_or_invited(True)

    def test_school_invitation_delete(self):
        """Tests school class group invitation delete."""
        self.load_class_group_detail_as_teacher(invitation=True)
        self.browser.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);"
        )
        invitation_delete = self.browser.find_element_by_class_name(
            "class-group__invitation_delete"
        )

        hover = ActionChains(self.browser).move_to_element(invitation_delete)
        hover.perform()
        invitation_delete.click()

        time.sleep(2)

        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name(
                "class-group__invitation--normal"
            )
        )

    def test_school_invitation_delete_invitation_doesnt_exist(self):
        """Tests school class group invitation delete with invalid data."""
        self.load_class_group_detail_as_teacher(invitation=True)
        self.browser.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);"
        )

        Invitation.objects.first().delete()

        invitation_delete = self.browser.find_element_by_class_name(
            "class-group__invitation_delete"
        )

        hover = ActionChains(self.browser).move_to_element(invitation_delete)
        hover.perform()
        invitation_delete.click()

        time.sleep(2)

        self.check_error_info_block("Invitation doesn't exist.")

    def check_invitation_already_user_or_invited(self, invitation):
        """
        Checks if invitation form generates error with message that user
        desired to invite is already a student of class group or is already
        invited.

        Arg:
            invitation: boolean that indicates if Invitation object needs to
            be created
        """
        if invitation:
            self.load_class_group_detail_as_teacher(invitation=True)
        else:
            self.load_class_group_detail_as_teacher(True)

        self.browser.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);"
        )

        student_name_field = self.browser.find_element_by_css_selector(
            "[name='student_name']"
        )

        student_name_field.send_keys("tester")
        time.sleep(2)

        # Choose invitation user autocomplete suggestion
        student_suggestion = self.browser.find_element_by_class_name(
            "class-group__autocomplete"
        )
        hover = ActionChains(self.browser).move_to_element(student_suggestion)
        hover.perform()
        student_suggestion.click()
        time.sleep(5)

        student_name_field.send_keys(Keys.ENTER)
        time.sleep(2)

        # Warning info block about already invited/student should be displayed
        info_block = self.get_info_block_data()
        self.assertEqual(info_block["heading"], "ARE YOU SURE?")
        self.assertEqual(
            info_block["text"],
            "This user is student or has been already invited."
        )
        self.assertIn(
            "info-block--warning", info_block["el"].get_attribute("class")
        )

    def load_class_group_detail_as_teacher(
        self,
        additional_users=False,
        invitation=False):
        """
        Loads class group detail with teacher as current user.

        Args:
            additional_users: boolean that indicates if invoking method
            responsible for creating new users (students) is necessary
            invitation: boolean that indicates if creating new Invitation
            object is necessary
        Returns:
            Class group selenium element
        """
        class_groups = self.create_class_groups()
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.school_url}/{class_groups[0].school.id}/")

        # Index of class group that is related with current user
        class_index = next(
            (i for i, c in enumerate(class_groups) if c.teacher.username == self.teacher_data["username"])
        )

        if additional_users:
            self.create_additional_users(class_groups[class_index])

        if invitation:
            users = self.create_additional_users()
            Invitation.objects.create(
                class_group=class_groups[class_index], student=users[0]
            )

        school_detail = self.browser.find_elements_by_class_name(
            "class-group-detail"
        )[class_index]
        school_detail_show = school_detail.find_elements_by_tag_name("button")
        school_detail_content = school_detail.find_element_by_class_name(
            "class-group-content"
        )

        school_detail_show[-1].click()
        time.sleep(2)

        return class_groups[class_index]

    def create_class_groups(self):
        """Creates class groups object for test purposes.

        Returns:
            List that contains Class group objects
        """
        school_obj = self.create_school(self.principal)
        class_groups = []

        teacher_2 = User.objects.create(
            username="Marian", first_name="Janusz", last_name="Pazdzioch"
        )
        teacher_2.groups.add(Group.objects.filter(name="teachers").first())

        for i, class_name in enumerate(self.class_group_names):
            if i % 2 == 0:
                teacher = teacher_2
            else:
                teacher = self.teacher

            class_obj = ClassGroup.objects.create(
                school=school_obj, name=class_name, teacher=teacher
            )
            class_obj.students.add(self.student)

            class_groups.append(class_obj)

        return class_groups

    def create_additional_users(self, class_group=None, amount=2):
        """Creates additional activated User objects for testing purposes.

        Args:
            class_group: Class group object that is desired to be related with
            new users
            amount: amount of User objects that need to be created
        Returns
            List full of new User objects
        """
        users = []

        for i in range(amount):
            user = User.objects.create(
                username=f"tester{i}", first_name="Jan", last_name="Nowak",
                email=f"jan@kowalski{i}.pl"
            )
            user.groups.add(Group.objects.filter(name="students").first())

            Profile.objects.create(
                user=user, city=self.city, birthday=datetime.now().date()
            )

            if class_group:
                class_group.students.add(user)

            users.append(user)

        return users

    @staticmethod
    def order_by_teacher_name(class_group):
        """Key function for class group list ordering.

        Arg:
            class_group: Class group object
        """
        if not class_group.teacher:
            return ""
        else:
            return class_group.teacher.last_name

    def check_school_with_teacher(self, autocomplete, name, class_group_name):
        """
        Checks school class group form send with teacher has been successful.

        Args:
            autocomplete: phrase for teacher autocomplete field
            name: expected full autocomplete teacher name
            class group name: name of school class group
        """
        self.create_school(self.principal)
        self.login_user(self.principal_data)

        class_group_form = self.browser.find_element_by_class_name(
            "class-group"
        )

        # Fill class group name field
        name_field = class_group_form.find_element_by_css_selector(
            "[name=name]"
        )
        name_field.send_keys(class_group_name)

        teacher_id_field = class_group_form.find_element_by_css_selector(
            "[name='teacher.id']"
        )
        self.assertEqual(teacher_id_field.get_attribute("value"), "")

        # Fill teacher name field
        teacher_field = class_group_form.find_element_by_css_selector(
            "[name=teacher]"
        )
        teacher_field.send_keys(autocomplete)
        time.sleep(2)

        # Checking autocomplete suggestion text content
        teacher_suggestion = self.browser.find_element_by_class_name(
            "class-group__autocomplete"
        )
        self.assertEqual(teacher_suggestion.text, name)

        # Choose teacher autocomplete
        hover = ActionChains(self.browser).move_to_element(teacher_suggestion)
        hover.perform()
        teacher_suggestion.click()

        time.sleep(2)

        # Checking if teacher fields have been filled after choosing
        # autocomplete teacher suggestion
        self.assertEqual(teacher_field.get_attribute("value"), name)
        self.assertEqual(
            teacher_id_field.get_attribute("value"), str(self.teacher.id)
        )

        teacher_field.send_keys(Keys.ENTER)
        class_group_label = self.browser.find_element_by_class_name(
            "class-group__label--detail"
        )

        self.assertTrue(class_group_label.text, class_group_name)

    def create_school(self, principal):
        """Creates School object for testing purposes.

        Arg:
            principal: User object desired to be a principal of school
        Returns:
            New School object
        """
        return School.objects.create(
            principal=principal, city=self.city, name=self.school_data["name"]
        )

    def fill_school_form_fields(self, city_place_id=""):
        """
        Fills school form with proper data.

        Arg:
            city_place_id: place id of city desired to be related with school
        """
        field = None

        for name, value in self.school_data.items():
            field = self.browser.find_element_by_css_selector(f'[name={name}]')
            field.send_keys(value)

        time.sleep(2)
        field.send_keys(Keys.ARROW_DOWN)
        field.send_keys(Keys.ENTER)
        time.sleep(2)

        if city_place_id:
            city_field = self.browser.find_element_by_css_selector(
                "[name='city.place_id']"
            )
            self.browser.execute_script(
                f"return arguments[0].setAttribute('value', '{city_place_id}')",
                city_field
            )

        field.send_keys(Keys.ENTER)

    def check_school_without_query_params_as_non_principal(self, user_data):
        """
        Checks if user as non principal after visiting school page without
        school id, was redirected to cockpit page.

        Arg:
            user_data: data of user to login
        """
        self.login_user(user_data)
        self.browser.get(self.school_url)
        time.sleep(3)
        self.assertNotIn('school', self.browser.current_url)

    def school_failed_create_form(self, principal, error_msg):
        """Checks if expected error of school creating proccess has occured.

        Args:
            principal: User object desired to be a principal of school
            error_msg: expected error message
        """
        self.login_user(self.principal_data)
        self.browser.get(self.school_url)
        time.sleep(5)

        self.create_school(principal)
        self.fill_school_form_fields()
        time.sleep(5)

        self.check_error_info_block(error_msg)

        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name("school-name")
        )

    def login_user(self, user_data):
        """Sends login user form with correct data.

        Arg:
            user_data: dictionary with username and password of user to login
        """
        self.browser.get(self.live_server_url + reverse('authorization:login'))

        for name in ["username", "password"]:
            field = self.browser.find_element_by_css_selector(f'[name={name}]')
            field.send_keys(user_data[name])

        self.browser.find_element_by_tag_name("button").click()
        time.sleep(2)

        self.browser.get(self.school_url)
        time.sleep(5)


class CockpitSeleniumTest(
        DashboardTestMixin,
        SeleniumViewHelper,
        InfoBlockHelper,
        StaticLiveServerTestCase):
    def test_cockpit_blocks(self):
        """
        Tests cockpit info blocks content. Content should based on current user
        data.
        """
        self.activate_user(self.principal_1)
        self.login_user(self.principal_data_1)

        age, city, points, role = self.browser.find_elements_by_css_selector(
            ".cockpit-info__block .option-block__heading"
        )

        # Checking if age is valid
        today = datetime.now().date()
        exp_age = ((today - self.principal_1.profile.birthday)/365).days
        self.assertEqual(f"{exp_age} Y.O", age.text)

        # Checking if city name is valid
        exp_city = f"{self.city.name}, {self.city.country_short}".upper()
        self.assertEqual(exp_city, city.text)

        # Checking if amount of points is equal to current user points
        self.assertEqual(str(self.principal_1.profile.points), points.text)

        # Current user is principal so principal text should be displayed
        exp_role = str(self.principal_1.profile.role.name[:-1]).upper()
        self.assertEqual(exp_role, role.text)

        section_headings = self.browser.find_elements_by_class_name(
            "class-group__heading--cockpit"
        )

        self.assertEqual(
            [heading.text for heading in section_headings],
            ["CLASSES", "INVITATIONS", "CONVERSATIONS"]
        )

    def test_cockpit_classes_content_as_principal(self):
        """
        Tests cockpit class groups section as principal user. Info about lack of
        class groups should be displayed.
        """
        self.activate_user(self.principal_1)
        self.login_user(self.principal_data_1)

        no_classes_heading = self.browser.find_element_by_class_name(
            "cockpit-class-group__heading--empty"
        )

        # Despite fact that current user is principal of school and that
        # school has class groups, there are not any class groups related
        # with current user
        self.assertGreater(self.principal_1.school.classes.count(), 0)
        self.assertEqual(no_classes_heading.text, "No class groups")

    def test_cockpit_classes_content_as_teacher(self):
        """
        Tests cockpit class groups section as teacher user. Class groups that
        current user is related with should be displayed.
        """
        self.activate_user(self.teacher)
        self.login_user(self.teacher_data)

        class_group_headings = self.browser.find_elements_by_css_selector(
            ".cockpit-class-group .cockpit-class-group__heading"
        )

        teacher_class_groups = self.teacher.teacher_classes.all()

        self.assertEqual(
            [class_group.text for class_group in class_group_headings],
            [class_group.name for class_group in teacher_class_groups]
        )

    def test_cockpit_classes_content_as_student(self):
        """
        Tests cockpit class groups section as student user. Class groups that
        current user is related with should be displayed.
        """
        self.activate_user(self.student)
        self.login_user(self.student_data)

        class_group_headings = self.browser.find_elements_by_css_selector(
            ".cockpit-class-group .cockpit-class-group__heading"
        )

        student_class_groups = self.student.student_classes.all()

        self.assertEqual(
            [class_group.text for class_group in class_group_headings],
            [class_group.name for class_group in student_class_groups]
        )

    def test_cockpit_invitations_content_without_invitations(self):
        """
        Tests cockpit invitations section without invitations. Info about lack
        of invitations should be displayed.
        """
        self.activate_user(self.student)
        self.login_user(self.student_data)

        no_invitations_heading = self.browser.find_element_by_class_name(
            "cockpit-class-group__heading--empty"
        )

        self.assertEqual(no_invitations_heading.text, "No invitations")

    def test_cockpit_invitations_content_with_invitation(self):
        """
        Tests cockpit invitations section with invitations. Invitation that
        current user is related with should be displayed.
        """
        invitation = self.create_invitation()

        self.activate_user(self.student)
        self.login_user(self.student_data)

        time.sleep(2)

        invitation_blocks = self.browser.find_elements_by_class_name(
            "cockpit-class-group--invitation"
        )
        self.assertEqual(
            len(invitation_blocks), self.student.invitations.count()
        )

        inv_class_name = invitation_blocks[0].find_element_by_tag_name('h4')
        self.assertEqual(inv_class_name.text, invitation.class_group.name)

        inv_school_name = invitation_blocks[0].find_element_by_tag_name('h5')
        self.assertEqual(
            inv_school_name.text, invitation.class_group.school.name
        )

    def test_cockpit_invitation_accept(self):
        """
        Tests cockpit accept invitation. Accepted class group block should be
        displayed.
        """
        invitation = self.create_invitation()

        self.activate_user(self.student)
        self.login_user(self.student_data)

        time.sleep(2)

        before_class_groups_amount = len(
            self.browser.find_elements_by_css_selector("a.cockpit-class-group")
        )

        invitation_block = self.browser.find_element_by_class_name(
            "cockpit-class-group--invitation"
        )
        accept_button = invitation_block.find_element_by_class_name(
            "cockpit__button--accept"
        )

        accept_button.click()
        time.sleep(2)

        after_class_groups_amount = len(
            self.browser.find_elements_by_css_selector("a.cockpit-class-group")
        )

        self.assertGreater(
            after_class_groups_amount, before_class_groups_amount
        )

    def test_cockpit_invitation_reject(self):
        """
        Tests cockpit reject invitation. Rejected class group block shouldn't be
        displayed.
        """
        invitation = self.create_invitation()

        self.activate_user(self.student)
        self.login_user(self.student_data)

        time.sleep(2)

        before_class_groups_amount = len(
            self.browser.find_elements_by_css_selector("a.cockpit-class-group")
        )

        invitation_block = self.browser.find_element_by_class_name(
            "cockpit-class-group--invitation"
        )
        reject_button = invitation_block.find_element_by_class_name(
            "cockpit__button--reject"
        )

        reject_button.click()
        time.sleep(2)

        after_class_groups_amount = len(
            self.browser.find_elements_by_css_selector("a.cockpit-class-group")
        )

        self.assertEqual(
            after_class_groups_amount, before_class_groups_amount
        )

    def test_cockpit_conversations_content_without_conversations(self):
        """
        Tests cockpit conversations section without conversations. Info about
        lack of invitations should be displayed.
        """
        self.activate_user(self.principal_1)
        self.login_user(self.principal_data_1)

        no_conversations_heading = self.browser.find_elements_by_class_name(
            "cockpit-class-group__heading--empty"
        )[-1]

        self.assertEqual(no_conversations_heading.text, "No conversations")

    def test_cockpit_conversations_content_with_conversations(self):
        """
        Tests cockpit conversations section without conversations. Info about
        lack of invitations should be displayed.
        """
        msg = Message.objects.create(
            content="Lorem ipsum", to=self.principal_1, author=self.principal_2
        )

        self.activate_user(self.principal_1)
        self.login_user(self.principal_data_1)

        time.sleep(2)

        conversation_block = self.browser.find_element_by_class_name(
            "cockpit-class-group--invitation"
        )

        self.assertEqual(
            conversation_block.find_element_by_tag_name('h4').text,
            f"@{msg.author.username}"
        )

        conversations_date = conversation_block.find_element_by_tag_name('h5')
        self.assertTrue(conversations_date.text.endswith("ago"))

    def create_invitation(self):
        """Creats Invitation object for testing purposes."""
        self.student.student_classes.set([])
        class_group_obj = ClassGroup.objects.first()

        return Invitation.objects.create(
            class_group=class_group_obj, student=self.student
        )

    def activate_user(self, user):
        """
        Fills profile fields with correct data in order to activate user
        account.

        Arg:
            user: User object to activate
        """
        user.profile.birthday = date(1998, 5, 10)
        user.profile.city = self.city
        user.profile.save()

    def login_user(self, user_data):
        """Sends login user form with correct data.

        Arg:
            user_data: dictionary with username and password of user to login
        """
        self.browser.get(self.live_server_url + reverse('authorization:login'))

        for name in ["username", "password"]:
            field = self.browser.find_element_by_css_selector(f'[name={name}]')
            field.send_keys(user_data[name])

        self.browser.find_element_by_tag_name("button").click()
        time.sleep(2)

class SettingsSeleniumTest(
        InfoBlockHelper,
        MainSeleniumViewHelper,
        StaticLiveServerTestCase):

    def setUp(self):
        super().setUp()
        self.user.email = "my@mail.com"
        self.user.save()

        self.activate_user()
        self.login_user()

        settings_url = f"{self.live_server_url}{reverse('main:home')}#settings"
        self.browser.get(settings_url)

    def test_settings_content(self):
        """
        Tests settings forms and content of fields related with these forms.
        """
        main = self.browser.find_element_by_tag_name("main")

        user_form, pass_form = main.find_elements_by_tag_name("form")
        user_form_fields = user_form.find_elements_by_class_name(
            "customize-option"
        )

        time.sleep(2)

        # Checking field values related with User object
        fields = ["email", "username", "first_name", "last_name"]
        correct_labels_and_inputs = []

        for i, field in enumerate(fields):
            field_el = user_form_fields[i]
            label = field_el.find_element_by_tag_name('label')
            input = field_el.find_element_by_tag_name('input')

            exp_label = field.capitalize().replace('_', ' ')

            correct_labels_and_inputs.extend(
                [
                    label.text == exp_label,
                    input.get_attribute("value") == getattr(self.user, field)
                ]
            )

        self.assertTrue(all(correct_labels_and_inputs))

        # Checking city inputs and label
        city_field_el = user_form_fields[len(fields)]
        label = city_field_el.find_element_by_tag_name('label')
        city_inputs = city_field_el.find_elements_by_tag_name('input')
        city_obj = self.user.profile.city

        self.assertEqual(label.text, "City")
        self.assertEqual(
            city_inputs[0].get_attribute("value"),
            f"{city_obj.name}, {city_obj.region}, {city_obj.country}"
        )
        self.assertEqual(
            city_inputs[1].get_attribute("value"), city_obj.place_id
        )

        # Checking birthday inputs and label
        birthday_el = user_form_fields[len(fields) + 1]
        label = birthday_el.find_element_by_tag_name('label')
        birthday_inputs = birthday_el.find_elements_by_tag_name('select')
        birthday = self.user.profile.birthday

        exp_birthday = [
            getattr(birthday, field) for field in ["day", "month", "year"]
        ]

        birthday_values = []

        for inp in birthday_inputs:
            value = Select(inp).first_selected_option.get_attribute("value")
            birthday_values.append(int(value))

        self.assertEqual(label.text, "Birthday")
        self.assertEqual(exp_birthday, birthday_values)

        # Checking password form fields
        pass_form_fields = pass_form.find_elements_by_class_name(
            "customize-option"
        )

        exp_form_fields = [
            "Old password", "New password", "Repeat new password"
        ]

        # Checking if all fields of passwords form are empty and have proper
        # input type
        for i, field in enumerate(exp_form_fields):
            field_el = pass_form_fields[i]
            label = field_el.find_element_by_tag_name('label')
            input = field_el.find_element_by_tag_name('input')

            self.assertEqual(label.text, field)
            self.assertEqual(input.get_attribute("value"), "")
            self.assertEqual(input.get_attribute("type"), "password")

    def test_correct_settings(self):
        """
        Tests correct update user data via settings form. User should be
        updated.
        """
        main = self.browser.find_element_by_tag_name("main")
        user_form = main.find_element_by_tag_name("form")

        success_msg = "Your account has been successfully updated."
        info_block_text = self.browser.find_element_by_class_name(
            "info-block__text"
        )

        submit_button = user_form.find_element_by_tag_name("button")
        submit_button.click()

        time.sleep(5)

        # User has sent the form, but data hasn't been changed so form hasn't
        # been sent in fact
        self.assertNotEqual(info_block_text.text, success_msg)

        # Passing new data to update user form
        username_field = user_form.find_element_by_css_selector(
            "[name='username']"
        )

        username_field.clear()
        username_field.send_keys("BrandNewUsername123")

        submit_button.click()

        time.sleep(5)

        # Info about successful updating user data should be displayed
        self.assertEqual(info_block_text.text, success_msg)

    def test_invalid_settings(self):
        """
        Tests invalid update user data via settings form. Error shoulds be
        displayed.
        """
        main = self.browser.find_element_by_tag_name("main")
        user_form = main.find_element_by_tag_name("form")

        error_msg = "A user with that username already exists."
        info_block_text = self.browser.find_element_by_class_name(
            "info-block__text"
        )

        # No info displayed yet
        self.assertNotEqual(info_block_text.text, error_msg)

        user_2 = User.objects.create_user(username="BrandNewUsername123")

        # Passing new invalid data to update user form
        username_field = user_form.find_element_by_css_selector(
            "[name='username']"
        )

        username_field.clear()
        username_field.send_keys(user_2.username)
        username_field.send_keys(Keys.ENTER)

        time.sleep(5)

        # Info about invalid updating user data should be displayed
        self.assertEqual(info_block_text.text, error_msg)
        self.check_error_info_block(error_msg)

    def test_successful_change_password(self):
        """
        Tests correct change password form send. Password should be changed.
        """
        old_passwd, new_passwd, repeat_new_passwd = self.get_password_inputs()

        old_passwd.send_keys(self.user_data["password"])

        new_password_text = "ThisIsNewPassword123"

        for inp in [new_passwd, repeat_new_passwd]:
            inp.send_keys(new_password_text)

        old_passwd.send_keys(Keys.ENTER)

        time.sleep(5)

        info_block_text = self.browser.find_element_by_class_name(
            "info-block__text"
        )
        self.assertEqual(
            info_block_text.text, "Your password has been changed"
        )

    def test_invalid_change_password_passwords_not_the_same(self):
        """
        Tests invalid change password form send. Password shouldn't be changed.
        """
        old_passwd, new_passwd, repeat_new_passwd = self.get_password_inputs()

        # New password and repeat new password fields don't have the same value
        new_password_text = "ThisIsNewPassword123"
        new_passwd.send_keys(self.user_data["password"])

        for inp in [old_passwd, repeat_new_passwd]:
            inp.send_keys(new_password_text)

        new_passwd.send_keys(Keys.ENTER)

        time.sleep(3)

        self.check_error_info_block('Passwords are not the same.')

    def test_invalid_change_password_old_passwords_incorrect(self):
        """
        Tests invalid change password form send. Password shouldn't be changed.
        """
        new_password_text = "ThisIsNewPassword123"

        # Old password isn't correct
        for inp in self.get_password_inputs():
            inp.send_keys(new_password_text)

        inp.send_keys(Keys.ENTER)

        time.sleep(5)

        self.check_error_info_block("Old password isn't correct.")

    def get_password_inputs(self):
        """Gets input fields related change password form

        Returns:
            List with password input elements
        """
        pass_form = self.browser.find_element_by_class_name(
            "customize-article__card--password"
        )

        return pass_form.find_elements_by_tag_name("input")


class SearchSeleniumTest(
        MainSeleniumViewHelper,
        InfoBlockHelper,
        StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.phrase = "test"
        self.url = f"{self.live_server_url}{reverse('main:home')}#/search" \
                                                            f"/{self.phrase}"

    def test_search_content(self):
        """Tests search page headings content."""
        self.activate_user()
        self.login_user()
        self.browser.get(self.url)

        time.sleep(2)

        search_heading = self.browser.find_element_by_class_name(
            "search-heading"
        )
        self.assertEqual(
            search_heading.text, f"Search results for phrase {self.phrase}"
        )

        search_block_headings = self.browser.find_elements_by_class_name(
            "class-group__heading--search"
        )
        self.assertEqual(
            [heading.text for heading in search_block_headings],
            ["USERS", "SCHOOLS"]
        )

    def test_search_with_results(self):
        """Tests search page with results expected to be displayed."""
        self.activate_user()
        amount_of_users = 12

        for i in range(amount_of_users):
            user = User.objects.create_user(
                username=f"{self.phrase}{i}",
                first_name="John",
                last_name="McClain"
            )
            Profile.objects.create(user=user)

        school = School.objects.create(
            name=f"{self.phrase} university", city=self.user.profile.city
        )

        self.login_user()
        self.browser.get(self.url)

        time.sleep(5)

        # All expected users and schools have been displayed
        user_blocks = self.browser.find_elements_by_class_name("search-user")
        self.assertEqual(amount_of_users + 1, len(user_blocks))

    def test_search_without_results(self):
        """Tests search page without results expected to be displayed."""
        self.activate_user()
        self.login_user()
        self.browser.get(self.url)

        time.sleep(5)

        empty_headings = self.browser.find_elements_by_class_name(
            "cockpit-class-group__heading--empty"
        )
        self.assertEqual(
            ["No users", "No schools"],
            [heading.text for heading in empty_headings]
        )

    def test_search_user_block_content(self):
        """Tests search user block result content."""
        self.activate_user()
        self.login_user()

        self.user.username = self.phrase
        self.user.save()

        self.browser.get(self.url)

        time.sleep(5)

        user_block_full_name = self.browser.find_element_by_class_name(
            "search-user__full_name"
        )
        user_block_username = self.browser.find_element_by_class_name(
            "search-user__username"
        )

        self.assertEqual(user_block_full_name.text, self.user.get_full_name())
        self.assertEqual(user_block_username.text, f"@{self.user.username}")

    def test_search_school_block_content(self):
        """Tests search school block result content."""
        self.activate_user()
        self.login_user()

        city_obj = self.user.profile.city
        school = School.objects.create(
            name=f"{self.phrase} university", city=city_obj
        )

        self.browser.get(self.url)

        time.sleep(5)

        school_block_name = self.browser.find_element_by_class_name(
            "search-user__full_name"
        )
        school_block_city = self.browser.find_element_by_class_name(
            "search-user__username"
        )

        self.assertEqual(school_block_name.text, school.name)
        self.assertEqual(
            school_block_city.text,
            f"{city_obj.name}, {city_obj.country_short}"
        )
