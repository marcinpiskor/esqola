from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.urls import reverse_lazy


class HomeView(LoginRequiredMixin, TemplateView):
    """Template view that contains JS file with front-end app."""
    login_url = reverse_lazy("authorization:login")
    template_name = "main/home.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["GOOGLE_MAPS_API_KEY"] = settings.GOOGLE_MAPS_API_KEY
        return context
