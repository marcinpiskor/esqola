import time
from datetime import date
from django.shortcuts import reverse
from django.core.management import call_command
from django.contrib.auth.models import User, Group
from authorization.utils import new_city_data, SeleniumViewHelper
from authorization.models import City, Profile


class MainSeleniumViewHelper(SeleniumViewHelper):
    """
    Mixin class that provides method that improves selenium tests related with
    home view.
    """
    def setUp(self):
        super().setUp()
        self.user_data = {
            "username": "admin",
            "password": "AdminAdmin123"
        }
        extra_info = {
            "first_name": "Maria",
            "last_name": "Youvegottaseeher",
        }

        self.user = User.objects.create_user(**self.user_data, **extra_info)
        self.profile = Profile.objects.create(user=self.user)

    def activate_user(self, role=""):
        """
        Fills profile fields with correct data in order to activate user
        account.
        """
        call_command("creategroups")

        self.profile.role = Group.objects.filter(name__startswith=role).first()
        self.profile.birthday = date(1998, 5, 10)
        self.profile.city = City.objects.create(**new_city_data)
        self.profile.save()

    def login_user(self):
        """Logs in user on website."""
        self.browser.get(self.live_server_url + reverse("authorization:login"))
        self.send_form()
        time.sleep(5)


class InfoBlockHelper:
    """
    Mixin class that provides methods for checking info block status content.
    """
    def get_info_block_data(self):
        """Gets info block element and extracts text content of it.

        Returns:
            Dictionary with info block element and error message of it
        """
        info_data = {
            "el": self.browser.find_element_by_class_name("info-block")
        }

        for content in ["heading", "text"]:
            content_class = f"info-block__{content}"
            el = info_data["el"].find_element_by_class_name(content_class)
            info_data[content] = el.text

        return info_data

    def check_error_info_block(self, error_msg):
        """Checks if info block contains expected error message.

        Arg:
            error_msg: expected error message
        """
        info_block = self.get_info_block_data()
        self.assertEqual(info_block["heading"], "SOMETHING WENT WRONG")
        self.assertEqual(info_block["text"], error_msg)
        self.assertIn(
            "info-block--error", info_block["el"].get_attribute("class")
        )
