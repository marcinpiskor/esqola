from django.db import models
from authorization.models import ClassGroup, User
from game.utils import ContentHelper


class Post(ContentHelper, models.Model):
    """Stores info about post.

    Attributes:
        author - user that is author of post
        content - text of post
        class_group - class group related with post
        created - datetime when post has been created
    """
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="posts",
        blank=True,
        null=True
    )
    content = models.TextField()
    class_group = models.ForeignKey(
        ClassGroup,
        on_delete=models.CASCADE,
        related_name="posts"
    )
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return f"{self.shorter_content} ({self.class_group})"


class Comment(ContentHelper, models.Model):
    """Stores info about comment to post.

    Attributes:
        author - user that is author of comment
        content - text of comment
        reply_to - post which this comment is reply to
        created - datetime when comment has been created
    """
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="comments",
        blank=True,
        null=True
    )
    content = models.TextField()
    reply_to = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name="comments"
    )
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.shorter_content} - {self.reply_to}"
