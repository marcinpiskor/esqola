import json
from urllib.parse import urlencode
from django.test import TestCase
from .models import Post, Comment
from .utils import DashboardTestMixin


class PostAPITest(DashboardTestMixin, TestCase):
    def setUp(self):
        super().setUp()

        self.post_data = {
            "content": "It's the final countdown!",
            "class_group": self.school_1.classes.first().id
        }

    def test_list_without_user(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/post/")
        self.assertEqual(res.status_code, 403)

    def test_retrieve_without_user(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/post/2/")
        self.assertEqual(res.status_code, 403)

    def test_post_without_user(self):
        """
        Tests POST method without user logged in. Forbidden error expected.
        """
        res = self.client.post("/api/post/", self.post_data)
        self.assertEqual(res.status_code, 403)

    def test_put_without_user(self):
        """Tests PUT method without user logged in. Forbidden error expected."""
        res = self.client.put("/api/post/3/", {"content": "Dolor sit amet"})
        self.assertEqual(res.status_code, 403)

    def test_patch_without_user(self):
        """
        Tests PATCH method without user logged in. Forbidden error expected.
        """
        res = self.client.patch("/api/post/4/", {"content": "Lorem dolor"})
        self.assertEqual(res.status_code, 403)

    def test_delete_without_user(self):
        """
        Tests DELETE method without user logged in. Forbidden error expected.
        """
        res = self.client.delete("/api/post/5/")
        self.assertEqual(res.status_code, 403)

    def test_list_as_teacher_related_with_post(self):
        """
        Tests list action as teacher related with class group of post. Posts
        should be returned.
        """
        self.client.force_login(self.teacher)
        self.create_post_object(self.school_1.classes.first())
        self.check_post_list_correct()

    def test_list_as_teacher_unrelated_with_post(self):
        """
        Tests list action as user unrelated with class group of post. Posts
        shouldn't be returned.
        """
        self.client.force_login(self.teacher)

        post = self.create_post_object(self.school_1.classes.first())
        post.class_group.teacher = None
        post.class_group.save()

        self.check_post_list_empty()

    def test_list_as_student_related_with_post_author(self):
        """
        Tests list action as student who is author of post. Posts should be
        returned.
        """
        self.client.force_login(self.student)
        self.create_post_object(self.school_1.classes.first())
        self.check_post_list_correct()

    def test_list_as_student_unrelated_with_post_class_group(self):
        """
        Tests list action as student unrelated with class group of post. Posts
        shouldn't be returned.
        """
        self.client.force_login(self.student)

        post = self.create_post_object(self.school_1.classes.first())
        post.class_group.students.remove(self.student)

        self.check_post_list_empty()

    def test_list_as_student_related_with_post_class_group(self):
        """
        Tests list action as student related with class group of post. Posts
        should be returned.
        """
        self.client.force_login(self.student)

        post = self.create_post_object(self.school_1.classes.first())
        post.author = None
        post.save()

        self.check_post_list_correct()

    def test_list_as_principal_of_school_related_with_post(self):
        """
        Tests list action as principal related with class group of post. Posts
        shouldn't be returned.
        """
        self.client.force_login(self.principal_1)
        self.create_post_object(self.school_1.classes.first())
        self.check_post_list_invalid()

    def test_list_as_principal_of_school_unrelated_with_post(self):
        """
        Tests list action as principal unrelated with class group of post. Posts
        shouldn't be returned.
        """
        self.client.force_login(self.principal_2)
        self.create_post_object(self.school_1.classes.first())
        self.check_post_list_invalid()

    def test_retrieve_without_comment(self):
        """
        Tests retrieve action of post without comment. Post should be returned.
        """
        self.client.force_login(self.teacher)
        post = self.create_post_object(self.school_1.classes.first())
        self.check_post_detail_correct(post)

    def test_retrieve_with_comment(self):
        """
        Tests retrieve action of post with comment. Post should be returned.
        """
        self.client.force_login(self.teacher)
        args = self.create_post_object(self.school_1.classes.first(), True)
        self.check_post_detail_correct(*args)

    def test_retrieve_as_student_of_class_group(self):
        """
        Tests retrieve action as student related with class group of post.
        Post should be returned.
        """
        self.client.force_login(self.student)

        post = self.create_post_object(self.school_1.classes.first())
        post.author = None
        post.save()

        self.check_post_detail_correct(post)

    def test_retrieve_as_student_related_with_post_author(self):
        """
        Tests retrieve action as student who is author of post. Post should be
        returned.
        """
        self.client.force_login(self.student)
        args = self.create_post_object(self.school_1.classes.first(), True)
        self.check_post_detail_correct(*args)

    def test_post_successful_as_teacher(self):
        """
        Tests POST method as teacher of class group. Post should be created.
        """
        self.client.force_login(self.teacher)
        self.check_post_create_correct(self.teacher.id, self.teacher_data)

    def test_post_successful_as_student(self):
        """
        Tests POST method as student of class group. Post should be created.
        """
        self.client.force_login(self.student)
        self.check_post_create_correct(self.student.id, self.student_data)

    def test_post_invalid_as_principal(self):
        """
        Tests POST method as principal of class group. Post shouldn't be
        created.
        """
        self.client.force_login(self.principal_1)
        exp_res = {"detail": "Only students and teachers are allowed."}
        self.check_post_create_invalid(403, exp_res)

    def test_post_invalid_class_group_doesnt_exist(self):
        """Tests POST method with invalid data. Post shouldn't be created."""
        self.client.force_login(self.teacher)

        # Class group with that id doesn't exist
        invalid_class_pk = 1234567
        self.post_data["class_group"] = invalid_class_pk
        exp_res = {
            "class_group": [
                f'Invalid pk "{invalid_class_pk}" - object does not exist.'
            ]
        }
        self.check_post_create_invalid(400, exp_res)

    def test_post_invalid_user_is_not_teacher_of_class_group(self):
        """
        Tests POST method as teacher user unrelated with class group. Post
        shouldn't be created.
        """
        self.teacher.teacher_classes.set([])
        self.client.force_login(self.teacher)

        exp_res = {
            "class_group": ["Current user doesn't belong to class group."]
        }
        self.check_post_create_invalid(400, exp_res)

    def test_post_invalid_user_is_not_student_of_class_group(self):
        """
        Tests POST method as student user unrelated with class group. Post
        shouldn't be created.
        """
        self.student.student_classes.set([])
        self.client.force_login(self.student)

        exp_res = {
            "class_group": ["Current user doesn't belong to class group."]
        }
        self.check_post_create_invalid(400, exp_res)

    def test_post_invalid_data_empty_content(self):
        """
        Tests POST method as student user unrelated with class group. Post
        shouldn't be created.
        """
        self.client.force_login(self.teacher)
        self.post_data["content"] = ""
        exp_res = {"content": ["This field may not be blank."]}
        self.check_post_create_invalid(400, exp_res)

    def test_put_correct(self):
        """Tests PUT method with correct data. Post should be updated."""
        self.client.force_login(self.student)

        post = self.create_post_object(self.school_1.classes.first())
        post_new_class_group = self.school_2.classes.first().id
        post_new_content = "We livin' together"

        data = {
            "content": post_new_content,
            "class_group": post_new_class_group
        }

        res = self.client.put(
            f"/api/post/{post.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )
        res_json = json.loads(res.content)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res_json["content"], post_new_content)
        self.assertNotEqual(res_json["class_group"], post_new_class_group)

    def test_update_invalid_not_author_of_post(self):
        """
        Tests PUT method as user who is not author of post. Post shouldn't be
        updated.
        """
        self.client.force_login(self.teacher)

        post = self.create_post_object(self.school_1.classes.first())
        self.post_data["content"] = "And maybe we'll come back?"

        msg = "Only author of post/comment is allowed to do this action."
        exp_res = {"detail": msg}

        # Current user is not author of post
        res = self.client.put(
            f"/api/post/{post.id}/", urlencode(self.post_data),
            content_type="application/x-www-form-urlencoded"
        )
        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_delete_post_correct(self):
        """Tests DELETE method with correct data. Post should be deleted."""
        self.client.force_login(self.student)

        post = self.create_post_object(self.school_1.classes.first())
        before_post_amount = Post.objects.count()

        res = self.client.delete(f"/api/post/{post.id}/")
        self.assertEqual(res.status_code, 204)
        self.assertGreater(before_post_amount, Post.objects.count())

    def test_delete_post_invalid_not_author_of_post(self):
        """Tests DELETE method with invalid data. Post should not be deleted."""
        self.client.force_login(self.teacher)

        post = self.create_post_object(self.school_1.classes.first())
        before_post_amount = Post.objects.count()

        msg = "Only author of post/comment is allowed to do this action."
        exp_res = {"detail": msg}

        # Current user is not author of post
        res = self.client.delete(f"/api/post/{post.id}/")
        self.assertEqual(res.status_code, 403)
        self.assertEqual(before_post_amount, Post.objects.count())
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def check_post_list_correct(self):
        """Checks if list action response contains expected data."""
        res = self.client.get("/api/post/")
        res_json = json.loads(res.content)

        # Properties related with pagination are expected
        self.assertEqual(res.status_code, 200)
        self.assertGreater(res_json["count"], 0)
        self.assertGreater(len(res_json["results"]), 0)
        self.assertEqual(res_json["previous"], None)
        self.assertEqual(res_json["next"], None)

    def check_post_list_empty(self):
        """Checks if list action response is empty."""
        res = self.client.get("/api/post/")
        res_json = json.loads(res.content)
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), [])

    def check_post_list_invalid(self):
        """Checks if invalid list action response contains error."""
        res = self.client.get("/api/post/")
        self.assertEqual(res.status_code, 403)
        exp_res = {"detail": "Only students and teachers are allowed."}
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def check_post_detail_correct(self, post, comment=None):
        """Checks if retrieve action response is correct.

        Args:
            post - Post object
            comment - boolean that indicates if Post object has comment
        """
        res = self.client.get(f"/api/post/{post.id}/")
        self.assertEqual(res.status_code, 200)
        self.student_data.pop("password")

        if post.author:
            exp_author = {
                **self.student_data,
                "id": post.author.id,
                "profile": {
                    "photo": None,
                },
                "school": None
            }
        else:
            exp_author = None

        if comment:
            exp_comments = [
                {
                    "id": comment.id,
                    "content": comment.content,
                    "created": f"{comment.created.isoformat()[:-6]}Z",
                    "author": exp_author,
                }
            ]

        else:
            exp_comments = []

        exp_res = {
            "id": post.id,
            "author": exp_author,
            "content": "I have a dream",
            "class_group": post.class_group.id,
            "created": f"{post.created.isoformat()[:-6]}Z",
            "comments": exp_comments
        }

        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def check_post_create_correct(self, author_id, author_data):
        """Checks if POST method with correct data, creates new Post object.

        Args:
            author_id - id of post author
            author_data - data of post author
        """
        before_post_amount = Post.objects.count()

        res = self.client.post(f"/api/post/", self.post_data)
        self.assertEqual(res.status_code, 201)
        self.assertGreater(Post.objects.count(), before_post_amount)

        author_data.pop("password")

        exp_author = {
            **author_data,
            "id": author_id,
            "profile": {
                "photo": None,
            },
            "school": None
        }

        created_post_obj = Post.objects.first()

        exp_res = {
            "id": created_post_obj.id,
            "author": exp_author,
            "content": self.post_data["content"],
            "class_group": self.post_data["class_group"],
            "created": f"{created_post_obj.created.isoformat()[:-6]}Z",
            "comments": []
        }

        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def check_post_create_invalid(self, exp_code, exp_res):
        """Checks if POST method with invalid data displays expected error.

        Args:
            exp_code - expected status code of response
            exp_res - expected create post response
        """
        before_post_amount = Post.objects.count()

        res = self.client.post(f"/api/post/", self.post_data)
        self.assertEqual(res.status_code, exp_code)
        self.assertEqual(Post.objects.count(), before_post_amount)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def create_post_object(self, class_group, comment=False):
        """Creates Post object for testing purposes.

        Args:
            class_group - Class group object desired to be related with new post
            comment - boolean that indicates if Comment object needs to be
            related with new Post object
        """
        post = Post.objects.create(
            content="I have a dream",
            author=self.student,
            class_group=class_group
        )

        if comment:
            comment_obj = Comment.objects.create(
                reply_to=post, author=post.author, content="I am MLK!"
            )
            return post, comment_obj

        return post


class CommentAPITest(DashboardTestMixin, TestCase):
    def setUp(self):
        super().setUp()

        self.post = Post.objects.create(
            content="Wind of change...",
            author=self.student,
            class_group=self.school_1.classes.first()
        )

        self.comment_data = {
            "content": "What are you talkin' about, man?",
            "reply_to.id": self.post.id
        }

    def test_retrieve_as_author(self):
        """Tests retrieve as author of comment. Comment should be returned."""
        self.client.force_login(self.teacher)

        comment = self.create_comment_object()
        exp_res = self.get_exp_comment_response(comment)

        res = self.client.get(f"/api/comment/{comment.id}/")
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_retrieve_as_non_author(self):
        """
        Tests retrieve as author of comment. Comment shouldn't be returned.
        """
        self.client.force_login(self.principal_1)
        comment = self.create_comment_object()

        msg = "Only author of post/comment is allowed to do this action."
        exp_res = {"detail": msg}

        res = self.client.get(f"/api/comment/{comment.id}/")
        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_post_comment_correct(self):
        """Tests POST method with correct data. Post object should be created."""
        self.client.force_login(self.teacher)

        before_comment_amount = Comment.objects.count()

        res = self.client.post("/api/comment/", self.comment_data)
        self.assertEqual(res.status_code, 201)
        self.assertGreater(Comment.objects.count(), before_comment_amount)

        exp_res = self.get_exp_comment_response(Comment.objects.last())
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_post_comment_invalid_reply_to_post_does_not_exist(self):
        """
        Tests POST method with invalid data. Post object shouldn't be created.
        """
        msg = "Post object with that id related with current user doesn't " \
                                                                    "exist."
        exp_res = {"reply_to": {"id": [msg]}}

        # Post object with that id doesn't exist
        self.comment_data["reply_to.id"] = 1234567
        self.check_create_comment_invalid(exp_res)

    def test_post_comment_invalid_reply_to_post_user_does_not_belong(self):
        """
        Tests POST method with invalid data. Post object shouldn't be created.
        """
        # Current user is not related with class group that is related with
        # post to reply
        self.teacher.teacher_classes.set([])

        msg = "Post object with that id related with current user doesn't " \
                                                                    "exist."
        exp_res = {"reply_to": {"id": [msg]}}

        self.check_create_comment_invalid(exp_res)

    def test_put_comment_as_author(self):
        """
        Tests PUT method as user who is author of comment. Comment should be
        updated.
        """
        self.client.force_login(self.teacher)
        comment = self.create_comment_object()

        data = {"content": "Lorem ipsum"}

        res = self.client.put(
            f"/api/comment/{comment.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content)["content"], data["content"])

    def test_put_comment_as_non_author(self):
        """
        Tests PUT method as user who is not author of comment. Comment should
        not be updated.
        """
        self.client.force_login(self.principal_1)
        comment = self.create_comment_object()

        data = {"content": "Dolor sit amet"}
        msg = "Only author of post/comment is allowed to do this action."
        exp_res = {"detail": msg}

        res = self.client.put(
            f"/api/comment/{comment.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )
        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_delete_comment_as_author(self):
        """
        Tests DELETE method as user who is author of comment. Comment should be
        deleted.
        """
        self.client.force_login(self.teacher)
        comment = self.create_comment_object()
        before_comment_amount = Comment.objects.count()

        res = self.client.delete(f"/api/comment/{comment.id}/")
        self.assertGreater(before_comment_amount, Comment.objects.count())
        self.assertEqual(res.status_code, 204)

    def test_delete_comment_as_non_author(self):
        """
        Tests DELETE method as user who is not author of comment. Comment should
        be deleted.
        """
        self.client.force_login(self.principal_2)
        comment = self.create_comment_object()
        before_comment_amount = Comment.objects.count()

        msg = "Only author of post/comment is allowed to do this action."
        exp_res = {"detail": msg}

        res = self.client.delete(f"/api/comment/{comment.id}/")
        self.assertEqual(res.status_code, 403)
        self.assertEqual(before_comment_amount, Comment.objects.count())
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def create_comment_object(self):
        """Creates Comment object for testing purposes."""
        return Comment.objects.create(
            content="Tu tu tu tu tu",
            author=self.teacher,
            reply_to=self.post
        )

    def check_create_comment_invalid(self, exp_res):
        """Checks if POST method with invalid data displays expected error.

        Args:
            exp_res - expected create comment response
        """
        self.client.force_login(self.teacher)

        before_post_amount = Comment.objects.count()

        res = self.client.post("/api/comment/", self.comment_data)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(Comment.objects.count(), before_post_amount)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def get_exp_comment_response(self, comment):
        """Prepares expected correct comment response.

        Arg:
            comment - Comment object desired to be serialized
        Returns:
            Dictionary that contains data for expected comment response
        """
        for key in ["student", "teacher"]:
            getattr(self, f"{key}_data").pop("password")

        return {
            "id": comment.id,
            "author": {
                **self.teacher_data,
                "id": self.teacher.id,
                "profile": {
                    "photo": None
                },
                "school": None
            },
            "reply_to": {
                "id": comment.reply_to.id,
                "author": {
                    **self.student_data,
                    "id": self.student.id,
                    "profile": {
                        "photo": None
                    },
                    "school": None
                },
                "content": comment.reply_to.content,
                "class_group": comment.reply_to.class_group.id,
                "created": f"{comment.reply_to.created.isoformat()[:-6]}Z"
            },
            "content": comment.content,
            "created": f"{comment.created.isoformat()[:-6]}Z"
        }
