import json
from channels.generic.websocket import AsyncWebsocketConsumer
from django.db.models import Q
from authorization.models import ClassGroup
from .serializers import PostDetailSerializer


class DashboardConsumer(AsyncWebsocketConsumer):
    """
    Websocket consumer class responsible for creating and receiving dashboard
    Post objects.
    """
    async def connect(self):
        self.class_group_id = self.scope["url_route"]["kwargs"]["class_group_id"]
        self.class_group_name = f"class_group_{self.class_group_id}"

        await self.accept()

        class_group_exists = ClassGroup.objects.filter(
            Q(teacher=self.scope["user"]) | Q(students=self.scope["user"])
        ).count() > 0

        # Add user to class group dashboard channel if user is related with
        # Class group object
        if class_group_exists:
            await self.channel_layer.group_add(
                self.class_group_name,
                self.channel_name
            )
        else:
            await self.send(json.dumps({
                "type": "dashboard_error",
                "message": {
                    "detail": "Only teacher and students are allowed."
                }
            }))

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.class_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)

        if text_data_json["type"] == "create_post":
            text_data_json["data"]["class_group"] = self.class_group_id
            await self.create_post(text_data_json["data"])

    async def create_post(self, post_data):
        """Creates Post object and sends serialized version of it.

        Arg:
            post_data - dictionary with data of new Post object
        """
        serializer = PostDetailSerializer(
            data=post_data, context={"user": self.scope["user"]}
        )

        if serializer.is_valid():
            serializer.save()

            await self.channel_layer.group_send(
                self.class_group_name,
                {
                    "type": "post_created",
                    "message": serializer.data
                }
            )
        else:
            error_data = {
                "type": "post_failed",
                "message": serializer.errors
            }

            await self.send(json.dumps(error_data))

    async def post_created(self, event):
        await self.send(text_data=json.dumps(event))
