from rest_framework import permissions


POST_ALLOWED_PARAMS = {
    "class_group__id", "author__id", "author__username", "author__first_name",
    "author__last_name", "created__gt", "created__lt", "limit", "offset"
}

class IsStudentOrTeacher(permissions.BasePermission):
    """Permission that checks if current user is student or teacher."""
    message = f"Only students and teachers are allowed."

    def has_permission(self, request, view):
        allowed_roles = ["students", "teachers"]

        return (
            request.user.profile
            and request.user.profile.role
            and request.user.profile.role.name in allowed_roles
        )


class IsAuthorOfPostOrComment(permissions.BasePermission):
    """
    Permission that checks if current user is author of Post/Comment object.
    """
    message = f"Only author of post/comment is allowed to do this action."

    def has_object_permission(self, request, view, obj):
        return obj.author == request.user


class HasPostAllowedQueryParams(permissions.BasePermission):
    """
    Permission that checks if post allowed query params has been passed.
    """
    message = "Only these query params are allowed in this request: " \
                                    f"{', '.join(POST_ALLOWED_PARAMS)}."

    def has_permission(self, request, view):
        passed_params = set(request.query_params.keys())

        if (len(passed_params) == 0
            or len(passed_params) == len(POST_ALLOWED_PARAMS&passed_params)):
            return True
        else:
            return False
