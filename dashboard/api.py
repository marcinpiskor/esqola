from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.pagination import LimitOffsetPagination
from django.db.models import Q
from authorization.utils import QueryParamsMixin
from .models import Post, Comment
from .serializers import (
    PostListSerializer,
    PostDetailSerializer,
    PostEditSerializer,
    CommentListSerializer,
    CommentDetailSerializer,
    CommentEditSerializer
)
from .permissions import (
    IsStudentOrTeacher,
    IsAuthorOfPostOrComment,
    HasPostAllowedQueryParams
)


class PostViewSet(
        QueryParamsMixin,
        LimitOffsetPagination,
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.CreateModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    """
    Viewset that handles getting, creating, updating and deleting Post objects.

    Attributes:
        str_query_params - string Post model fields allowed to be query params
        default_limit - default amount of Post objects per page
    """
    str_query_params = [
        "author__username", "author__first_name", "author__last_name"
    ]
    default_limit = 1

    def get_serializer_class(self):
        if self.action == "list":
            return PostListSerializer
        if self.action == "update" or self.action == "partial_update":
            return PostEditSerializer
        return PostDetailSerializer

    def get_queryset(self):
        if self.action == "list" and len(self.request.query_params) > 0:
            kwargs = self.get_query_params(True)
        else:
            kwargs = {}

        return Post.objects.filter(
            Q(class_group__teacher=self.request.user)
            | Q(class_group__students=self.request.user), **kwargs
        ).distinct()

    def get_permissions(self):
        permissions = (IsAuthenticated, IsStudentOrTeacher)
        exception_actions = ["update", "partial_update", "destroy"]

        if self.action in exception_actions:
            permissions = (*permissions, IsAuthorOfPostOrComment)
        if self.action == "list":
            permissions = (*permissions, HasPostAllowedQueryParams)

        return [permission() for permission in permissions]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset, request)

        if page:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CommentViewSet(
        mixins.RetrieveModelMixin,
        mixins.CreateModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    """
    Viewset that handles getting, creating, updating and deleting Comment
    objects.

    Attribute:
        permission_classes - only authenticated students or teacher are allowed
    """
    queryset = Comment.objects.all()
    permission_classes = (IsAuthenticated, IsStudentOrTeacher)

    def get_serializer_class(self):
        if self.action == "update" or self.action == "partial_update":
            return CommentEditSerializer
        return CommentDetailSerializer

    def get_permissions(self):
        if self.action == "create":
            permissions = (IsAuthenticated, IsStudentOrTeacher)
        else:
            permissions = (IsAuthenticated, IsAuthorOfPostOrComment)

        return [permission() for permission in permissions]
