from rest_framework import serializers
from django.db.models import Q
from authorization.serializers import UserPostSerializer
from .utils import DashboardModelUpdateHelper
from .models import Post, Comment


class CommentPostSerializer(serializers.ModelSerializer):
    """Comments model serializer for objects related with Post

    Attribute:
        author - User model serializer for objects related with Post/Comment
    """
    author = UserPostSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = ("id", "author", "content", "created")


class PostListSerializer(serializers.ModelSerializer):
    """Post model serializer for list action.

    Attribute:
        author - User model serializer for objects related with Post/Comment
    """
    author = UserPostSerializer(read_only=True)

    class Meta:
        model = Post
        fields = (
            "id", "author", "content", "class_group", "created", "comments"
        )


class PostDetailSerializer(serializers.ModelSerializer):
    """Post model serializer for detail action.

    Attribute:
        comments - Comment model serializer for objects related with Post
    """
    comments = CommentPostSerializer(read_only=True, many=True)
    author = UserPostSerializer(read_only=True)

    class Meta:
        model = Post
        fields = (
            "id", "author", "content", "class_group", "created", "comments"
        )
        read_only_fields = ("id", "author", "created", "comments")

    def validate_content(self, value):
        """Validates if post content is not empty

        Arg:
            value - post content
        Returns:
            Validated post content
        Raises:
            Validation error with message that post content can't be empty
        """
        if (value.strip()) == 0:
            raise serializers.ValidationError("Content can't be empty.")
        return value

    def validate_class_group(self, value):
        """Validates if current user is related with class group.

        Arg:
            value - Class group object
        Returns:
            Class group object
        Raises:
            Validation error with message that class group is not related with
            current user (current user is not a student or teacher of class
            group)
        """
        user = self.get_current_user()

        if (value.teacher == user
            or value.students.filter(id=user.id).count() > 0):
            return value
        else:
            msg = "Current user doesn't belong to class group."
            raise serializers.ValidationError([msg])

    def create(self, validated_data):
        validated_data["author"] = self.get_current_user()
        return Post.objects.create(**validated_data)

    def get_current_user(self):
        """Extracts current user object from serializer context.

        Returns:
            Current user object
        """
        if self.context.get("request"):
            return self.context["request"].user
        else:
            return self.context["user"]


class PostEditSerializer(DashboardModelUpdateHelper, PostDetailSerializer):
    """Post model serializer for update and partial update actions."""
    class Meta:
        model = Post
        fields = (
            "id", "author", "content", "class_group", "created", "comments"
        )
        read_only_fields = (
            "id", "author", "class_group", "created", "comments"
        )


class PostCommentSerializer(serializers.ModelSerializer):
    """Post model serializer for objects related with Comment.

    Attributes:
        author - User model serializer for objects related with Post/Comment
        id - integer field with id of Post objecct
    """
    author = UserPostSerializer(read_only=True)
    id = serializers.IntegerField()

    class Meta:
        model = Post
        fields = ("id", "author", "content", "class_group", "created")
        read_only_fields = ("author", "content", "class_group", "created")

    def validate(self, validated_data):
        user = self.context["request"].user

        # Post object needs to be related with class group of current user
        post_to_reply = Post.objects.filter(
            (Q(class_group__teacher=user) | Q(class_group__students=user)),
            id=validated_data["id"]
        ).first()

        if not post_to_reply:
            msg = "Post object with that id related with current user " \
                                                        "doesn't exist."
            raise serializers.ValidationError({"id": [msg]})

        return post_to_reply


class CommentListSerializer(serializers.ModelSerializer):
    """Comment model serializer for list action.

    Attribute:
        author - User model serializer for objects related with Post/Comment
    """
    author = UserPostSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = ("id", "author", "content", "reply_to", "created")


class CommentDetailSerializer(serializers.ModelSerializer):
    """Comment model serializer for detail action.

    Attributes:
        author - User model serializer for objects related with Post/Comment
        reply_to - Post model serializer for objects related with Comment
    """
    author = UserPostSerializer(read_only=True)
    reply_to = PostCommentSerializer()

    class Meta:
        model = Comment
        fields = ("id", "author", "content", "reply_to", "created")
        read_only_fields = ("id", "author", "created")

    def create(self, validated_data):
        validated_data["author"] = self.context["request"].user
        return Comment.objects.create(**validated_data)


class CommentEditSerializer(
        DashboardModelUpdateHelper,
        serializers.ModelSerializer
    ):
    """Comment model serializer for update and partial update actions.

    Attributes:
        author - User model serializer for objects related with Post/Comment
        reply_to - Post model serializer for objects related with Comment
    """
    author = UserPostSerializer(read_only=True)
    reply_to = PostCommentSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = ("id", "author", "content", "reply_to", "created")
        read_only_fields = ("id", "author", "reply_to", "created")
