from django.core.management import call_command
from django.contrib.auth.models import User, Group
from authorization.models import School, ClassGroup, City, Profile
from authorization.utils import new_city_data, new_user_data
from management.utils import principal_data, teacher_data, student_data


class DashboardModelUpdateHelper:
    """
    Mixin class that provides update method for models related with dashboard
    app.
    """
    def update(self, instance, validated_data):
        for field in validated_data:
            setattr(instance, field, validated_data[field])

        instance.save()

        return instance


class DashboardTestMixin:
    def setUp(self):
        super().setUp()
        
        call_command("creategroups")
        self.city = City.objects.create(**new_city_data)

        principals = Group.objects.get(name="principals")

        self.principal_data_1 = dict(new_user_data)
        self.principal_data_1.pop("password_repeat")
        self.principal_1 = User.objects.create_user(**self.principal_data_1)

        self.principal_data_2 = dict(principal_data)
        self.principal_2 = User.objects.create_user(**self.principal_data_2)

        for princ in [self.principal_1, self.principal_2]:
            princ.groups.add(principals)

        self.teacher_data = dict(teacher_data)

        self.teacher = User.objects.create_user(**self.teacher_data)
        self.teacher.groups.add(Group.objects.get(name="teachers"))

        self.student_data = dict(student_data)

        self.student = User.objects.create_user(**self.student_data)
        self.student.groups.add(Group.objects.get(name="students"))

        self.school_1 = School.objects.create(
            name="University of Cambridge", city=self.city,
            principal=self.principal_1
        )

        self.school_2 = School.objects.create(
            name="University of Oxford", city=self.city,
            principal=self.principal_2
        )

        for school in [self.school_1, self.school_2]:
            class_group = ClassGroup.objects.create(
                school=school, name="test", teacher=self.teacher
            )
            class_group.students.add(self.student)

        users = [
            self.principal_1, self.principal_2, self.teacher, self.student
        ]

        for user in users:
            Profile.objects.create(user=user)
