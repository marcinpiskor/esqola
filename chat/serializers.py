from rest_framework import serializers
from authorization.serializers import UserListSerializer
from .models import Message


class MessageListSerializer(serializers.ModelSerializer):
    """Message model serializer for list action."""
    class Meta:
        model = Message
        fields = ("author", "to", "content", "created")
        read_only_fields = ("author", "created")

    def validate_to(self, value):
        if value == self.get_current_user():
            msg = "You can't send a message to yourself."
            raise serializers.ValidationError([msg])

        return value

    def create(self, validated_data):
        validated_data["author"] = self.get_current_user()
        return Message.objects.create(**validated_data)

    def get_current_user(self):
        if self.context.get("request"):
            return self.context["request"].user
        else:
            return self.context["user"]


class MessageDetailSerializer(serializers.ModelSerializer):
    """Message model serializer for retrieve action.

    Attributes:
        author - User model serializer for author of message
        to - User model serializer for receiver of message
    """
    author = UserListSerializer(read_only=True)
    to = UserListSerializer(read_only=True)

    class Meta:
        model = Message
        fields = ("author", "to", "content", "created")
        read_only_fields = ("author", "to", "content", "created")
