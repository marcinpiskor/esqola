from rest_framework import viewsets, mixins
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from django.db.models import Q
from django.contrib.auth.models import User
from authorization.serializers import UserConversationSerializer
from .serializers import MessageListSerializer, MessageDetailSerializer
from .permissions import IsAllowedMessage, HasRequiredQueryParam
from .models import Message


class MessageViewSet(
        LimitOffsetPagination,
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.CreateModelMixin,
        viewsets.GenericViewSet):
    """Viewset that handles getting and creating Message objects."""
    def get_queryset(self):
        if self.action == "list":
            user_id = self.request.query_params["user_id"]

            return Message.objects.filter(
                Q(author=self.request.user, to__id=user_id)
                | Q(author__id=user_id, to=self.request.user)
            )
        return Message.objects.all()

    def get_serializer_class(self):
        if self.action == "list" or self.action == "create":
            return MessageListSerializer
        return MessageDetailSerializer

    def get_permissions(self):
        if self.action == "list":
            permissions = (IsAuthenticated, HasRequiredQueryParam)
        elif self.action == "create":
            permissions = (IsAuthenticated,)
        else:
            permissions = (IsAuthenticated, IsAllowedMessage)

        return [permission() for permission in permissions]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset, request)

        if page:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=["GET"])
    def conversations(self, request, *args, **kwargs):
        """
        Action that handles getting info about users that have been chatting
        with current user recently.

        Arguments:
            request - DRF request object
        Returns:
            Response with serialized User objects
        """
        queryset = self.filter_queryset(
            User.objects.filter(
                Q(received_messages__author=request.user)
                | Q(created_messages__to=request.user)
            ).distinct()
        )

        page = self.paginate_queryset(queryset, request)
        context = {"request": request}

        if page:
            serializer = UserConversationSerializer(
                page, many=True, context=context
            )
            return self.get_paginated_response(serializer.data)

        serializer = UserConversationSerializer(
            queryset, many=True, context=context
        )
        return Response(serializer.data)
