from django.db import models
from django.contrib.auth.models import User
from game.utils import ContentHelper

class Message(ContentHelper, models.Model):
    """Stores info about chat messages between two users.

    Fields:
        author - author of message
        to - user that is receiver of message
        content - text of message
        created - datetime when message has been created
    """
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="created_messages"
    )
    to = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="received_messages"
    )
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return f"{self.shorter_content} - {self.author.username}"
