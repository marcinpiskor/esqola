import json
from channels.generic.websocket import AsyncWebsocketConsumer
from django.contrib.auth.models import User
from .models import Message
from .serializers import MessageListSerializer


class ChatConsumer(AsyncWebsocketConsumer):
    """
    Websocket consumer class responsible for creating and receiving dashboard
    Message objects.
    """
    async def connect(self):
        self.user_id = int(self.scope["url_route"]["kwargs"]["user_id"])

        chat_data = [self.user_id, self.scope["user"].id]

        # Prepare unique chat room for two users
        self.chat_id = f"{min(chat_data)}_{max(chat_data)}"
        self.chat_name = f"chat_{self.chat_id}"

        await self.accept()

        # Checking if user doesn't try to chat with itself
        if len(chat_data) == len(set(chat_data)):
            # Add user to chat channel if user desired to chat with exists
            if User.objects.filter(id=self.user_id).count() > 0:
                await self.channel_layer.group_add(
                    self.chat_name,
                    self.channel_name
                )
            else:
                await self.send(json.dumps({
                    "type": "chat_error",
                    "message": {
                        "detail": "User doesn't exist."
                    }
                }))
        else:
            await self.send(json.dumps({
                "type": "chat_error",
                "message": {
                    "detail": "You can't chat with yourself."
                }
            }))

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.chat_name,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)

        if (text_data_json["type"] == "create_msg"
            and "content" in text_data_json["data"]):
            # Invoke method responsible for creating message if proper data
            # has been sent
            text_data_json["data"]["to"] = self.user_id
            await self.create_message(text_data_json["data"])

    async def create_message(self, msg_data):
        """Creates Message object and sends serialized version of it.

        Argument:
            msg_data - dictionary with data of new Message object
        """
        serializer = MessageListSerializer(
            data=msg_data, context={"user": self.scope["user"]}
        )

        if serializer.is_valid():
            serializer.save()

            await self.channel_layer.group_send(
                self.chat_name,
                {
                    "type": "msg_created",
                    "message": serializer.data
                }
            )
        else:
            await self.send(json.dumps({
                "type": "chat_error",
                "message": serializer.errors
            }))

    async def msg_created(self, event):
        await self.send(json.dumps({
            "type": event["type"],
            "message": event["message"]
        }))
