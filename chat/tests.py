import time
import json
from django.test import TestCase
from django.contrib.auth.models import User
from authorization.models import Profile, City
from authorization.utils import new_city_data, new_user_data, basic_user_data
from management.utils import principal_data
from .models import Message
from .permissions import REQUIRED_PARAMS


class MessageAPITest(TestCase):
    def setUp(self):
        self.city = City.objects.create(**new_city_data)

        self.user_data_1 = dict(new_user_data)
        [self.user_data_1.pop(f) for f in ["password", "password_repeat"]]
        self.user_1 = User.objects.create_user(**self.user_data_1)

        self.user_data_2 = dict(principal_data)
        self.user_2 = User.objects.create_user(**self.user_data_2)

        for user in [self.user_1, self.user_2]:
            Profile.objects.create(user=user)

        self.msg_1 = Message.objects.create(
            author=self.user_1, to=self.user_2, content="Hello ;)"
        )
        time.sleep(1)
        self.msg_2 = Message.objects.create(
            author=self.user_2, to=self.user_1, content="Hi, how are you?"
        )

    def test_list_without_user(self):
        """Test list action without user logged in. Forbidden error expected."""
        res = self.client.get("/api/message/")
        self.assertEqual(res.status_code, 403)

    def test_receive_without_user(self):
        """
        Test retrieve action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/message/1/")
        self.assertEqual(res.status_code, 403)

    def test_post_without_user(self):
        """Test POST method without user logged in. Forbidden error expected."""
        res = self.client.post("/api/message/", {"content": "Fine."})
        self.assertEqual(res.status_code, 403)

    def test_conversations_without_user(self):
        """
        Test conversations action without user logged in. Forbidden error
        expected.
        """
        res = self.client.get("/api/message/conversations/")
        self.assertEqual(res.status_code, 403)

    def test_list_correct_as_author_and_receiver_of_msg(self):
        """
        Test list action as author of message. Messages should be returned.
        """
        self.client.force_login(self.user_1)

        exp_res = {
            "count": 2,
            "next": None,
            "previous": None,
            "results": [
                self.get_exp_message(self.msg_2),
                self.get_exp_message(self.msg_1)
            ]
        }

        res = self.client.get(
            f"/api/message/?user_id={self.user_2.id}&limit=2&offset=0",
        )
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_list_invalid_as_no_author_and_no_receiver_of_msg(self):
        """
        Test list action as not author of message. Messages shouldn't be
        returned.
        """
        user = User.objects.create_user(**basic_user_data)
        self.client.force_login(user)

        url = f"/api/message/?user_id={self.user_1.id}&limit=2&offset=0"
        res = self.client.get(url)
        self.assertJSONEqual(str(res.content, encoding="utf8"), [])

    def test_list_invalid_without_user_id(self):
        """
        Test list action without user id query param. Error should be returned.
        """
        self.check_wrong_query_params(f"/api/message/?limit=2&offset=0")

    def test_list_invalid_without_limit(self):
        """
        Test list action without limit query param. Error should be returned.
        """
        self.check_wrong_query_params(f"/api/message/?user_id=1&offset=0")

    def test_list_invalid_without_offset(self):
        """
        Test list action without offset query param. Error should be returned.
        """
        self.check_wrong_query_params(f"/api/message/?user_id=1&limit=2")

    def test_receive_correct_as_author_and_receiver_of_msg(self):
        """
        Test receive action as author of message. Message should be returned.
        """
        self.client.force_login(self.user_1)

        exp_res = self.get_exp_message(self.msg_1, True)
        res = self.client.get(f"/api/message/{self.msg_1.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_receive_invalid_as_no_author_and_no_receiver_of_msg(self):
        """
        Test receive action as not author of message. Message shouldn't be
        returned.
        """
        user = User.objects.create_user(**basic_user_data)
        self.client.force_login(user)

        msg = "You have to be an author or person who receives the message."
        exp_res = {"detail": msg}
        res = self.client.get(f"/api/message/{self.msg_1.id}/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_post_correct(self):
        """Test POST method with correct data. Message should be created."""
        self.client.force_login(self.user_1)

        before_msg_amount = Message.objects.count()
        data = {"to": self.user_2.id, "content": "I am fine, thanks!"}

        res = self.client.post("/api/message/", data)
        self.assertEqual(res.status_code, 201)
        self.assertGreater(Message.objects.count(), before_msg_amount)

        data["author"] = self.user_1.id
        res_json = json.loads(str(res.content, encoding="utf8"))
        res_json.pop("created")

        self.assertJSONEqual(json.dumps(res_json), data)

    def test_post_invalid_user_to_doesnt_exist(self):
        """Test POST method with invalid data. Message shouldn't be created."""
        self.client.force_login(self.user_1)

        before_msg_amount = Message.objects.count()
        data = {"to": 123456, "content": "I am fine, thanks!"}

        exp_res = {
        "to": [f'Invalid pk "{data["to"]}" - object does not exist.']
        }

        # User with that id doesn't exist
        res = self.client.post("/api/message/", data)

        self.assertEqual(res.status_code, 400)
        self.assertEqual(Message.objects.count(), before_msg_amount)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_post_create_invalid_user_the_same(self):
        """Test POST method with invalid data. Message shouldn't be created."""
        self.client.force_login(self.user_1)

        before_msg_amount = Message.objects.count()
        data = {"to": self.user_1.id, "content": "I am fine, thanks!"}

        exp_res = {"to": ["You can't send a message to yourself."]}

        # User can't send message to itself
        res = self.client.post("/api/message/", data)

        self.assertEqual(res.status_code, 400)
        self.assertEqual(Message.objects.count(), before_msg_amount)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_conversations_with_active_conversations(self):
        """
        Test conversations as user who has active conversations. Users should
        be returned.
        """
        self.client.force_login(self.user_1)

        exp_res = [
            {
                **self.user_data_2,
                "id": self.user_2.id,
                "school": None,
                "profile": {
                    "photo": None,
                    "last_active": f"{self.msg_2.created.isoformat()[:-6]}Z"
                },
            }
        ]

        res = self.client.get("/api/message/conversations/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_conversations_with_active_conversations(self):
        """
        Test conversations action as user who has active conversations. Users
        should be returned.
        """
        self.client.force_login(self.user_1)

        exp_res = [
            {
                **self.user_data_2,
                "id": self.user_2.id,
                "school": None,
                "profile": {
                    "photo": None,
                    "last_active": f"{self.msg_2.created.isoformat()[:-6]}Z"
                },
            }
        ]

        res = self.client.get("/api/message/conversations/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_conversations_with_active_conversations(self):
        """
        Test conversations action as user who hasn't active conversations. Users
        shouldn't be returned.
        """
        self.client.force_login(User.objects.create_user(**basic_user_data))

        # Current user hasn't been chatting with any other user
        res = self.client.get("/api/message/conversations/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), [])

    def check_wrong_query_params(self, url):
        """
        Checks if error is returned when required query params are not passed.

        Arg:
            url - url with invalid query params
        """
        self.client.force_login(self.user_1)

        res = self.client.get(url)
        msg = "These query params are required in this request: " \
                                    f"{', '.join(REQUIRED_PARAMS)}."
        exp_res = {"detail": msg}

        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def get_exp_message(self, msg_obj, detail=False):
        """Prepares expected serialized version of Message object.

        Args:
            msg_obj - Message object desired to be serialized
            detail - boolean that indicates if detail serialization is expected
        """
        exp_message = {
            "content": msg_obj.content,
            "created": f"{msg_obj.created.isoformat()[:-6]}Z"
        }

        if detail:
            for field in ["author", "to"]:
                field_obj = getattr(msg_obj, field)
                field_data = getattr(self, f"user_data_{field_obj.id}")
                field_data["id"] = field_obj.id
                field_data["profile"] = field_obj.profile.id
                field_data["school"] = None

                exp_message[field] = field_data
        else:
            exp_message["author"] = msg_obj.author.id,
            exp_message["author"] = exp_message["author"][0]
            exp_message["to"] = msg_obj.to.id

        return exp_message
