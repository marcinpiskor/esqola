from rest_framework import permissions


REQUIRED_PARAMS = {"user_id", "limit", "offset"}

class IsAllowedMessage(permissions.BasePermission):
    """
    Permission that checks if current user is author or receiver of message.
    """
    message = f"You have to be an author or person who receives the message."

    def has_object_permission(self, request, view, obj):
        return obj.author == request.user or obj.to == request.user


class HasRequiredQueryParam(permissions.BasePermission):
    """
    Permission that checks if current user is author or receiver of message.
    """
    message = "These query params are required in this request: " \
                                    f"{', '.join(REQUIRED_PARAMS)}."

    def has_permission(self, request, view):
        amount_of_required_params = len(REQUIRED_PARAMS)
        passed_required_params = len(set(request.query_params)&REQUIRED_PARAMS)

        return amount_of_required_params == passed_required_params
