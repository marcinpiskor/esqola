const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const dist_path = path.join(path.dirname(__dirname), 'dist');

module.exports = {
  context: path.join(__dirname, 'src'),
  entry: {
    auth: './auth/index.js',
    home: './home/index.js'
  },
  output: {
    path: dist_path,
    filename: 'bundle-[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: "/node_modules/",
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(scss|sass)$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      },
      {
        test: /\.(ttf|eot|woff|woff2|png|jpg|jpeg|gif|svg)$/,
        use: {
          loader: "file-loader",
          options: {}
        }
      },
    ]
  },
  plugins: [
    new ExtractTextPlugin("style.css"),
  ]
}
