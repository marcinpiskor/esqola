const answers = new Map();

// Setting map with proper game answers
answers.set('formiga', 'ant');
answers.set('jogador', 'player');
answers.set('pão', 'bread');
answers.set('irmão', 'brother');
answers.set('futebol', 'football');
answers.set('maçã', 'apple');
answers.set('amigo', 'friend');
answers.set('bonita', 'beautiful');
answers.set('mulher', 'woman');

export default answers;
