import style from '../../../sass/style.sass';
import game_example from './game_example';

const fields = document.querySelectorAll('.form-field__wrapper input');

fields.forEach(input => {
  input.addEventListener('focus', (event) => {
    event.target.parentNode.classList.add('form-field__wrapper--active');
  });

  input.addEventListener('blur', (event) => {
    event.target.parentNode.classList.remove('form-field__wrapper--active');
  });
});
