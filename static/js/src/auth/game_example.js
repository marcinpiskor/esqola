import answers from './answers';

const words = document.querySelectorAll('.game-word');
const active_words = {
  english: null,
  portuguese: null
}

let start_date;

words.forEach(word => word.addEventListener('click', click_word));

function click_word(event) {
  // Unmark clicked answer if user has chosen already used word as an answer
  if (event.target.classList.contains('game-word--correct')) {
    Object.keys(active_words).forEach(key => {
      if (active_words[key]) {
        active_words[key].classList.remove('game-word--marked');
        active_words[key] = null;
      }
    });
  }
  else if (!event.target.classList.contains('game-word--marked')) {
    init_date();

    // Unmark word if user has chosen different word from the same language
    if (active_words[event.target.parentNode.id]) {
      active_words[event.target.parentNode.id].classList.remove(
        'game-word--marked'
      );
    }

    active_words[event.target.parentNode.id] = event.target;
    event.target.classList.add('game-word--marked');

    if (Object.values(active_words).every(value => value)) {
      check_words();
    }
  }
}

function init_date() {
  if (!start_date) {
    start_date = new Date();
  }
}

function check_words() {
  const portuguese_word = active_words.portuguese.innerHTML.toLowerCase();
  const correct_answer = answers.get(portuguese_word);
  const user_answer = active_words.english.innerHTML.toLowerCase();

  const correct = correct_answer == user_answer;

  Object.keys(active_words).forEach(key => {
    active_words[key].classList.remove('game-word--marked');

    if (correct) {
      active_words[key].classList.add('game-word--correct');
      answers.delete(portuguese_word);
    }

    active_words[key] = null;
  })

  show_success_block();
}

function show_success_block() {
  if (answers.size === 0) {
    const score_el = document.querySelector('#score');
    const time = `${parseFloat((new Date() - start_date)/1000).toFixed(3)}s`;
    score_el.innerHTML = time;
    score_el.parentNode.parentNode.classList.add(
      'game-example__success--visible'
    );
  }
}
