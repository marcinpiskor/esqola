import { createStore, combineReducers } from 'redux';
import root_reducer from '../reducers/index';

const store = createStore(combineReducers(root_reducer));
export default store;
