import React from 'react';
import PropTypes from 'prop-types';
import ContentTemplate from './components/layout/ContentTemplate';
import { connect } from 'react-redux';
import { get_current_user } from './actions/CurrentUserActionCreators.js';
import { withRouter } from 'react-router';

class App extends React.Component {
  componentWillMount() {
    // Fetching info about current user
    fetch('/api/current-user/', {
      method: 'GET',
      credentials: 'same-origin',
      headers: {'content-type': 'application/json'}
    })
    .then(response => response.json())
    .then(current_user => this.props.get_current_user(current_user[0]))
  }

  componentDidUpdate() {
    const { history, location, current_user } = this.props;
    const custom_path = '/customize';

    if (current_user.id) {
      // Redirect current user to customization page if current user account is
      // not activated
      if (
        !current_user.profile.is_activated
        && location.pathname !== custom_path
      ) {
        history.push(custom_path);
      }
      // Redirect current user to cockpit if current user tries to reach
      // customization page, but current user account is already activated
      else if (
        current_user.profile.is_activated
        && location.pathname === custom_path
      ) {
        history.push('/');
      }
    }
  }

  render() {
    return <ContentTemplate />;
  }
}

App.propTypes = {
  current_user: PropTypes.object.isRequired,
  get_current_user: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
};

const map_dispatch_to_props = {get_current_user};

export default withRouter(
  connect(map_state_to_props, map_dispatch_to_props)(App)
);
