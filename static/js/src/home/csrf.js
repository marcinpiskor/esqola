export default function get_csrf_token() {
  // Extract CSRF token from cookie
  const csrf = document.cookie.match(/csrftoken=[A-Za-z0-9]+/g)[0];
  return csrf.slice('csrftoken='.length, csrf.length);
}
