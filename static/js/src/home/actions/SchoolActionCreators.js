import {
  GET_SCHOOL,
  EDIT_SCHOOL,
  FINISH_EDIT_SCHOOL,
  CLEAR_SCHOOL,
  ORDER_CLASSES,
  ADD_CLASS_GROUP,
  UPDATE_CLASS_GROUP,
  SET_CLASS_GROUP_DETAIL_INFO,
  REMOVE_STUDENTS,
  DELETE_CLASS_GROUP,
  ADD_INVITATION,
  DELETE_INVITATION
} from './SchoolActionTypes';

export function get_school(school_data) {
  return {
    type: GET_SCHOOL,
    school_data
  };
}

export function edit_school() {
  return {type: EDIT_SCHOOL};
}

export function finish_edit_school() {
  return {type: FINISH_EDIT_SCHOOL};
}

export function clear_school() {
  return {type: CLEAR_SCHOOL};
}

export function order_classes(order_type, order_field) {
  return {
    type: ORDER_CLASSES,
    order_type,
    order_field
  };
}

export function add_class_group(class_group) {
  return {
    type: ADD_CLASS_GROUP,
    class_group
  }
}

export function update_class_group(index, class_group) {
  return {
    type: UPDATE_CLASS_GROUP,
    index,
    class_group
  }
}

export function set_class_group_detail_info(index, class_group) {
  return {
    type: SET_CLASS_GROUP_DETAIL_INFO,
    index,
    class_group
  }
}

export function remove_students(class_group_index, students) {
  return {
    type: REMOVE_STUDENTS,
    class_group_index,
    students
  }
}

export function delete_class_group(index) {
  return {
    type: DELETE_CLASS_GROUP,
    index
  }
}

export function add_invitation(class_group_index, invitation) {
  return {
    type: ADD_INVITATION,
    class_group_index,
    invitation
  }
}

export function delete_invitation(class_group_index, invitation_index) {
  return {
    type: DELETE_INVITATION,
    class_group_index,
    invitation_index
  }
}
