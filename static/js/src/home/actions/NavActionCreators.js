import { CHANGE_NAV_VISIBILITY } from "./NavActionTypes";

export function change_nav_visibility(visibility) {
  return {
    type: CHANGE_NAV_VISIBILITY,
    payload: visibility
  }
}
