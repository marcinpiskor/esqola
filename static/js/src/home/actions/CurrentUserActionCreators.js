import {
  GET_CURRENT_USER,
  ADD_STUDENT_CLASS_GROUP,
  SET_CURRENT_USER_PHOTO,
  SET_CURRENT_USER_SCHOOL
} from './CurrentUserActionTypes';

export function get_current_user(current_user_data) {
  return {
    type: GET_CURRENT_USER,
    current_user_data,
  };
}

export function set_current_user_photo(photo_data) {
  return {
    type: SET_CURRENT_USER_PHOTO,
    photo_data
  };
}

export function set_current_user_school(school) {
  return {
    type: SET_CURRENT_USER_SCHOOL,
    school
  };
}

export function add_class_group(class_group_obj) {
  return {
    type: ADD_STUDENT_CLASS_GROUP,
    class_group_obj
  };
}
