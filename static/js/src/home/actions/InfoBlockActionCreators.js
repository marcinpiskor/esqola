import {
  UPDATE_INFO_BLOCK,
  SHOW_INFO_BLOCK,
  HIDE_INFO_BLOCK,
  SUCCESS_CUSTOMIZATION,
  SHOW_ERROR_BLOCK
} from './InfoBlockActionTypes';

export function update_info_block(info_data) {
  return {
    type: UPDATE_INFO_BLOCK,
    info_data
  }
}

export function show_info_block() {
  return {
    type: SHOW_INFO_BLOCK
  }
}

export function hide_info_block() {
  return {
    type: HIDE_INFO_BLOCK
  }
}

export function success_customization() {
  return {
    type: SUCCESS_CUSTOMIZATION
  }
}

export function show_error_block(error_data) {
  return {
    type: SHOW_ERROR_BLOCK,
    error_data
  }
}
