import {
  LOAD_OLD_POST,
  INIT_POST_LOAD,
  CLEAR_DISCUSSION,
  ADD_NEW_POST,
  ADD_POST_TO_WAITING,
  LOAD_WAITING_POSTS,
  LOAD_POST_COMMENTS,
  UPDATE_POST,
  DELETE_POST,
  ADD_NEW_COMMENT,
  UPDATE_COMMENT,
  DELETE_COMMENT
} from "./DiscussionActionTypes";

export function init_post_load(post_data) {
  return {
    type: INIT_POST_LOAD,
    payload: post_data
  }
}

export function load_old_post(posts) {
  return {
    type: LOAD_OLD_POST,
    payload: posts
  }
}

export function clear_discussion(class_group_id) {
  return {
    type: CLEAR_DISCUSSION,
    payload: class_group_id
  }
}

export function add_new_post(new_post) {
  return {
    type: ADD_NEW_POST,
    payload: new_post
  }
}

export function add_post_to_waiting(waiting_post) {
  return {
    type: ADD_POST_TO_WAITING,
    payload: waiting_post
  }
}

export function load_waiting_posts() {
  return {type: LOAD_WAITING_POSTS};
}

export function load_post_comments(post_index, comments) {
  return {
    type: LOAD_POST_COMMENTS,
    payload: {post_index, comments}
  }
}

export function update_post(post_index, content) {
  return {
    type: UPDATE_POST,
    payload: {post_index, content}
  }
}

export function delete_post(post_index) {
  return {
    type: DELETE_POST,
    payload: post_index
  }
}

export function add_new_comment(post_index, new_comment) {
  return {
    type: ADD_NEW_COMMENT,
    payload: {post_index, new_comment}
  }
}

export function update_comment(post_index, comment_index, content) {
  return {
    type: UPDATE_COMMENT,
    payload: {post_index, comment_index, content}
  }
}

export function delete_comment(post_index, comment_index) {
  return {
    type: DELETE_COMMENT,
    payload: {post_index, comment_index}
  }
}
