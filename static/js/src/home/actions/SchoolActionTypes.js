export const GET_SCHOOL = 'GET_SCHOOL';
export const EDIT_SCHOOL = 'EDIT_SCHOOL';
export const FINISH_EDIT_SCHOOL = 'FINISH_EDIT_SCHOOL';
export const ORDER_CLASSES = 'ORDER_CLASSES';
export const ADD_CLASS_GROUP = 'ADD_CLASS_GROUP';
export const UPDATE_CLASS_GROUP = 'UPDATE_CLASS_GROUP';
export const SET_CLASS_GROUP_DETAIL_INFO = 'SET_CLASS_GROUP_DETAIL_INFO';
export const REMOVE_STUDENTS = 'REMOVE_STUDENTS';
export const DELETE_CLASS_GROUP = 'DELETE_CLASS_GROUP';
export const ADD_INVITATION = 'ADD_INVITATION';
export const DELETE_INVITATION = 'DELETE_INVITATION';
export const CLEAR_SCHOOL = 'CLEAR_SCHOOL';
