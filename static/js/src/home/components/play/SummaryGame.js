import React from 'react';
import PropTypes from 'prop-types';
import GameScore from './game/GameScore';
import GameHeader from './game/GameHeader';

export default class SummaryGame extends React.Component {
  componentWillMount() {
    this.props.game_socket.close();
  }

  render() {
    const { type_of_game, results, aborted } = this.props.game_data;
    const summary = results.concat(aborted);

    return (
      <article className="school-article school-article--center">
        <GameHeader type_of_game={type_of_game} />
        <div className="game-summary">
          <GameScore username="Username" score="Points" is_heading={true} />
          {summary.map(result => {
            return (
              <GameScore
                key={result.id}
                username={result.username}
                score={result.score}
                is_heading={false}
              />
            );
          })}
        </div>
      </article>
    )
  }
}

SummaryGame.propTypes = {
  game_data: PropTypes.object.isRequired,
  game_socket: PropTypes.instanceOf(WebSocket).isRequired
}
