import React from 'react';
import PropTypes from 'prop-types';
import PlayLobbyHeader from './PlayLobbyHeader';
import PlayLobbyPlayers from './player/PlayLobbyPlayers';

export default class PlayLobby extends React.Component {
  constructor() {
    super();
    this.start_game = this.start_game.bind(this);
  }

  start_game() {
    if (Object.keys(this.props.game_data.joined).length >= 2) {
      this.props.game_socket.send(JSON.stringify({message: 'start'}));
    }
  }

  render() {
    const { language, class_group, joined, type_of_game } = this.props.game_data;
    let lobby_info;

    if (!this.props.is_teacher) {
      lobby_info = (
        <h5 className="game-lobby-loading">
          <span className="game-lobby-loading__span">
            Waiting for start...
          </span>
        </h5>
      )
    }
    else {
      lobby_info = (
        <button
          className="index-forms__button index-forms__button--game-setup"
          onClick={this.start_game}
        >
          Start game session
        </button>
      )
    }

    return (
      <article className="school-article school-article--fade-in">
        <PlayLobbyHeader
          language={language}
          class_group_name={class_group.name}
          type_of_game={type_of_game}
        />
        <div className="game-lobby-content">
          {lobby_info}
          <PlayLobbyPlayers players={Object.values(joined)} />
        </div>
			</article>
    )
  }
}

PlayLobby.propTypes = {
  game_socket: PropTypes.instanceOf(WebSocket).isRequired,
  game_data: PropTypes.object.isRequired,
  is_teacher: PropTypes.bool.isRequired
}
