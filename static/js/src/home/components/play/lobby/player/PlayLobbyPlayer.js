import React from 'react';
import PropTypes from 'prop-types';

export default class PlayLobbyPlayer extends React.Component {
  render() {
    const { username } = this.props;

    return (
      <div className="game-lobby-players__player">
        <span className="game-lobby-players__player_at">@</span> {username}
      </div>
    )
  }
}

PlayLobbyPlayer.propTypes = {
  username: PropTypes.string.isRequired
}
