import React from 'react';
import PropTypes from 'prop-types';
import PlayLobbyPlayer from './PlayLobbyPlayer';

export default class PlayLobbyPlayers extends React.Component {
  render() {
    return (
      <div className="game-lobby-players">
        {this.props.players.map(player => {
          return <PlayLobbyPlayer key={player.id} username={player.username} />;
        })}
      </div>
    )
  }
}

PlayLobbyPlayers.propTypes = {
  players: PropTypes.arrayOf(PropTypes.object)
}
