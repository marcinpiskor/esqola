import React from 'react';
import PropTypes from 'prop-types';
import game_types from '../../game/type/game_types';

export default class PlayLobbyHeader extends React.Component {
  render() {
    const { name, flag } = this.props.language;
    const style = {backgroundImage: `url(${flag})`};

    const game_info = game_types.find(g => g.type === this.props.type_of_game);
    const { text, description } = game_info;

    return (
      <header className="game-lobby-header">
        <hgroup className="game-lobby-header__part game-lobby-header__info_wrapper">
          <div className="game-setup-flag game-setup-flag--lobby">
            <div
              className="game-setup-flag__img game-setup-flag__img--active"
              style={style}
            />
          </div>
          <div>
            <h2 className="game-lobby-header__language">{name}</h2>
            <h3 className="game-lobby-header__class_group">
              {this.props.class_group_name}
            </h3>
          </div>
        </hgroup>
        <hgroup className="game-lobby-header__part">
          <h2 className="game-lobby-header__game_name">{text}</h2>
          <p className="game-lobby-header__game_desc">{description}</p>
        </hgroup>
      </header>
    )
  }
}

PlayLobbyHeader.propTypes = {
  language: PropTypes.object.isRequired,
  type_of_game: PropTypes.string.isRequired,
  class_group_name: PropTypes.string.isRequired
}
