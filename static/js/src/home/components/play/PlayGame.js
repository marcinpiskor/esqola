import React from 'react';
import PropTypes from 'prop-types';
import PlayLobby from './lobby/PlayLobby';
import Game from './game/Game';
import TeacherGame from './TeacherGame';
import SummaryGame from './SummaryGame';
import { connect } from 'react-redux';
import { show_error_block } from '../../actions/InfoBlockActionCreators';

class PlayGame extends React.Component {
  constructor(props) {
    super(props);
    const { game_id } = props.match.params;

    this.state = {
      game_socket: new WebSocket(`ws://${window.location.host}/ws/game/${game_id}/`),
      game_data: null
    }

    this.state.game_socket.onmessage = (event) => {
      const {type, game_data} = JSON.parse(event.data);

      switch (type) {
        case 'game_error':
          props.show_error_block(game_data);
          props.history.push(`/`);
          break;

        case 'game_update':
          const updated_game_data = {};
          Object.assign(updated_game_data, this.state.game_data);
          Object.assign(updated_game_data, game_data);
          this.setState({game_data: updated_game_data});
          break;

        case 'game_message':
          const game_data_with_messages = {};
          Object.assign(game_data_with_messages, this.state.game_data);
          game_data_with_messages.messages.push(game_data);
          this.setState({game_data: game_data_with_messages});
          break;

        case 'finish':
          document.title = `Summary of game / ${game_data.class_group.name} - Esqola`;
          this.setState({game_data}, () => this.state.game_socket.close());
          break;

        default:
          const { language, type_of_game, class_group } = game_data;
          document.title = `${language.name} ${type_of_game} / ${class_group.name} - Esqola`;
          this.setState({game_data});
          break;
      }
    };
  }

  componentWillUnmount() {
    if (this.state.game_socket) {
      this.state.game_socket.close();
    }
  }

  render() {
    if (this.state.game_data) {
      const { game_data, game_socket } = this.state;
      const is_teacher = game_data.teacher === this.props.current_user.id;

      if (this.state.game_data.status === "start") {
        return (
          <PlayLobby
            is_teacher={is_teacher}
            game_data={game_data}
            game_socket={game_socket}
          />
        );
      }
      else if (this.state.game_data.status === "in_progress") {
        if (is_teacher) {
          return <TeacherGame />
        }
        else {
          return <Game game_socket={game_socket} game_data={game_data} />
        }
      }
      else if (this.state.game_data.status === "finish") {
        return <SummaryGame game_socket={game_socket} game_data={game_data} />
      }
    }
    else {
      return "";
    }
  }
}

PlayGame.propTypes = {
  current_user: PropTypes.object.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
}

const map_dispatch_to_props = {show_error_block};

export default connect(map_state_to_props, map_dispatch_to_props)(PlayGame);
