import React from 'react';

export default class PlayClassGroup extends React.Component {
  constructor() {
    super();
    this.state = {
      text: "Looking for active game session..."
    }
  }

  componentDidMount() {
    fetch("/api/game-session/active/", {
      method: 'GET',
      credentials: 'same-origin',
      headers: {'content-type': 'application/json'}
    })
    .then(response => response.json())
    .then(data => {
      if (data.length == 0) {
        this.setState({text: "Sorry, no active game session now."});
      }
      else {
        this.props.history.push(`/play/${data[0]}`);
      }
    })
  }

  render() {
    return (
      <article className="school-article">
        <div className="game-teacher">
          <div className="game-teacher__icon game-teacher__icon--play" />
          <h2 className="game-teacher__heading">{this.state.text}</h2>
        </div>
      </article>
    )
  }
}
