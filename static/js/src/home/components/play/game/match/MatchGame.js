import React from 'react';
import PropTypes from 'prop-types';
import GameHeader from '../GameHeader';
import MatchLanguage from './MatchLanguage';
import MatchFinished from './MatchFinished';

export default class MatchGame extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      foreign: null,
      english: null
    }

    this.set_word = this.set_word.bind(this);
  }

  set_word(language, word) {
    const other_lang = language === 'english' ? 'foreign' : 'english';
    const obj = {};

    if (!this.state[other_lang]) {
      obj[language] = word;
      this.setState(obj);
    }
    else {
      const value = {'word_1': word, 'word_2': this.state[other_lang]};
      this.props.game_socket.send(JSON.stringify(value));
      obj[other_lang] = null;
      this.setState(obj);
    }
  }

  render() {
    const {
      language, type_of_game, content, marked_words, started
    } = this.props.game_data;
    let finished_message;

    if (marked_words && content[0].length * 2 === marked_words.length) {
      finished_message = <MatchFinished started={started} />;
    }

    return (
      <article className="school-article school-article--center game-example game-example--normal">
        <GameHeader type_of_game={type_of_game} />
        <MatchLanguage
          active_word={this.state.foreign}
          words={content[0]}
          marked_words={marked_words}
          lang_name="foreign"
          lang_flag={language.flag}
          set_word={this.set_word}
        />
        <MatchLanguage
          active_word={this.state.english}
          words={content[1]}
          marked_words={marked_words}
          lang_name="english"
          lang_flag="/static/img/united-kingdom-flag.png"
          set_word={this.set_word}
        />
        {finished_message}
      </article>
    )
  }
}

MatchGame.propTypes = {
  game_socket: PropTypes.instanceOf(WebSocket).isRequired,
  game_data: PropTypes.object.isRequired
}
