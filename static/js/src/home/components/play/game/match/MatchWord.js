import React from 'react';
import PropTypes from 'prop-types';

export default class MatchWord extends React.Component {
  constructor() {
    super();
    this.click_word = this.click_word.bind(this);
  }

  click_word() {
    if (
      !this.props.marked_words
      || this.props.marked_words.indexOf(this.props.word.id) !== 0
     ) {
      this.props.set_word(this.props.lang_name, this.props.word.id);
    }
  }

  render() {
    let word_class = '';

    if (
      this.props.marked_words
      && this.props.marked_words.indexOf(this.props.word.id) !== -1
    ) {
      word_class = "game-word--correct";
    }
    else if (this.props.word.id === this.props.active_word) {
      word_class = "game-word--marked-normal";
    }

    return (
      <div
        className={`game-word game-word--normal no-select ${word_class}`}
        onClick={this.click_word}
      >
        {this.props.word.content}
      </div>
    )
  }
}

MatchWord.propTypes = {
  word: PropTypes.object.isRequired,
  lang_name: PropTypes.string.isRequired,
  marked_words: PropTypes.arrayOf(PropTypes.number),
}
