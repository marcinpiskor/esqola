import React from 'react';
import PropTypes from 'prop-types';
import MatchWord from './MatchWord';

export default class MatchLanguage extends React.Component {
  render() {
    return (
      <article className="game-example__language game-example__language--normal">
        <img className="game-setup-flag" src={this.props.lang_flag} />
        <div className="game-words">
          {this.props.words.map(word => {
            return (
              <MatchWord
                key={word.id}
                word={word}
                active_word={this.props.active_word}
                marked_words={this.props.marked_words}
                set_word={this.props.set_word}
                lang_name={this.props.lang_name}
              />
            );
          })}
        </div>
      </article>
    )
  }
}

MatchLanguage.propTypes = {
  lang_flag: PropTypes.string.isRequired,
  lang_name: PropTypes.string.isRequired,
  words: PropTypes.arrayOf(PropTypes.object).isRequired,
  set_word: PropTypes.func.isRequired,
  active_word: PropTypes.number,
  marked_words: PropTypes.arrayOf(PropTypes.number),
}
