import React from 'react';
import PropTypes from 'prop-types';

export default class MatchFinished extends React.Component {
  constructor(props) {
    super(props);
    const start_date = new Date(Date.parse(props.started));

    this.state = {
      duration: `${parseFloat((new Date() - start_date)/1000).toFixed(3)}s`
    }
  }

  render() {
    return (
      <article className="game-example__success game-example__success--visible">
        <h3 className="game-example__success--score">
          <i className="material-icons game-example__success--icon">timer</i> |
          <span>{this.state.duration}</span>
        </h3>
        <h4 className="game-example__success--info game-example__success--info-normal">
          Good job, keep it up!
        </h4>
        <h5 className="game-example__success--hint">
          Wait until the end of the game.
        </h5>
      </article>
    )
  }
}

MatchFinished.propTypes = {
  started: PropTypes.string.isRequired
}
