import React from 'react';
import PropTypes from 'prop-types';

export default class GameScore extends React.Component {
  render() {
    const base_score_class = 'game-summary__score game-summary__score--';
    let score_class, heading_class = '';

    if (this.props.is_heading) {
      score_class = `${base_score_class}heading-wrapper`;
      heading_class = 'game-summary__heading';
    }
    else {
      score_class = `${base_score_class}normal`;
    }

    return (
      <div className={score_class}>
        <div className={heading_class}>{this.props.username}</div>
        <div className={heading_class}>{this.props.score}</div>
      </div>
    )
  }
}

GameScore.propTypes = {
  is_heading: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
  score: PropTypes.oneOfType([
    PropTypes.number, PropTypes.string
  ]).isRequired
}
