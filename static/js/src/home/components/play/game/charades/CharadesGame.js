import React from 'react';
import PropTypes from 'prop-types';
import GameHeader from '../GameHeader';
import CharadesCanvasPainter from './canvas/CharadesCanvasPainter';
import CharadesCanvasPlayer from './canvas/CharadesCanvasPlayer';
import CharadesInfoPainter from './info/CharadesInfoPainter';
import CharadesInfoPlayer from './info/CharadesInfoPlayer';
import CharadesChat from './chat/CharadesChat';
import { connect } from 'react-redux';

class CharadesGame extends React.Component {
  render() {
    const { type_of_game, painter, messages } = this.props.game_data;
    const is_painter = painter == this.props.current_user.id;
    let charades_canvas, charades_info;

    if (is_painter) {
      const { content_translation } = this.props.game_data;
      charades_canvas = <CharadesCanvasPainter game_socket={this.props.game_socket} />;
      charades_info = <CharadesInfoPainter content={content_translation} />
    }
    else {
      const { language, image } = this.props.game_data;
      charades_canvas = <CharadesCanvasPlayer image={image} />;
      charades_info = <CharadesInfoPlayer language={language} />
    }

    return (
      <article className={`school-article school-article--center`}>
        <GameHeader type_of_game={type_of_game} />
        <div className="game-charades">
          {charades_canvas}
          <div className="game-charades__details">
            <div className="game-charades__wrapper">
              {charades_info}
              <CharadesChat
                messages={messages}
                is_painter={is_painter}
                game_socket={this.props.game_socket}
              />
            </div>
          </div>
        </div>
      </article>
    )
  }
}

CharadesGame.propTypes = {
  game_socket: PropTypes.instanceOf(WebSocket).isRequired,
  game_data: PropTypes.object.isRequired,
  current_user: PropTypes.object.isRequired,
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
}

export default connect(map_state_to_props)(CharadesGame);
