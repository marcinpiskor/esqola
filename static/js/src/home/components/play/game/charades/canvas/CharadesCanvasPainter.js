import React from 'react';
import PropTypes from 'prop-types';
import CharadesColor from './CharadesColor';

const colors = [
  'black', 'yellow', 'red', 'green', 'orange', 'white', 'brown', 'blue'
];

export default class CharadesCanvasPainter extends React.Component {
  constructor() {
    super();
    this.state = {
      canvas_data: {
        lineWidth: 12,
        strokeStyle: colors[0],
        lineCap: 'round'
      },
      drawing_mode: false,
      previous_move: {}
    }

    this.change_color = this.change_color.bind(this);
    this.start_drawing = this.start_drawing.bind(this);
    this.start_touch_drawing = this.start_touch_drawing.bind(this);
    this.draw = this.draw.bind(this);
    this.end_drawing = this.end_drawing.bind(this);
  }

  change_color(color) {
    this.state.canvas_data.strokeStyle = color;
    this.setState({canvas_data: this.state.canvas_data});
  }

  start_drawing(event) {
    const previous_move = {
      x: event.nativeEvent.layerX,
      y: event.nativeEvent.layerY
    }

    const context = event.target.getContext('2d');
    Object.keys(this.state.canvas_data).forEach(key => {
      context[key] = this.state.canvas_data[key];
    });

    this.setState({drawing_mode: true, previous_move});
  }

  start_touch_drawing(event) {
    this.setState({
      drawing_mode: true,
      previous_move: this.get_touch_move(event)
    });
  }

  draw(event) {
    if (this.state.drawing_mode) {
      const context = event.target.getContext('2d');
      let new_move;

      if (event.nativeEvent.layerX) {
        new_move = {
          x: event.nativeEvent.layerX,
          y: event.nativeEvent.layerY
        };
      }
      else {
        new_move = this.get_touch_move(event);
      }

      context.beginPath();
      context.moveTo(this.state.previous_move.x, this.state.previous_move.y);
      context.lineTo(new_move.x, new_move.y);
      context.stroke();
      context.closePath();

      this.setState({previous_move: new_move});
    }
  }

  end_drawing(event) {
    if (this.state.drawing_mode) {
      this.props.game_socket.send(
        JSON.stringify({image: event.target.toDataURL()})
      );
    }

    this.setState({drawing_mode: false});
  }

  get_touch_move(event) {
    const rect = event.target.getBoundingClientRect();

    return {
      x: event.nativeEvent.touches[0].clientX - rect.x,
      y: event.nativeEvent.touches[0].clientY - rect.y
    };
  }

  render() {
    return (
      <div className="game-charades__content">
        <div className="game-charades__colors">
          {colors.map(color => {
            return (
              <CharadesColor
                key={color}
                color={color}
                change_color={this.change_color}
              />
            );
          })}
        </div>
        <canvas
          width="640"
          height="320"
          className="game-charades__canvas"
          onMouseDown={this.start_drawing}
          onMouseMove={this.draw}
          onMouseUp={this.end_drawing}
          onMouseOut={this.end_drawing}
          onTouchStart={this.start_touch_drawing}
          onTouchMove={this.draw}
          onTouchEnd={this.end_drawing}
          onTouchCancel={this.end_drawing}
        />
      </div>
    )
  }
}

CharadesCanvasPainter.propTypes = {
  game_socket: PropTypes.instanceOf(WebSocket).isRequired
}
