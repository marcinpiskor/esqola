import React from 'react';
import PropTypes from 'prop-types';

export default class CharadesInfoPlayer extends React.Component {
  render() {
    const { name, flag } = this.props.language;
    const info_style = {backgroundImage: `url(${flag})`};

    return (
      <div
        className="game-charades__info game-charades__info--player"
        style={info_style}
      >
        {name}
      </div>
    )
  }
}

CharadesInfoPlayer.propTypes = {
  language: PropTypes.object.isRequired
}
