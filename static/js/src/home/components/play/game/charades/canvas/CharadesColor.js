import React from 'react';
import PropTypes from 'prop-types';

export default class CharadesColor extends React.Component {
  constructor() {
    super();
    this.update_color = this.update_color.bind(this)
  }

  update_color() {
    this.props.change_color(this.props.color);
  }

  render() {
    const color_style = {backgroundColor: this.props.color};

    return (
      <div
        className="school-principal__avatar school-principal__avatar--color"
        style={color_style}
        onClick={this.update_color}
      />
    )
  }
}

CharadesColor.propTypes = {
  color: PropTypes.string.isRequired
}
