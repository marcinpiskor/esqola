import React from 'react';
import PropTypes from 'prop-types';

export default class CharadesMessage extends React.Component {
  render() {
    return (
      <div className="user-message user-message--charades">
        <div className="user-message__content">{this.props.message}</div>
      </div>
    )
  }
}

CharadesMessage.propTypes = {
  message: PropTypes.string
}
