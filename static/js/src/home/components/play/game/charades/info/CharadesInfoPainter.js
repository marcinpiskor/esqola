import React from 'react';
import PropTypes from 'prop-types';

export default class CharadesInfoPainter extends React.Component {
  render() {
    return (
      <div className="game-charades__info game-charades__info--painter">
        Draw <i>{this.props.content}</i>
      </div>
    )
  }
}

CharadesInfoPainter.propTypes = {
  content: PropTypes.string.isRequired
}
