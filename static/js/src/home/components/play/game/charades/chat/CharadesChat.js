import React from 'react';
import PropTypes from 'prop-types';
import CharadesMessage from './CharadesMessage';
import CharadesChatForm from './CharadesChatForm';

export default class CharadesChat extends React.Component {
  render() {
    let charades_form;

    if (!this.props.is_painter) {
      charades_form = <CharadesChatForm game_socket={this.props.game_socket} />
    }

    return (
      <div className="user-messages user-messages--charades">
        <div className="user-messages__box">
          {this.props.messages.map((msg, i) => {
            return <CharadesMessage key={i} message={msg.content}/>;
          })}
        </div>
        {charades_form}
      </div>
    )
  }
}

CharadesChat.propTypes = {
  is_painter: PropTypes.bool.isRequired,
  game_socket: PropTypes.instanceOf(WebSocket).isRequired,
  message: PropTypes.arrayOf(PropTypes.string)
}
