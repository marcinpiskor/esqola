import React from 'react';
import PropTypes from 'prop-types';

export default class CharadesCanvasPlayer extends React.Component {
  componentDidUpdate() {
    const image = new Image();

    image.onload = () => {
      this.canvas.getContext('2d').drawImage(image, 0, 0);
    }

    image.src = this.props.image;
  }

  render() {
    return (
      <div className="game-charades__content">
        <canvas
          width="640"
          height="320"
          className="game-charades__canvas"
          ref={(el) => this.canvas = el}
        />
      </div>
    )
  }
}

CharadesCanvasPlayer.propTypes = {
  image: PropTypes.string.isRequired
}
