import React from 'react';
import PropTypes from 'prop-types';

export default class CharadesChatForm extends React.Component {
  constructor() {
    super();
    this.send_answer = this.send_answer.bind(this);
  }

  send_answer(event) {
    event.preventDefault();
    const form = new FormData(event.target);

    if (form.get('answer').trim().length > 0) {
      this.props.game_socket.send(
        JSON.stringify({answer: form.get('answer').trim()})
      );
      event.target.reset();
    }
  }

  render() {
    return (
      <form
        className="user-messages__form user-messages__form--charades"
        onSubmit={this.send_answer}
      >
        <textarea name="answer" className="user-messages__input" required/>
        <button className="user-messages__button">
          <i className="material-icons">send</i>
        </button>
      </form>
    )
  }
}

CharadesChatForm.propTypes = {
  game_socket: PropTypes.instanceOf(WebSocket).isRequired,
}
