import React from 'react';
import PropTypes from 'prop-types';
import MatchGame from './match/MatchGame';
import ChooseGame from './choose/ChooseGame';
import EnterGame from './enter/EnterGame';
import CharadesGame from './charades/CharadesGame';

const game_components = {
  match: MatchGame,
  choose: ChooseGame,
  enter: EnterGame,
  charades: CharadesGame
};

export default class Game extends React.Component {
  render() {
    const { game_socket, game_data } = this.props;

    return React.createElement(
        game_components[game_data.type_of_game], {game_socket, game_data}
    );
  }
}

Game.propTypes = {
  game_socket: PropTypes.instanceOf(WebSocket).isRequired,
  game_data: PropTypes.object.isRequired
}
