import React from 'react';
import PropTypes from 'prop-types';
import GameHeader from '../GameHeader';

export default class EnterGame extends React.Component {
  constructor() {
    super();
    this.send_answer = this.send_answer.bind(this);
  }

  componentDidUpdate() {
    if (this.enter_input) {
      this.enter_input.focus();
    }
  }

  send_answer(event) {
    event.preventDefault();
    const form = new FormData(event.target);

    if (form.get('answer').trim().length > 0) {
      this.props.game_socket.send(
        JSON.stringify({'answer': form.get('answer').trim()})
      );
      event.target.reset();
      event.target.focus();
    }
  }

  render() {
    const { language, type_of_game, current_question } = this.props.game_data;
    const avatar_style = {backgroundImage: `url(${language.flag})`};
    let answer_input;

    if (current_question.id === 0) {
      answer_input = (
        <textarea
          name="answer"
          className="game-enter__input"
          placeholder="Type the answer here"
          disabled
        />
      );
    }
    else {
      answer_input = (
        <textarea
          name="answer"
          className="game-enter__input"
          placeholder="Type the answer here"
          autoFocus
          ref={el => this.enter_input = el}
        />
      );
    }

    return (
      <article className="school-article school-article--center">
        <GameHeader type_of_game={type_of_game} />
        <div className="game-enter__question">
          <div
            className="school-principal__avatar school-principal__avatar--flag"
            style={avatar_style}
          />
          <h4 className="game-enter__text">{current_question.content}</h4>
        </div>
        <form onSubmit={this.send_answer}>
          {answer_input}
          <button className="index-forms__button index-forms__button--center">
            Check answer
          </button>
        </form>
      </article>
    )
  }
}

EnterGame.propTypes = {
  game_socket: PropTypes.instanceOf(WebSocket).isRequired,
  game_data: PropTypes.object.isRequired
}
