import React from 'react';
import PropTypes from 'prop-types';
import game_types from '../../game/type/game_types';

export default class GameHeader extends React.Component {
  render() {
    const { text, description } = game_types.find(game => {
      return this.props.type_of_game === game.type;
    });

    const base_class = 'game-example__heading';
    const primary_class = `${base_class} ${base_class}--primary ${base_class}--game-name`;
    const secondary_class = `${base_class} ${base_class}--secondary ${base_class}--game-desc`;

    return (
      <header className="game-example__group">
        <h2 className={primary_class}>{text}</h2>
        <h3 className={secondary_class}>{description}</h3>
      </header>
    )
  }
}

GameHeader.propTypes = {
  type_of_game: PropTypes.string.isRequired
}
