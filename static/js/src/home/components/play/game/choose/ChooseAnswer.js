import React from 'react';
import PropTypes from 'prop-types';

export default class ChooseAnswer extends React.Component {
  constructor() {
    super();
    this.choose_answer = this.choose_answer.bind(this);
  }

  choose_answer() {
    this.props.set_answer(this.props.answer.id);
  }

  render() {
    const base_class = 'game-choose__answer';
    let choose_answer_class = `no-select ${base_class}`

    if (this.props.clicked_answer == this.props.answer.id) {
      choose_answer_class = `${choose_answer_class} ${base_class}--active`;
    }

    return (
      <div className={choose_answer_class} onClick={this.choose_answer}>
        {this.props.answer.content}
      </div>
    )
  }
}

ChooseAnswer.propTypes = {
  set_answer: PropTypes.func.isRequired,
  answer: PropTypes.object.isRequired
}
