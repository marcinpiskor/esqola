import React from 'react';
import PropTypes from 'prop-types';
import GameHeader from '../GameHeader';
import ChooseAnswer from './ChooseAnswer';

export default class ChooseGame extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      current_question_id: props.game_data.current_question.id,
      clicked_answer: null
    }

    this.set_answer = this.set_answer.bind(this);
  }

  componentDidUpdate() {
    if (this.props.game_data.current_question.id !== this.state.current_question_id) {
      this.setState({
        current_question_id: this.props.game_data.current_question.id,
        clicked_answer: null
      })
    }
  }

  set_answer(answer_id) {
    if (!this.state.clicked_answer) {
      this.setState({clicked_answer: answer_id});
      this.props.game_socket.send(JSON.stringify({answer: answer_id}));
    }
  }

  render() {
    const {
      language, type_of_game, current_question, answered
    } = this.props.game_data;
    let loading_animation;

    if (answered === 0) {
      loading_animation = (
        <div className="game-choose__waiting">
          <i className="material-icons game-choose__question_load">
            help_outline
          </i>
        </div>
      )
    }
    else {
      loading_animation = (
        <div className="game-choose__waiting">
          <i className="material-icons game-choose__dot">brightness_1</i>
          <i className="material-icons game-choose__dot">brightness_1</i>
          <i className="material-icons game-choose__dot">brightness_1</i>
        </div>
      )
    }

    return (
      <article className={`school-article school-article--center`}>
        <GameHeader type_of_game={type_of_game} />
        <h4 className="game-choose__question">
          <span className="game-choose__question_span">
            {current_question.content}
          </span>
        </h4>
        {loading_animation}

        <div className="game-choose__answers">
          {current_question.answers.map(answer => {
            return (
              <ChooseAnswer
                answer={answer}
                key={answer.id}
                set_answer={this.set_answer}
                clicked_answer={this.state.clicked_answer}
              />
            );
          })}
        </div>
      </article>
    )
  }
}

ChooseGame.propTypes = {
  game_socket: PropTypes.instanceOf(WebSocket).isRequired,
  game_data: PropTypes.object.isRequired
}
