import React from 'react';

export default class CustomizeHeader extends React.Component {
  render() {
    return (
      <header className="customize-section__header">
        <hgroup className="rules__header rules__header--customize">
          <h2 className="rules__heading rules__heading--customize">
            Before we begin
          </h2>
        </hgroup>
        <hgroup className="rules__header rules__header--customize rules__header--gray">
          <h2 className="rules__heading rules__heading--customize rules__heading--gray">
            configure your profile.
          </h2>
        </hgroup>
      </header>
    )
  }
}
