export function pick_file() {
  this.input_file.click();
}

export function drag_file(event) {
  event.preventDefault();
}

export function prepare_image_preview(image_file, prop_name) {
  const canvas = document.createElement('canvas');
  const img = new Image();

  img.onload = () => {
    canvas.width = img.naturalWidth;
    canvas.height = img.naturalHeight;

    canvas.getContext('2d').drawImage(img, 0, 0);

    const obj = {};
    obj[prop_name] = canvas.toDataURL();

    this.setState(obj);
  }

  img.onerror = () => {}

  img.src = URL.createObjectURL(image_file);
}
