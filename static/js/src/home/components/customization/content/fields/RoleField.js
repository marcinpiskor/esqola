import React from 'react';

export default class RoleField extends React.Component {
  constructor() {
    super();
    this.state = {
      role_name: '',
      roles: []
    };

    this.set_role_name = this.set_role_name.bind(this);
  }

  componentWillMount() {
    // Fetching info about Esqola groups (roles)
    fetch('/api/group/', {
      method: 'GET',
      credentials: 'same-origin',
      headers: {'content-type': 'application/json'}
    })
    .then(response => response.json())
    .then(roles => this.setState({roles}))
  }

  set_role_name(event) {
    const role_name = `${event.target.options[event.target.selectedIndex].text}s`;
    this.setState({role_name});
  }

  render() {
    return (
      <div>
        <label htmlFor="profile.role.id" className="customize-option-label">
          Who are you?
        </label>
        <span className="role-choose-span">I am</span>
        <select
          className="custom-select custom-select--role"
          name="profile.role.id"
          onChange={this.set_role_name}
          required
        >
          <option value="">---</option>
          {this.state.roles.map(role => {
            const name = role.name.slice(0, role.name.length - 1);
            return <option key={role.id} value={role.id}>{name}</option>;
          })}
        </select>
        <input
          type="hidden"
          name="profile.role.name"
          value={this.state.role_name}
        />
      </div>
    )
  }
}
