import React from 'react';
import PropTypes from 'prop-types';
import { pick_file, drag_file } from "./photo_functions";
import { connect } from 'react-redux';
import {
  set_current_user_photo
} from '../../../../actions/CurrentUserActionCreators';

class PhotoField extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      label: props.current_value ? 'Photo' : 'How do you look like?',
      extensions: '.png, .jpg, .jpeg'
    };

    this.pick_file = pick_file.bind(this);
    this.drop_file = this.drop_file.bind(this);

    this.set_attached = this.set_attached.bind(this);
    this.delete_photo = this.delete_photo.bind(this);
  }

  pick_file() {
    this.input_file.click();
  }

  drag_file(event) {
    event.preventDefault();
  }

  drop_file(event) {
    event.preventDefault();
    const valid_ext = this.state.extensions.split(', ').map(ext => {
      return ext.slice(1, ext.length);
    });

    for (let i=0; i < event.dataTransfer.items.length; i++) {
      const item = event.dataTransfer.items[i];

      // If dropped element is file and has image extension, set it as profile
      // photo
      if (
        item.kind === 'file'
        && valid_ext.indexOf(item.type.split('/')[1]) !== -1
      ) {
        this.props.set_current_user_photo(item.getAsFile());
        break;
      }
    }
  }

  set_attached(event) {
    this.props.set_current_user_photo(event.target.files[0])
  }

  delete_photo() {
    this.props.set_current_user_photo(null);
  }

  render() {
    const { photo } = this.props.current_user.profile;
    const photo_text = (
      photo ? 'Photo has been attached' : 'Click here or drop image here'
    );

    return (
      <div>
        <label htmlFor="profile.photo" className="customize-option-label">
          {this.state.label}
        </label>
        <div className="photo-wrapper">
          <div
            className="photo-block"
            onClick={this.pick_file}
            onDragOver={drag_file}
            onDrop={this.drop_file}
          >
            <i className="material-icons photo-block__icon">add_a_photo</i>
            <span>{photo_text}</span>
          </div>
          {photo && (
            <button className="photo-delete" onClick={this.delete_photo}>
              <i className="material-icons">delete_outline</i>
            </button>
          )}
        </div>

        <input
          type="file"
          className="file-hidden"
          name="photo_file"
          accept={this.state.extensions}
          onChange={this.set_attached}
          ref={el => this.input_file = el}
        />
      </div>
    )
  }
}

PhotoField.propTypes = {
  current_user: PropTypes.object.isRequired,
  set_current_user_photo: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
};

const map_dispatch_to_props = {set_current_user_photo};

export default connect(map_state_to_props, map_dispatch_to_props)(PhotoField);
