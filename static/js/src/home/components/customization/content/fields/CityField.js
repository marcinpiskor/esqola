import React from 'react';
import PropTypes from 'prop-types';
import UnderlineInput from '../../../UnderlineInput';

export default class CityField extends React.Component {
  constructor(props) {
    super(props);
    const { current_value } = props;

    // Fill city data with current data (if city field is used in settings) mode
    // or fill city data with empty strings
    if (current_value) {
      const { place_id, name, region, country, country_short } = current_value;

      this.state = {
        place_id,
        name,
        country_short,
        autocomplete_value: [name, region, country].filter(x => x).join(', '),
        label: 'City',
        autocomplete: null
      };
    }
    else {
      this.state = {
        place_id: '',
        name: '',
        country_short: '',
        autocomplete_value: '',
        label: 'Where do you live?',
        autocomplete: null
      }
    }

    this.handle_autocomplete_change = this.handle_autocomplete_change.bind(this);
  }

  componentDidMount() {
    const options = {types: ['(cities)']};
    const autocomplete = new google.maps.places.Autocomplete(
      this.city_input, options
    );
    this.setState({autocomplete});

    // Prepare autocomplete value text after choosing city (place) by user
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const city_obj = autocomplete.getPlace();
      const country_short = city_obj.address_components.find(component => {
        return component.types.indexOf('country') !== -1;
      }).short_name;

      const get_city_prop = (prop) => {
        const value = city_obj.address_components.find(comp => {
          return comp.types.indexOf(prop) !== -1;
        });

        return value ? value.long_name : "";
      }

      const autocomplete_value = [
        get_city_prop("locality"),
        get_city_prop("administrative_area_level_1"),
        get_city_prop("country")
      ].filter(prop => prop).join(", ");

      this.setState({
        country_short,
        autocomplete_value,
        place_id: city_obj.place_id,
        name: city_obj.name,
      });
    })
  }

  componentWillUnmount() {
    if (this.state.autocomplete) {
      google.maps.event.clearListeners(this.state.autocomplete);
    }
  }

  handle_autocomplete_change(event) {
    this.setState({autocomplete_value: event.target.value})
  }

  render() {
    return (
      <div>
        <label htmlFor="autocomplete" className="customize-option-label">
          {this.state.label}
        </label>
        <UnderlineInput additional_classes={"form-field__wrapper--full"}>
          <input
            className="form-field"
            name="autocomplete"
            type="text"
            ref={input => this.city_input = input}
            value={this.state.autocomplete_value}
            onChange={this.handle_autocomplete_change}
          />
        </UnderlineInput>
        <input
          type="hidden"
          name="profile.city.place_id"
          value={this.state.place_id}
        />
        <input
          type="hidden"
          name="profile.city.name"
          value={this.state.name}
        />
        <input
          type="hidden"
          name="profile.city.country_short"
          value={this.state.country_short}
        />
      </div>
    )
  }
}

CityField.propTypes = {
  current_value: PropTypes.object
}
