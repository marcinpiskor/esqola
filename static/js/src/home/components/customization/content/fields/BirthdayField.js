import React from 'react';
import PropTypes from 'prop-types';

export default class BirthdayField extends React.Component {
  constructor(props) {
    super(props);

    const { current_value } = props;
    const select = {
      day: this.generate_days(31),
      month: this.generate_months(),
      year: this.generate_years()
    };

    // Set birthday of current user by default or fill birthday data with empty
    // strings
    if (current_value) {
      const birthday = new Date(props.current_value);

      this.state = {
        select,
        day: birthday.getDate(),
        month: birthday.getMonth() + 1,
        year: birthday.getFullYear(),
        label: 'Birthday'
      };
    }
    else {
      this.state = {
        select,
        day: '',
        month: '',
        year: '',
        label: 'What is your birthday?'
      }
    }

    this.check_amount_of_days = this.check_amount_of_days.bind(this)
  }

  generate_days(max_days) {
    const days = [];

    for (let i=1; i<=max_days; i++) {
      days.push(i);
    }

    return days;
  }

  generate_months() {
    const months = [];

    for (let i=1; i<=12; i++) {
      months.push(i);
    }

    return months;
  }

  generate_years() {
    const last_year = new Date().getFullYear() - 1;
    const years = [];

    for (let i=last_year; i>=last_year-100; i--) {
      years.push(i);
    }

    return years;
  }

  check_amount_of_days(event) {
    // Invoke function that checks if amount of days in month is different than
    // current after change value of month/year field
    if (event.target.name !== 'day') {
      const update_state_data = {};
      update_state_data[event.target.name] = parseInt(event.target.value);
      this.setState(update_state_data, this.change_amount_of_days);
    }
    else {
      this.setState({day: parseInt(event.target.value)})
    }
  }

  change_amount_of_days() {
    if (this.state.year && this.state.month) {
      const amount_of_days = new Date(this.state.year, this.state.month, 0).getDate();
      const { select } = this.state;

      // Change amount of days and day value if current day value is bigger than
      // amount of days in month
      if (amount_of_days !== select.day.length) {
        const day = (
          this.state.day > amount_of_days ? amount_of_days : this.state.day
        );

        select.day = this.generate_days(amount_of_days);
        this.setState({day, select});
      }
    }
  }

  render() {
    // Rendering day, month and year select fields
    const select_keys = Object.keys(this.state.select);
    const date_fields = select_keys.map(select_name => {
      const option_name = `${select_name.toUpperCase().charAt(0)}${select_name.slice(1, select_name.length)}`
      const select_field = this.state.select[select_name];

      return (
        <select
          key={select_name}
          name={select_name}
          value={this.state[select_name]}
          className="custom-select custom-select--birthday"
          onChange={this.check_amount_of_days}
          required
        >
          <option value="">{option_name}</option>
          {select_field.map(option => {
            const name = String(option).length === 1 ? `0${option}` : option;

            return (
              <option key={`${option_name}.${option}`} value={option}>
                {name}
              </option>
            );
          })}
        </select>
      );
    });

    return (
      <div>
        <label htmlFor={select_keys[0]} className="customize-option-label">
          {this.state.label}
        </label>
        {date_fields}
      </div>
    )
  }
}

BirthdayField.propTypes = {
  current_value: PropTypes.string
}
