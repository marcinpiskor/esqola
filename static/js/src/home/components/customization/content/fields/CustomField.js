import React from 'react';
import PropTypes from 'prop-types';

export default class CustomField extends React.Component {
  render() {
    let base_class = 'customize-option';
    let custom_field_class = `${base_class} ${base_class}--${this.props.name}`;

    if (this.props.step > 1) {
      custom_field_class = `${custom_field_class} ${base_class}--roll-up`;
    }

    return (
      <div className={custom_field_class}>
        {React.cloneElement(this.props.children)}
      </div>
    )
  }
}

CustomField.propTypes = {
  step: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
}
