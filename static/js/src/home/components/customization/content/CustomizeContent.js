import React from 'react';
import PropTypes from 'prop-types';
import UserCard from './preview/UserCard';
import CustomField from './fields/CustomField';
import CityField from './fields/CityField';
import BirthdayField from './fields/BirthdayField';
import RoleField from './fields/RoleField';
import PhotoField from './fields/PhotoField';
import get_csrf_token from '../../../csrf';
import { connect } from 'react-redux';
import { get_current_user } from '../../../actions/CurrentUserActionCreators';
import {
  show_error_block,
  success_customization
} from '../../../actions/InfoBlockActionCreators';

const components = {
  city: CityField,
  birthday: BirthdayField,
  role: RoleField,
  photo: PhotoField
}

class CustomizeContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      fields: [this.create_customize_field(Object.keys(components)[0], 1)]
    }

    this.send_customize_form = this.send_customize_form.bind(this);
  }

  send_customize_form(event) {
    event.preventDefault();
    const form_data = new FormData(event.target);

    if (form_data.get('profile.city.place_id')) {
      const required_field_filled = this.prepare_form_data(form_data);

      // Update user card preview content and display another field to fill if
      // customization process has not ended yet
      if (this.state.step !== Object.keys(components).length) {
        this.update_user_card_preview(form_data);
        this.next_step();
      }
      // Otherwise, remove unnecessary fields from form (city name, role name)
      // and send customization form
      else if (required_field_filled) {
        this.clear_customize_form(form_data)
        this.customize_user_data(form_data);
      }
    }
  }

  prepare_form_data(form_data) {
    const { photo } = this.props.current_user.profile;

    if (photo && photo instanceof File) {
      form_data.set('profile.photo', photo);
    }

    // Check if all birthday fields has been filled
    const birthday_fields = ['year', 'month', 'day'];
    const all_birthday_fields_filled = birthday_fields.every(field => {
      return form_data.get(field)
    });

    if (all_birthday_fields_filled) {
      const format = (x) => String(x).length === 1 ? `0${x}`: x;
      const month = format(form_data.get('month'));
      const day = format(form_data.get('day'));

      const birthday_value = `${form_data.get('year')}-${month}-${day}`;
      form_data.set('profile.birthday', birthday_value);

      // If profile role id field is not null that means that all required
      // fields
      return form_data.get('profile.role.id');
    }
    else {
      return false;
    }
  }

  update_user_card_preview(form_data) {
    const undesired_fields = [
      'autocomplete', 'photo_file', 'year', 'month', 'day'
    ];
    const update_data = {profile: {}};

    // Converts form data to object. It is necessary to update user card info.
    for (let key of form_data.keys()) {
      if (undesired_fields.indexOf(key) === -1) {
        const fields = key.split('.');
        const value = form_data.get(key);
        this.handle_nested_fields(fields, value, update_data);
      }
    }

    this.props.get_current_user(update_data);
  }

  handle_nested_fields(fields, value, update_data) {
    let current_data_obj = update_data;

    fields.forEach((field, i) => {
      if (!current_data_obj[field]) {
        // Set field value if field is not an object
        if (i == fields.length - 1) {
          current_data_obj[field] = value;
        }
        // Otherwise, set field value to new object
        else {
          current_data_obj[field] = {};
        }
      }

      current_data_obj = current_data_obj[field];
    })

    return update_data;
  }

  next_step() {
    const fields = [...this.state.fields];
    const step = this.state.step + 1;

    fields.push(
      this.create_customize_field(Object.keys(components)[this.state.step], step)
    );

    this.setState({fields, step});
  }

  clear_customize_form(form_data) {
    const fields_to_remove = [
      'autocomplete', 'profile.city.name', 'profile.city.country_short',
      'profile.role.name', 'photo_file', 'day', 'month', 'year'
    ];

    // Remove fields from form data that were only for user card preview purposes
    fields_to_remove.forEach(field => form_data.delete(field));
  }

  customize_user_data(form_data) {
    let success;

    fetch(`/api/current-user/${this.props.current_user.id}/`, {
      method: 'PATCH',
      body: form_data,
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(data => {
      if (success) {
        this.props.get_current_user(data);
        window.scroll(0, 0)

        this.props.success_customization();
      }
      else {
        this.props.show_error_block(data);
      }
    })
  }

  create_customize_field(component_name, step) {
    return (
      <CustomField step={step} key={component_name} name={component_name}>
        {React.createElement(components[component_name])}
      </CustomField>
    );
  }

  render() {
    const button_text = (
      this.state.step === Object.keys(components).length ? 'Save' : 'Proceed'
    );

    return (
      <article className="customize-article">
        <form
          className="customize-article__form"
          onSubmit={this.send_customize_form}
        >
          <div className="customize-article__card">
            <div className="customize-fields">
              {this.state.fields.map(field => field)}
            </div>
          </div>
          <button className="index-forms__button">
            {button_text}
          </button>
        </form>
        <div></div>
        {this.props.current_user.id &&
          <UserCard
            user={this.props.current_user}
            is_current_user={true}
            additional_classes={"customize-user-card--custom"}
          />
        }
      </article>
    )
  }
}

CustomizeContent.propTypes = {
  current_user: PropTypes.object.isRequired,
  get_current_user: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired,
  success_customization: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
};

const map_dispatch_to_props = {
  get_current_user,
  show_error_block,
  success_customization
};

export default connect(map_state_to_props, map_dispatch_to_props)(CustomizeContent);
