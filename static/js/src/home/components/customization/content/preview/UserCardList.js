import React from 'react';
import PropTypes from 'prop-types';

export default class UserCardList extends React.Component {
  render() {
    const { first_name, last_name, profile } = this.props.user;
    const { points, city, birthday } = profile;

    const list_data = [
      {icon: 'face', text: `${first_name} ${last_name}`},
      {icon: 'star', text: `${profile.points || 0} points`}
    ];

    if (city && city.name) {
      list_data.push(
        {icon: 'location_on', text: `${city.name}, ${city.country_short}`}
      );
    }

    if (birthday) {
      const age = new Date() - new Date(birthday);

      list_data.push({
          icon: 'date_range',
          text: `${parseInt(age/1000/60/60/24/365)} y.o`
      });
    }

    const icon_class = 'material-icons customize-user-card__mark customize-user-card__icon';
    const list_el_class = this.props.is_current_user ? 'customize-user-card__list_el' : '';

    return (
      <ul className="customize-user-card__list">
        {list_data.map(el => {
          return (
            <li key={el.icon} className={list_el_class}>
              <i className={icon_class}>{el.icon}</i> <span>{el.text}</span>
            </li>
          );
        })}
      </ul>
    )
  }
}

UserCardList.propTypes = {
  user: PropTypes.object.isRequired,
  is_current_user: PropTypes.bool.isRequired
}
