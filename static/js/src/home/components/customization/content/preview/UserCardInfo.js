import React from 'react';
import PropTypes from 'prop-types';
import UserCardList from './UserCardList';

export default class UserCardInfo extends React.Component {
  render() {
    const { profile, username } = this.props.user;
    let profile_role;

    if (profile.role) {
      const profile_role_name = profile.role.name.slice(
        0, profile.role.name.length - 1
      );
      let custom_role_class = 'customize-user-card__role';

      if (this.props.is_current_user) {
        custom_role_class = `${custom_role_class} customize-user-card__list_el`;
      }

      profile_role = (
        <h4 className={custom_role_class}>
          {profile_role_name}
        </h4>
      )
    }

    return (
      <div className="customize-user-card__info">
        <h3 className="customize-user-card__nickname">
          <span className="customize-user-card__mark">@</span>{username}
        </h3>
        {profile_role}
        <UserCardList
          user={this.props.user}
          is_current_user={this.props.is_current_user}
        />
      </div>
    )
  }
}

UserCardInfo.propTypes = {
  user: PropTypes.object.isRequired,
  is_current_user: PropTypes.bool.isRequired
}
