import React from 'react';
import PropTypes from 'prop-types';
import UserCardInfo from './UserCardInfo';
import { prepare_image_preview } from "../fields/photo_functions";

export default class UserCard extends React.Component {
  constructor() {
    super();
    this.state = {
      image: null,
      file: null
    }

    this.prepare_image_preview = prepare_image_preview.bind(this);
  }

  componentDidMount() {
    this.check_photo();
  }

  componentDidUpdate() {
    this.check_photo();
  }

  check_photo() {
    const { photo } = this.props.user.profile;

    if (this.state.image !== photo) {
      this.setState({image: photo});
    }
    else if (photo instanceof File && this.state.file !== photo) {
      this.setState({file: photo});
      this.prepare_image_preview(photo, "image");
    }
  }

  render() {
    let card_class = 'customize-user-card', photo_style = {};

    if (this.props.additional_classes) {
      card_class = `${card_class} ${this.props.additional_classes}`;
    }

    if (this.state.image) {
      photo_style.backgroundImage = `url(${this.state.image})`;
    }

    return (
      <div className={card_class}>
    		<div className="customize-user-card__photo" style={photo_style} />
    		<UserCardInfo
          user={this.props.user}
          is_current_user={this.props.is_current_user}
        />
  		</div>
    )
  }
}

UserCard.propTypes = {
  user: PropTypes.object.isRequired,
  is_current_user: PropTypes.bool.isRequired,
  additional_classes: PropTypes.string
}
