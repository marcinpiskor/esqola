import React from 'react';
import CustomizeHeader from './CustomizeHeader';
import CustomizeContent from './content/CustomizeContent';

export default class Customize extends React.Component {
  componentDidMount() {
    document.title = 'Customize your profile - Esqola';
  }

  render() {
    return(
      <section className="customize-section">
        <CustomizeHeader />
        <CustomizeContent />
      </section>
    )
  }
}
