import React from 'react';
import PropTypes from 'prop-types';
import UnderlineAutocomplete from './autocomplete/UnderlineAutocomplete';

export default class UnderlineInput extends React.Component {
  constructor(props) {
    super();
    this.state = {
      active: false,
      autocomplete_enabled: !!props.suggestions,
      visible_autocomplete: false,
      active_autocomplete: false,
    };

    this.input_active = this.input_active.bind(this);
    this.input_unactive = this.input_unactive.bind(this);

    this.choose_suggestion = this.choose_suggestion.bind(this);
    this.set_autocomplete_active = this.set_autocomplete_active.bind(this);
    this.set_autocomplete_unactive = this.set_autocomplete_unactive.bind(this);
  }

  input_active() {
    this.setState({active: true, visible_autocomplete: true});
  }

  input_unactive() {
    this.setState({
      active: false,
      visible_autocomplete: this.state.active_autocomplete ? true : false
    });
  }

  set_autocomplete_active() {
    if (!this.state.active_autocomplete) {
      this.setState({active_autocomplete: true});
    }
  }

  set_autocomplete_unactive(event) {
    if (this.state.active_autocomplete) {
      const children_field_name = this.props.children.props.name;
      const active_field_name = document.activeElement.getAttribute("name");

      const visible_autocomplete = active_field_name === children_field_name;

      this.setState({
        visible_autocomplete,
        active_autocomplete: false
      });
    }
  }

  choose_suggestion(suggestion) {
    this.props.choose_suggestion(suggestion);
    this.setState({
      active_autocomplete: false,
      visible_autocomplete: false
    });
  }

  render() {
    const active_class = `${this.state.active ? 'form-field__wrapper--active' : ''}`;
    const wrapper_class = `form-field__wrapper ${active_class} ${this.props.additional_classes}`;

    let autocomplete_section;

    if (this.state.autocomplete_enabled) {
      autocomplete_section = (
        <UnderlineAutocomplete
          suggestions={this.props.suggestions}
          choose_suggestion={this.choose_suggestion}
          visible_autocomplete={this.state.visible_autocomplete}
          set_autocomplete_active={this.set_autocomplete_active}
          set_autocomplete_unactive={this.set_autocomplete_unactive}
        />
      )
    }


    return (
      <div className={wrapper_class}>
        {autocomplete_section}
        {React.cloneElement(this.props.children, {
          onFocus: this.input_active,
          onBlur: this.input_unactive
        })}
      </div>
    );
  }
}

UnderlineInput.propTypes = {
  additional_classes: PropTypes.string,
  children: PropTypes.element.isRequired,
  autocomplete_section: PropTypes.element
}
