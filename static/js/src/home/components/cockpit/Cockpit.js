import React from "react";
import PropTypes from "prop-types";
import CockpitInfo from "./info/CockpitInfo";
import CockpitClassGroups from "./classgroup/CockpitClassGroups";
import CockpitInvitations from "./invitation/CockpitInvitations";
import CockpitConversations from "./conversation/CockpitConversations";
import { connect } from "react-redux";

class Cockpit extends React.Component {
  componentDidMount() {
    document.title = "Home - Esqola";
  }

  render() {
    const { id, profile } = this.props.current_user;

    if (id && profile.is_activated) {
      const { teacher_classes, student_classes } = this.props.current_user;
      const class_groups = [].concat(teacher_classes, student_classes);

      return (
        <article className="school-article school-article--fade-in">
          <CockpitInfo profile={profile} />
          <div className="cockpit-content">
            <CockpitClassGroups class_groups={class_groups} />
            <CockpitInvitations />
            <CockpitConversations />
          </div>
        </article>
      );
    }
    else {
      return "";
    }
  }
}

Cockpit.propTypes = {
  current_user: PropTypes.object.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
};

export default connect(map_state_to_props)(Cockpit);
