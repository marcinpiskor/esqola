import React from "react";
import PropTypes from "prop-types";
import CockpitClassGroup from "./CockpitClassGroup";

export default class CockpitClassGroups extends React.Component {
  render() {
    let class_groups;

    if (this.props.class_groups.length > 0) {
      class_groups = this.props.class_groups.map(class_group => {
        return (
          <CockpitClassGroup
            key={class_group.id}
            class_group={class_group}
          />
        );
      });
    }
    else {
      class_groups = (
        <h4 className="cockpit-class-group__heading cockpit-class-group__heading--empty">
          No class groups
        </h4>
      );
    }

    return (
      <div>
        <h3 className="class-group__heading class-group__heading--cockpit">
          <span className="class-group__heading--underline">Classes</span>
        </h3>
        {class_groups}
      </div>
    )
  }
}

CockpitClassGroups.propTypes = {
  class_groups: PropTypes.arrayOf(PropTypes.object)
}
