import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default class CockpitClassGroup extends React.Component {
  render() {
    const { id, name } = this.props.class_group;

    return (
      <Link className="cockpit-class-group" to={`/dashboard/${id}`}>
        <div className="option-block__icon option-block__icon--class-group" />
        <h4 className="cockpit-class-group__heading">{name}</h4>
      </Link>
    )
  }
}

CockpitClassGroup.propTypes = {
  class_group: PropTypes.object.isRequired
}
