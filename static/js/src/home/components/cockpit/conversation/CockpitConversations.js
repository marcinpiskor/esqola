import React from "react";
import get_csrf_token from "../../../csrf";
import CockpitConversation from "./CockpitConversation";

export default class CockpitConversations extends React.Component {
  constructor() {
    super();
    this.state = {
      conversations: null,
    }
  }

  componentWillMount() {
    fetch('/api/message/conversations/', {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'content-type': 'application/json',
        'X-CSRFToken': get_csrf_token()
      }
    })
    .then(response => response.json())
    .then(conversations => this.setState({conversations}))
  }

  render() {
    let conversations_content;

    if (this.state.conversations && this.state.conversations.length > 0) {
      conversations_content = this.state.conversations.map(conversation => {
        return <CockpitConversation key={conversation.id} conversation={conversation} />;
      });
    }
    else {
      conversations_content = (
        <h4 className="cockpit-class-group__heading cockpit-class-group__heading--empty">
          No conversations
        </h4>
      );
    }

    return (
      <div>
        <h3 className="class-group__heading class-group__heading--cockpit">
          <span className="class-group__heading--underline">Conversations</span>
        </h3>
        {conversations_content}
      </div>
    )
  }
}
