import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { prepare_date_text } from "../../dashboard/discussion/post/prepare_date";

export default class CockpitConversation extends React.Component {
  constructor(props) {
    super(props);
    const last_active = new Date(props.conversation.profile.last_active);

    this.state = {
      date_formatted: prepare_date_text(last_active)
    }
  }

  render() {
    const { id, username, profile } = this.props.conversation;
    const avatar_style = {};

    if (profile.photo) {
      avatar_style.backgroundImage = `url(${profile.photo})`;
    }

    return (
      <Link
        className="cockpit-class-group cockpit-class-group--invitation"
        to={`/user/${id}`}
      >
        <div className="cockpit__buttons">
          <div
            className="school-principal__avatar school-principal__avatar--conversation"
            style={avatar_style}
          />
          <h4 className="cockpit-class-group__heading">@{username}</h4>
        </div>
        <h5 className="cockpit-class-group__heading cockpit-class-group__heading--time">
          {this.state.date_formatted}
        </h5>
      </Link>
    )
  }
}

CockpitConversation.propTypes = {
  conversation: PropTypes.object.isRequired
}
