import React from "react";
import PropTypes from "prop-types";
import CockpitInvitation from "./CockpitInvitation";
import { connect } from "react-redux";
import { add_class_group } from "../../../actions/CurrentUserActionCreators";

class CockpitInvitations extends React.Component {
  constructor() {
    super();
    this.state = {
      invitations: null,
    }

    this.update_invitation = this.update_invitation.bind(this);
  }

  componentWillMount() {
    fetch('/api/invitation/', {
      method: 'GET',
      credentials: 'same-origin',
      headers: {'content-type': 'application/json'}
    })
    .then(response => response.json())
    .then(invitations => this.setState({invitations}))
  }

  update_invitation(index, class_group_obj=null) {
    const invitations = [...this.state.invitations];
    invitations.splice(index, 1);
    this.setState({invitations});

    if (class_group_obj) {
      this.props.add_class_group(class_group_obj);
    }
  }

  render() {
    let invitations_content;

    if (this.state.invitations && this.state.invitations.length > 0) {
      invitations_content = this.state.invitations.map((invitation, i) => {
        return (
          <CockpitInvitation
            key={invitation.id}
            invitation={invitation}
            index={i}
            update_invitation={this.update_invitation}
          />
        );
      });
    }
    else {
      invitations_content = (
        <h4 className="cockpit-class-group__heading cockpit-class-group__heading--empty">
          No invitations
        </h4>
      );
    }

    return (
      <div>
        <h3 className="class-group__heading class-group__heading--cockpit">
          <span className="class-group__heading--underline">Invitations</span>
        </h3>
        {invitations_content}
      </div>
    )
  }
}

CockpitInvitations.propTypes = {
  add_class_group: PropTypes.func.isRequired
}

const map_dispatch_to_props = {add_class_group}

export default connect(null, map_dispatch_to_props)(CockpitInvitations);
