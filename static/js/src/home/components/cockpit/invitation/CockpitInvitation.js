import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { show_error_block } from '../../../actions/InfoBlockActionCreators';

class CockpitInvitation extends React.Component {
  constructor() {
    super();
    this.accept_invitation = this.accept_invitation.bind(this);
    this.reject_invitation = this.reject_invitation.bind(this);
  }

  handle_invitation(action) {
    let success;

    fetch(`/api/invitation/${this.props.invitation.id}/${action}/`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'content-type': 'application/json'
      }
    })
    .then(response => {
      success = response.ok
      return response.json()
    })
    .then(data => {
      if (success) {
        if (action === "accept") {
          this.props.update_invitation(
            this.props.index, this.props.invitation.class_group
          );
        }
        else {
          this.props.update_invitation(this.props.index);
        }
      }
      else {
        this.props.show_error_block(data);
      }
    })
  }

  accept_invitation() {
    this.handle_invitation("accept");
  }

  reject_invitation() {
    this.handle_invitation("reject");
  }

  render() {
    const { class_group } = this.props.invitation;

    return (
      <div className="cockpit-class-group cockpit-class-group--invitation">
        <div>
          <h4 className="cockpit-class-group__heading">{class_group.name}</h4>
          <h5 className="cockpit-class-group__heading cockpit-class-group__heading--school">
            {class_group.school.name}
          </h5>
        </div>
        <div className="cockpit__buttons">
          <button
            className="cockpit__button cockpit__button--accept"
            onClick={this.accept_invitation}
          >
            <i className="material-icons">done</i>
          </button>
          <button
            className="cockpit__button cockpit__button--reject"
            onClick={this.reject_invitation}
          >
            <i className="material-icons">close</i>
          </button>
        </div>
      </div>
    )
  }
}

CockpitInvitation.propTypes = {
  invitation: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  update_invitation: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {show_error_block}

export default connect(null, map_dispatch_to_props)(CockpitInvitation)
