import React from "react";
import PropTypes from "prop-types";

export default class CockpitInfoBlock extends React.Component {
  render() {
    const { type, text } = this.props.info;
    const base_class = 'option-block__icon';
    const option_class = `${base_class} ${base_class}--cockpit ${base_class}--${type}`;

    return (
      <div className="cockpit-info__block cockpit-info__block--normal">
        <div className={option_class} />
        <h4 className="option-block__heading">{text}</h4>
      </div>
    );
  }
}

CockpitInfoBlock.propTypes = {
  info: PropTypes.object.isRequired
}
