import React from "react";
import PropTypes from "prop-types";
import CockpitInfoBlock from "./CockpitInfoBlock";

export default class CockpitInfo extends React.Component {
  constructor(props) {
    super(props);

    const { name, birthday, role, city, points } = props.profile;

    const age = parseInt((new Date() - new Date(birthday))/1000/60/60/24/365);
    const role_name = role.name.slice(0, role.name.length - 1);

    this.state = {
      info_data: [
        {type: "birthday", text: `${age} y.o`},
        {type: "location", text: `${city.name}, ${city.country_short}`},
        {type: "points", text: points},
        {type: role_name, text: role_name}
      ]
    }
  }

  render() {
    let cockpit_photo_class = "cockpit-info__block";
    let photo_block = {};

    if (this.props.profile.photo) {
      photo_block.backgroundImage = `url(${this.props.profile.photo})`;
      cockpit_photo_class = `${cockpit_photo_class} ${cockpit_photo_class}--photo`;
    }
    else {
      cockpit_photo_class = `${cockpit_photo_class} ${cockpit_photo_class}--no-photo`;
    }

    return (
      <div className="cockpit-info">
        <div
          className={cockpit_photo_class}
          style={photo_block}
        />
        {this.state.info_data.map(info => {
          return <CockpitInfoBlock key={info.type} info={info} />;
        })}
      </div>
    );
  }
}

CockpitInfo.propTypes = {
  profile: PropTypes.object.isRequired
}
