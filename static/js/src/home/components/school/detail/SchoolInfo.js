import React from 'react';
import PropTypes from 'prop-types';

export default class SchoolInfo extends React.Component {
  render() {
    const city_text = `${this.props.city.name}, ${this.props.city.country}`;
    const school_info_style = {};
    let school_info_class = 'school-logo';

    if (this.props.logo) {
      school_info_style.backgroundImage = `url(${this.props.logo})`;
    }
    else {
      school_info_class = `${school_info_class} ${school_info_class}--no-image ${school_info_class}--school`;
    }

    return (
      <div className="school-article__info_fields">
        <div className={school_info_class} style={school_info_style}></div>
        <div className="school-article__info_fields__wrapper">
          <h2 className="school-name">{this.props.name}</h2>
          <h3 className="school-city">
            <i className="material-icons school-city__icon">location_on</i>
            {city_text}
          </h3>
        </div>
      </div>
    );
  }
}

SchoolInfo.propTypes = {
  logo: PropTypes.string,
  name: PropTypes.string.isRequired,
  city: PropTypes.object.isRequired,
}
