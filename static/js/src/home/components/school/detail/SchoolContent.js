import React from 'react';
import PropTypes from 'prop-types';
import ClassGroupsOrder from './classgroup/ClassGroupsOrder';
import ClassGroupForm from './classgroup/form/ClassGroupForm';
import ClassGroupDetail from './classgroup/detail/ClassGroupDetail';

export default class SchoolContent extends React.Component {
  render() {
    const { classes, is_principal } = this.props;
    let school_content;

    return (
      <div>
        {classes.length > 0 &&
          <ClassGroupsOrder />
        }

        {classes.map((c, i) => {
          return (
            <ClassGroupDetail
              key={c.id}
              class_group={c}
              index={i}
              is_principal={is_principal}
            />
          );
        })}

        {is_principal &&
          <ClassGroupForm />
        }

        {!is_principal && classes.length === 0 &&
          <h3 className="cockpit-class-group__heading cockpit-class-group__heading--empty cockpit-class-group__heading--school">
            No class groups created yet
          </h3>
        }
      </div>
    );
  }
}

SchoolContent.propTypes = {
  classes: PropTypes.arrayOf(PropTypes.object),
  is_principal: PropTypes.bool.isRequired
}
