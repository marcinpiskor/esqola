import React from 'react';
import PropTypes from 'prop-types';
import ClassGroupBasicInfo from '../detail/ClassGroupBasicInfo';
import ClassGroupTableHeader from './ClassGroupTableHeader';
import ClassGroupTableRow from './ClassGroupTableRow';
import get_csrf_token from '../../../../../csrf';
import { connect } from 'react-redux';
import { remove_students} from '../../../../../actions/SchoolActionCreators';
import { show_error_block } from '../../../../../actions/InfoBlockActionCreators';

class ClassGroupTable extends React.Component {
  constructor() {
    super();

    this.send_delete_students_form = this.send_delete_students_form.bind(this);
    this.toggle_students = this.toggle_students.bind(this);
  }

  send_delete_students_form(event) {
    event.preventDefault();
    const form_data = new FormData(event.target);
    const students_to_delete = form_data.getAll('students');

    if (students_to_delete.length > 0) {
      let success;

      const students_form = new FormData();
      students_form.set('students', students_to_delete.join(','));

      fetch(`/api/class-group/${this.props.id}/delete-students/`, {
        method: 'POST',
        body: students_form,
        credentials: 'same-origin',
        headers: {'X-CSRFToken': get_csrf_token()}
      })
      .then(response => {
        success = response.ok;
        return response.json();
      })
      .then(data => {
        if (success) {
          const students = students_to_delete.map(s => parseInt(s));
          this.props.remove_students(this.props.index, students);
        }
        else {
          this.props.show_error_block(data);
        }
      })
      .catch(err => console.error(err))
    }
  }

  toggle_students() {
    const form_data = new FormData(this.students_form);
    const clicked_students = form_data.getAll('students').length;

    if (
        clicked_students === this.props.students.length || clicked_students === 0
      ) {
      this.click_unclick_students();
    }
    else if ((clicked_students/this.props.students.length)*100 >= 50) {
      this.click_unclick_students(false);
    }
    else {
      this.click_unclick_students(true);
    }
  }

  click_unclick_students(checked) {
    const extra_condition = (el) => checked !== undefined ? el.checked === checked : true;

    for (let i = 0; i < this.students_form.elements.length; i++) {
      const form_el = this.students_form[i];

      if (form_el.type === 'checkbox' && extra_condition(form_el)){
        form_el.click()
      }
    }
  }

  render() {
    let table_buttons;

    if (this.props.is_teacher) {
      table_buttons = (
        <div className="class-group__table__buttons">
          <button
            type="button"
            className="index-forms__button index-forms__button--students-table"
            onClick={this.toggle_students}
          >
            Mark/unmark students
          </button>
          <button
            className="index-forms__button index-forms__button--students-table"
          >
            Remove students
          </button>
        </div>
      );
    }
    else if (this.props.teacher) {
      table_buttons = (
        <ClassGroupBasicInfo
          teacher={this.props.teacher}
          additional_classes="class-group__teacher--detail"
        />
      );
    }

    return (
      <form
        ref={el => this.students_form = el}
        onSubmit={this.send_delete_students_form}
      >
        <h3 className="class-group__heading">
          <span className="class-group__heading--underline">
            List of students
          </span>
        </h3>

        <div className="class-group__table-wrapper">
          <table className="class-group__table">
            <ClassGroupTableHeader is_teacher={this.props.is_teacher} />
            {
              this.props.students.map(student => {
                return (
                  <ClassGroupTableRow
                    key={student.id}
                    student={student}
                    is_teacher={this.props.is_teacher}
                  />
                );
              })
            }
          </table>
        </div>

        {table_buttons}
      </form>
    );
  }
}

ClassGroupTable.propTypes = {
  id: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  students: PropTypes.arrayOf(PropTypes.object).isRequired,
  is_teacher: PropTypes.bool.isRequired,
  teacher: PropTypes.object,
  remove_students: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  remove_students,
  show_error_block
}

export default connect(null, map_dispatch_to_props)(ClassGroupTable);
