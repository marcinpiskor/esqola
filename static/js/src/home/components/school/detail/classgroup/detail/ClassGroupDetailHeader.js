import React from 'react';
import PropTypes from 'prop-types';
import get_csrf_token from '../../../../../csrf';
import { connect } from 'react-redux';
import { delete_class_group } from '../../../../../actions/SchoolActionCreators';
import { show_error_block } from '../../../../../actions/InfoBlockActionCreators';

class ClassGroupDetailHeader extends React.Component {
  constructor() {
    super();

    this.delete_class_group = this.delete_class_group.bind(this);
    this.enable_class_group_edit = this.enable_class_group_edit.bind(this);
  }

  delete_class_group() {
    let success;

    fetch(`/api/class-group/${this.props.id}/`, {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;

      if (response.ok) {
        this.props.delete_class_group(this.props.index);
      }
      else {
        const error = {detail: "Class group doesn't exist."};
        this.props.show_error_block(error);
      }
    })
  }

  enable_class_group_edit() {
    this.props.set_edit_mode(true);
  }

  render() {
    let principal_buttons;

    if (this.props.is_principal) {
      principal_buttons = [
        <button
          key="edit"
          className="class-group__button no-select"
          onClick={this.enable_class_group_edit}
        >
          <i className="material-icons class-group__button_icon">edit</i>
        </button>,
        <button
          key="delete"
          className="class-group__button no-select"
          onClick={this.delete_class_group}
        >
          <i className="material-icons class-group__button_icon">
            delete_forever
          </i>
        </button>
      ]
    }

    return (
      <div
        className="class-group class-group--no-padding"
        onClick={this.props.toggle_content}
      >
        <h3 className="class-group__label class-group__label--detail">
          {this.props.name}
        </h3>
        <div className="class-group__part class-group__part--icons">
          {principal_buttons}
          <button className="class-group__button no-select">
            <i
              className="material-icons class-group__button_icon"
              id="click-to-expand"
            >
              {this.props.visible ? "keyboard_arrow_up" : "keyboard_arrow_down"}
            </i>
          </button>
        </div>
      </div>
    );
  }
}

ClassGroupDetailHeader.propTypes = {
  id: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  set_edit_mode: PropTypes.func.isRequired,
  toggle_content: PropTypes.func.isRequired,
  delete_class_group: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired,
  is_principal: PropTypes.bool.isRequired,
  visible: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
}

const map_dispatch_to_props = {
  delete_class_group,
  show_error_block
}

export default connect(null, map_dispatch_to_props)(ClassGroupDetailHeader);
