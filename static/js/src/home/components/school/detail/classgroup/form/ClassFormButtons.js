import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const form_buttons_class = 'index-forms__button index-forms__button--class-group-form';

class ClassFormButtons extends React.Component {
  constructor() {
    super();
    this.cancel_class_group_edit = this.cancel_class_group_edit.bind(this);
  }

  cancel_class_group_edit() {
    this.props.set_edit_mode(false);
  }

  render() {
    const base_class = 'index-forms__button';
    const form_buttons_class = `${base_class} ${base_class}--class-group-form`;
    let delete_teacher_button;

    if (this.props.teacher_id) {
      delete_teacher_button = (
        <button
          type='button'
          className={`${form_buttons_class} ${base_class}--delete`}
          onClick={this.props.clear_teacher}
        >
          Delete teacher
        </button>
      );
    }


    if (this.props.class_group_id) {
      return (
        <div className="class-group__part">
          {delete_teacher_button}
          <button className={`${form_buttons_class} ${base_class}--add`}>
            Save
          </button>
          <button
            type="button"
            className={`${form_buttons_class} ${base_class}--delete`}
            onClick={this.cancel_class_group_edit}
          >
            Cancel
          </button>
        </div>
      );
    }
    else {
      return (
        <div className="class-group__part">
          {delete_teacher_button}
          <button className={`${form_buttons_class} ${base_class}--add`}>
            Create
          </button>
        </div>
      );
    }
  }
}

ClassFormButtons.propTypes = {
  clear_teacher: PropTypes.func.isRequired,
  set_edit_mode: PropTypes.func,
  teacher_id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  class_group_id: PropTypes.number
}

export default connect()(ClassFormButtons);
