import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class ClassGroupBasicInfo extends React.Component {
  render() {
    const { additional_classes, teacher } = this.props;
    let class_group_class = 'class-group__teacher';

    if (additional_classes) {
      class_group_class = `${class_group_class} ${additional_classes}`;
    }

    const { id, first_name, last_name } = teacher;
    const name = `${first_name} ${last_name}`;

    return (
      <Link to={`/user/${id}`} className={class_group_class}>
        <h4 className="class-group__teacher__heading">
          Teacher <span className="class-group__teacher__heading--name">{name}</span>
        </h4>
      </Link>
    );
  }
}

ClassGroupBasicInfo.propTypes = {
  teacher: PropTypes.object.isRequired,
  additional_classes: PropTypes.string
}
