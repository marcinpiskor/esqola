import React from 'react';
import PropTypes from 'prop-types';
import UnderlineInput from '../../../../UnderlineInput';
import ClassFormButtons from './ClassFormButtons';
import { get_suggestions } from './prepare_school_autocomplete';
import get_csrf_token from '../../../../../csrf';
import { connect } from 'react-redux';
import {
  show_error_block
} from '../../../../../actions/InfoBlockActionCreators';
import {
  add_class_group,
  update_class_group
} from '../../../../../actions/SchoolActionCreators';

const form_wrapper_class = 'form-field__wrapper--no-margin form-field__wrapper--class-group-form';

class ClassGroupForm extends React.Component {
  constructor(props) {
    super(props);
    const { name, teacher, dispatch } = props;

    this.state = {
      name: name ? name : '',
      teacher: teacher ? teacher.username : '',
      teacher_id: teacher ? teacher.id : '',
      suggestions: []
    }

    this.send_class_form = this.send_class_form.bind(this);
    this.set_input_value = this.set_input_value.bind(this);
    this.get_suggestions = get_suggestions.bind(this);
    this.choose_suggestion = this.choose_suggestion.bind(this);
    this.clear_teacher = this.clear_teacher.bind(this);
  }

  clear_teacher() {
    this.setState({teacher: '', teacher_id: ''});
  }

  send_class_form(event) {
    event.preventDefault();

    const form_data = new FormData(event.target);
    form_data.delete('teacher');

    if (!this.props.id) {
      this.send_class_data(form_data);
    }
    else {
      let fields_modified = 2

      // Check if teacher id has changed
      if (
          (this.state.teacher_id.length == 0 && !this.props.teacher)
          || (this.props.teacher && this.state.teacher_id == this.props.teacher.id)
        ) {
        form_data.delete('teacher.id');
        fields_modified -= 1;
      }
      // Check if name of class group has changed
      if (this.state.name == this.props.name) {
        form_data.delete('name');
        fields_modified -= 1;
      }

      // If any class group data field has been changed, send update request
      if (fields_modified > 0) {
        this.send_class_data(form_data);
      }
    }
  }

  send_class_data(form) {
    const request_data = this.get_request_data();
    let success;

    fetch(request_data.url, {
      method: request_data.method,
      body: form,
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(class_group_data => {
      if (success) {
        if (request_data.method === 'POST') {
          this.props.add_class_group(class_group_data);
          this.setState({teacher: '', teacher_id: '', name: ''});
        }
        else {
          this.props.update_class_group(this.props.index, class_group_data)
          this.props.set_edit_mode(false);
        }
      }
      else {
        this.props.show_error_block(class_group_data);
      }
    })
    .catch(err => console.error(err))
  }

  get_request_data() {
    const base_url = '/api/class-group/';

    if (this.props.id) {
      return {
        url: `${base_url}${this.props.id}/`,
        method: 'PATCH',
      }
    }
    else {
      return {
        url: base_url,
        method: 'POST',
      }
    }
  }

  set_input_value(event) {
    const { name, value } = event.target

    if (name === 'teacher') {
      this.get_suggestions(value, 'teachers');
    }

    const obj = {};
    obj[name] = value;

    this.setState(obj);
  }

  choose_suggestion(suggestion) {
    this.setState({
      teacher: suggestion.content,
      teacher_id: suggestion.id,
    });
  }

  render() {
    return (
      <form className="class-group" onSubmit={this.send_class_form}>
        <div className="class-group__part">
          <div className="class-group__part_field">
            <label className="class-group__label">Name</label>
            <UnderlineInput
              additional_classes={form_wrapper_class}
            >
              <input
                type="text"
                name="name"
                className="form-field form-field--class-group-form"
                autoComplete="off"
                onChange={this.set_input_value}
                value={this.state.name}
                required
              />
            </UnderlineInput>
          </div>

          <div className="class-group__part_field">
            <label className="class-group__label">Teacher</label>
            <UnderlineInput
              additional_classes={form_wrapper_class}
              suggestions={this.state.suggestions}
              choose_suggestion={this.choose_suggestion}
            >
              <input
                type="text"
                name="teacher"
                className="form-field form-field--class-group-form"
                autoComplete="off"
                onChange={this.set_input_value}
                value={this.state.teacher}
              />
            </UnderlineInput>
            <input
              type="hidden"
              name="teacher.id"
              value={this.state.teacher_id}
            />
          </div>
        </div>

        <ClassFormButtons
          class_group_id={this.props.id}
          teacher_id={this.state.teacher_id}
          set_edit_mode={this.props.set_edit_mode}
          clear_teacher={this.clear_teacher}
        />
      </form>
    );
  }
}

ClassGroupForm.propTypes = {
  name: PropTypes.string,
  teacher: PropTypes.object,
  id: PropTypes.number,
  index: PropTypes.number,
  set_edit_mode: PropTypes.func,
  add_class_group: PropTypes.func.isRequired,
  update_class_group: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  add_class_group,
  update_class_group,
  show_error_block
}

export default connect(null, map_dispatch_to_props)(ClassGroupForm);
