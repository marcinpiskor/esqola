export function get_suggestions(value, group_name) {
  const query_params = prepare_query_params(value);

  if (query_params) {
    fetch(`/api/user/?${query_params}&groups__name=${group_name}`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {'content-type': 'application/json'}
    })
    .then(response => response.json())
    .then(autocomplete => {
      if (query_params.indexOf('username') !== -1) {
          const suggestions = autocomplete.map(user => {
            return {id: user.id, content: user.username};
          });
          this.setState({suggestions});
      }
      else {
        const suggestions = autocomplete.map(user => {
          return {
            id: user.id, content: `${user.first_name} ${user.last_name}`
          };
        });
        this.setState({suggestions});
      }
    })
  }
  else {
    this.setState({suggestions: []});
  }
}

function prepare_query_params(value) {
  if (value.length >= 2) {
    const params_content = value.trim().replace(/[ ]+/g, ' ').split(' ');

    if (params_content.length == 1 && params_content[0].match(/[a-zA-Z]/g)) {
      return `username=${params_content[0]}`;
    }
    else if (params_content.length > 1) {
      const last_name = params_content.splice(1, params_content.length).join(' ');
      return `first_name=${params_content[0]}&last_name=${last_name}`;
    }
  }
}
