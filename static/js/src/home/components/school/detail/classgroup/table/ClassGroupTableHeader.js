import React from 'react';
import PropTypes from 'prop-types';

export default class ClassGroupTableHeader extends React.Component {
  render() {
    const headings = [
      'Username', 'First name', 'Last name', 'Birthday', 'City', 'Points'
    ];

    if (this.props.is_teacher) {
      headings.unshift('');
    }

    return (
      <thead>
        <tr>
        {headings.map(text => {
          return (
            <th key={text} className="class-group__table__head">{text}</th>
          );
        })}
        </tr>
      </thead>
    );
  }
}

ClassGroupTableHeader.propTypes = {
  is_teacher: PropTypes.bool.isRequired
}
