import React from 'react';
import PropTypes from 'prop-types';
import ClassGroupBasicInfo from './ClassGroupBasicInfo';
import ClassGroupDetailInfo from './ClassGroupDetailInfo';
import ClassGroupDetailHeader from './ClassGroupDetailHeader';
import ClassGroupForm from '../form/ClassGroupForm';
import { connect } from 'react-redux';
import {
  set_class_group_detail_info
} from '../../../../../actions/SchoolActionCreators';

class ClassGroupDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      fetched_detail: false,
      edit_mode: false
    };

    this.toggle_content = this.toggle_content.bind(this);
    this.set_edit_mode = this.set_edit_mode.bind(this);
  }

  toggle_content(event) {
    if (event.target.tagName !== 'I' || event.target.id === 'click-to-expand') {
      const visible = !this.state.visible;

      if (
          visible
          && !this.state.fetched_detail
          && !this.state.edit_mode
          && this.is_related()
      ) {
        this.fetch_class_group_detail();
      }
      else {
        this.setState({visible, fetched_detail: true});
      }
    }
  }

  is_related() {
      const { id, student_classes } = this.props.current_user;
      const { teacher } = this.props.class_group;

      return (
        this.props.is_principal
        || (teacher && id == teacher.id)
        || student_classes.find(c => c.id === this.props.class_group.id)
      );
  }

  fetch_class_group_detail() {
    fetch(`/api/class-group/${this.props.class_group.id}/`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {'content-type': 'application/json'}
    })
    .then(response => response.json())
    .then(class_detail => {
      this.props.set_class_group_detail_info(this.props.index, class_detail);
      this.setState({visible: true, fetched_detail: true});
    })
  }

  set_edit_mode(edit_mode) {
    this.setState({edit_mode});
  }

  render() {
    const { teacher } = this.props.class_group
    let class_group_header, class_group_info;

    // Class group header component (class edit form or block with name and
    // buttons)
    if (this.state.edit_mode) {
      class_group_header = (
        <ClassGroupForm
          id={this.props.class_group.id}
          index={this.props.index}
          name={this.props.class_group.name}
          teacher={this.props.class_group.teacher}
          set_edit_mode={this.set_edit_mode}
        />
      )
    }
    else {
      class_group_header = (
        <ClassGroupDetailHeader
          visible={this.state.visible}
          name={this.props.class_group.name}
          is_principal={this.props.is_principal}
          toggle_content={this.toggle_content}
          set_edit_mode={this.set_edit_mode}
          id={this.props.class_group.id}
          index={this.props.index}
        />
      )
    }

    // Class group info component (table with students or info about teacher)
    if (this.state.visible) {
      if (this.props.class_group.students) {
        const is_teacher = (
          !!teacher && teacher.id === this.props.current_user.id
        );

        class_group_info = (
          <ClassGroupDetailInfo
            index={this.props.index}
            class_group={this.props.class_group}
            is_teacher={is_teacher}
          />
        );
      }
      else if (teacher) {
        class_group_info = <ClassGroupBasicInfo teacher={teacher} />;
      }
    }

    // Class group info wrapper class name
    const { visible, fetched_detail } = this.state;
    let wrapper_class = 'class-group-content';

    if (visible & fetched_detail) {
      wrapper_class = `${wrapper_class} ${wrapper_class}--visible`;
    }

    return (
      <div className="class-group-detail">
        {class_group_header}
        <div className={wrapper_class}>
          {class_group_info}
        </div>
      </div>
    );
  }
}

ClassGroupDetail.propTypes = {
  class_group: PropTypes.object.isRequired,
  current_user: PropTypes.object.isRequired,
  is_principal: PropTypes.bool.isRequired,
  index: PropTypes.number.isRequired,
  set_class_group_detail_info: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
}

const map_dispatch_to_props = {set_class_group_detail_info}

export default connect(map_state_to_props, map_dispatch_to_props)(ClassGroupDetail);
