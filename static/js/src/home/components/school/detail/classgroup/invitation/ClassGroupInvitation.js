import React from 'react';
import PropTypes from 'prop-types';
import get_csrf_token from '../../../../../csrf';
import { connect } from 'react-redux';
import { show_error_block } from '../../../../../actions/InfoBlockActionCreators';
import { delete_invitation } from '../../../../../actions/SchoolActionCreators';

class ClassGroupInvitation extends React.Component {
  constructor() {
    super();

    this.delete_invitation = this.delete_invitation.bind(this);
  }

  delete_invitation() {
    fetch(`/api/invitation/${this.props.invitation.id}/`, {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      if (response.ok) {
        this.props.delete_invitation(
          this.props.class_group_index, this.props.index
        );
      }
      else {
        const error = {detail: "Invitation doesn't exist."}
        this.props.show_error_block(error);
      }
    })
  }

  render() {
    const {
      username, first_name, last_name, profile
    } = this.props.invitation.student;
    const avatar_style = {};

    if (profile.photo) {
      avatar_style.backgroundImage = `url(${profile.photo})`;
    }

    return (
      <div className="class-group__invitation class-group__invitation--normal">
        <div
          className="school-principal__avatar school-principal__avatar-invitation"
          style={avatar_style}
        />
        <div>
          <h5 className="class-group__invitation_full_name">
            {first_name} {last_name}
          </h5>
          <h6 className="class-group__invitation_username">@{username}</h6>
        </div>

        <div
          className="class-group__invitation_delete"
          onClick={this.delete_invitation}
        >
          <div className="school-principal__avatar school-principal__avatar-invitation school-principal__avatar--delete" />
          <div>
            <h5 className="class-group__invitation_full_name">
              Delete invitation
            </h5>
            <h6 className="class-group__invitation_username class-group__invitation_username--delete">
              for this user?
            </h6>
          </div>
        </div>
      </div>
    );
  }
}

ClassGroupInvitation.propTypes = {
  invitation: PropTypes.object.isRequired,
  class_group_index: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  delete_invitation: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  show_error_block,
  delete_invitation
}

export default connect(null, map_dispatch_to_props)(ClassGroupInvitation);
