import React from 'react';
import PropTypes from 'prop-types';
import UnderlineInput from '../../../../UnderlineInput';
import get_csrf_token from '../../../../../csrf';
import { get_suggestions }  from '../form/prepare_school_autocomplete';
import { connect } from 'react-redux';
import { add_invitation } from '../../../../../actions/SchoolActionCreators';
import {
  update_info_block,
  show_error_block
} from '../../../../../actions/InfoBlockActionCreators';

class ClassGroupInvitationForm extends React.Component {
  constructor() {
    super();
    this.state = {
      student_name: '',
      student_id: '',
      suggestions: []
    }

    this.send_invitation_form = this.send_invitation_form.bind(this);
    this.get_suggestions = get_suggestions.bind(this);
    this.set_input_value = this.set_input_value.bind(this);
    this.choose_suggestion = this.choose_suggestion.bind(this);
  }

  send_invitation_form(event) {
    event.preventDefault();

    if (this.props.students.indexOf(parseInt(this.state.student_id)) !== -1) {
      const warning_block = {
        heading: 'Are you sure?',
        text: 'This user is student or has been already invited.',
        type: 'warning',
        clickable: false
      };

      this.props.update_info_block(warning_block);
    }
    else if (this.state.student_id) {
      const form = new FormData();

      form.set('class_group.id', this.props.class_group_id)
      form.set('student.id', parseInt(this.state.student_id));
      this.post_invitation_form(form);
    }
  }

  post_invitation_form(form) {
    let success;

    fetch('/api/invitation/', {
      method: 'POST',
      body: form,
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(invitation_data => {
      if (success) {
        this.props.add_invitation(this.props.class_group_index, invitation_data);
        this.setState({student_name: '', student_id: '', suggestions: []});
      }
      else {
        this.props.show_error_block(data);
      }
    })
  }

  set_input_value(event) {
    const { name, value } = event.target

    if (name == 'student_name') {
      this.get_suggestions(value, 'students');
    }

    const obj = {};
    obj[name] = value;

    this.setState(obj);
  }

  choose_suggestion(suggestion) {
    this.setState({
      student_name: suggestion.content,
      student_id: suggestion.id
    });
  }

  render() {
    return (
      <form
        className="class-group__invitation"
        onSubmit={this.send_invitation_form}
      >
        <button className="class-group__invitation_form_button">
          <i className="material-icons class-group__invitation_form_icon">
            add
          </i>
        </button>
        <UnderlineInput
          additional_classes="form-field__wrapper--no-margin form-field__wrapper--invitation-form"
          suggestions={this.state.suggestions}
          choose_suggestion={this.choose_suggestion}
        >
          <input
            type="text"
            name="student_name"
            className="form-field form-field--invitation-form"
            placeholder="Full name or username"
            value={this.state.student_name}
            autoComplete="off"
            onChange={this.set_input_value}
            onFocus={this.set_autocomplete_visible}
            onBlur={this.set_autocomplete_invisible}
            required
          />
        </UnderlineInput>
        <input type="hidden" name="student.id" value={this.state.student_id} />
      </form>
    );
  }
}

ClassGroupInvitationForm.propTypes = {
  students: PropTypes.arrayOf(PropTypes.number).isRequired,
  class_group_id: PropTypes.number.isRequired,
  class_group_index: PropTypes.number.isRequired,
  add_invitation:  PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired,
  update_info_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  add_invitation,
  show_error_block,
  update_info_block
}

export default connect(null, map_dispatch_to_props)(ClassGroupInvitationForm);
