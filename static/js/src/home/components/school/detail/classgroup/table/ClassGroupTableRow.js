import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class ClassGroupTableRow extends React.Component {
  constructor(props) {
    super(props);

    const format = (x) => String(x).length === 1 ? `0${x}` : x;
    const { username, first_name, last_name, profile } = props.student;

    let { birthday, city, points } = profile;
    birthday = new Date(birthday);

    this.state = {
      student_data: [
        username,
        first_name,
        last_name,
        `${format(birthday.getDate())}.${format(birthday.getMonth() + 1)}.${birthday.getFullYear()}`,
        city ? `${city.name}, ${city.country}` : '-',
        points
      ],
      marked: false
    }

    this.toggle_marked = this.toggle_marked.bind(this);
  }

  toggle_marked() {
    this.setState({marked: !this.state.marked});
  }

  render() {
    let checkbox_input;

    if (this.props.is_teacher) {
      checkbox_input = (
        <td>
          <button
            type="button"
            className="class-group__checkbox_button"
            onClick={this.toggle_marked}
          >
            <i className="material-icons">
              {this.state.marked ? "check_box" : "check_box_outline_blank"}
            </i>
          </button>
          <input
            className="class-group__checkbox"
            type="checkbox" name="students"
            value={this.props.student.id}
            checked={this.state.marked ? "checked" : ""}
            onChange={this.toggle_marked}
          />
        </td>
      )
    }

    return (
      <thead>
        <tr>
          {checkbox_input}
          {this.state.student_data.map(data => {
            return (
              <td key={data} className="class-group__table__cell">
                <Link
                  to={`/user/${this.props.student.id}`}
                  className="class-group__table__link"
                >
                  {data}
                </Link>
              </td>
            );
          })}
        </tr>
      </thead>
    );
  }
}

ClassGroupTableRow.propTypes = {
  is_teacher: PropTypes.bool.isRequired,
  student: PropTypes.object.isRequired
}
