import React from 'react';
import PropTypes from 'prop-types';
import ClassGroupInvitation from './ClassGroupInvitation';
import ClassGroupInvitationForm from './ClassGroupInvitationForm';

export default class ClassGroupInvitations extends React.Component {
  render() {
    const {
      students, invitations, class_group_index, class_group_id
    } = this.props;
    const already_students_or_invited = students.concat(
      invitations.map(inv => inv.student.id)
    );

    return (
      <div>
        <h3 className="class-group__heading">
          <span className="class-group__heading--underline">Invited users</span>
        </h3>
        <div className="class-group__invitation-wrapper">
          {invitations.map((inv, i) => {
            return (
              <ClassGroupInvitation
                key={inv.id}
                invitation={inv}
                index={i}
                class_group_index={class_group_index}
              />
            )
          })}
          <ClassGroupInvitationForm
            students={already_students_or_invited}
            class_group_id={class_group_id}
            class_group_index={class_group_index}
          />
        </div>
      </div>
    );
  }
}

ClassGroupInvitations.propTypes = {
  students: PropTypes.arrayOf(PropTypes.number).isRequired,
  invitations: PropTypes.arrayOf(PropTypes.object).isRequired,
  class_group_id: PropTypes.number.isRequired,
  class_group_index: PropTypes.number.isRequired,
}
