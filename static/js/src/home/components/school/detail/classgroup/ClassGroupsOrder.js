import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { order_classes } from '../../../../actions/SchoolActionCreators';

class ClassGroupsOrder extends React.Component {
  constructor() {
    super();

    const order_fields = [
      {value: 'id', text: 'id'},
      {value: 'name', text: "class' name"},
      {value: 'teacher.last_name', text: "teacher's name"},
    ]

    this.state = {
      order_fields,
      order_field: order_fields[0].value,
      order: 'ascending'
    }

    this.set_order_field = this.set_order_field.bind(this);
    this.change_order = this.change_order.bind(this);
  }

  set_order_field(event) {
    const select = event.target
    const order_field = (select.options[select.selectedIndex].value);

    this.props.order_classes(this.state.order, order_field);
    this.setState({order_field});
  }

  change_order() {
    let order;

    if (this.state.order === 'ascending') {
      order = 'descending';
    }
    else {
      order = 'ascending';
    }

    this.props.order_classes(order, this.state.order_field);
    this.setState({order});
   }

  render() {
    const options = this.state.order_fields.map(field => {
      return (
        <option key={field.value} value={field.value}>{field.text}</option>
      );
    });

    return (
      <div className="class-group__order">
        <select
          name="order_type"
          className="custom-select custom-select--order"
          onChange={this.set_order_field}
        >
          {options}
        </select>
        <span className="class-group__order_text">order by</span>
        <button
          className="class-group__order_button"
          onClick={this.change_order}
        >
          {this.state.order}
        </button>
      </div>
    );
  }
}

ClassGroupsOrder.propTypes = {
  order_classes: PropTypes.func.isRequired
}

const map_dispatch_to_props = {order_classes};

export default connect(null, map_dispatch_to_props)(ClassGroupsOrder);
