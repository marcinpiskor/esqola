import React from 'react';
import PropTypes from 'prop-types';
import ClassGroupTable from '../table/ClassGroupTable';
import ClassGroupInvitations from '../invitation/ClassGroupInvitations';

export default class ClassGroupDetailInfo extends React.Component {
  render() {
    const { class_group, index, is_teacher } = this.props;
    const { id, students, teacher, invited_users } = class_group;

    return (
      <div className="class-group__info">
        <ClassGroupTable
          id={id}
          index={index}
          students={students}
          teacher={teacher}
          is_teacher={is_teacher}
        />
        {is_teacher &&
          <ClassGroupInvitations
            students={students.map(s => s.id)}
            invitations={invited_users}
            class_group_id={id}
            class_group_index={index}
          />
        }
      </div>
    );
  }
}

ClassGroupDetailInfo.propTypes = {
  class_group: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  is_teacher: PropTypes.bool.isRequired
}
