import React from 'react';
import PropTypes from 'prop-types';
import SchoolInfo from './SchoolInfo';
import SchoolButtons from './SchoolButtons';
import SchoolPrincipal from './SchoolPrincipal';
import SchoolContent from './SchoolContent';

export default class SchoolDetail extends React.Component {
  render() {
    const { school, current_user_id } = this.props;
    const { id, logo, city, name, principal, classes } = school;
    const is_principal = !!(
      current_user_id && principal && current_user_id === principal.id
    );

    const base_class = 'school-article';
    let school_detail_class = `${base_class} ${base_class}--hidden`;

    if (this.props.loaded) {
      school_detail_class = `${base_class} ${base_class}--fade-in`;
    }

    let school_detail_info;

    if (is_principal) {
      school_detail_info = <SchoolButtons id={id} />
    }
    else if (principal) {
      school_detail_info = <SchoolPrincipal principal={principal} />
    }

    return (
      <article className={school_detail_class}>
        <div className="school-article__info">
          <SchoolInfo logo={logo} city={city} name={name} />
          {school_detail_info}
        </div>
        <SchoolContent is_principal={is_principal} classes={classes} />
      </article>
    );
  }
}

SchoolDetail.propTypes = {
  school: PropTypes.object.isRequired,
  loaded: PropTypes.bool.isRequired,
  current_user_id: PropTypes.number.isRequired
}
