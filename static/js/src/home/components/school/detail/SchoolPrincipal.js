import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class SchoolPrincipal extends React.Component {
  render() {
    const { id, first_name, last_name, profile } = this.props.principal;
    const principal_text = `${first_name} ${last_name}`;
    const avatar_style = {};

    if (profile.photo) {
      avatar_style.backgroundImage = `url(${profile.photo})`
    }

    return (
      <Link
        to={`/user/${id}`}
        className="school-article__button_wrapper school-article__button_wrapper--school"
      >
        <div className="school-principal__avatar" style={avatar_style} />
        <h4 className="school-principal__text school-principal__text--school">
          {principal_text}
        </h4>
      </Link>
    );
  }
}

SchoolPrincipal.propTypes = {
  principal: PropTypes.object.isRequired
}
