import React from 'react';
import PropTypes from 'prop-types';
import get_csrf_token from '../../../csrf';
import { connect } from 'react-redux';
import { show_error_block } from '../../../actions/InfoBlockActionCreators';
import { edit_school, clear_school } from '../../../actions/SchoolActionCreators';
import { set_current_user_school } from '../../../actions/CurrentUserActionCreators';

class SchoolButtons extends React.Component {
  constructor() {
    super();
    this.delete_school = this.delete_school.bind(this);
  }

  delete_school() {
    let success;

    fetch(`/api/school/${this.props.id}/`, {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;

      if (response.ok) {
        this.props.set_current_user_school(null);
        this.props.clear_school();
      }
      else {
        const error = {detail: "School doesn't exist."};
        this.props.show_error_block(error);
      }
    })
  }

  render() {
    const base_class = 'index-forms__button';
    const button_class = `${base_class} ${base_class}--school`;

    return (
      <div className='school-article__button_wrapper'>
        <button
          onClick={this.props.edit_school}
          className={`${button_class} ${base_class}--modify`}
        >
          Modify
        </button>
        <button
          onClick={this.delete_school}
          className={`${button_class} ${base_class}--delete`}
        >
          Delete
        </button>
      </div>
    );
  }
}

SchoolButtons.propTypes = {
  id: PropTypes.number.isRequired,
  show_error_block: PropTypes.func.isRequired,
  set_current_user_school: PropTypes.func.isRequired,
  edit_school: PropTypes.func.isRequired,
  clear_school: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  show_error_block,
  set_current_user_school,
  edit_school,
  clear_school
}

export default connect(null, map_dispatch_to_props)(SchoolButtons);
