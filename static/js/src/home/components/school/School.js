import React from 'react';
import PropTypes from 'prop-types';
import CreateSchool from './create/CreateSchool';
import SchoolDetail from './detail/SchoolDetail';
import { Redirect } from 'react-router'
import { connect } from 'react-redux';
import { get_school, clear_school } from '../../actions/SchoolActionCreators';
import { show_error_block } from '../../actions/InfoBlockActionCreators';

class School extends React.Component {
  constructor(props) {
    super(props);

    const { profile } = this.props.current_user;
    const { school_id } = this.props.match.params;
    const if_loaded = (
      !school_id && profile.role && profile.role.name !== 'principals'
    );

    this.state = {
      loaded: !!if_loaded,
      in_process: false,
      failed: false,
      initialized: false
    };
  }

  componentWillMount() {
    const school_id = this.get_school_id()

    if (this.props.current_user.profile.is_activated) {
      if (school_id != this.props.school.id) {
        this.fetch_school_data(school_id);
      }
      else if (
        !this.state.initialized
        && (!school_id || school_id == this.props.school.id)
      ) {
        this.setState({initialized: true, loaded: true});
      }
    }
  }

  componentDidUpdate() {
    const school_id = this.get_school_id();

    // Fetch info about school if data of this school hasn't been fetched yet
    if (!this.state.in_process && this.props.current_user.profile.is_activated) {
      if (school_id != this.props.school.id) {
        this.fetch_school_data(school_id);
      }
      else if (!this.state.initialized && !school_id) {
        this.setState({initialized: true, loaded: true});
      }
    }
  }

  get_school_id() {
    const { school_id } = this.props.match.params;
    return school_id ? school_id : this.props.current_user.school;
  }

  fetch_school_data(school_id) {
    if (school_id) {
      this.setState({loaded: false, in_process: true});

      let success;

      fetch(`/api/school/${school_id}/`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'}
      })
      .then(response => {
        success = response.ok
        return response.json()
      })
      .then(school_data => {
        if (success) {
          document.title = `${school_data.name} school - Esqola`;

          this.props.get_school(school_data);
          this.setState({
            in_process: false,
            loaded: true,
            initialized: true
          });
        }
        else {
          const error = {detail: "School doesn't exist."};

          this.setState({failed: true});
          this.props.clear_school();
          this.props.show_error_block(error);
        }
      })
    }
    else {
      this.props.clear_school();
      this.setState({loaded: true});
    }
  }

  render() {
    const { id, school, profile } = this.props.current_user;
    const { school_id } = this.props.match.params;

    if (
      this.state.failed
      || (this.state.loaded && !school_id && profile.role && profile.role.name !== "principals")
    ) {
      return <Redirect to="/" />;
    }
    if (this.props.school.id && !this.props.school.edit_mode) {
      return (
        <SchoolDetail
          loaded={this.state.loaded}
          school={this.props.school}
          current_user_id={id}
        />
    );
    }
    if (
      this.props.school.edit_mode
      || !school && profile.role && profile.role.name === "principals"
    ) {
      return (
        <CreateSchool loaded={this.state.loaded} school={this.props.school} />
      );
    }
    else {
      return "";
    }
  }
}

School.propTypes = {
  school: PropTypes.object.isRequired,
  current_user: PropTypes.object.isRequired,
  get_school: PropTypes.func.isRequired,
  clear_school: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {school: state.school, current_user: state.current_user};
}

const map_dispatch_to_props = {
  get_school,
  clear_school,
  show_error_block
}

export default connect(map_state_to_props, map_dispatch_to_props)(School);
