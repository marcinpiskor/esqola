import React from 'react';
import PropTypes from 'prop-types';
import UnderlineInput from '../../UnderlineInput';

export default class SchoolFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      autocomplete: null,
      name: props.name ? props.name : '',
      place_id: props.city ? props.city.place_id : '',
      city_name: props.city ? `${props.city.name}, ${props.city.country}` : '',
    }

    this.set_input_value = this.set_input_value.bind(this);
  }

  componentDidMount() {
    const options = {types: ['(cities)']};
    const autocomplete = new google.maps.places.Autocomplete(
      this.city_input, options
    );
    this.setState({autocomplete});

    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const { place_id, address_components } = autocomplete.getPlace();
      const get_city_prop = (prop) => {
        const value = address_components.find(comp => {
          return comp.types.indexOf(prop) !== -1;
        });

        return value ? value.long_name : '';
      }

      const city_name = [
        get_city_prop('locality'),
        get_city_prop('administrative_area_level_1'),
        get_city_prop('country')
      ].filter(prop => prop).join(', ');

      this.setState({place_id, city_name});
    })
  }

  componentWillUnmount() {
    google.maps.event.clearListeners(this.state.autocomplete);
  }

  set_input_value(event) {
    const { name, value } = event.target

    const obj = {};
    obj[name] = value;

    this.setState(obj);
  }

  render() {
    const wrapper_class_name = 'form-field__wrapper--no-margin form-field__wrapper--full';

    return (
      <div className="school-article__info_fields__wrapper">
        <UnderlineInput additional_classes={wrapper_class_name}>
          <input
            type="text"
            name="name"
            placeholder="Name"
            className="form-field form-field--school-name"
            value={this.state.name}
            onChange={this.set_input_value}
            required
          />
        </UnderlineInput>
        <UnderlineInput additional_classes={wrapper_class_name}>
          <input
            type="text"
            name="city_name"
            placeholder="City"
            className="form-field form-field--school-city"
            value={this.state.city_name}
            onChange={this.set_input_value}
            ref={input => this.city_input = input}
            required
          />
        </UnderlineInput>
        <input type="hidden" name="city.place_id" value={this.state.place_id} />
      </div>
    )
  }
}

SchoolFields.propTypes = {
  city: PropTypes.object,
  name: PropTypes.string
}
