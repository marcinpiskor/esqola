import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { finish_edit_school } from '../../../../actions/SchoolActionCreators';

class SchoolEditButtons extends React.Component {
  render() {
    const base_class = 'index-forms__button';
    const button_class_name = `${base_class} ${base_class}--school`;

    return (
      <div className="school-article__button_wrapper">
        {this.props.logo &&
          <button
            type="button"
            className={button_class_name}
            onClick={this.props.clear_school_logo}
          >
            Delete logo
          </button>
        }

        <button className={`${button_class_name} ${base_class}-add`}>
          Save
        </button>
        <button
          type="button"
          className={`${button_class_name} ${base_class}-delete`}
          onClick={this.props.finish_edit_school}
        >
          Cancel
        </button>
      </div>
    );
  }
}


SchoolEditButtons.propTypes = {
  logo: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(File),
    PropTypes.instanceOf(null)
  ]),
  clear_school_logo: PropTypes.func.isRequired,
  finish_edit_school: PropTypes.func.isRequired,
}

const map_dispatch_to_props = {finish_edit_school};

export default connect(null, map_dispatch_to_props)(SchoolEditButtons);
