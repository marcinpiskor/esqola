import React from 'react';
import PropTypes from 'prop-types';

const button_class_name = 'index-forms__button index-forms__button--school';

export default class SchoolCreateButton extends React.Component {
  render() {
    return (
      <div className="school-article__button_wrapper">
        <button className={button_class_name}>
          Create new school
        </button>
        {this.props.logo instanceof File &&
          <button
            type="button"
            className={button_class_name}
            onClick={this.props.clear_school_logo}
          >
            Delete logo
          </button>
        }
      </div>
    );
  }
}

SchoolCreateButton.propTypes = {
  logo: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(File)
  ]),
  clear_school_logo: PropTypes.func.isRequired
}
