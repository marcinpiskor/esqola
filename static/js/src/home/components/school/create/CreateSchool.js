import React from 'react';
import PropTypes from 'prop-types';
import SchoolForm from './SchoolForm';
import SchoolHelp from './SchoolHelp';

export default class CreateSchool extends React.Component {
  render() {
    const base_class = 'school-article';
    let create_school_class = `${base_class} ${base_class}--hidden`;

    if (this.props.loaded) {
      create_school_class = `${create_school_class} ${base_class}--fade-in`
    }

    return (
      <article className={create_school_class}>
        <SchoolForm school={this.props.school} />
        <SchoolHelp edit_mode={this.props.school.edit_mode} />
      </article>
    );
  }
}

CreateSchool.propTypes = {
  loaded: PropTypes.bool.isRequired,
  school: PropTypes.object.isRequired,
}
