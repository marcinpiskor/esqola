import React from 'react';
import PropTypes from 'prop-types';

const tips = [
  'Enter a name of new school (needs to be unique for particular city).',
  'Select a city where school is placed.',
  '(optional) Upload logo school by file picker (click on school icon) or drop image.'
]

export default class SchoolHelp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: `How to ${props.edit_mode ? 'edit': 'create'} a school?`
    }
  }

  render() {
    return (
      <div className="school-help">
        <h2 className="school-help-heading">
          <span className="school-help-heading__content">
            {this.state.text}
          </span>
        </h2>
        <ul className="school-help-tips">
          {tips.map(tip => {
            return <li key={tip} className="school-help-tip">{tip}</li>;
          })}
        </ul>
      </div>
    );
  }
}

SchoolHelp.propTypes = {
  edit_mode: PropTypes.bool.isRequired
}
