import React from 'react';
import PropTypes from 'prop-types';
import SchoolFields from './SchoolFields';
import SchoolCreateButton from './buttons/SchoolCreateButton';
import SchoolEditButtons from './buttons/SchoolEditButtons';
import {
  pick_file,
  drag_file,
  prepare_image_preview
} from '../../customization/content/fields/photo_functions';
import get_csrf_token from '../../../csrf';
import { connect } from 'react-redux';
import { get_school } from '../../../actions/SchoolActionCreators';
import {
  set_current_user_school
} from '../../../actions/CurrentUserActionCreators';
import {
  update_info_block,
  show_error_block
} from '../../../actions/InfoBlockActionCreators';

class SchoolForm extends React.Component {
  constructor(props) {
    super(props);
    const { logo } = props.school;

    this.state = {
      image_file: logo,
      image_data: logo ? logo: '',
      extensions: '.png, .jpg, .jpeg',
    }

    this.pick_file = pick_file.bind(this);
    this.drop_file = this.drop_file.bind(this);

    this.prepare_logo_preview = prepare_image_preview.bind(this);
    this.clear_school_logo = this.clear_school_logo.bind(this);
    this.set_image_file = this.set_image_file.bind(this);
    this.send_school_form = this.send_school_form.bind(this);
  }

  componentDidMount() {
    if (!this.props.school.id) {
      document.title = "Create new school - Esqola";
    }
  }

  clear_school_logo() {
    if (this.state.image_file instanceof File) {
      const { logo } = this.props.school;
      this.setState({image_file: logo, image_data: logo});
    }
    else {
      this.setState({image_file: '', image_data: ''});
    }
  }

  send_school_form(event) {
    event.preventDefault();

    const { edit_mode } = this.props.school;
    const form_data = new FormData(event.target);

    form_data.delete('city_name');

    if (edit_mode) {
      const changed_fields = this.prepare_school_edit_data(form_data);

      // If any school data field has been changed, send update request
      if (changed_fields > 0) {
        this.send_school_data(form_data);
      }
    }
    else {
      if (this.state.image_file !== null) {
        form_data.set('logo', this.state.image_file);
      }
      else {
        form_data.delete('logo');
      }

      this.send_school_data(form_data);
    }
  }

  prepare_school_edit_data(form_data) {
    const { name, city, logo } = this.props.school;
    let new_data_fields = 3;

    // Check if name of school has changed
    if (form_data.get('name') && form_data.get('name').trim() === name) {
      form_data.delete('name');
      new_data_fields -= 1;
    }
    // Check if city of school has changed
    if (
      form_data.get('city.place_id')
      && form_data.get('city.place_id') === city.place_id
    ) {
      form_data.delete('city.place_id');
      new_data_fields -= 1;
    }
    // Check if logo of school has changed
    if (this.state.image_file === logo) {
      form_data.delete('logo');
      new_data_fields -= 1;
    }
    else {
      form_data.set('logo', this.state.image_file);
    }

    return new_data_fields;
  }

  send_school_data(form) {
    const request = this.get_request_data();
    let success;

    fetch(request.url, {
      method: request.method,
      body: form,
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(school_data => {
      if (success) {
        this.props.get_school(school_data);

        if (request.method !== 'PATCH') {
          this.success_new_school(school_data);
        }
      }
      else {
        this.props.show_error_block(school_data);
      }
    })
  }

  get_request_data() {
    const base_url = '/api/school/';

    // Choose type of request dependently on school edit mode
    if (this.props.school.edit_mode) {
      return {
        url: `${base_url}${this.props.school.id}/`,
        method: 'PATCH',
      }
    }
    else {
      return {
        url: base_url,
        method: 'POST',
      }
    }
  }

  success_new_school(school_data) {
    const info_data = {
      heading: `${school_data.name} school has been created`,
      text: 'Now you can create class groups for students.',
      clickable: true,
      type: 'success'
    };

    this.props.set_current_user_school(school_data.id);
    this.props.update_info_block(info_data);
  }

  set_image_file(event) {
    const image_file = event.target.files[0];

    if (image_file) {
      this.setState({image_file});
      this.prepare_logo_preview(image_file, 'image_data');
    }
  }

  drop_file(event) {
    event.preventDefault();
    const valid_ext = this.state.extensions.split(', ').map(ext => {
      return ext.slice(1, ext.length)
    });

    for (let i=0; i<event.dataTransfer.items.length; i++) {
      const item = event.dataTransfer.items[i];

      if (
        item.kind === 'file'
        && valid_ext.indexOf(item.type.split('/')[1]) !== -1
      ) {
        const image_file = item.getAsFile();
        this.setState({image_file});
        this.prepare_logo_preview(image_file, 'image_data');
        break;
      }
    }
  }

  render() {
    const { name, logo, city, edit_mode } = this.props.school;
    const base_class = 'school-logo';
    const logo_style = {};
    let logo_class = `${base_class} ${base_class}--create`;
    let school_form_buttons;

    // Preparing logo style and class names
    if (this.state.image_data) {
      logo_style.backgroundImage = `url(${this.state.image_data})`;
    }
    else {
      logo_class = `${logo_class} ${base_class}--no-image ${base_class}--school`;
    }

    // Preparing school form buttons
    if (edit_mode) {
      const { finish_edit_school, dispatch } = this.props;

      school_form_buttons = (
        <SchoolEditButtons
          finish_edit_school={finish_edit_school}
          dispatch={dispatch}
          clear_school_logo={this.clear_school_logo}
          logo={this.state.image_file}
        />
      );
    }
    else {
      school_form_buttons = (
        <SchoolCreateButton
          clear_school_logo={this.clear_school_logo}
          logo={this.state.image_file}
        />
      );
    }

    return (
      <form className="school-article__info" onSubmit={this.send_school_form}>
        <div className="school-article__info_fields">
          <div
            className={logo_class}
            style={logo_style}
            onClick={this.pick_file}
            onDragOver={drag_file}
            onDrop={this.drop_file}
          />
          <input
            type="file"
            className="file-hidden"
            name="logo"
            accept={this.state.extensions}
            onChange={this.set_image_file}
            ref={el => this.input_file = el}
          />
          <SchoolFields name={name} logo={logo} city={city} />
        </div>
        {school_form_buttons}
      </form>
    )
  }
}

SchoolForm.propTypes = {
  school: PropTypes.object.isRequired,
  get_school: PropTypes.func.isRequired,
  set_current_user_school: PropTypes.func.isRequired,
  update_info_block: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  get_school,
  set_current_user_school,
  update_info_block,
  show_error_block
}

export default connect(null, map_dispatch_to_props)(SchoolForm);
