import React from 'react';
import PropTypes from 'prop-types';
import UnderlineInput from "../UnderlineInput";
import HeaderSearch from "./HeaderSearch";
import { connect } from "react-redux";
import { change_nav_visibility } from "../../actions/NavActionCreators";

class MainHeader extends React.Component {
  constructor() {
    super();
    this.show_nav = this.show_nav.bind(this);
  }

  show_nav() {
    this.props.change_nav_visibility(true);
  }

  render() {
    const { photo } = this.props.current_user.profile;
    const owl_style = {};

    if (photo && typeof photo === "string") {
      owl_style.backgroundImage = `url('${photo}')`;
    }

    const { first_name, last_name } = this.props.current_user;
    let welcome_heading;

    if (first_name && last_name) {
      welcome_heading = (
        <h1 className="main-header__greeting">
          Hello, <strong>{`${first_name} ${last_name}`}</strong>
        </h1>
      )
    }

    return (
      <header className="main-header">
        <div className="main-header__part">
          <div
            className="logo-owl logo-owl--animation logo-owl--home"
            onClick={this.show_nav}
          >
            <div
              className="logo-owl__image logo-owl__image--avatar"
              style={owl_style}
            />
            <div className="logo-owl__image logo-owl__image--color" />
          </div>
          {welcome_heading}
        </div>
        <HeaderSearch />
      </header>
    )
  }
}

MainHeader.propTypes = {
  current_user: PropTypes.object.isRequired,
  change_nav_visibility: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
};

const map_dispatch_to_props = {change_nav_visibility};

export default connect(map_state_to_props, map_dispatch_to_props)(MainHeader);
