import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { hide_info_block } from "../../actions/InfoBlockActionCreators";

class InfoBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      display: 'none',
      opacity: 0
    };
  }

  componentDidUpdate() {
    const { visible } = this.props.info_block;

    if (visible && this.state.display === 'none') {
      this.setState({display: 'block'});
    }
    else if (visible && this.state.display === 'block') {
      setTimeout(() => this.setState({opacity: 1}), 400);
    }
    else if (!visible && this.state.opacity === 1) {
      this.setState({opacity: 0});
    }
    else if (!visible && this.state.display === 'block') {
      setTimeout(() => this.setState({display: 'none'}), 400);
    }
  }

  render() {
    const { heading, text, type, clickable } = this.props.info_block;
    let info_block_class = "info-block";
    let touch_icon;

    if (type) {
      info_block_class = `${info_block_class} ${info_block_class}--${type}`;
    }

    if (clickable) {
      touch_icon = (
        <i className="material-icons info-block__icon info-block__icon--blink">
          touch_app
        </i>
      );
    }

    return (
      <div
        className={info_block_class}
        style={this.state}
        onClick={this.props.hide_info_block}
      >
      	<article className="info-block__content">
      		<h2 className="info-block__heading">{heading}</h2>
      		<p className="info-block__text">{text}</p>
      	</article>
    		{touch_icon}
      </div>
    )
  }
}

InfoBlock.propTypes = {
  info_block: PropTypes.object.isRequired,
  hide_info_block: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {info_block: state.info_block};
};

const map_dispatch_to_props = {hide_info_block};

export default connect(map_state_to_props, map_dispatch_to_props)(InfoBlock);
