import React from 'react';
import MainHeader from "./MainHeader";
import InfoBlock from "./InfoBlock";
import Menu from "./nav_menu/Menu";
import Customize from '../customization/Customize';
import School from '../school/School';
import GameSetup from '../game/GameSetup';
import PlayGame from "../play/PlayGame";
import PlayClassGroup from "../play/PlayClassGroup";
import Dashboard from "../dashboard/Dashboard";
import User from "../user/User";
import Cockpit from "../cockpit/Cockpit";
import Search from "../search/Search";
import Settings from "../settings/Settings";
import { Switch, Route } from 'react-router-dom';

export default class ContentTemplate extends React.Component {
  render() {
    return(
      <div>
        <InfoBlock />
        <MainHeader />
        <Menu />
        <main>
          <Switch>
            <Route exact path="/game" component={GameSetup} />
            <Route exact path="/game/:class_group_id" component={GameSetup} />
            <Route exact path="/play/" component={PlayClassGroup} />
            <Route exact path="/play/:game_id" component={PlayGame} />
            <Route exact path="/school" component={School} />
            <Route exact path="/school/:school_id" component={School} />
            <Route exact path="/dashboard/:class_group_id" component={Dashboard} />
            <Route exact path="/user/:user_id" component={User} />
            <Route exact path="/customize" component={Customize} />
            <Route exact path="/search/:query" component={Search} />
            <Route exact path="/settings" component={Settings} />
            <Route exact path="/" component={Cockpit} />
            <Route exact path="*" component={Cockpit} />
          </Switch>
        </main>
      </div>
    )
  }
}
