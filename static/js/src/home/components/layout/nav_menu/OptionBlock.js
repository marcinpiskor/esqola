import React from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { change_nav_visibility } from "../../../actions/NavActionCreators";

class OptionBlock extends React.Component {
  constructor() {
    super();
    this.hide_nav = this.hide_nav.bind(this);
  }

  hide_nav() {
    this.props.change_nav_visibility(false);
  }

  render() {
    const { path, name } = this.props;
    const base_class = "option-block__icon";
    let option_icon_class = `${base_class} ${base_class}--nav ${base_class}--${name.toLowerCase()}`;

    return (
      <Link
        to={path}
        className="option-block option-block--nav"
        onClick={this.hide_nav}
      >
        <h4 className="option-block__heading">{name}</h4>
        <div className={option_icon_class} />
      </Link>
    )
  }
}

OptionBlock.propTypes = {
  path: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  change_nav_visibility: PropTypes.func.isRequired
}

const map_dispatch_to_props = {change_nav_visibility};

export default connect(null, map_dispatch_to_props)(OptionBlock);
