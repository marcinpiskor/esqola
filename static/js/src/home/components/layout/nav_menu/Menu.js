import React from "react";
import PropTypes from 'prop-types';
import OptionBlocks from "./OptionBlocks";
import { connect } from "react-redux";
import { change_nav_visibility } from "../../../actions/NavActionCreators";

class Menu extends React.Component {
  constructor() {
    super();
    this.state = {touch: false, value: null};

    this.hide_nav = this.hide_nav.bind(this);
    this.touch_show_or_hide = this.touch_show_or_hide.bind(this);
  }

  componentDidMount() {
    document.body.addEventListener('touchstart', (event) => {
      const close_touch = this.get_close_touch(event.targetTouches);

      if (!this.owl_logo_clicked(event.targetTouches) && !!close_touch) {
        this.setState({touch: true});
      }
    });

    document.body.addEventListener('touchmove', (event) => {
      if (this.state.touch) {
        const value = event.targetTouches[0].clientX/this.nav_el.offsetWidth;
        this.setState({value: parseInt(value*100)});
      }
    });

    document.body.addEventListener('touchcancel', this.touch_show_or_hide);
    document.body.addEventListener('touchend', this.touch_show_or_hide);
  }

  componentDidUpdate() {
    if (!this.state.touch) {
      if (this.props.nav.visible && this.state.value !== 100) {
        this.setState({value: 100});
      }
      else if (!this.props.nav.visible && this.state.value !== 0) {
        this.setState({value: 0});
      }
    }
  }

  touch_show_or_hide() {
    const visible = this.state.value > 50 ? true : false;
    this.setState({touch: false, value: visible ? 100 : 0});
    this.props.change_nav_visibility(visible);
  }

  owl_logo_clicked(touches) {
    for (let i=0; i < touches.length; i++) {
      if (touches[i].target.className.indexOf('logo-owl') !== -1) {
        return true;
      }
    }

    return false;
  }

  get_close_touch(touches) {
    const { visible } = this.props.nav;

    for (let i=0; i < touches.length; i++) {
      const left_side_touch = touches[i].clientX <= 15 && !visible;
      const right_side_touch = this.nav_el.offsetWidth - touches[i].clientX <= 15 && visible;

      if (left_side_touch || right_side_touch) {
        return touches[i];
      }
    }
  }

  hide_nav() {
    this.props.change_nav_visibility(false);
  }

  render() {
    let nav_class = "home-nav";

    if (this.props.nav.visible && !this.state.touch) {
      nav_class = `${nav_class} ${nav_class}--visible`;
    }

    let nav_style = {};

    if (!this.state.touch) {
      nav_style.transition = ".6s transform";
    }
    else if (this.state.value) {
      const width = -100 + this.state.value;
      const sign = this.props.nav.visible ? '-' : '+';
      const value = `translateX(calc(${width > 0 ? 0 : width}% ${sign} .5em))`;

      nav_style.transform = value;
    }

    return (
      <nav className={nav_class} style={nav_style} ref={el => this.nav_el = el}>
        <h2 className="home-nav__heading home-nav__main-heading">
          <span className="pink-circle home-nav__pink-circle">Esqola</span>
        </h2>
        <OptionBlocks />
        <h3
          className="home-nav__heading home-nav__close-heading"
          onClick={this.hide_nav}
        >
          Close
        </h3>
      </nav>
    )
  }
}

Menu.propTypes = {
  nav: PropTypes.object.isRequired,
  change_nav_visibility: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {nav: state.nav};
}

const map_dispatch_to_props = {change_nav_visibility};

export default connect(map_state_to_props, map_dispatch_to_props)(Menu);
