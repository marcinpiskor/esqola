import React from "react";
import PropTypes from 'prop-types';
import OptionBlock from "./OptionBlock";
import { connect } from "react-redux";

const links = [
  {path: '/', name: 'Home'},
  {path: '/settings', name: 'Settings'},
]

class OptionBlocks extends React.Component {
  render() {
    const option_blocks = links.map(link => {
      return <OptionBlock key={link.path} {...link} />;
    });

    if (this.props.current_user.profile.role) {
      switch (this.props.current_user.profile.role.name) {
        case "principals":
          option_blocks.push(
            <OptionBlock key="school" path="/school" name="School" />
          )
          break;
        case "teachers":
          let teacher_dashboard;

          if (this.props.current_user.teacher_classes.length > 0) {
            const teacher_id = this.props.current_user.teacher_classes[0].id;

            teacher_dashboard = (
              <OptionBlock
                key="dashboard"
                path={`/dashboard/${teacher_id}`}
                name="Dashboard"
              />
            );
          }

          option_blocks.push(
            teacher_dashboard,
            <OptionBlock key="game" path="/game" name="Game" />
          )
          break;
        case "students":
          let student_dashboard;

          if (this.props.current_user.student_classes.length > 0) {
            const student_id = this.props.current_user.student_classes[0].id;

            student_dashboard = (
              <OptionBlock
                key="dashboard"
                path={`/dashboard/${student_id}`}
                name="Dashboard"
              />
            );
          }

          option_blocks.push(
            student_dashboard,
            <OptionBlock key="play" path="/play" name="Play" />
          )
          break;
      }
    }

    return (
      <div className="home-nav__option-wrapper">
        {option_blocks}
        <a href="/logout" className="option-block option-block--nav">
          <h4 className="option-block__heading">Sign out</h4>
          <div className="option-block__icon option-block__icon--nav option-block__icon--sign-out" />
        </a>
      </div>
    )
  }
}

OptionBlocks.propTypes = {
  current_user: PropTypes.object.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
}

export default connect(map_state_to_props)(OptionBlocks);
