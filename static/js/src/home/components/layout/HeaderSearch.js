import React from 'react';
import UnderlineInput from "../UnderlineInput";
import { withRouter } from 'react-router';

class HeaderSearch extends React.Component {
  constructor() {
    super();
    this.state = {
      suggestions: [],
      value: "",
    }

    this.get_suggestions = this.get_suggestions.bind(this);
    this.redirect_to_search = this.redirect_to_search.bind(this);
    this.choose_suggestion = this.choose_suggestion.bind(this);
  }

  get_suggestions(event) {
    const search_query = event.target.value;

    if (search_query.trim().length >= 2 && search_query.trim() !== this.state.value) {
      fetch(`/api/search/autocomplete/?query=${search_query.trim()}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'}
      })
      .then(response => response.json())
      .then(data => {
        const suggestions = [];

        if (search_query.trim().split(" ").length >= 2) {
          const user_data = []

          data.users.forEach(user => {
            const full_name = `${user.first_name} ${user.last_name}`;

            if (!user_data.find(u => u.content === full_name)) {
              user_data.push({id: user.id, content: full_name});
            }
          });

          suggestions.push(...user_data);
        }
        else {
          const user_data = data.users.map(u => {
            return {content: u.username, id: u.id};
          });

          suggestions.push(...user_data);
        }

        const school_data = new Set(
          data.schools.map(s => {
            return {id: s.id, content: s.name}
          })
        );
        suggestions.push(...school_data);

        this.setState({suggestions, value: search_query});
      })
      .catch(err => console.error(err))
    }
    else {
      this.setState({suggestions: [], value: search_query});
    }
  }

  choose_suggestion(suggestion) {
    this.setState({value: suggestion.content});
  }

  redirect_to_search(event) {
    event.preventDefault();
    const search_query = new FormData(event.target).get("search");

    if (search_query.trim().length >= 2) {
      this.props.history.push(`/search/${this.state.value}`);
    }
  }

  render() {
    return (
      <form
        className="main-header__part main-header__part--search"
        onSubmit={this.redirect_to_search}
      >
        <UnderlineInput
          additional_classes={"form-field__wrapper--search"}
          suggestions={this.state.suggestions}
          choose_suggestion={this.choose_suggestion}
        >
          <input
            className="form-field"
            type="search"
            name="search"
            autoComplete="off"
            placeholder="Search for users, schools etc."
            value={this.state.value}
            onChange={this.get_suggestions}
          />
        </UnderlineInput>
        <button className="search-submit">
          <i className="material-icons search-submit--icon">zoom_in</i>
        </button>
      </form>
    )
  }
}

export default withRouter(HeaderSearch);
