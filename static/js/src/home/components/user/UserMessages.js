import React from 'react';
import PropTypes from "prop-types";
import UserMessage from "./UserMessage";

export default class UserMessages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: props.user.id,
      messages: [],
      offset: 0,
      limit: 20,
      count: 0,
      chat_socket: null
    };

    this.load_messages = this.load_messages.bind(this);
    this.send_message = this.send_message.bind(this);
    this.chat_message = this.chat_message.bind(this);
    this.handle_messages_scroll = this.handle_messages_scroll.bind(this);
  }

  componentWillMount() {
    this.load_messages();
  }

  componentDidUpdate() {
    if (this.props.user.id !== this.state.user_id) {
      const state_update = {
        offset: 0,
        count: 0,
        messages: [],
        user_id: this.props.user.id
      };

      this.setState(state_update, () => this.load_messages());
    }
  }

  load_messages() {
    const { offset, limit, user_id } = this.state;

    fetch(`/api/message/?limit=${limit}&offset=${offset}&user_id=${user_id}`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'content-type': 'application/json',
      }
    })
    .then(response => response.json())
    .then(data => {
      // If there are any messages, load these messages and connect with chat
      // socket if this hasn't been done yet
      if (data.count) {
        const { count, results } = data;
        const update_obj = {
          count,
          messages: [].concat(results.reverse(), this.state.messages),
          offset: offset + results.length
        };

        if (!this.state.chat_socket) {
          const chat_socket = new WebSocket(
            `ws://${window.location.host}/ws/chat/${user_id}/`
          );
          chat_socket.onmessage = this.chat_message;
          update_obj.chat_socket = chat_socket;
          this.setState(update_obj, () => {
            this.messages_box.scrollTop = this.messages_box.scrollHeight
          });
        }

        // Keep the vertical scroll bar position after loading messages
        else {
          const scroll_height_before = this.messages_box.scrollHeight - this.messages_box.scrollTop;
          this.setState(update_obj, () => this.messages_box.scrollTop = this.messages_box.scrollHeight - scroll_height_before);
        }
      }
      // Otherwise, only connect with chat socket if it is necessary
      else if (!this.state.chat_socket || this.state.count === 0) {
        const chat_socket = new WebSocket(
          `ws://${window.location.host}/ws/chat/${user_id}/`
        );
        chat_socket.onmessage = this.chat_message;
        this.setState({chat_socket});
      }
    })
    .catch(err => console.error(err))
  }

  componentWillUnmount() {
    if (this.state.chat_socket) {
      this.state.chat_socket.close();
    }
  }

  chat_message(event) {
    const {type, message} = JSON.parse(event.data);

    switch (type) {
      case "msg_created":
        this.setState({
          messages: [].concat(this.state.messages, message),
          offset: this.state.offset + 1,
          count: this.state.count + 1
        });
        break;
    }
  }

  send_message(event) {
    event.preventDefault();
    const form_data = new FormData(event.target);

    if (form_data.get("content").trim().length > 0) {
      this.state.chat_socket.send(JSON.stringify({
        type: "create_msg", data: {content: form_data.get("content")}
      }));
      event.target.reset();
    }
  }

  handle_messages_scroll(event) {
    if (event.target.scrollTop === 0 && this.state.offset < this.state.count) {
      this.load_messages();
    }
  }

  render() {
    let load_button;

    if (this.state.offset < this.state.count && this.state.count !== 0) {
      load_button = (
        <button
          className="index-forms__button index-forms__button--center"
          onClick={this.load_messages}
        >
          Load messages
        </button>
      );
    }

    return (
      <div className="user-messages">
        <div
          className="user-messages__box"
          onScroll={this.handle_messages_scroll}
          ref={(el) => this.messages_box = el}
        >
          {load_button}
          {this.state.messages.map(msg => {
            return (
              <UserMessage
                key={msg.created}
                message={msg}
                current_user_id={this.props.current_user.id}
              />
            );
          })}
        </div>
        <form className="user-messages__form" onSubmit={this.send_message}>
          <textarea name="content" className="user-messages__input" required />
          <button className="user-messages__button">
            <i className="material-icons">send</i>
          </button>
        </form>
      </div>
    )
  }
}

UserMessages.propTypes = {
  user: PropTypes.object.isRequired,
  current_user: PropTypes.object.isRequired
}
