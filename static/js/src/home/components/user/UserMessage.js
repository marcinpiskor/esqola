import React from 'react';
import PropTypes from "prop-types";
import { prepare_date_detail } from "../dashboard/discussion/post/prepare_date";

export default class UserMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date_formatted: prepare_date_detail(new Date(props.message.created))
    }
  }

  render() {
    let message_class = "user-message";

    if (this.props.current_user_id === this.props.message.author) {
      message_class = `${message_class} ${message_class}--current-user`;
    }

    return (
      <div className={message_class} data-date={this.state.date_formatted}>
        <div className="user-message__content">
          {this.props.message.content}
        </div>
      </div>
    )
  }
}

UserMessage.propTypes = {
  message: PropTypes.object.isRequired,
  current_user_id: PropTypes.number.isRequired
}
