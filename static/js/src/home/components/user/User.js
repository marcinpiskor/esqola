import React from 'react';
import PropTypes from "prop-types";
import UserCard from "../customization/content/preview/UserCard";
import UserMessages from "./UserMessages";
import { Redirect } from 'react-router'
import { connect } from "react-redux";
import { show_error_block } from "../../actions/InfoBlockActionCreators";

class User extends React.Component {
  constructor() {
    super();
    this.state = {
      user_data: null,
      in_progress: false,
      redirect: false
    };
  }

  componentWillMount() {
    if (this.props.current_user.profile.is_activated) {
      this.get_user_data();
    }
  }

  componentDidUpdate() {
    if (this.props.current_user.profile.is_activated) {
      this.get_user_data();
    }
  }

  get_user_data() {
    const { user_id } = this.props.match.params

    if (this.props.current_user.id && !this.state.in_progress) {
      if (this.props.current_user.id == user_id) {
        this.setState({redirect: true});
      }
      else if (!this.state.user_data || this.state.user_data.id != user_id) {
        this.fetch_user_data(user_id);
      }
    }
  }

  fetch_user_data(user_id) {
    let success;
    this.setState({in_progress: true});

    fetch(`/api/user/${user_id}/`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'content-type': 'application/json',
      }
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(user_data => {
      if (success) {
        document.title = `Chat with @${user_data.username} - Esqola`;
        this.setState({user_data, in_progress: false});
      }
      else {
        this.setState({redirect: true});
        this.props.show_error_block(user_data);
      }
    })
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />
    }
    else if (this.state.user_data) {
      return (
        <section
          className="school-article school-article--user school-article--fade-in"
        >
          <UserCard
            user={this.state.user_data}
            is_current_user={false}
            additional_classes="customize-user-card--user"
          />
          <UserMessages
            user={this.state.user_data}
            current_user={this.props.current_user}
          />
        </section>
      )
    }
    else {
      return "";
    }
  }
}

User.propTypes = {
  current_user: PropTypes.object.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user}
}

const map_dispatch_to_props = {show_error_block};

export default connect(map_state_to_props, map_dispatch_to_props)(User);
