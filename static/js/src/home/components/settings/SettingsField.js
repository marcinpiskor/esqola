import React from "react";
import PropTypes from 'prop-types';
import UnderlineInput from "../UnderlineInput";

export default class SettingsField extends React.Component {
  constructor(props) {
    super(props);
    const label = props.name.replace(/_/g, " ");

    this.state = {
      name: props.name,
      field_type: props.name === "email" ? "email" : "text",
      label: `${label.charAt(0).toUpperCase()}${label.slice(1, label.length)}`,
      value: props.current_value ? props.current_value : "",
      additional_attrs: props.attributes ? props.attributes : {}
    }

    this.handle_value_change = this.handle_value_change.bind(this);
  }

  handle_value_change(event) {
    this.setState({value: event.target.value})
  }

  render() {
    return (
      <div>
        <label htmlFor={this.state.name} className="customize-option-label">
          {this.state.label}
        </label>
        <UnderlineInput additional_classes={"form-field__wrapper--full"}>
          <input
            className="form-field"
            name={this.state.name}
            type={this.state.field_type}
            value={this.state.value}
            {...this.state.additional_attrs}
            onChange={this.handle_value_change}
            required
          />
        </UnderlineInput>
      </div>
    )
  }
}

SettingsField.propTypes = {
  name: PropTypes.string.isRequired,
  current_value: PropTypes.string,
  attributes: PropTypes.object
}
