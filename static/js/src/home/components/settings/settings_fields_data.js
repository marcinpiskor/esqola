export const text_fields = ['email', 'username', 'first_name', 'last_name'];
export const field_attrs = {
  username: {
    minLength: 4,
    maxLength: 150,
    pattern: '^(?=.*[a-zA-Z]).*$',
    title: 'Username needs to contain at least one character.'
  },
  first_name: {
    minLength: 2,
    maxLength: 30,
    pattern: '^[a-zA-Z]+$',
    title: 'First name need to contain only characters.'
  },
  last_name: {
    minLength: 2,
    maxLength: 150,
    pattern: '^[a-zA-Z ]+$',
    title: 'Last name needs to contain characters and spaces.'
  },
  password: {
    type: 'password',
    pattern: '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$',
    title: 'Password needs to have at least one small/big char and number.',
    minLength: 6
  }
}
