import React from 'react';
import PropTypes from 'prop-types';
import CustomField from '../customization/content/fields/CustomField';
import SettingsField from './SettingsField';
import { field_attrs } from './settings_fields_data';
import get_csrf_token from '../../csrf';
import { connect } from 'react-redux';
import {
  update_info_block, show_error_block
} from '../../actions/InfoBlockActionCreators';

class SettingsPasswordChange extends React.Component {
  constructor() {
    super();
    this.state = {
      password_names: ['old_password', 'new_password', 'repeat_new_password'],
      password_attrs: field_attrs.password
    };

    this.send_password_change_form = this.send_password_change_form.bind(this);
  }

  send_password_change_form(event) {
    event.preventDefault();
    const form_data = new FormData(event.target);

    if (form_data.get('new_password') === form_data.get('repeat_new_password')) {
      form_data.delete('repeat_new_password');
      this.post_password_change_form(form_data);
    }
    else {
      this.props.show_error_block(['Passwords are not the same.']);
    }
  }

  post_password_change_form(form_data) {
    let success;

    fetch(`/api/current-user/change-password/`, {
      method: 'POST',
      body: form_data,
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(data => {
      if (success) {
        this.success_password();
      }
      else {
        this.props.show_error_block(data);
      }
    })
    .catch(err => console.error(err))
  }

  success_password() {
    const info_data = {
      heading: 'Success',
      text: 'Your password has been changed',
      clickable: true,
      type: 'success'
    };

    this.props.update_info_block(info_data);
  }

  render() {
    const password_fields = this.state.password_names.map(field => {
      return (
        <CustomField step={1} key={field} name={field}>
          <SettingsField name={field} attributes={this.state.password_attrs} />
        </CustomField>
      );
    });

    return (
      <form onSubmit={this.send_password_change_form}>
        <div className='customize-article__card customize-article__card--password'>
          <div className='customize-fields'>
            {password_fields}
          </div>
        </div>
        <button className='index-forms__button index-forms__button--add'>
          Change password
        </button>
      </form>
    );
  }
}

SettingsPasswordChange.propTypes = {
  update_info_block: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  update_info_block,
  show_error_block
}

export default connect(null, map_dispatch_to_props)(SettingsPasswordChange);
