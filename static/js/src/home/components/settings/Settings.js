import React from 'react';
import PropTypes from 'prop-types';
import UserCard from '../customization/content/preview/UserCard';
import CustomField from '../customization/content/fields/CustomField';
import BirthdayField from '../customization/content/fields/BirthdayField';
import CityField from '../customization/content/fields/CityField';
import PhotoField from '../customization/content/fields/PhotoField';
import SettingsField from './SettingsField';
import SettingsPasswordChange from './SettingsPasswordChange';
import { text_fields, field_attrs } from './settings_fields_data';
import get_csrf_token from '../../csrf';
import { connect } from 'react-redux';
import {
  update_info_block,
  show_error_block
} from '../../actions/InfoBlockActionCreators';
import { get_current_user } from '../../actions/CurrentUserActionCreators';

const components = {
  email: SettingsField,
  username: SettingsField,
  first_name: SettingsField,
  last_name: SettingsField,
  city: CityField,
  birthday: BirthdayField,
  photo: PhotoField
}

class Settings extends React.Component {
  constructor(props) {
    super(props);
    const { current_user, dispatch } = props

    this.state = {
      original_photo: current_user.profile ? current_user.profile.photo : null
    };

    this.send_settings_form = this.send_settings_form.bind(this);
    this.prepare_form_data = this.prepare_form_data.bind(this);
    this.prepare_settings_field_props = this.prepare_settings_field_props.bind(this);
  }

  componentDidMount() {
    document.title = 'Settings - Esqola';
  }

  componentDidUpdate() {
    const { profile } = this.props.current_user;

    if (
      !this.state.original_photo
      && profile.photo
      && typeof profile.photo === 'string'
    ) {
      this.setState({original_photo: profile.photo});
    }
  }

  componentWillUnmount() {
    if (
      this.state.original_photo ||
      this.props.current_user.profile.photo instanceof File
    ) {
      const { photo } = this.props.current_user.profile;

      // If user has changed photo file, but hasn't sent changes, set user
      // photo to previous one
      if (this.state.original_photo !== photo) {
        this.props.current_user.profile.photo = this.state.original_photo;
        const update_obj = {profile: this.props.current_user.profile};
        this.props.get_current_user(update_obj);
      }
    }
  }

  send_settings_form(event) {
    event.preventDefault();
    const form_data = new FormData(event.target);
    const { settings_form, user_data_changed } = this.prepare_form_data(form_data);

    // Send settings form only if current user data has been changed
    if (user_data_changed) {
      this.update_user_data(settings_form)
    }
  }

  prepare_form_data(form_data) {
    const { photo, city } = this.props.current_user.profile;

    const settings_form = new FormData();
    let user_data_changed = false;

    // Check if value of current user text fields (e-mail, username etc) have
    // been changed
    for (let name of text_fields) {
      const value = form_data.get(name);

      if (value && value.trim() !== this.props.current_user[name]) {
        settings_form.set(name, value);
        user_data_changed = true;
      }
    }

    // Check if current user city has been changed
    const place_id = form_data.get('profile.city.place_id');

    if (place_id && place_id !== city.place_id) {
      settings_form.set('profile.city.place_id', place_id);
      user_data_changed = true;
    }

    // Check if current user photo has been changed
    if (photo !== this.state.original_photo) {
      settings_form.set('profile.photo', photo ? photo : '');
      user_data_changed = true;
    }

    const birthday_fields = ['year', 'month', 'day'];
    const all_birthday_fields_filled = birthday_fields.every(field => {
      return form_data.get(field);
    });

    // Check current user birthday field only if all birthday fields have been
    // filled
    if (all_birthday_fields_filled) {
      const format = (x) => String(x).length === 1 ? `0${x}`: x;
      const month = format(parseInt(form_data.get('month')));
      const day = format(form_data.get('day'));

      const birthday_value = `${form_data.get('year')}-${month}-${day}`;
      const current_user_birthday = new Date(
        this.props.current_user.profile.birthday
      );

      if (new Date(birthday_value).toDateString() !== current_user_birthday.toDateString()) {
        settings_form.set('profile.birthday', birthday_value);
        user_data_changed = true;
      }
    }

    return {settings_form, user_data_changed};
  }

  update_user_data(settings_form) {
    let success;

    fetch(`/api/current-user/${this.props.current_user.id}/`, {
      method: 'PATCH',
      body: settings_form,
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(data => {
      if (success) {
        this.success_update(data);
      }
      else {
        this.props.show_error_block(data);
      }
    })
  }

  success_update(user_data) {
    this.props.get_current_user(user_data);

    const info_data = {
      heading: 'Success',
      text: 'Your account has been successfully updated.',
      clickable: true,
      type: 'success'
    };

    this.props.update_info_block(info_data);
  }

  prepare_settings_field_props(field_name) {
    const field_props = {};

    // Text fields do not have separete field components (like e.g. photo, city)
    // so name and attributes are required for inputs
    if (text_fields.indexOf(field_name) !== -1) {
      field_props.current_value = this.props.current_user[field_name];
      field_props.name = field_name;

      if (field_name !== 'email') {
        field_props.attributes = field_attrs[field_name];
      }
    }
    else {
      field_props.current_value = this.props.current_user.profile[field_name];
    }

    return field_props;
  }

  render() {
    if (this.props.current_user.id && this.props.current_user.profile.is_activated) {
      const settings_fields = Object.keys(components).map(key => {
        const field_props = this.prepare_settings_field_props(key);

        return (
          <CustomField step={1} key={key} name={key}>
            {React.createElement(components[key], field_props)}
          </CustomField>
        );
      });

      return (
        <section className='customize-section'>
          <article className='customize-article'>
            <form onSubmit={this.send_settings_form}>
              <div className='customize-article__card'>
                <div className='customize-fields'>
                  {settings_fields}
                </div>
              </div>
              <button className='index-forms__button index-forms__button--save'>
                Save
              </button>
            </form>
            <div></div>
            <div>
              <UserCard
                user={this.props.current_user}
                is_current_user={false}
              />
              <SettingsPasswordChange />
            </div>
          </article>
        </section>
      );
    }
    else {
      return '';
    }
  }
}

Settings.propTypes = {
  current_user: PropTypes.object.isRequired,
  get_current_user: PropTypes.func.isRequired,
  update_info_block: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
}

const map_dispatch_to_props = {
  get_current_user,
  update_info_block,
  show_error_block
}

export default connect(map_state_to_props, map_dispatch_to_props)(Settings)
