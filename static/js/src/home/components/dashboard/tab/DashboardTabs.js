import React from 'react';
import PropTypes from 'prop-types';
import DashboardTab from './DashboardTab';

export default class DashboardTabs extends React.Component {
  render() {
    const { active_tab, set_active_tab, tabs } = this.props;

    return (
      <div className='dashboard-content__header'>
        {tabs.map(tab => {
          return (
            <DashboardTab
              key={tab}
              tab={tab}
              active_tab={active_tab}
              set_active_tab={set_active_tab}
            />
          );
        })}
      </div>
    )
  }
}

DashboardTabs.propTypes = {
  set_active_tab: PropTypes.func.isRequired,
  active_tab: PropTypes.string.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
}
