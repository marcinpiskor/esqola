import React from 'react';
import PropTypes from 'prop-types';

export default class DashboardTab extends React.Component {
  constructor() {
    super();

    this.set_active_tab = this.set_active_tab.bind(this);
  }

  set_active_tab() {
    this.props.set_active_tab(this.props.tab);
  }

  render() {
    const { active_tab, tab } = this.props;
    let tab_class = 'dashboard-content__header_tab';

    if (active_tab === tab) {
      tab_class = `${tab_class} ${tab_class}--active`;
    }

    tab_class = `${tab_class} no-select`;

    return (
      <div className={tab_class} onClick={this.set_active_tab}>{tab}</div>
    )
  }
}

DashboardTab.propTypes = {
  set_active_tab: PropTypes.func.isRequired,
  tab: PropTypes.string.isRequired,
  active_tab: PropTypes.string.isRequired
}
