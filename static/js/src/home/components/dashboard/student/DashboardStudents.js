import React from 'react';
import PropTypes from 'prop-types';
import ClassGroupDetailInfo from "../../school/detail/classgroup/detail/ClassGroupDetailInfo";

export default class DashboardStudents extends React.Component {
  render() {
    const is_teacher = (
      this.props.class_group.teacher &&
      this.props.class_group.teacher.id === this.props.current_user.id
    );

    return (
      <ClassGroupDetailInfo
        is_teacher={is_teacher}
        index={0}
        class_group={this.props.class_group}
      />
    );
  }
}

DashboardStudents.propTypes = {
  class_group: PropTypes.object,
  current_user: PropTypes.object.isRequired
}
