import React from 'react';
import PropTypes from 'prop-types';
import DashboardHeader from './DashboardHeader';
import DashboardContent from './DashboardContent';
import { connect } from 'react-redux';
import { set_class_group_detail_info } from '../../actions/SchoolActionCreators';
import {
  add_new_post,
  add_post_to_waiting,
  clear_discussion
} from '../../actions/DiscussionActionCreators';
import { show_error_block } from '../../actions/InfoBlockActionCreators';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      in_progress: false,
      dashboard_socket: null
    }

    this.fetch_class_group = this.fetch_class_group.bind(this);
    this.dashboard_socket_message = this.dashboard_socket_message.bind(this);
  }

  componentWillMount() {
    if (this.props.current_user.profile.is_activated) {
      this.fetch_class_group();
    }
  }

  componentDidUpdate() {
    if (this.props.current_user.profile.is_activated) {
      this.fetch_class_group();
    }
  }

  componentWillUnmount() {
    if (this.state.dashboard_socket) {
      this.state.dashboard_socket.close();
    }

    this.props.clear_discussion();
  }

  fetch_class_group() {
    const { class_group_id } = this.props.match.params;

    if (
        !this.state.in_progress &&
        ((!this.props.class_group) ||
        (this.props.class_group && (this.props.class_group.id != class_group_id) || !this.state.dashboard_socket))
      ) {
      this.setState({in_progress: true});

      let success;

      fetch(`/api/class-group/${class_group_id}/`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'}
      })
      .then(response => {
        success = response.ok;
        return response.json();
      })
      .then(data => {
        if (success) {
          document.title = `${data.name} dashboard - Esqola`;

          this.props.set_class_group_detail_info(0, data);

          if (this.state.dashboard_socket) {
            this.state.dashboard_socket.close();
          }

          const dashboard_socket = new WebSocket(
            `ws://${window.location.host}/ws/dashboard/${data.id}/`
          );
          dashboard_socket.onmessage = this.dashboard_socket_message;
          this.setState({in_progress: false, dashboard_socket});
        }
        else {
          this.props.history.push('/');
          this.props.dispatch(show_error_block(data));
        }
      })
    }
  }

  dashboard_socket_message(event) {
    const {type, message} = JSON.parse(event.data);

    switch(type) {
      case 'dashboard_error':
        this.props.show_error_block(message);
        this.state.dashboard_socket.close();
        break;
      case 'post_created':
        if (this.props.current_user.id == message.author.id) {
          this.props.add_new_post(message);
        }
        else {
          this.props.add_post_to_waiting(message);
        }
        break;
      case 'post_failed':
        this.props.dispatch(show_error_block(message))
        break;
    }
  }

  render() {
    if (this.props.class_group && this.props.class_group.students) {
      return (
        <section className='school-article'>
          <DashboardHeader class_group_name={this.props.class_group.name} />
          <DashboardContent
            class_group={this.props.class_group}
            dashboard_socket={this.state.dashboard_socket}
            current_user={this.props.current_user}
          />
        </section>
      )
    }
    else {
      return '';
    }
  }
}

Dashboard.propTypes = {
  class_group: PropTypes.object,
  current_user: PropTypes.object.isRequired,
  add_new_post: PropTypes.func.isRequired,
  add_post_to_waiting: PropTypes.func.isRequired,
  clear_discussion: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {
    class_group: state.school.classes[0],
    current_user: state.current_user
  };
}

const map_dispatch_to_props = {
  set_class_group_detail_info,
  add_new_post,
  add_post_to_waiting,
  clear_discussion,
  show_error_block
};

export default connect(map_state_to_props, map_dispatch_to_props)(Dashboard);
