import React from 'react';
import PropTypes from 'prop-types';
import DashboardTabs from './tab/DashboardTabs';
import DashboardDiscussion from './discussion/DashboardDiscussion';
import DashboardStudents from './student/DashboardStudents';
import DashboardInfoTeacher from './info/DashboardInfoTeacher';
import DashboardInfoStudents from './info/DashboardInfoStudents';
import DashboardPlay from './info/DashboardPlay';
import GameReports from './report/GameReports';

const dashboard_components = {
  'Discussion': DashboardDiscussion,
  'Students': DashboardStudents,
  'Reports': GameReports
}

export default class DashboardContent extends React.Component {
  constructor() {
    super();
    this.state = {
      active_dashboard_component: Object.keys(dashboard_components)[0]
    }

    this.set_active_dashboard_component = this.set_active_dashboard_component.bind(this);
  }

  set_active_dashboard_component(component) {
    this.setState({active_dashboard_component: component});
  }

  render() {
    const content_props = {
      class_group: this.props.class_group,
      current_user: this.props.current_user,
      dashboard_socket: this.props.dashboard_socket
    };

    const is_teacher = (
      this.props.class_group.teacher &&
      this.props.class_group.teacher.id === this.props.current_user.id
    );

    return (
      <section className='dashboard-section'>
        <article className='dashboard-content'>
          <DashboardTabs
            tabs={Object.keys(dashboard_components)}
            active_tab={this.state.active_dashboard_component}
            set_active_tab={this.set_active_dashboard_component}
          />
          {React.createElement(
            dashboard_components[this.state.active_dashboard_component],
            content_props
          )}
        </article>
        <aside className='dashboard-info'>
          <DashboardPlay
            class_group_id={this.props.class_group.id}
            is_teacher={is_teacher}
          />
          <DashboardInfoTeacher teacher={this.props.class_group.teacher} />
          <DashboardInfoStudents
            students={this.props.class_group.students}
            set_active_tab={this.set_active_dashboard_component}
          />
        </aside>
      </section>
    )
  }
}

DashboardContent.propTypes = {
  class_group: PropTypes.object.isRequired,
  current_user: PropTypes.object.isRequired,
  dashboard_socket: PropTypes.instanceOf(WebSocket)
}
