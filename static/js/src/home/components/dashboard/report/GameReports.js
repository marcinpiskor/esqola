import React from 'react';
import GameReportBlock from './GameReportBlock';
import GameReportDetail from './GameReportDetail';
import PropTypes from 'prop-types';

export default class GameReports extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      active_report: null,
      active_class_group_id: props.class_group.id,
      reports: [],
      in_progress: false
    };

    this.set_active_report = this.set_active_report.bind(this);
  }

  componentWillMount() {
    this.get_game_reports();
  }

  componentDidUpdate() {
    if (
      !this.state.in_progress &&
      this.state.active_class_group_id !== this.props.class_group.id
    ) {
      this.get_game_reports();
    }
  }

  get_game_reports() {
    this.setState({in_progress: true});

    fetch(`/api/game-report/?class_group__id=${this.props.class_group.id}`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'content-type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(reports => {
      this.setState({
        reports,
        in_progress: false,
        active_class_group_id: this.props.class_group.id
      });
    })
  }

  set_active_report(report) {
    this.setState({active_report: report});
  }

  render() {
    let report_content;

    if (this.state.active_report) {
      return (
        <GameReportDetail
          report={this.state.active_report}
          set_active_report={this.set_active_report}
        />
      );
    }
    else if (this.state.reports) {
      if (this.state.reports.length > 0) {
        report_content = this.state.reports.map(report => {
          return (
            <GameReportBlock
              key={report.id}
              report={report}
              set_active_report={this.set_active_report}
            />
          );
        })
      }
      else {
        report_content = (
          <h4 className="cockpit-class-group__heading cockpit-class-group__heading--empty">
            No reports
          </h4>
        )
      }
    }

    return <div>{report_content}</div>;
  }
}

GameReports.propTypes = {
  class_group: PropTypes.object.isRequired,
}
