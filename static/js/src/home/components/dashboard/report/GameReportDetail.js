import React from 'react';
import PropTypes from 'prop-types';
import {
  prepare_date_text, prepare_date_detail
} from '../discussion/post/prepare_date';
import GameScore from '../../play/game/GameScore';

export default class GameReportDetail extends React.Component {
  constructor(props) {
    super(props);

    const date_fields = ['started', 'finished'];
    date_fields.forEach(field => {
      props.report[field] = prepare_date_detail(new Date(props.report[field]));
    });

    props.report.report_file.forEach(user => {
      const player_data = props.report.players.find(player => player.id == user.id);
      user.username = player_data ? player_data.username : 'Deleted account';
    })

    this.back_to_reports = this.back_to_reports.bind(this);
  }

  back_to_reports() {
    this.props.set_active_report(null);
  }

  render() {
    const {
      type_of_game, language, started, finished, report_file
    } = this.props.report;

    return (
      <div>
        <h2 className='game-example__heading game-example__heading--primary game-example__heading--game-name'>
          {type_of_game}
        </h2>
        <ul className='game-report-info'>
          <li className='game-report-info__el'>
            <b>{language.name}</b> language
          </li>
          <li className='game-report-info__el'>
            <b>started:</b> {started}
          </li>
          <li className='game-report-info__el'>
            <b>finished:</b> {finished}
          </li>
        </ul>
        <GameScore username='Username' score='Points' is_heading={true} />
        {report_file.map(result => {
          return (
            <GameScore
              key={result.id}
              username={result.username}
              score={result.points}
              is_heading={false}
            />
          );
        })}
        <button
          className='index-forms__button index-forms__button--center'
          onClick={this.back_to_reports}
        >
          Back to game reports
        </button>
      </div>
    )
  }
}

GameReportDetail.propTypes = {
  report: PropTypes.object.isRequired,
  set_active_report: PropTypes.func.isRequired
}
