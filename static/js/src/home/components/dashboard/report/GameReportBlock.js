import React from 'react';
import PropTypes from 'prop-types';
import game_types from '../../game/type/game_types';
import { prepare_date_detail } from '../discussion/post/prepare_date';

export default class GameReportBlock extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: game_types.find(g => g.short_type === props.report.type_of_game).text,
      date: prepare_date_detail(new Date(props.report.finished))
    }

    this.load_report = this.load_report.bind(this);
  }

  load_report() {
    fetch(`/api/game-report/${this.props.report.id}/`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'content-type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(report => {
      report.type_of_game = this.state.name;
      this.props.set_active_report(report);
    })
  }

  render() {
    return (
      <div className='game-report-block' onClick={this.load_report}>
        <div className='game-report-block__wrapper'>
          <div className='dashboard-info__play dashboard-info__play--report' />
          <i className='game-report-block__name'>{this.state.name}</i>
        </div>
        <div>{this.state.date}</div>
      </div>
    )
  }
}

GameReportBlock.propTypes = {
  report: PropTypes.object.isRequired,
  set_active_report: PropTypes.func.isRequired
}
