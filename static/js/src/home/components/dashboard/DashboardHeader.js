import React from 'react';
import PropTypes from 'prop-types';

export default class DashboardHeader extends React.Component {
  render() {
    return (
      <header className='school-article__info_fields school-article__info_fields--dashboard'>
        <div className='school-logo school-logo--no-image school-logo--dashboard' />
        <h2 className='school-name'>{this.props.class_group_name}</h2>
      </header>
    )
  }
}

DashboardHeader.propTypes = {
  class_group_name: PropTypes.string.isRequired
}
