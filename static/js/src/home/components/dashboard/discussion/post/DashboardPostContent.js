import React from 'react';
import PropTypes from 'prop-types';
import DashboardPostHeader from './DashboardPostHeader';
import get_csrf_token from '../../../../csrf';
import { connect } from 'react-redux';
import {
  load_post_comments,
  delete_post
} from '../../../../actions/DiscussionActionCreators';
import { show_error_block } from '../../../../actions/InfoBlockActionCreators';

class DashboardPostContent extends React.Component {
  constructor(props) {
    super(props);

    this.enable_post_edit = this.enable_post_edit.bind(this);
    this.enable_post_reply = this.enable_post_reply.bind(this);
    this.load_comments = this.load_comments.bind(this);
    this.delete_post = this.delete_post.bind(this);
  }

  enable_post_edit() {
    this.props.set_post_edit(true);
  }

  enable_post_reply() {
    this.props.set_post_reply();
  }

  load_comments() {
    if (this.props.comments_amount > 0) {
      let success;

      fetch(`/api/post/${this.props.post_id}/`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'}
      })
      .then(response => {
        success = response.ok;
        return response.json();
      })
      .then(data => {
        if (success) {
          this.props.load_post_comments(this.props.index, data.comments);
        }
        else {
          this.props.show_error_block(data);
        }
      })
    }
  }

  delete_post() {
    let success;

    fetch(`/api/post/${this.props.post_id}/`, {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      if (response.ok) {
        this.props.delete_post(this.props.index);
      }
      else {
        const error = {detail: "Post doesn't exist."};
        this.props.show_error_block(error);
      }
    })
  }

  render() {
    let comments_text;

    if (this.props.comments_amount === 1) {
      comments_text = '1 comment';
    }
    else {
      comments_text = `${this.props.comments_amount} comments`;
    }

    let post_buttons = [
      <button
        key={'comment'}
        className='dashboard-post__button dashboard-content__dot'
        onClick={this.load_comments}
      >
        <i className='material-icons'>comment</i>
        <span className='dashboard-post__button_text'>{comments_text}</span>
      </button>,
      <button
        key={'reply'}
        onClick={this.enable_post_reply}
        className='dashboard-post__button dashboard-content__dot'
      >
        <i className='material-icons'>add_comment</i>
        <span className='dashboard-post__button_text'>Reply</span>
      </button>
    ]

    if (this.props.author.id == this.props.current_user_id) {
      post_buttons = post_buttons.concat([
        <button
          key={'edit'}
          className='dashboard-post__button dashboard-content__dot'
          onClick={this.enable_post_edit}
        >
          <i className='material-icons'>edit</i>
          <span className='dashboard-post__button_text'>Edit</span>
        </button>,
        <button
          key={'delete'}
          className='dashboard-post__button dashboard-content__dot'
          onClick={this.delete_post}
        >
          <i className='material-icons'>delete</i>
          <span className='dashboard-post__button_text'>Delete</span>
        </button>
      ])
    }

    return (
      <div className='dashboard-post-content'>
        <DashboardPostHeader
          created={this.props.created}
          author={this.props.author}
        />
        <p className='dashboard-post__text'>{this.props.content}</p>
        <div className='dashboard-post__buttons'>
          {post_buttons}
        </div>
      </div>
    )
  }
}

DashboardPostContent.propTypes = {
  set_post_edit: PropTypes.func.isRequired,
  set_post_reply: PropTypes.func.isRequired,
  comments_amount: PropTypes.number.isRequired,
  post_id: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  author: PropTypes.object,
  content: PropTypes.string.isRequired,
  created: PropTypes.string.isRequired,
  current_user_id: PropTypes.number.isRequired,
  load_post_comments: PropTypes.func.isRequired,
  delete_post: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  load_post_comments,
  delete_post,
  show_error_block
}

export default connect(null, map_dispatch_to_props)(DashboardPostContent);
