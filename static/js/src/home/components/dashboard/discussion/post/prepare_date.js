const time_info = [
  {div: 60, name: "minute"},
  {div: 60, name: "hour"},
  {div: 24, name: "day"},
  {div: 7, name: "week"},
  {div: 52, name: "year"},
];

export function prepare_date_text(created) {
  let time_name = "second";
  let time_delta = (new Date() - created)/1000;

  for (let i=0; i < time_info.length; i++) {
    const value = time_delta/time_info[i].div;

    if (value < 1) {
      break;
    }
    else {
      time_delta = value;
      time_name = time_info[i].name;
    }
  }

  time_delta = Math.abs(parseInt(time_delta));

  return time_delta === 1 ? `1 ${time_name} ago` : `${time_delta} ${time_name}s ago`
}

export function prepare_date_detail(created) {
  const format = (x) => String(x).length === 1 ? `0${x}` : x;
  const day_info = `${format(created.getDate())}.${format(created.getMonth()+1)}.${created.getFullYear()}`;
  const time_info = `${format(created.getHours())}:${format(created.getMinutes())}`;

  return `${day_info} ${time_info}`;
}
