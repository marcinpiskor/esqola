import React from 'react';
import PropTypes from 'prop-types';
import { prepare_date_text, prepare_date_detail } from './prepare_date';
import { Link } from "react-router-dom";

export default class DashboardPostHeader extends React.Component {
  constructor(props) {
    super(props);

    const created = new Date(props.created);

    this.state = {
      post_date: prepare_date_text(created),
      date_formatted: prepare_date_detail(created)
    }
  }

  render() {
    let heading_content, avatar_style = {};

    if (this.props.author) {
      const { id, first_name, last_name } = this.props.author;
      heading_content = `${first_name} ${last_name}`;

      heading_content = (
        <Link to={`/user/${id}`} className="school-principal__text dashboard-content__dot">
          {first_name} {last_name}
        </Link>
      )

      if (this.props.author.profile.photo) {
        avatar_style.backgroundImage = `url(${this.props.author.profile.photo})`;
      }
    }
    else {
      heading_content = (
        <h4 className="school-principal__text dashboard-content__dot">
          Deleted account
        </h4>
      );
    }

    return (
      <header className="school-article__button_wrapper">
        <div
          className="school-principal__avatar school-principal__avatar--author"
          style={avatar_style}
        />
        {heading_content}
        <h4
          className="school-principal__text school-principal__text--date"
          title={this.state.date_formatted}
        >
          {this.state.post_date}
        </h4>
      </header>
    )
  }
}

DashboardPostHeader.propTypes = {
  created: PropTypes.string.isRequired,
  author: PropTypes.object
}
