import React from 'react';
import PropTypes from 'prop-types';
import DashboardPostContent from './DashboardPostContent';
import DashboardPostForm from './DashboardPostForm';
import DashboardComment from '../comment/DashboardComment';

export default class DashboardPost extends React.Component {
  constructor() {
    super();

    this.state = {
      edit: false,
      reply: false
    }

    this.set_post_edit = this.set_post_edit.bind(this);
    this.set_post_reply = this.set_post_reply.bind(this);
  }

  set_post_edit(value) {
    this.setState({edit: value});
  }

  set_post_reply() {
    this.setState({reply: true});
  }

  render() {
    const { id, content, created, author, comments } = this.props.post;
    let post_content, comments_section;

    if (this.state.edit) {
      post_content = (
        <DashboardPostForm
          mode='post'
          text={content}
          post_index={this.props.index}
          post_id={id}
          set_post_edit={this.set_post_edit}
        />
      );
    }
    else {
      post_content = (
        <DashboardPostContent
          index={this.props.index}
          post_id={id}
          created={created}
          content={content}
          author={author}
          comments={comments}
          comments_amount={comments.length}
          current_user_id={this.props.current_user_id}
          set_post_edit={this.set_post_edit}
          set_post_reply={this.set_post_reply}
        />
      );
    }

    const is_comment_to_display = comments.find(comment => {
      return typeof comment !== 'number'
    });

    if (is_comment_to_display) {
      comments_section = (
        <div className='dashboard-comments'>
          {comments.map((comment, i) => {
            if (typeof comment !== 'number') {
              return (
                <DashboardComment
                  post_index={this.props.index}
                  post_id={id}
                  comment_index={i}
                  key={i}
                  comment={comment}
                  current_user_id={this.props.current_user_id}
                />
              );
            }
            else {
              return '';
            }
          })}
          <DashboardPostForm
            mode='comment'
            post_index={this.props.index}
            post_id={id}
          />
        </div>
      );
    }
    else if (this.state.reply) {
      comments_section = (
        <div className='dashboard-comments'>
          <DashboardPostForm
            mode='comment'
            post_index={this.props.index}
            post_id={id}
          />
        </div>
      );
    }

    return (
      <div>
        <div className='dashboard-post'>
          {post_content}
        </div>
        <div className='dashboard-comments'>
          {comments_section}
        </div>
      </div>
    )
  }
}

DashboardPost.propTypes = {
  post: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  current_user_id: PropTypes.number.isRequired
}
