import React from "react";
import PropTypes from 'prop-types';
import get_csrf_token from "../../../../csrf";
import { connect } from "react-redux";
import {
  add_new_comment,
  update_post,
  update_comment,
} from "../../../../actions/DiscussionActionCreators";
import { show_error_block } from "../../../../actions/InfoBlockActionCreators";

class DashboardPostForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      content: props.text ? props.text : "",
    }

    this.send_post_form = this.send_post_form.bind(this);
    this.set_form_content = this.set_form_content.bind(this);
    this.cancel_edit = this.cancel_edit.bind(this);
  }

  send_post_form(event) {
    event.preventDefault();
    const form_data = new FormData(event.target);

    if (form_data.get("content").trim().length > 0) {
      if (
        this.props.post_id &&
        this.props.mode === "post" || this.props.comment_id
      ) {
        this.edit_post_or_coment(form_data);
      }
      else {
        if (this.props.mode === "post") {
          this.create_post(form_data);
        }
        else {
          this.create_comment(form_data);
        }
      }
    }
  }

  create_post(post_form) {
    const new_post_data = {
      type: "create_post",
      data: {
        content: post_form.get("content")
      }
    };

    this.props.dashboard_socket.send(JSON.stringify(new_post_data));
    this.setState({content: ""});
  }

  create_comment(form) {
    form.set("reply_to.id", this.props.post_id);
    let success;

    fetch(`/api/comment/`, {
      method: 'POST',
      body: form,
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(data => {
      if (success) {
        this.setState({content: ""});
        this.props.add_new_comment(this.props.post_index, data);
      }
      else {
        this.props.show_error_block(data);
      }
    })
  }

  edit_post_or_coment(form) {
    let success;

    const get_object_id = () => {
      if (this.props.post_id && this.props.mode === "post") {
        return this.props.post_id;
      }
      else {
        return this.props.comment_id;
      }
    }

    fetch(`/api/${this.props.mode}/${get_object_id()}/`, {
      method: 'PUT',
      body: form,
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      success = response.ok;
      return response.json();
    })
    .then(data => {
      if (success) {
        if (this.props.mode === "post") {
          this.props.update_post(this.props.post_index, data.content);
        }
        else {
          this.props.update_comment(
            this.props.post_index, this.props.comment_index, data.content
          );
        }
        this.cancel_edit();
      }
      else {
        this.props.show_error_block(data);
      }
    })
  }

  set_form_content(event) {
    this.setState({content: event.target.value});
  }

  cancel_edit() {
    this.props[`set_${this.props.mode}_edit`](false);
  }

  render() {
    let buttons;

    if (this.props.post_id && this.props.mode === "post" || this.props.comment_id) {
      buttons = (
        <div>
          <button
            className="index-forms__button index-forms__button--add index-forms__button--post"
          >
            Save
          </button>
          <button
            type="button"
            className="index-forms__button index-forms__button--delete index-forms__button--post"
            onClick={this.cancel_edit}
          >
            Cancel
          </button>
        </div>
      )
    }
    else {
      buttons = (
        <button className="index-forms__button index-forms__button--add">
          Send
        </button>
      );
    }

    return (
      <form className="dashboard-content__form" onSubmit={this.send_post_form}>
        <textarea
          className="dashboard-content__text-area"
          name="content"
          value={this.state.content}
          onChange={this.set_form_content}
          required
        />
        {buttons}
      </form>
    )
  }
}

DashboardPostForm.propTypes = {
  mode: PropTypes.string.isRequired,
  post_id: PropTypes.number,
  comment_id: PropTypes.number,
  post_index: PropTypes.number,
  comment_index: PropTypes.number,
  dashboard_socket: PropTypes.instanceOf(WebSocket),
  add_new_comment: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired,
  update_post: PropTypes.func.isRequired,
  update_comment: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  add_new_comment,
  show_error_block,
  update_post,
  update_comment,
  show_error_block
}

export default connect(null, map_dispatch_to_props)(DashboardPostForm)
