import React from 'react';
import PropTypes from 'prop-types';
import DashboardPostForm from './post/DashboardPostForm';
import DashboardPost from './post/DashboardPost';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  clear_discussion,
  load_old_post,
  init_post_load,
  load_waiting_posts
} from '../../../actions/DiscussionActionCreators';
import { show_error_block } from '../../../actions/InfoBlockActionCreators';

class DashboardDiscussion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      load_button: true,
      in_progress: false
    }

    this.fetch_posts = this.fetch_posts.bind(this);
  }

  componentWillMount() {
    if (
      this.props.discussion.class_group_id != this.props.class_group.id &&
      this.props.discussion.offset != 0
    ) {
      this.setState({load_button: true});
      this.props.clear_discussion(this.props.class_group.id);
    }
    else if (this.props.discussion.offset === 0) {
      this.fetch_posts();
    }
  }

  componentDidUpdate() {
    if (
      this.props.discussion.class_group_id != this.props.class_group.id &&
      this.props.discussion.offset != 0
    ) {
      this.setState({load_button: true});
      this.props.clear_discussion(this.props.class_group.id);
    }
    else if (
      this.props.discussion.offset === 0 && !this.state.in_progress
    ) {
      this.fetch_posts();
    }
  }

  fetch_posts() {
    const { limit, offset, count } = this.props.discussion;

    if ((count == 0 && count === offset) || count > offset) {
      this.setState({in_progress: true});

      let success;

      fetch(`/api/post/?class_group__id=${this.props.class_group.id}&limit=${limit}&offset=${offset}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'}
      })
      .then(response => {
        success = response.ok;
        return response.json();
      })
      .then(data => {
        if (success) {
          const update_state_data = {in_progress: false};

          if (this.props.discussion.class_group_id == this.props.class_group.id) {
            this.props.load_old_post(data.results);
          }
          else {
            data.class_group_id = this.props.class_group.id;

            if (!data.results || !data.next) {
              update_state_data.load_button = false;
            }

            this.props.init_post_load(data);
          }

          this.setState(update_state_data);
        }
        else {
          this.props.show_error_block(data);
          this.props.history.push('/');
        }
      })
    }
    else {
      this.setState({load_button: false});
    }
  }

  render() {
    const { id } = this.props.current_user;
    const { results, waiting_posts } = this.props.discussion;
    let button, load_posts;

    if (this.state.load_button) {
      button = (
        <button
          className='index-forms__button index-forms__button--center'
          onClick={this.fetch_posts}
        >
          Load posts
        </button>
      );
    }

    if (waiting_posts.length > 0) {
      let load_text;

      if (waiting_posts.length === 1) {
        load_text = 'Load 1 post';
      }
      else {
        load_text = `Load ${waiting_posts.length} posts`;
      }

      load_posts = (
        <button
          className='dashboard-content__load'
          onClick={this.props.load_waiting_posts}
        >
          {load_text}
        </button>
      );
    }

    return (
      <div className='school-article--fade-in'>
        <DashboardPostForm
          mode='post'
          dashboard_socket={this.props.dashboard_socket}
        />
        {load_posts}
        {results.map((post, i) => {
          return (
            <DashboardPost
              key={post.id}
              post={post}
              index={i}
              current_user_id={id}
            />
          );
        })}
        {button}
      </div>
    );
  }
}

DashboardDiscussion.propTypes = {
  class_group_id: PropTypes.number,
  class_group: PropTypes.object.isRequired,
  discussion: PropTypes.object.isRequired,
  dashboard_socket: PropTypes.instanceOf(WebSocket),
  clear_discussion: PropTypes.func.isRequired,
  load_old_post: PropTypes.func.isRequired,
  init_post_load: PropTypes.func.isRequired,
  load_waiting_posts: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {discussion: state.discussion};
}

const map_dispatch_to_props = {
  clear_discussion,
  load_old_post,
  init_post_load,
  load_waiting_posts,
  show_error_block
};

export default withRouter(connect(map_state_to_props, map_dispatch_to_props)(DashboardDiscussion));
