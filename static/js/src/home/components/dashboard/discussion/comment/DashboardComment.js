import React from 'react';
import PropTypes from 'prop-types';
import DashboardPostForm from '../post/DashboardPostForm';
import DashboardCommentContent from './DashboardCommentContent';

export default class DashboardComment extends React.Component {
  constructor() {
    super();
    this.state = {edit: false};

    this.set_comment_edit = this.set_comment_edit.bind(this);
  }

  set_comment_edit(value) {
    this.setState({edit: value});
  }

  render() {
    const { id, author, created, content } = this.props.comment;

    let comment_content;

    if (this.state.edit) {
      comment_content = (
        <DashboardPostForm
          mode={'comment'}
          post_id={this.props.post_id}
          post_index={this.props.post_index}
          comment_index={this.props.comment_index}
          comment_id={id}
          text={content}
          set_comment_edit={this.set_comment_edit}
        />
      );
    }
    else {
      comment_content = (
        <DashboardCommentContent
          author={author}
          created={created}
          comment_id={id}
          post_index={this.props.post_index}
          comment_index={this.props.comment_index}
          content={content}
          current_user_id={this.props.current_user_id}
          set_comment_edit={this.set_comment_edit}
        />
      );
    }

    return (
      <div className='dashboard-post dashboard-post--comment'>
        {comment_content}
      </div>
    )
  }
}

DashboardComment.propTypes = {
  comment: PropTypes.object.isRequired,
  post_id: PropTypes.number.isRequired,
  post_index: PropTypes.number.isRequired,
  comment_index: PropTypes.number.isRequired,
  current_user_id: PropTypes.number.isRequired
}
