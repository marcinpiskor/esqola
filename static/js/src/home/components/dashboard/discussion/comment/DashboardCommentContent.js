import React from 'react';
import PropTypes from 'prop-types';
import DashboardPostHeader from '../post/DashboardPostHeader';
import get_csrf_token from '../../../../csrf';
import { connect } from 'react-redux';
import { delete_comment } from '../../../../actions/DiscussionActionCreators';
import { show_error_block } from '../../../../actions/InfoBlockActionCreators';

class DashboardCommentContent extends React.Component {
  constructor(props) {
    super(props);
    this.enable_comment_edit = this.enable_comment_edit.bind(this);
    this.delete_comment = this.delete_comment.bind(this);
  }

  enable_comment_edit() {
    this.props.set_comment_edit(true);
  }

  delete_comment() {
    let success;

    fetch(`/api/comment/${this.props.comment_id}/`, {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {'X-CSRFToken': get_csrf_token()}
    })
    .then(response => {
      if (response.ok) {
        this.props.delete_comment(this.props.post_index, this.props.comment_index);
      }
      else {
        const error = {detail: "Post comment doesn't exist."};
        this.props.show_error_block(error);
      }
    })
  }

  render() {
    let comment_buttons;

    if (this.props.author.id == this.props.current_user_id) {
      comment_buttons = [
        <button
          key={'edit'}
          className='dashboard-post__button dashboard-content__dot'
          onClick={this.enable_comment_edit}
        >
          <i className='material-icons'>edit</i>
          <span className='dashboard-post__button_text'>Edit</span>
        </button>,
        <button
          key={'delete'}
          className='dashboard-post__button dashboard-content__dot'
          onClick={this.delete_comment}
        >
          <i className='material-icons'>delete</i>
          <span className='dashboard-post__button_text'>Delete</span>
        </button>
      ]
    }

    return (
      <div>
        <DashboardPostHeader
          author={this.props.author}
          created={this.props.created}
        />
        <p className='dashboard-post__text'>{this.props.content}</p>
        <div className='dashboard-post__buttons'>
          {comment_buttons}
        </div>
      </div>
    )
  }
}

DashboardCommentContent.propTypes = {
  author: PropTypes.object.isRequired,
  created: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  current_user_id: PropTypes.number.isRequired,
  set_comment_edit: PropTypes.func.isRequired,
  comment_id: PropTypes.number.isRequired,
  post_index: PropTypes.number.isRequired,
  comment_index: PropTypes.number.isRequired,
  current_user_id: PropTypes.number.isRequired,
  delete_comment: PropTypes.func.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_dispatch_to_props = {
  delete_comment,
  show_error_block
}

export default connect(null, map_dispatch_to_props)(DashboardCommentContent);
