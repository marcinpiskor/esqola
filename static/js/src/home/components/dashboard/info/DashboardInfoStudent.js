import React from 'react';
import PropTypes from 'prop-types';

export default class DashboardInfoStudent extends React.Component {
  render() {
    const { profile, first_name, last_name } = this.props.student;
    const full_name = `${first_name} ${last_name}`;
    const avatar_style = {};

    if (profile.photo) {
      avatar_style.backgroundImage = `url(${profile.photo})`;
    }

    return (
      <div className='school-article__button_wrapper'>
        <div className='school-principal__avatar' style={avatar_style}></div>
        <h4 className='school-principal__text'>{full_name}</h4>
      </div>
    )
  }
}

DashboardInfoStudent.propTypes = {
  student: PropTypes.object.isRequired
}
