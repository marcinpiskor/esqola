import React from 'react';
import PropTypes from 'prop-types';
import DashboardInfoPart from './DashboardInfoPart';

export default class DashboardTeacher extends React.Component {
  render() {
    let heading_text;

    if (this.props.teacher) {
      const { first_name, last_name } = this.props.teacher;
      heading_text = `${first_name} ${last_name}`;
    }
    else {
      heading_text = 'No teacher'
    }

    return (
      <DashboardInfoPart text={'Teacher'}>
        <h3 className='dashboard-info__part_content'>{heading_text}</h3>
      </DashboardInfoPart>
    )
  }
}

DashboardTeacher.propTypes = {
  teacher: PropTypes.object
}
