import React from 'react';
import PropTypes from 'prop-types';
import DashboardInfoPart from './DashboardInfoPart';
import DashboardInfoStudent from './DashboardInfoStudent';

export default class DashboardInfoStudents extends React.Component {
  constructor() {
    super();
    this.show_students = this.show_students.bind(this);
  }

  show_students() {
    this.props.set_active_tab('Students');
  }

  render() {
    let content;

    if (this.props.students.length === 0) {
      content = (
        <h3 className='dashboard-info__part_content'>
          No students related
        </h3>
      );
    }
    else {
      const students = this.props.students.slice(0, 5);

      content = (
        <div>
          {students.map(student => {
            return <DashboardInfoStudent key={student.id} student={student} />;
          })}
          <button
            className='dashboard-info__detail_button'
            onClick={this.show_students}
          >
            Check more details
          </button>
        </div>
      )
    }

    return (
      <DashboardInfoPart text={'Students'}>
        {content}
      </DashboardInfoPart>
    )
  }
}

DashboardInfoStudents.propTypes = {
  students: PropTypes.arrayOf(PropTypes.object).isRequired
}
