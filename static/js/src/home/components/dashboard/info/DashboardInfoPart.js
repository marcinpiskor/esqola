import React from 'react';
import PropTypes from 'prop-types';

export default class DashboardInfoPart extends React.Component {
  render() {
    return (
      <div className='dashboard-info__part'>
        <h2 className='dashboard-info__part_heading'>{this.props.text}</h2>
        {React.cloneElement(this.props.children)}
      </div>
    )
  }
}

DashboardInfoPart.propTypes = {
  text: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
}
