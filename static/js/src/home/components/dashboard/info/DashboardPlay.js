import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class DashboardPlay extends React.Component {
  render() {
    const { is_teacher, class_group_id } = this.props;
    let play_text, url;

    if (is_teacher) {
      play_text = 'Setup the game';
      url = `/game/${class_group_id}`
    }
    else {
      play_text = 'Play the game';
      url = `/play/${class_group_id}`;
    }

    return (
      <Link to={url} className='dashboard-info__link'>
        <div className='dashboard-info__part dashboard-info__part--play'>
          <div className='dashboard-info__play' />
          <h2 className='dashboard-info__part_heading dashboard-info__part_heading--play'>
            {play_text}
          </h2>
        </div>
      </Link>
    )
  }
}

DashboardPlay.propTypes = {
  is_teacher: PropTypes.bool.isRequired,
  class_group__id: PropTypes.number
}
