import React from 'react';
import PropTypes from 'prop-types';
import SearchUser from "./SearchUser";
import SearchSchool from "./SearchSchool";
import SearchBlock from "./SearchBlock"
import { connect } from "react-redux";

class Search extends React.Component {
  constructor() {
    super();
    this.state = {
      current_query: "",
      users: [],
      schools: []
    };
  }

  componentWillMount() {
    if (this.props.current_user.profile.is_activated) {
      this.start_search();
    }
  }

  componentDidUpdate() {
    if (
      this.props.current_user.profile.is_activated &&
      this.state.current_query != this.props.match.params.query
    ) {
      this.start_search();
    }
  }

  start_search() {
    const current_query = this.props.match.params.query;

    if (current_query.trim().length >= 2) {
      fetch(`/api/search/?query=${this.props.match.params.query}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'}
      })
      .then(response => response.json())
      .then(search_data => {
        document.title = `Search ${current_query} - Esqola`;
        const { users, schools } = search_data;
        this.setState({current_query, users, schools});
      })
    }
  }

  render() {
    const { users, schools } = this.state;

    return (
      <article className="school-article">
        <h2 className="search-heading">
          Search results for phrase <span className="search-heading__query">{this.props.match.params.query}</span>
        </h2>
        <SearchBlock
          name="Users"
          content={users}
          results_component={SearchUser}
        />
        <SearchBlock
          name="Schools"
          content={schools}
          results_component={SearchSchool}
        />
      </article>
    )
  }
}

Search.propTypes = {
  current_user: PropTypes.object.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user};
}

export default connect(map_state_to_props)(Search);
