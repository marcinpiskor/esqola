import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

export default class SearchUser extends React.Component {
  render() {
    const { id, first_name, last_name, username, profile } = this.props.content;
    const full_name = `${first_name} ${last_name}`;
    const avatar_style = {};

    if (profile.photo) {
      avatar_style.backgroundImage = `url(${profile.photo})`;
    }

    return (
      <Link to={`/user/${id}`} className="search-user">
        <div className="search-user__avatar" style={avatar_style}></div>
        <div className="search-user__wrapper">
          <div className="search-user__full_name">{full_name}</div>
          <div className="search-user__username">@{username}</div>
        </div>
      </Link>
    )
  }
}

SearchUser.propTypes = {
  content: PropTypes.object.isRequired
}
