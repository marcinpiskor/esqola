import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

export default class SearchSchool extends React.Component {
  render() {
    const { id, name, logo, city } = this.props.content;
    const city_name = `${city.name}, ${city.country_short}`;
    const avatar_style = {};

    if (logo) {
      avatar_style.backgroundImage = `url(${logo})`;
    }

    return (
      <Link to={`/school/${id}`} className="search-user">
        <div className="search-user__avatar" style={avatar_style}></div>
        <div className="search-user__wrapper">
          <div className="search-user__full_name">{name}</div>
          <div className="search-user__username">{city_name}</div>
        </div>
      </Link>
    )
  }
}

SearchSchool.propTypes = {
  content: PropTypes.object.isRequired
}
