import React from 'react';
import PropTypes from 'prop-types';

export default class SearchBlock extends React.Component {
  render() {
    let block_content;

    if (this.props.content) {
      if (this.props.content.length > 0) {
        block_content = this.props.content.map(el => {
          const result_props = {key: el.id, content: el};
          return React.createElement(this.props.results_component, result_props);
        });
      }
      else {
        block_content = (
          <h4 className="cockpit-class-group__heading cockpit-class-group__heading--empty">
            No {this.props.name.toLowerCase()}
          </h4>
        );
      }
    }

    return (
      <div>
        <h3 className="class-group__heading class-group__heading--search">
          <span className="class-group__heading--underline">
            {this.props.name}
          </span>
        </h3>
        <div className="search-content">{block_content}</div>
      </div>
    )
  }
}

SearchBlock.propTypes = {
  name: PropTypes.string.isRequired,
  results_component: PropTypes.func.isRequired
}
