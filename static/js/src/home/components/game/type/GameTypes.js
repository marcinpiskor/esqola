import React from 'react';
import PropTypes from 'prop-types';
import game_types from './game_types';
import GameType from './GameType';

export default class GameTypes extends React.Component {
  render() {
    return (
      <div className="game-setup-type__wrapper">
        {game_types.map(game_type => {
          return (
            <GameType
              game_type={game_type}
              active_type={this.props.active_type}
              key={game_type.type}
              set_game_type={this.props.set_game_type}
            />
        )})}
      </div>
    );
  }
}

GameTypes.propTypes = {
  set_game_type: PropTypes.func.isRequired,
  active_type: PropTypes.string
}
