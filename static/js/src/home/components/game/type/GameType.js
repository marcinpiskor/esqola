import React from 'react';
import PropTypes from 'prop-types';

export default class GameType extends React.Component {
  constructor() {
    super();
    this.choose_game_type = this.choose_game_type.bind(this);
  }

  choose_game_type() {
    this.props.set_game_type({type_of_game: this.props.game_type.type});
  }

  render() {
    const { type, text, description } = this.props.game_type;
    const base_class = 'game-setup-type';
    const active_class = this.props.active_type == type ? 'active' : type;
    const game_type_class = `${base_class} ${base_class}--${active_class} no-select`;

    return (
      <div className={game_type_class} onClick={this.choose_game_type}>
        <div className="game-setup-type__part game-setup-type__part--presentation">
          <i className="material-icons game-setup-type__part__icon">
						videogame_asset
					</i>
        </div>
        <div className="game-setup-type__part game-setup-type__part--description">
          <div className="game-setup-type__part__content">
            <h4 className="game-setup-type__part__heading">{text}</h4>
            <p className="game-setup-type__part__description">{description}</p>
          </div>
        </div>
      </div>
    );
  }
}

GameType.propTypes = {
  set_game_type: PropTypes.func.isRequired,
  game_type: PropTypes.object.isRequired,
  active_type: PropTypes.string
}
