const game_types = [
  {
    type: 'match',
    short_type: 'ma',
    text: 'Match words',
    description: 'Connect english word with its proper translation.'
  },
  {
    type: 'choose',
    short_type: 'co',
    text: 'Choose answer',
    description: 'Choose proper translation of the word or phrase.'
  },
  {
    type: 'charades',
    short_type: 'ch',
    text: 'Charades',
    description: 'Guess the word or phrase by looking on the picture.'
  },
  {
    type: 'enter',
    short_type: 'en',
    text: 'Enter the text',
    description: 'Type a proper translation of the text.'
  }
];

export default game_types;
