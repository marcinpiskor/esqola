import React from 'react';
import PropTypes from 'prop-types';

export default class GameLanguage extends React.Component {
  constructor() {
    super();
    this.choose_language = this.choose_language.bind(this);
  }

  choose_language() {
    this.props.set_game_language({language: this.props.language.id});
  }

  render() {
    const { name, flag } = this.props.language;
    const style = {backgroundImage: `url(${this.props.language.flag})`};

    const base_class = 'game-setup-flag'
    let flag_class = `${base_class} ${base_class}--list`;
    let flag_img_class = 'game-setup-flag__img';

    if (this.props.active_language == this.props.language.id) {
      flag_class = `${flag_class} ${base_class}--active`;
      flag_img_class = `${flag_img_class} ${flag_img_class}--active`;
    }

    return (
      <div
        className={flag_class}
        data-language={name}
        onClick={this.choose_language}
      >
        <div className={flag_img_class} style={style}></div>
      </div>
    );
  }
}

GameLanguage.propTypes = {
  language: PropTypes.object.isRequired,
  set_game_language: PropTypes.func.isRequired,
  active_language: PropTypes.number
}
