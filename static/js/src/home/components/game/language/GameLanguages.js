import React from 'react';
import PropTypes from 'prop-types';
import GameLanguage from './GameLanguage';

export default class GameLanguages extends React.Component {
  constructor() {
    super();
    this.state = {
      scroll: 0,
      scroll_process: null,
      touch_mode: false,
      previous_move: null
    }

    this.reset_flags_scroll = this.reset_flags_scroll.bind(this);

    this.start_touch = this.start_touch.bind(this);
    this.touch_moving = this.touch_moving.bind(this);
    this.end_touch = this.end_touch.bind(this);

    this.scroll_left = this.scroll_left.bind(this);
    this.scroll_right = this.scroll_right.bind(this);
    this.cancel_clicked = this.cancel_clicked.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.reset_flags_scroll);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.reset_flags_scroll);
  }

  reset_flags_scroll() {
    this.setState({scroll: 0});
  }

  start_touch(event) {
    const previous_move = this.get_touch_x(event);
    this.setState({previous_move, touch_mode: true});
  }

  touch_moving(event) {
    const current_move = this.get_touch_x(event);
    const scroll_amount = Math.abs(this.state.previous_move - current_move);

    if (
      current_move < this.state.previous_move
      && this.has_not_reached_the_last_flag()
    ) {
      this.setState({scroll: this.state.scroll - scroll_amount});
    }
    else if (
      current_move > this.state.previous_move
      && this.state.scroll < 0
    ) {
      this.setState({scroll: this.state.scroll + scroll_amount});
    }
  }

  end_touch() {
    this.setState({touch_mode: false, previous_move: null});
  }

  get_touch_x(event) {
    const x = event.touches[0].clientX;
    const flag_wrapper_margins = event.target.getBoundingClientRect().x;

    return x - flag_wrapper_margins;
  }

  scroll_left() {
    const process = setInterval(() => {
        if (this.state.scroll < 0) {
          this.setState({scroll: this.state.scroll + 5});
        }
        else {
          clearInterval(this.state.scroll_process);
          this.setState({scroll_process: null});
        }
      },
      10
    );

    this.setState({scroll_process: process});
  }

  scroll_right() {
    const process = setInterval(() => {
        if (this.has_not_reached_the_last_flag()) {
          this.setState({scroll: this.state.scroll - 5});
        }
        else {
          clearInterval(this.state.scroll_process);
          this.setState({scroll_process: null});
        }
      },
      10
    );

    this.setState({scroll_process: process});
  }

  has_not_reached_the_last_flag() {
    const flags_width = this.flags.offsetWidth;
    const flag_el = this.flags.children[0];
    const flag_margins = parseInt(getComputedStyle(flag_el).marginRight) * 2;
    const flag_default_width = flag_el.offsetWidth + flag_margins;

    const current_flags_position = flags_width + Math.abs(this.state.scroll);
    const max_flags_position = flag_default_width * this.props.languages.length;

    return current_flags_position < max_flags_position;
  }

  cancel_clicked() {
    if (this.state.scroll_process) {
      clearInterval(this.state.scroll_process);
    }
  }

  render() {
    const wrapper_style = {transform: `translateX(${this.state.scroll}px)`};
    const { languages, active_language, set_game_language} = this.props;

    return (
      <div className="game-setup-flags">
        <div
          className="game-setup-flags__arrow no-select"
          onMouseDown={this.scroll_left}
          onMouseUp={this.cancel_clicked}
          onMouseLeave={this.cancel_clicked}
        >
          <i className="material-icons game-setup-flags__arrow-icon">
            keyboard_arrow_left
          </i>
        </div>
        <div
          className="game-setup-flags__wrapper"
          onTouchStart={this.start_touch}
          onTouchMove={this.touch_moving}
          onTouchEnd={this.end_touch}
        >
          <div style={wrapper_style} ref={el => this.flags = el}>
            {languages.map(language => {
              return (
                <GameLanguage
                  active_language={active_language}
                  language={language}
                  key={language.id}
                  set_game_language={set_game_language}
                />
              );
            })}
          </div>
        </div>
        <div
          className="game-setup-flags__arrow no-select"
          onMouseDown={this.scroll_right}
          onMouseUp={this.cancel_clicked}
          onMouseLeave={this.cancel_clicked}
        >
          <i className="material-icons game-setup-flags__arrow-icon">
            keyboard_arrow_right
          </i>
        </div>
      </div>
    )
  }
}

GameLanguages.propTypes = {
  languages: PropTypes.arrayOf(PropTypes.object),
  set_game_language: PropTypes.func.isRequired,
  active_language: PropTypes.number
}
