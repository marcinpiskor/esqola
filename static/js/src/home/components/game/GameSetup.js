import React from 'react';
import PropTypes from 'prop-types';
import get_csrf_token from '../../csrf';
import GameLanguages from './language/GameLanguages';
import GameTypes from './type/GameTypes';
import { Redirect } from 'react-router'
import { connect } from 'react-redux';
import { show_error_block } from '../../actions/InfoBlockActionCreators';

class GameSetup extends React.Component {
  constructor(props) {
    super(props);
    const { class_group_id } = props.match.params;

    this.state = {
      class_group: class_group_id ? class_group_id : "",
      languages: [],
      teacher_class_groups: [],
      fetching: false,
      loaded: false,
      language: null,
      type_of_game: null
    }

    this.handle_change = this.handle_change.bind(this);
    this.handle_state = this.handle_state.bind(this);
    this.init_game_session = this.init_game_session.bind(this);
  }

  componentWillMount() {
    if (this.props.current_user.profile.is_activated) {
      this.fetch_game_session_data();
    }
  }

  componentDidUpdate() {
    if (this.props.current_user.profile.is_activated) {
      this.fetch_game_session_data();

      const { class_group_id } = this.props.match.params;

      if (class_group_id && class_group_id != this.state.class_group) {
        this.setState({class_group: class_group_id});
      }
    }
  }

  handle_change(event) {
    this.setState({class_group: event.target.value});
  }

  handle_state(update) {
    this.setState(update);
  }

  init_game_session() {
    const { type_of_game, language, class_group } = this.state;
    let success;

    if (type_of_game && language && class_group) {
      fetch('/api/game-session/', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
          'content-type': 'application/json',
          'X-CSRFToken': get_csrf_token()
        },
        body: JSON.stringify({type_of_game, language, class_group})
      })
      .then(response => {
        success = response.ok;
        return response.json();
      })
      .then(data => {
        if (success) {
          this.props.history.push(`/play/${class_group}`);
        }
        else {
          this.props.show_error_block(data);
        }
      })
    }
  }

  fetch_game_session_data() {
    const { id, profile, teacher_classes } = this.props.current_user;

    if (
      !this.state.fetching
      && id
      && profile.role.name === 'teachers'
      && teacher_classes.length > 0
    ) {
      this.setState({fetching: true});
      this.get_languages();
      this.get_teacher_class_groups();
    }
  }

  get_languages() {
    fetch('/api/language/', {
      method: 'GET',
      credentials: 'same-origin',
      headers: {'content-type': 'application/json'}
    })
    .then(response => response.json())
    .then(languages => {
      this.setState({
        languages,
        loaded: this.state.teacher_class_groups.length > 0 ? true : false
      });
    })
  }

  get_teacher_class_groups() {
    const { teacher_classes } = this.props.current_user;
    const class_groups_to_fetch = teacher_classes.map(c => c.id).join(',');

    fetch(`/api/class-group/?id__in=${class_groups_to_fetch}`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {'content-type': 'application/json'}
    })
    .then(response => response.json())
    .then(teacher_class_groups => {
      document.title = 'Setup a game session - Esqola';

      this.setState({
        teacher_class_groups,
        loaded: this.state.languages.length > 0 ? true : false
      });
    })
  }

  render() {
    const { id, profile, teacher_classes } = this.props.current_user;

    if (
        id
        && profile.is_activated
        && (profile.role.name !== "teachers" || teacher_classes.length === 0)
      ) {
      return <Redirect to="/" />
    }
    else if (id) {
      let game_setup_class_name = "school-article";

      if (this.state.loaded) {
        game_setup_class_name = `${game_setup_class_name} school-article--fade-in`;
      }
      else {
        game_setup_class_name = `${game_setup_class_name} school-article--hidden`;
      }

      return(
        <section className={game_setup_class_name}>
          <header className="game-setup-header">
            <h2 className="game-setup-header__heading">Start a game for</h2>
            <select
              className="custom-select custom-select--game-class-group"
              value={this.state.class_group}
              onChange={this.handle_change}
            >
              <option value="">---</option>
              {this.state.teacher_class_groups.map(class_group => {
                return (
                  <option key={class_group.id} value={class_group.id}>
                    {class_group.name}
                  </option>
                );
              })}
            </select>
          </header>
          <article className="game-setup-article">
            <GameLanguages
              active_language={this.state.language}
              languages={this.state.languages}
              set_game_language={this.handle_state}
            />
            <h3 className="game-setup-heading">
              <span className="game-setup-heading__span">
                Choose a language of this session
              </span>
            </h3>
            <GameTypes
              active_type={this.state.type_of_game}
              set_game_type={this.handle_state}
            />
            <h3 className="game-setup-heading">
              <span className="game-setup-heading__span">
                Choose a game
              </span>
            </h3>

            <button
              className="index-forms__button index-forms__button--game-setup"
              onClick={this.init_game_session}
            >
              Start game session
            </button>
          </article>
        </section>
      )
    }
    else {
      return "";
    }
  }
}

GameSetup.propTypes = {
  current_user: PropTypes.object.isRequired,
  show_error_block: PropTypes.func.isRequired
}

const map_state_to_props = (state) => {
  return {current_user: state.current_user}
}

const map_dispatch_to_props = {show_error_block};

export default connect(map_state_to_props, map_dispatch_to_props)(GameSetup);
