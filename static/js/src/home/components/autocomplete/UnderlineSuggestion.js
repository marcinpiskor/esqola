import React from "react";

export default class UnderlineSuggestion extends React.Component {
  constructor() {
    super();
    this.click_suggestion = this.click_suggestion.bind(this);
  }

  click_suggestion() {
    this.props.choose_suggestion(this.props.suggestion);
  }

  render() {
    let suggestion_class = 'class-group__autocomplete';

    if (this.props.index === this.props.active_suggestion) {
      suggestion_class = `${suggestion_class} ${suggestion_class}--active`;
    }

    return (
      <div className={suggestion_class} onClick={this.click_suggestion}>
        {this.props.suggestion.content}
      </div>
    )
  }
}
