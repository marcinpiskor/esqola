import React from "react";
import UnderlineSuggestion from './UnderlineSuggestion';

export default class UnderlineAutocomplete extends React.Component {
  constructor() {
    super();
    this.state = {
      active_suggestion: -1,
      active_keyboard: false
    }

    this.handle_suggestion_keyboard = this.handle_suggestion_keyboard.bind(this);
    this.unset_keyboard = this.unset_keyboard.bind(this);
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handle_suggestion_keyboard);
  }

  componentWillUnmount() {
    document.addEventListener("keydown", this.handle_suggestion_keyboard);
  }

  handle_suggestion_keyboard(event) {
    if (this.props.visible_autocomplete) {
      let { active_suggestion } = this.state;
      const { length } = this.props.suggestions;

      switch (event.code) {
        case "ArrowDown":
          if (active_suggestion + 1 < length) {
            active_suggestion += 1;
            this.setState({active_keyboard: true, active_suggestion});
          }
          break;
        case "ArrowUp":
          if (active_suggestion - 1 >= -1) {
            active_suggestion -= 1;
            this.setState({active_keyboard: true, active_suggestion});
          }
          break;
        case "Enter":
          if (this.state.active_keyboard) {
            this.props.choose_suggestion(
              this.props.suggestions[this.state.active_suggestion]
            );
          }
          break;
      }
    }
  }

  unset_keyboard() {
    if (this.state.active_keyboard) {
      this.setState({
        active_suggestion: -1,
        active_keyboard: false
      });
    }
  }

  render() {
    let autocomplete_class = "class-group__autocomplete_wrapper";

    if (!this.props.visible_autocomplete) {
      autocomplete_class = `${autocomplete_class} ${autocomplete_class}--hidden`;
    }

    return (
      <div
        className={autocomplete_class}
        onMouseOver={this.unset_keyboard}
        onMouseEnter={this.props.set_autocomplete_active}
        onMouseLeave={this.props.set_autocomplete_unactive}
      >
        {this.props.suggestions.map((suggestion, index) => {
          return (
            <UnderlineSuggestion
              key={suggestion.id}
              index={index}
              active_suggestion={this.state.active_suggestion}
              suggestion={suggestion}
              choose_suggestion={this.props.choose_suggestion}
            />
          );
        })}
      </div>
    );
  }
}
