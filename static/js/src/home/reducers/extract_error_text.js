export default function extract_error_text(data) {
  const key = Object.keys(data)[0];
  const value = data[key];

  switch(Object.prototype.toString.call(value)) {
    case '[object Object]':
      return extract_error_text(data[key]);
    case '[object Array]':
      return value[0];
    default:
      return value;
  }
}
