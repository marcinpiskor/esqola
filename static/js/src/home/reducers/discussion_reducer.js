import {
  LOAD_OLD_POST,
  INIT_POST_LOAD,
  CLEAR_DISCUSSION,
  ADD_NEW_POST,
  ADD_POST_TO_WAITING,
  LOAD_WAITING_POSTS,
  LOAD_POST_COMMENTS,
  UPDATE_POST,
  DELETE_POST,
  ADD_NEW_COMMENT,
  UPDATE_COMMENT,
  DELETE_COMMENT
} from "../actions/DiscussionActionTypes";
import copy_state from "./copy_state";

const initial_state =  {
  class_group_id: null,
  count: 0,
  offset: 0,
  limit: 5,
  results: [],
  waiting_posts: []
}

const reducer = (state=initial_state, action) => {
  switch (action.type) {
    case INIT_POST_LOAD:
      state = copy_state(state, Object.keys(state));
      Object.assign(state, action.payload);
      state.waiting_posts = [];
      state.offset += state.limit;
      break;
    case LOAD_OLD_POST:
      state = copy_state(state, Object.keys(state));
      state.results = state.results.concat(action.payload);
      state.offset += state.limit;
      break;
    case CLEAR_DISCUSSION:
      state = copy_state(state, Object.keys(state));
      if (!action.payload) {
        state.class_group_id = null;
      }
      state.results = [];
      state.count = 0;
      state.offset = 0;
      break;
    case ADD_NEW_POST:
      state = copy_state(state, Object.keys(state));
      state.results.unshift(action.payload);
      state.offset += 1;
      state.count += 1;
      break;
    case ADD_POST_TO_WAITING:
      state = copy_state(state, Object.keys(state));
      state.waiting_posts.unshift(action.payload);
      state.offset += 1;
      state.count += 1;
      break;
    case LOAD_WAITING_POSTS:
      state = copy_state(state, Object.keys(state));
      state.results = [].concat(state.waiting_posts, state.results);
      state.waiting_posts = [];
      break;
    case UPDATE_POST:
      state = copy_state(state, Object.keys(state));
      state.results[action.payload.post_index].content = action.payload.content;
      break;
    case DELETE_POST:
      state = copy_state(state, Object.keys(state));
      state.results.splice(action.payload, 1);
      break;
    case LOAD_POST_COMMENTS:
      state = copy_state(state, Object.keys(state));
      state.results[action.payload.post_index].comments = action.payload.comments;
      break;
    case ADD_NEW_COMMENT:
      state = copy_state(state, Object.keys(state));
      state.results[action.payload.post_index].comments.push(action.payload.new_comment);
      break;
    case UPDATE_COMMENT:
      state = copy_state(state, Object.keys(state));
      state.results[action.payload.post_index].comments[action.payload.comment_index].content = action.payload.content;
      break;
    case DELETE_COMMENT:
      state = copy_state(state, Object.keys(state));
      state.results[action.payload.post_index].comments.splice(action.payload.comment_index, 1);
      break;
  }

  return state;
};

export default reducer;
