import copy_state from './copy_state';
import extract_error_text from './extract_error_text';
import {
  UPDATE_INFO_BLOCK,
  SHOW_INFO_BLOCK,
  HIDE_INFO_BLOCK,
  SUCCESS_CUSTOMIZATION,
  SHOW_ERROR_BLOCK
} from '../actions/InfoBlockActionTypes';

const initial_state = {
  heading: 'Something went wrong',
  text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  type: '',
  visible: false,
  clickable: true
}

const toggle_block_keys = (state) => {
  const keys = Object.keys(state);
  delete keys.visible;
  return keys;
}

const reducer = (state=initial_state, action) => {
  switch(action.type) {
    case UPDATE_INFO_BLOCK:
      state = copy_state(state, Object.keys(state));
      Object.assign(state, action.info_data);
      state.visible = true;
      break;

    case SHOW_INFO_BLOCK:
      state = copy_state(state, toggle_block_keys(state));
      state.visible = true;
      break;

    case HIDE_INFO_BLOCK:
      state = copy_state(state, toggle_block_keys(state));
      state.visible = false;
      break;

    case SUCCESS_CUSTOMIZATION:
      state = {
        heading: 'Done',
        text: 'Your account has been successfully configured. Now, close this dialog and click the red owl icon to open a menu',
        clickable: true,
        visible: true,
        type: 'logo'
      };
      break;

    case SHOW_ERROR_BLOCK:
      state = {
        heading: 'Something went wrong',
        text: extract_error_text(action.error_data),
        clickable: false,
        visible: true,
        type: 'error'
      };
      break;
  }

  return state;
};

export default reducer
