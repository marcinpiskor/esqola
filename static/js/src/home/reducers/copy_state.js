const copy_state = (old_state, obj_keys) => {
  // Make copy of redux state 
  const recursive_copy = (obj_to_copy, obj_keys) => {
    const type = Object.prototype.toString.call(obj_to_copy);

    if (type === '[object Object]') {
      const result = {};
      const keys_to_copy = obj_keys || Object.keys(obj_to_copy);

      keys_to_copy.forEach(key => {
        result[key] = recursive_copy(obj_to_copy[key]);
      });

      return result;
    }
    else if (type === '[object Array]') {
      return obj_to_copy.map(el => recursive_copy(el));
    }
    else if (type === '[object Date]') {
      return new Date(obj_to_copy);
    }
    else {
      return obj_to_copy;
    }
  }

  return recursive_copy(old_state, obj_keys);
}

export default copy_state;
