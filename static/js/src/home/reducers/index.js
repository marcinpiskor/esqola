import current_user_reducer from "./current_user_reducer";
import nav_reducer from "./nav_reducer";
import info_block_reducer from "./info_block_reducer";
import school_reducer from "./school_reducer";
import discussion_reducer from "./discussion_reducer";

const reducers = {
  current_user: current_user_reducer,
  nav: nav_reducer,
  info_block: info_block_reducer,
  school: school_reducer,
  discussion: discussion_reducer
}

export default reducers;
