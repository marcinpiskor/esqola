import { CHANGE_NAV_VISIBILITY } from "../actions/NavActionTypes";

const initial_state = {
  visible: false
}

const reducer = (state=initial_state, action) => {
  switch (action.type) {
    case CHANGE_NAV_VISIBILITY:
      state = {visible: action.payload};
      break;
  }

  return state;
};

export default reducer;
