import copy_state from './copy_state';
import {
  GET_SCHOOL,
  EDIT_SCHOOL,
  FINISH_EDIT_SCHOOL,
  CLEAR_SCHOOL,
  ORDER_CLASSES,
  ADD_CLASS_GROUP,
  UPDATE_CLASS_GROUP,
  SET_CLASS_GROUP_DETAIL_INFO,
  REMOVE_STUDENTS,
  DELETE_CLASS_GROUP,
  ADD_INVITATION,
  DELETE_INVITATION
} from '../actions/SchoolActionTypes';

const order_classes = (classes, type, field) => {
  const fields = field.split('.');
  const order_function = type === 'ascending' ? ascending_order : descending_order;

  return classes.sort((a, b) => {
    a = get_order_field_value(a, copy_state(fields));
    b = get_order_field_value(b, copy_state(fields));

    return order_function(a, b);
  });
}

const get_order_field_value = (obj, fields) => {
  const value = obj[fields.shift()];

  if (fields.length === 0) {
    return value;
  }
  else {
    return !value ? '' : get_order_field_value(value, fields);
  }
};

const ascending_order = (a, b) => {
  if (typeof a === 'string') {
    return a > b;
  }
  else if (typeof b === 'number') {
    return a - b;
  }
};

const descending_order = (a, b) => {
  if (typeof b === 'string') {
    return b > a;
  }
  else if (typeof b === 'number') {
    return b - a;
  }
};

const initial_state = {
  id: null,
  name: null,
  logo: null,
  city: null,
  principal: null,
  edit_mode: false,
  classes: []
};

const reducer = (state=initial_state, action) => {
  switch (action.type) {
    case GET_SCHOOL:
      state = copy_state(state);
      Object.assign(state, action.school_data);
      state.edit_mode = false;
      break;
    case EDIT_SCHOOL:
      state = copy_state(state);
      state.edit_mode = true;
      break;
    case FINISH_EDIT_SCHOOL:
      state = copy_state(state);
      state.edit_mode = false;
      break;
    case CLEAR_SCHOOL:
      state = {
        id: null,
        name: null,
        logo: null,
        city: null,
        principal: null,
        edit_mode: false,
        classes: []
      };
      break;
    case ORDER_CLASSES:
      const { order_type, order_field } = action;
      state = copy_state(state);
      state.classes = order_classes(state.classes, order_type, order_field);
      break;
    case ADD_CLASS_GROUP:
      state = copy_state(state);
      state.classes.push(action.class_group);
      break;
    case UPDATE_CLASS_GROUP:
      const { index, class_group } = action;
      state = copy_state(state);
      Object.assign(state.classes[index], class_group);
      break;
    case SET_CLASS_GROUP_DETAIL_INFO:
      state = copy_state(state);
      state.classes[action.index] = action.class_group;
      break;
    case REMOVE_STUDENTS:
      const { class_group_index, students } = action;
      state = copy_state(state);
      const class_to_update = state.classes[class_group_index];
      class_to_update.students = class_to_update.students.filter(s => {
         return students.indexOf(s.id) === -1;
      });
      break;
    case DELETE_CLASS_GROUP:
      state = copy_state(state);
      state.classes.splice(action.index, 1);
      break;
    case ADD_INVITATION:
      state = copy_state(state);
      state.classes[action.class_group_index].invited_users.push(
        action.invitation
      );
      break;
    case DELETE_INVITATION:
      state = copy_state(state);
      state.classes[action.class_group_index].invited_users.splice(
        action.invitation_index, 1
      );
      break;
  }

  return state;
};

export default reducer;
