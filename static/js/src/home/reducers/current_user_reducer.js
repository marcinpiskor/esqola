import {
  GET_CURRENT_USER,
  ADD_STUDENT_CLASS_GROUP,
  SET_CURRENT_USER_PHOTO,
  SET_CURRENT_USER_SCHOOL
} from '../actions/CurrentUserActionTypes.js';
import copy_state from './copy_state';

const initial_state =  {
  id: null,
  username: null,
  email: null,
  first_name: null,
  last_name: null,
  profile: {
    birthday: null,
    points: 0,
    city: null,
    photo: null,
    role: null,
    is_activated: false
  },
  school: null,
  teacher_classes: [],
  student_classes: []
}

const reducer = (state=initial_state, action) => {
  switch (action.type) {
    case GET_CURRENT_USER:
      state = copy_state(state, Object.keys(state));
      Object.assign(state, action.current_user_data);
      break;
    case SET_CURRENT_USER_PHOTO:
      state = copy_state(state, Object.keys(state));
      state.profile.photo = action.photo_data;
      break;
    case SET_CURRENT_USER_SCHOOL:
      state = copy_state(
        state, Object.keys(state).filter(key => key !== "school")
      );
      state.school = action.school;
      break;
    case ADD_STUDENT_CLASS_GROUP:
      state = copy_state(state, Object.keys(state));
      state.student_classes.push(action.class_group_obj);
      break;
  }

  return state;
};

export default reducer;
