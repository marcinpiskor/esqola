from rest_framework import routers
from authorization.api import (
    CurrentUserViewSet,
    GroupViewSet,
    UserViewSet,
    SearchViewSet
)
from management.api import SchoolViewSet, ClassGroupViewSet, InvitationViewSet
from game.api import LanguageViewSet, GameSessionViewSet, GameReportViewSet
from dashboard.api import PostViewSet, CommentViewSet
from chat.api import MessageViewSet


router = routers.DefaultRouter()
router.register(r'current-user', CurrentUserViewSet, base_name="current_user")
router.register(r'user', UserViewSet, base_name="user")
router.register(r'group', GroupViewSet, base_name="group")
router.register(r'school', SchoolViewSet, base_name="school")
router.register(r'class-group', ClassGroupViewSet, base_name="class_group")
router.register(r'invitation', InvitationViewSet, base_name="invitation")
router.register(r'language', LanguageViewSet, base_name="language")
router.register(r'game-session', GameSessionViewSet, base_name="game_session")
router.register(r'post', PostViewSet, base_name="post")
router.register(r'comment', CommentViewSet, base_name="comment")
router.register(r'message', MessageViewSet, base_name="chat")
router.register(r'search', SearchViewSet, base_name="search")
router.register(r'game-report', GameReportViewSet, base_name="game_report")
