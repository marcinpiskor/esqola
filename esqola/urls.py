from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from .api import router

handler404 = 'authorization.views.handler404'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('', include('authorization.urls', namespace="authorization")),
    path('main/', include('main.urls', namespace="main")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
