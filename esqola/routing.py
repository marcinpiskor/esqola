import game.routing
import dashboard.routing
import chat.routing
from channels.auth import AuthMiddlewareStack
from channels.sessions import SessionMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        SessionMiddlewareStack(
            URLRouter([
                *game.routing.websocket_urlpatterns,
                *dashboard.routing.websocket_urlpatterns,
                *chat.routing.websocket_urlpatterns
            ])
        )
    )
})
