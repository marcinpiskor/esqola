import io
from PIL import Image
from django.conf import settings
from django.core.files.base import File

class FinishGameMixin:
    """Mixin class with finish game method for game handler classes."""
    def finish_game(self, game_session, game_session_finish):
        i = 0.75
        last_score = None
        sorted_score = sorted(
            game_session["scores"].items(),
            key=self.finish_key_function,
            reverse=True
        )

        for user_id, value in sorted_score:
            if last_score is None or last_score > sum(value):
                last_score = sum(value)

                if last_score == 0:
                    i = 1001
                else:
                    i += 0.25

            user_data = game_session["joined"][user_id]
            user_data["score"] = int(1000/i)
            game_session_finish["results"].append(user_data)


class GameTestHelper:
    def generate_image(self, lang_name):
        file = io.BytesIO()
        image = Image.new("RGBA", size=(100, 100), color=(155, 0, 0))
        image.save(file, "png")

        file.name = f"flag-{lang_name}-{settings.SECRET_KEY}.png"
        file.seek(0)

        return File(file, name=file.name)

class ContentHelper:
    @property
    def shorter_content(self):
        if len(self.content) <= 10:
            return self.content
        else:
            return f"{self.content[:10]}..."
