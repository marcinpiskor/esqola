import redis
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.db.models import Q
from authorization.models import ClassGroup
from authorization.permissions import HasQueryParams
from authorization.utils import QueryParamsMixin
from .serializers import (
    LanguageSerializer,
    GameSessionSerializer,
    GameReportListSerializer,
    GameReportDetailSerializer
)
from .models import Language, GameReport
from .permissions import (
    HasGameReportAllowedQueryParams,
    IsRelatedWithGameReport
)


class LanguageViewSet(
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):
    """Viewset that handles getting info about languages.

    Attributes:
        serializer_class: read-only serializer
        queryset: app is only for users that speak English language
        permission_classes: only for authenticated users
    """
    serializer_class = LanguageSerializer
    queryset = Language.objects.exclude(name="English")
    permission_classes = (IsAuthenticated,)


class GameSessionViewSet(
        mixins.CreateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    """Viewset that handles creating game sessions.

    Attributes:
        serializer_class: read-only serializer
        permission_classes: only for authenticated users
    """
    serializer_class = GameSessionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = []

    def create(self, request):
        serializer = GameSessionSerializer(
            data=request.data, context={"request": request}
        )

        if serializer.is_valid():
            serializer.save()
            return Response("", status.HTTP_201_CREATED)

        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        queryset = ClassGroup.objects.all()
        class_group = get_object_or_404(queryset, pk=pk)

        if class_group.teacher != request.user:
            msg = "Only teacher of class group can delete game session."
            return Response(
                {"class_group": {"teacher": [msg]}},
                status.HTTP_403_FORBIDDEN
            )

        redis_handler = redis.StrictRedis()
        if str(class_group.id).encode() in redis_handler.hkeys("games"):
            redis_handler.hdel("games", str(class_group.id))

        return Response("", status.HTTP_204_NO_CONTENT)

    @list_route(methods=["GET"])
    def active(self, request, *args, **kwargs):
        """
        Action that handles getting id of active game sessions related with
        current user.

        Arg:
            request: DRF request object
        """
        # Gettings id of Class group objects in which current user is student
        current_user_classes = request.user.student_classes.values_list(
            "id", flat=True
        )
        # Converting class group id numbers to string and to bytes later
        current_user_classes = [str(c).encode() for c in current_user_classes]

        # Getting id of Class group objects that are related with active
        # game sessions
        redis_handler = redis.StrictRedis()
        current_games = redis_handler.hkeys("games")

        # Extracting active current user game sessions
        content = set(current_user_classes)&set(current_games)
        content = [int(game_id) for game_id in content]

        return Response(content, status.HTTP_200_OK)


class GameReportViewSet(QueryParamsMixin, viewsets.ReadOnlyModelViewSet):
    """
    Viewset that handles getting info about completed game sessions reports.
    """
    str_query_params = [
        "class_group__name", "class_group__school__name", "language__name",
        "type_of_game"
    ]

    def get_queryset(self):
        if self.action == "list":
            current = self.request.user

            return GameReport.objects.filter(
                (Q(class_group__teacher=current)
                | Q(class_group__school__principal=current)
                | Q(players=current)),
                **self.get_query_params()
            ).distinct()
        else:
            return GameReport.objects.all()

    def get_serializer_class(self):
        if self.action == "list":
            return GameReportListSerializer
        return GameReportDetailSerializer

    def get_permissions(self):
        if self.action == "list":
            permissions = (
                IsAuthenticated, HasQueryParams,
                HasGameReportAllowedQueryParams
            )
        else:
            permissions = (IsAuthenticated, IsRelatedWithGameReport)

        return [permission() for permission in permissions]
