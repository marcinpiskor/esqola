from rest_framework import permissions
from authorization.permissions import HasAllowedParams


GAME_REPORT_ALLOWED_PARAMS = {
    "class_group__id", "class_group__name", "class_group__school__id",
    "class_group__school__name", "language__id", "language__name",
    "type_of_game", "players__id"
}

class HasGameReportAllowedQueryParams(HasAllowedParams):
    """
    Permission that checks if game report allowed query params has been passed.
    """
    allowed_params = GAME_REPORT_ALLOWED_PARAMS
    message = "Only these query params are allowed in this request: " \
                            f"{', '.join(GAME_REPORT_ALLOWED_PARAMS)}."


class IsRelatedWithGameReport(permissions.BasePermission):
    """
    Permission that checks if current user is teacher of class group related
    with game report, principal of school related with class group of game
    report or current user has been player of the game session.
    """
    message = "Current user needs to be teacher of class group related with " \
                "game report, principal of school which is related with that" \
                " class group or student that has played the game."

    def has_object_permission(self, request, view, obj):
        return (
            obj.class_group.teacher == request.user or
            obj.class_group.school.principal == request.user or
            obj.players.filter(id=request.user.id).count() > 0
        )
