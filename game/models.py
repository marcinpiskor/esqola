import os
import csv
import random
import dateutil.parser
from django.db import models
from django.core.files import File
from django.conf import settings
from django.contrib.auth.models import User
from authorization.models import ClassGroup
from .utils import ContentHelper

TYPE_OF_GAMES = ("match", "choose", "charades", "enter")
GAMES_CHOICES = (
    ("ma", TYPE_OF_GAMES[0]),
    ("co", TYPE_OF_GAMES[1]),
    ("ch", TYPE_OF_GAMES[2]),
    ("en", TYPE_OF_GAMES[3])
)

# RECOMMENDED FLAGS: https://www.flaticon.com/packs/flags-collection
class Language(models.Model):
    """Stores basic info about language.

    Attributes:
        name: language name
        flag: flag of country related with language
    """
    name = models.CharField(max_length=128, db_index=True, unique=True)
    flag = models.ImageField(upload_to="flags/")

    def __str__(self):
        return self.name


class ApprovedExerciseManager(models.Manager):
    """Exercise model manager for game sessions."""
    use_for_related_fields = True

    def approved(self, *args, **kwargs):
        """Gets only approved Exercise objects.

        Returns:
            Queryset with approved exercises
        """
        return self.filter(approved=True, *args, **kwargs)

    def random(self, amount=10, *args, **kwargs):
        """Gets only random and approved Exercise objects.

        Arg:
            amount: amount of random Exercise objects
        Returns:
            Queryset with random and approved exercises
        """
        query = self.approved(*args, **kwargs)
        exercises = list(query.values_list("id", flat=True))

        if amount > len(exercises):
            return query.filter(id__in=exercises)
        else:
            return query.filter(id__in=random.sample(exercises, amount))


class Exercise(ContentHelper, models.Model):
    """Stores info about exercise type, content and answers.

    Attributes:
        TYPE_OF_EXERCISE: tuple that contains info about type of exercise(
        word or phrase)
        type: type of exercise
        content: content of exercise
        language: language of exercise
        answers: answers for exercise
        categories: categories related with exercise
        approved: boolean that indicates if exercise content has been approved
        objects: custom model manager that provide methods
    """
    WORD = "WO"
    PHRASE = "PH"
    TYPE_OF_EXERCISE = (
        (WORD, "Word"),
        (PHRASE, "Phrase")
    )
    type = models.CharField(
        max_length=2,
        choices=TYPE_OF_EXERCISE,
        default=WORD
    )
    content = models.TextField()
    language = models.ForeignKey(
        Language,
        on_delete=models.CASCADE,
        related_name="exercises"
    )
    answers = models.ManyToManyField(
        "self",
        related_name="answers",
        blank=True
    )
    approved = models.BooleanField(default=False)
    objects = ApprovedExerciseManager()

    def get_answer(self):
        """Gets answer for Exercise object related with english language.

        Returns:
            English answer for Exercise object
        """
        return self.answers.filter(language__name="English").first()

    def __str__(self):
        return f"{self.shorter_content} - {self.language.name}"


class GameReport(models.Model):
    """Stores info about already finished game sessions.

    Attributes:
        language: language of played game session
        class_group: class group that played game session
        players: student (users) that took part in the game
        type_of_game: type of played game
        started: time when game session has been started
        finished: time when game session has been finished
    """
    language = models.ForeignKey(
        Language,
        related_name="reports",
        on_delete=models.CASCADE
    )
    class_group = models.ForeignKey(
        ClassGroup,
        related_name="reports",
        on_delete=models.CASCADE
    )
    players = models.ManyToManyField(
        User,
        related_name="reports"
    )
    type_of_game = models.CharField(
        max_length=2,
        choices=GAMES_CHOICES
    )
    started = models.DateTimeField()
    finished = models.DateTimeField()
    report_file = models.FileField()

    def __str__(self):
        game_date = self.started.strftime("%d.%m.%Y")

        return f"{game_date} - {self.class_group.name}"

    @staticmethod
    def create_report(game_finish_data):
        """Creates Game report object of finished game session.

        Arg:
            game_finished_data: dictionary with info about already finished
            game session
        """
        report_data = {}

        # Get two letter version of game type string
        game_type = game_finish_data["type_of_game"]
        game_type = next(c[0] for c in GAMES_CHOICES if c[1] == game_type)
        report_data["type_of_game"] = game_type

        # Parse date time in ISO format to normal datetime
        for key in ["started", "finished"]:
            value = game_finish_data[key]
            report_data[key] = dateutil.parser.parse(value)

        # Get class group id and language id of game session
        for key in ["class_group", "language"]:
            report_data[f"{key}_id"] = game_finish_data[key]["id"]

        # Create Game report object
        report = GameReport(**report_data)

        # Invoke static method responsible for generating report players file
        report.report_file =  GameReport.generate_report_file(game_finish_data)
        report.save()

        # Set game session players (student users) to be related with this game
        # session report
        players = [player["id"] for player in game_finish_data["results"]]
        report.players.set(players)

        return report

    @staticmethod
    def generate_report_file(game_finish_data):
        """Creates CSV file with id number and score of players.

        Arg:
            game_finished_data: dictionary with info about already finished
            game session
        """
        report_file_path = GameReport.generate_report_file_path(
            game_finish_data
        )

        with open(report_file_path, "w") as report_file:
            csv_writer = csv.writer(report_file)
            csv_writer.writerow(["id", "points"])

            for player in game_finish_data["results"]:
                csv_writer.writerow([player["id"], player["score"]])

            for player in game_finish_data["aborted"]:
                csv_writer.writerow([player["id"], 0])

        return File(report_file).name

    @staticmethod
    def generate_report_file_path(game_finish_data):
        """Creates CSV file path based on game class group, type and started
        date.

        Arg:
            game_finished_data:dictionary with info about already finished
            game session
        """
        class_name = game_finish_data["class_group"]["name"].replace(" ", "")
        game_type = game_finish_data["type_of_game"]

        game_started = game_finish_data["started"]
        game_started = game_started.replace(":", "_").replace(".", "_")

        report_dir_path = os.path.join(settings.MEDIA_ROOT, "reports")
        report_file_name = f"{game_started}_{class_name}_{game_type}.csv"

        return os.path.join(report_dir_path, report_file_name)
