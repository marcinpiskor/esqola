from django.contrib import admin
from .models import Language, GameReport, Exercise


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    list_display = ("name", "flag")


@admin.register(GameReport)
class GameReportAdmin(admin.ModelAdmin):
    list_display = ("class_group", "language", "type_of_game", "started")


@admin.register(Exercise)
class ExerciseAdmin(admin.ModelAdmin):
    list_display = ("content", "language")

    def content(self, obj):
        return self.obj.shorter_content
