import re
import json
import random
from datetime import datetime
from django.db.models import Q
from authorization.models import Profile
from .models import Exercise, GameReport
from .serializers import ExerciseSerializer
from .utils import FinishGameMixin


class BaseGame:
    """Parent class for game module classes.

    Attribute:
        finish_key_function: (optional) lambda function used during sorting
        game finish score
        start_key: list with game session keys which value of needs to be
        send to players as initial message
        parent: Game consumer object
    """
    finish_key_function = None
    start_keys = []

    def __init__(self, parent):
        self.parent = parent

    def start_game(self, game_session):
        """
        Fills game session dictionary with info necessary to start a game (
        questions, answers etc.).

        Arg:
            game_session: dictionary with info about current game session
        """
        raise NotImplementedError

    def get_game_start_data(self, game_session):
        """
        Prepares dictionary with initial game session data based on start keys
        list.

        Arg:
            game_session: dictionary with info about current game session
        """
        return {key: game_session[key] for key in self.start_keys}

    async def verify_answer(self, answer_data, game_session):
        """
        Verifies if answer on question that has been sent by player is correct.

        Args:
            answer_data: dictionary with answer on question
            game_session: dictionary with info about current game session
        """
        raise NotImplementedError

    async def check_if_finished(self, game_session, current_score=False):
        """
        Checks if after answering on question or disconneting from game, game
        needs to be ended.

        Args:
            game_session: dictionary with info about current game session
            current_score: boolean that indicates if this method has been
            invoked because some user has been disconnected or answer has been
            sent
        """
        raise NotImplementedError

    def finish_game(self, game_session, game_session_finish):
        raise NotImplementedError

    async def end_game_and_generate_report(self, game_session):
        """
        Invokes methods responsible for finishing game, updating player points
        and generating final game session report.

        Arg:
            game_session: dictionary with info about ended game session
        """
        game_finish_data = self.parent.finish_game(game_session)

        await self.parent.channel_layer.group_send(
            self.parent.game_group_name,
            {
                "type": "game_finished",
                "message": game_finish_data
            }
        )

        Profile.update_scores(game_finish_data["results"])
        GameReport.create_report(game_finish_data)


class MatchGame(BaseGame):
    name = "match"
    start_keys = [
        "class_group", "language", "status", "teacher", "type_of_game",
        "content", "started"
    ]

    def start_game(self, game_session):
        game_session["content"] = self.prepare_match_content(
            game_session["language"]["id"]
        )
        game_session["answers_amount"] = len(game_session["content"][0])*2

        self.parent.redis.hset("games", self.parent.game_id, game_session)

    def prepare_match_content(self, lang_id):
        foreign_language = []
        english_language = []

        # Get random Exercise word objects
        exercises = Exercise.objects.random(
            type="WO", language__id=lang_id
        ).prefetch_related("answers").all()

        # Prepare lists with serialized exercises and answers on it
        for exercise in exercises:
            answer = exercise.get_answer()

            foreign_language.append(ExerciseSerializer(exercise).data)
            english_language.append(ExerciseSerializer(answer).data)

        game_content = [foreign_language, english_language]
        [random.shuffle(lang) for lang in game_content]

        return game_content

    async def verify_answer(self, answer_data, game_session):
        correct, current_user_score = await self.check_answer(
            answer_data, game_session
        )

        if correct:
            # If user has has matched all words already, finish game for this
            # user and check if is necessary to end game
            if len(current_user_score) == game_session["answers_amount"]:
                user_id = str(self.parent.scope["user"].id)
                game_session["finished"].append(user_id)
                await self.check_if_finished(game_session, current_user_score)

            else:
                await self.send_game_update(game_session, current_user_score)

    async def check_answer(self, text_data, game_session):
        if "word_1" in text_data and "word_2" in text_data:
            word_1 = text_data["word_1"]
            word_2 = text_data["word_2"]
            # If two chosen words are related to each other that means that
            # answer is correct
            correct = Exercise.objects.approved(
                id=word_1, answers__id=word_2
            ).count() > 0

            user_id = str(self.parent.scope["user"].id)
            current_user_score = game_session["scores"][user_id]

            if correct:
                # Add id numbers of matched words to current user score list
                current_user_score.extend([word_1, word_2])
                return True, current_user_score
            else:
                return False, current_user_score

    async def check_if_finished(self, game_session, current_score=False):
        finished = len(game_session["finished"])
        disconnected = len(game_session["disconnected"])
        joined = len(game_session["joined"])

        if finished + disconnected == joined:
            await self.end_game_and_generate_report(game_session)
        elif current_score:
            await self.send_game_update(game_session, current_score)

    async def send_game_update(self, game_session, current_user_score):
        self.parent.redis.hset("games", self.parent.game_id, game_session)

        await self.parent.send(text_data=json.dumps({
            "type": "game_update",
            "game_data": {"marked_words": current_user_score}
        }))

    def finish_game(self, game_session, game_session_finish):
        i = 1

        for user_id in game_session["finished"]:
            user_data = game_session["joined"][user_id]
            user_data["score"] = int(1000/i)
            game_session_finish["results"].append(user_data)

            i += 0.25

        for user_id in game_session["disconnected"]:
            user_data = game_session["joined"][user_id]
            user_data["score"] = "-"
            game_session_finish["aborted"].append(user_data)


class ChooseGame(FinishGameMixin, BaseGame):
    name = "choose"
    start_keys = [
        "class_group", "language", "status", "teacher", "type_of_game",
        "started", "current_question", "answered"
    ]
    finish_key_function = lambda self, x: len(x[1])

    def start_game(self, game_session):
        game_session["content"] = self.prepare_choose_content(
            game_session["language"]["id"]
        )
        game_session["current_question"] = 0
        game_session["answered"] = 0

        self.parent.redis.hset("games", self.parent.game_id, game_session)

    def get_game_start_data(self, game_session):
        data = super().get_game_start_data(game_session)
        data["current_question"] = game_session["content"][
            data["current_question"]
        ]

        return data

    def prepare_choose_content(self, lang_id):
        questions = []

        # Get random Exercise word and phrase objects
        exercises = Exercise.objects.random(
            language__id=lang_id
        ).prefetch_related("answers").all()

        # Get random Exercise word and phrase objects in english that will be
        # used as answers
        answers = Exercise.objects.random(
            30, ~Q(id__in=exercises.values_list("answers", flat=True)),
            language__name="English"
        )

        offset = 0

        for exercise in exercises:
            question = ExerciseSerializer(exercise).data,
            answers_of_question = [
                ExerciseSerializer(exercise.get_answer()).data
            ]

            # Get 3 wrong answers for this question
            for wrong_answer in answers[offset:offset+3]:
                serialized_answer = ExerciseSerializer(wrong_answer).data
                answers_of_question.append(serialized_answer)

            offset += 3
            random.shuffle(answers_of_question)

            question[0]["answers"] = answers_of_question
            question[0]["answered_by"] = []
            questions.append(question[0])

        random.shuffle(questions)

        return questions

    async def verify_answer(self, answer_data, game_session):
        correct, current_question = await self.check_answer(
            answer_data, game_session
        )

        if correct:
            game_session["scores"][str(self.parent.scope["user"].id)].append(
                current_question["id"]
            )

        await self.check_if_finished(game_session, True)

    async def check_if_finished(self, game_session, answered=False):
        current_question = game_session["content"][
            game_session["current_question"]
        ]

        answered_by = set(current_question["answered_by"])
        disconnected = set(game_session["disconnected"])
        joined = set(game_session["joined"])

        # End a game or load a next question if all active users has answered
        # on current question
        if answered_by - disconnected == joined - disconnected:
            game_session["current_question"] += 1

            if self.is_last_question_or_disconnected(game_session):
                await self.end_game_and_generate_report(game_session)

            else:
                self.parent.redis.hset(
                    "games", self.parent.game_id, game_session
                )
                new_question = game_session["content"][
                    game_session["current_question"]
                ]

                await self.parent.channel_layer.group_send(
                    self.parent.game_group_name,
                    {
                        "type": "game_updated",
                        "message": {
                            "answered": 0,
                            "current_question": new_question
                        }
                    }
                )
        elif answered:
            self.parent.redis.hset("games", self.parent.game_id, game_session)

            await self.parent.send(text_data=json.dumps({
                "type": "game_update",
                "game_data": {"answered": 1}
            }))

    async def check_answer(self, text_data, game_session):
        if "answer" in text_data:
            answer_id = text_data["answer"]
            current_question = game_session["content"][
                game_session["current_question"]
            ]
            game_answer_id = current_question["id"]

            # Answer is correct when passed answer is related with current
            # question
            correct = Exercise.objects.approved(
                id=answer_id, answers__id=game_answer_id
            ).count() > 0

            user_id = str(self.parent.scope["user"].id)
            current_question["answered_by"].append(user_id)

            return correct, current_question

    def is_last_question_or_disconnected(self, game_session):
        """
        Checks if that was the last question of the game or if all players have
        left the game before end of it.

        Args:
            game_session:dictionary with info about current game session
        """
        current_question = game_session["current_question"]
        amount_of_questions = len(game_session["content"])

        return (
            current_question == amount_of_questions or
            len(game_session["disconnected"]) == len(game_session["joined"])
        )


class EnterGame(FinishGameMixin, BaseGame):
    name = "enter"
    start_keys = [
        "class_group", "language", "status", "teacher", "type_of_game",
        "started"
    ]
    finish_key_function = lambda self, x: sum(x[1])

    def start_game(self, game_session):
        game_session["content"] = self.prepare_enter_content(
            game_session["language"]["id"]
        )

        self.parent.redis.hset("games", self.parent.game_id, game_session)

    def prepare_enter_content(self, lang_id):
        questions = []

        # Get random Exercise word and phrase objects
        exercises = Exercise.objects.random(language__id=lang_id).all()

        for exercise in exercises:
            question = ExerciseSerializer(exercise).data
            questions.append(question)

        random.shuffle(questions)

        return questions

    def get_game_start_data(self, game_session):
        data = super().get_game_start_data(game_session)
        data["current_question"] = game_session["content"][0]

        return data

    async def verify_answer(self, answer_data, game_session):
        correct, current_user_score = await self.check_answer(
            answer_data, game_session
        )

        # If user has has answered on all questions already, finish game for
        # this user and check if is necessary to end game
        if len(current_user_score) == len(game_session["content"]):
            game_session["finished"].append(str(self.parent.scope["user"].id))
            await self.check_if_finished(game_session, True)
        # Otherwise, load another question
        else:
            self.parent.redis.hset("games", self.parent.game_id, game_session)
            new_question = game_session["content"][len(current_user_score)]

            await self.parent.send(text_data=json.dumps({
                "type": "game_update",
                "game_data": {"current_question": new_question}
            }))

    async def check_answer(self, text_data, game_session):
        if "answer" in text_data:
            value = re.sub(r"[  ]+", " ", text_data["answer"].strip())
            user_id = str(self.parent.scope["user"].id)
            current_user_score = game_session["scores"][user_id]
            current_question = game_session["content"][len(current_user_score)]

            # Answer is correct when passed answer is related with current
            # question
            correct = Exercise.objects.approved(
                id=current_question["id"],
                answers__content__iexact=value
            ).count() > 0

            if correct:
                current_user_score.append(1)
                return True, current_user_score
            else:
                current_user_score.append(0)
                return False, current_user_score

    async def check_if_finished(self, game_session, answered=False):
        finished = len(game_session["finished"])
        disconnected = len(game_session["disconnected"])
        joined = len(game_session["joined"])

        if finished + disconnected == joined:
            await self.end_game_and_generate_report(game_session)
        elif answered:
            self.parent.redis.hset("games", self.parent.game_id, game_session)
            finish_info = {"id": 0, "content": "Wait until end of the game"}

            await self.parent.send(text_data=json.dumps({
                "type": "game_update",
                "game_data": {"current_question": finish_info}
            }))


class CharadesGame(BaseGame):
    name = "charades"
    start_keys = [
        "class_group", "language", "status", "teacher", "type_of_game",
        "started", "painter", "messages", "image"
    ]

    def start_game(self, game_session):
        lang_id = game_session["language"]["id"]
        question = self.prepare_charades_content(lang_id)

        game_session["content"] = question.content
        game_session["content_translation"] = question.get_answer().content
        game_session["painter"] = random.choice(list(game_session["joined"]))
        game_session["image"] = ""
        game_session["winner"] = ""
        game_session["messages"] = []

        self.parent.redis.hset("games", self.parent.game_id, game_session)

    def prepare_charades_content(self, lang_id):
        return Exercise.objects.random(
            1, language__id=lang_id
        ).prefetch_related("answers").first()

    def get_game_start_data(self, game_session):
        data = super().get_game_start_data(game_session)

        if str(self.parent.scope["user"].id) == game_session["painter"]:
            for key in ["content", "content_translation"]:
                data[key] = game_session[key]

        return data

    async def verify_answer(self, answer_data, game_session):
        if str(self.parent.scope["user"].id) == game_session["painter"]:
            if "image" in answer_data:
                await self.parent.channel_layer.group_send(
                    self.parent.game_group_name,
                    {
                        "type": "game_updated",
                        "message": {"image": answer_data["image"]}
                    }
                )
        else:
            await self.check_answer(answer_data, game_session)

    async def check_answer(self, answer_data, game_session):
        if "answer" in answer_data:
            user_answer = answer_data["answer"].lower().strip()
            user_answer = re.sub(r"[  ]+", " ", user_answer)
            correct_answer = game_session["content"].lower()

            # End game if correct answer has been passed
            if user_answer == correct_answer:
                game_session["winner"] = str(self.parent.scope["user"].id)
                await self.end_game_and_generate_report(game_session)
            # Otherwise, send chat message with passed answer
            else:
                await self.parent.channel_layer.group_send(
                    self.parent.game_group_name,
                    {
                        "type": "game_answered",
                        "message": {"content": answer_data["answer"]}
                    }
                )

    async def check_if_finished(self, game_session):
        user_id = str(self.parent.scope["user"].id)

        if (user_id == game_session["painter"] or
            len(game_session["disconnected"]) == len(game_session["joined"])-1):
            # End game if painter has left the game or if player is only player
            game_finish_data = self.parent.finish_game(game_session)

            await self.parent.channel_layer.group_send(
                self.parent.game_group_name,
                {
                    "type": "game_finished",
                    "message": game_finish_data
                }
            )

            GameReport.create_report(game_finish_data)

    def finish_game(self, game_session, game_session_finish):
        game_session_finish["answer"] = game_session["content"]

        for user_id, user_data in game_session["joined"].items():
            if user_id != game_session["winner"]:
                user_data["score"] = 0
                game_session_finish["results"].append(user_data)
            else:
                user_data["score"] = 1000
                winner_data = user_data
                game_session_finish["results"].insert(0, user_data)


class GameManager:
    """Class that contains factory method for game handler.

    Attributes:
        type_of_game: name of desired game to play
        parent: Game consumer object
    """
    game_handlers = [MatchGame, ChooseGame, EnterGame, CharadesGame]

    def __init__(self, type_of_game, parent):
        self.type_of_game = type_of_game
        self.parent = parent

    def get_game_handler(self):
        """
        Creates one of game handler classes instance based on passed type of
        game.

        Returns:
            Class handler instance of particular game
        """
        for game in self.game_handlers:
            if game.name == self.type_of_game:
                return game(self.parent)
