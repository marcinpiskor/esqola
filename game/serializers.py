import csv
import redis
from rest_framework import serializers
from authorization.models import ClassGroup
from authorization.serializers import UserGameSessionSerializer
from management.serializers import ClassGroupGameSessionSerializer
from .models import Language, Exercise, GameReport, TYPE_OF_GAMES


class LanguageSerializer(serializers.ModelSerializer):
    """Language model serializer for read-only purposes."""
    class Meta:
        model = Language
        fields = ("id", "name", "flag")


class ExerciseSerializer(serializers.ModelSerializer):
    """Exercise model serializer for read-only purposes."""
    class Meta:
        model = Exercise
        fields = ("id", "content")


class GameSessionSerializer(serializers.Serializer):
    """Game session serializer that handles creating game session.

    Attributes:
        class_group: id of Class group object that will be playing the game
        language: id of Language object that will provide exercises for game
        type_of_game: string that contains name of desired type of game
    """
    class_group = serializers.IntegerField()
    language = serializers.IntegerField()
    type_of_game = serializers.ChoiceField(choices=TYPE_OF_GAMES)

    def validate_class_group(self, value):
        """
        Validates if Class group object exist and all requirements are
        fulfilled.

        Arg:
            value: Class group object id
        Returns:
            Class group object
        Raises
            Validation error with message that class group doesn't exist or
            doesn't fullfil all requirements
        """
        class_group_obj = ClassGroup.objects.filter(id=value).select_related("teacher").prefetch_related("students").first()

        if not class_group_obj:
            error = {"id": ["Class group with that id doesn't exist."]}
            raise serializers.ValidationError(error)
        elif class_group_obj.teacher != self.context["request"].user:
            error = {
                "teacher": [
                    "Only teacher of class group can start game session."
                ]
            }
            raise serializers.ValidationError(error)
        elif class_group_obj.students.count() < 2:
            error = {
                "students": [
                    "At least two student are required to start game session."
                ]
            }
            raise serializers.ValidationError(error)

        return class_group_obj

    def validate_language(self, value):
        """Validates if Language object exists.

        Arg:
            value: Language object id
        Returns:
            Language object
        Raises
            Validation error with message that language doesn't exist
        """
        language_obj = Language.objects.filter(id=value).exclude(
            name="English"
        ).first()

        if not language_obj:
            error = {"id": ["Language with that id doesn't exist."]}
            raise serializers.ValidationError(error)

        return LanguageSerializer(language_obj).data

    def validate(self, validated_data):
        validated_data["teacher"] = validated_data["class_group"].teacher.id
        validated_data["status"] = "start"
        validated_data["joined"] = {}

        return validated_data

    def create(self, validated_data):
        allowed_users = validated_data["class_group"].students.values_list(
            "id", flat=True
        )

        # Fill game session data with users allowed to play the game and info
        # about class group
        validated_data["allowed_users"] = list(allowed_users)
        validated_data["class_group"] = ClassGroupGameSessionSerializer(
            validated_data["class_group"]
        ).data

        # Set game session in redis
        redis_handler = redis.StrictRedis()
        redis_handler.hset(
            "games", validated_data["class_group"]["id"], validated_data
        )

        return validated_data


class GameReportListSerializer(serializers.ModelSerializer):
    """Game report model serializer for list action."""
    class Meta:
        model = GameReport
        fields = (
            "id", "class_group", "language", "type_of_game", "finished"
        )


class GameReportDetailSerializer(serializers.ModelSerializer):
    """Game report model serializer for retrieve action."""
    class_group = ClassGroupGameSessionSerializer(read_only=True)
    language = LanguageSerializer(read_only=True)
    players = UserGameSessionSerializer(many=True)
    report_file = serializers.SerializerMethodField()

    class Meta:
        model = GameReport
        fields = (
            "id", "class_group", "language", "type_of_game", "players",
            "started", "finished", "report_file"
        )

    def get_report_file(self, obj):
        """Converts CSV file with game session scores to list.

        Arg:
            obj: Game report object
        """
        data = []

        with open(obj.report_file.name) as report_file:
            csv_reader = csv.DictReader(report_file)
            data = list(csv_reader)

        return data
