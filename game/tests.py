import re
import json
import time
import redis
from datetime import date, datetime, timedelta
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from django.test import TestCase
from django.shortcuts import reverse
from django.core.management import call_command
from django.contrib.auth.models import User, Group
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from authorization.models import City, School, ClassGroup, Profile
from authorization.utils import (
    SeleniumViewHelper,
    new_user_data,
    new_city_data,
    basic_user_data
)
from management.utils import principal_data, teacher_data, student_data
from main.utils import InfoBlockHelper
from .permissions import GAME_REPORT_ALLOWED_PARAMS
from .utils import GameTestHelper
from .models import Language, GameReport, TYPE_OF_GAMES, GAMES_CHOICES


class LanguageAPITest(GameTestHelper, TestCase):
    def setUp(self):
        user_data = dict(new_user_data)
        user_data.pop("password_repeat")

        self.user = User.objects.create_user(**user_data)
        self.user.save()

        self.languages = ["English", "German", "Polish"]

        for lang in self.languages:
            new_lang = Language.objects.create(
                name=lang, flag=self.generate_image(lang)
            )

    def test_list_without_login(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/language/")
        self.assertEqual(res.status_code, 403)

    def test_retrieve_without_login(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        res = self.client.get(f"/api/language/{Language.objects.first().id}/")
        self.assertEqual(res.status_code, 403)

    def test_list_correct(self):
        """
        Tests list action with user logged in. Languages should be returned.
        """
        self.client.force_login(self.user)

        res = self.client.get("/api/language/")
        self.assertEqual(res.status_code, 200)
        res_json = json.loads(res.content)

        # English language hasn't been returned
        self.assertRaises(
            StopIteration,
            lambda: next(
                lang for lang in res_json if lang["name"] == "English"
            )
        )
        self.assertEqual(len(res_json), len(self.languages) - 1)

    def test_retrieve_correct(self):
        """
        Tests retrieve action with user logged in. Language should be returned.
        """
        self.client.force_login(self.user)

        language_obj = Language.objects.exclude(name="English").first()

        res = self.client.get(f"/api/language/{language_obj.id}/")
        self.assertEqual(res.status_code, 200)
        res_json = json.loads(res.content)

        self.assertEqual(res_json["id"], language_obj.id)
        self.assertEqual(res_json["name"], language_obj.name)
        self.assertTrue(
            re.match(f".*flag-{language_obj.name}-.*.png$", res_json["flag"])
        )


class GameSessionAPITest(GameTestHelper, TestCase):
    def setUp(self):
        call_command("creategroups")
        self.redis = redis.StrictRedis()

        self.language = Language.objects.create(
            name="German", flag=self.generate_image("German")
        )
        self.city = City.objects.create(**new_city_data)

        self.principal_data = dict(new_user_data)
        self.principal_data.pop("password_repeat")

        self.principal = User.objects.create_user(**self.principal_data)
        self.principal.groups.add(Group.objects.get(name="principals"))

        self.teacher_data = dict(teacher_data)

        self.teacher = User.objects.create_user(**self.teacher_data)
        self.teacher.groups.add(Group.objects.get(name="teachers"))

        self.student_data_1 = dict(student_data)
        self.student_data_2 = dict(basic_user_data)

        self.student_group = Group.objects.get(name="students")

        self.student_1 = User.objects.create_user(**self.student_data_1)
        self.student_2 = User.objects.create_user(**self.student_data_2)

        for student in [self.student_1, self.student_2]:
            student.groups.add(self.student_group)

        school = School.objects.create(
            name="University of Cambridge", city=self.city,
            principal=self.principal
        )

        self.class_group = ClassGroup.objects.create(
            school=school, name="test", teacher=self.teacher
        )
        self.class_group.students.add(self.student_1.id, self.student_2.id)

        self.game_session_data = {
            "language": self.language.id,
            "class_group": self.class_group.id,
            "type_of_game": TYPE_OF_GAMES[0]
        }

    def tearDown(self):
        super().tearDown()
        self.redis.flushall()

    def test_post_without_user(self):
        """
        Tests POST method without user logged in. Forbidden error expected.
        """
        res = self.client.post("/api/game-session/", self.game_session_data)
        self.assertEqual(res.status_code, 403)

    def test_delete_without_user(self):
        """
        Tests DELETE method without user logged in. Forbidden error expected.
        """
        res = self.client.delete(
            "/api/game-session/1/", self.game_session_data
        )
        self.assertEqual(res.status_code, 403)

    def test_post_successful(self):
        """Tests POST method with valid data. Game session should be created."""
        self.client.force_login(self.teacher)

        # There is no active game sessions now
        self.assertFalse(self.redis.hgetall("games"))

        res = self.client.post("/api/game-session/", self.game_session_data)
        self.assertEqual(res.status_code, 201)
        self.assertTrue(self.redis.hgetall("games"))

        # Game session has been created
        game_session = self.redis.hget("games", str(self.class_group.id))
        game_session = json.loads(game_session.decode("utf8").replace("'", '"'))
        self.assertIsNotNone(game_session)

        exp_game_session = {
            "class_group": {
                key: getattr(self.class_group, key) for key in ["id", "name"]
            },
            "language": {
                **{key: getattr(self.language, key) for key in ["id", "name"]},
                "flag": f"/media/{self.language.flag}",
            },
            "type_of_game": self.game_session_data["type_of_game"],
            "teacher": self.teacher.id,
            "status": "start",
            "joined": {},
            "allowed_users": [s.id for s in self.class_group.students.all()]
        }

        self.assertJSONEqual(json.dumps(exp_game_session), game_session)

    def test_post_invalid_class_group_doesnt_exist(self):
        """
        Tests POST method with invalid data. Game session shouldn't be created.
        """
        self.client.force_login(self.teacher)

        self.game_session_data["class_group"] = 123456
        exp_res = {
            "class_group": {
                "id": ["Class group with that id doesn't exist."]
            }
        }

        # Class group with that id doesn't exist
        res = self.client.post("/api/game-session/", self.game_session_data)
        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_post_invalid_user_isnt_teacher(self):
        """
        Tests POST method with invalid data. Game session shouldn't be created.
        """
        self.client.force_login(self.teacher)

        self.class_group.teacher = None
        self.class_group.save()

        exp_res = {
            "class_group": {
                "teacher": [
                    "Only teacher of class group can start game session."
                ]
            }
        }

        # Current user is not a teacher of class group
        res = self.client.post("/api/game-session/", self.game_session_data)
        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_post_invalid_no_students(self):
        """
        Tests POST method with invalid data. Game session shouldn't be created.
        """
        self.client.force_login(self.teacher)
        self.class_group.students.set([])
        self.class_group.save()

        exp_res = {
            "class_group": {
                "students": [
                    "At least two student are required to start game session."
                ]
            }
        }

        # Class group has no students
        res = self.client.post("/api/game-session/", self.game_session_data)
        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_post_invalid_invalid_language(self):
        """
        Tests POST method with invalid data. Game session shouldn't be created.
        """
        self.client.force_login(self.teacher)
        self.game_session_data["language"] = 123456

        exp_res = {
            "language": {"id": ["Language with that id doesn't exist."]}
        }

        # Language with that id doesn't exist
        res = self.client.post("/api/game-session/", self.game_session_data)
        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_post_invalid_type_of_game(self):
        """
        Tests POST method with invalid data. Game session shouldn't be created.
        """
        self.client.force_login(self.teacher)
        invalid_type_of_game = "this is invalid game type"
        self.game_session_data["type_of_game"] = invalid_type_of_game

        msg = f'"{invalid_type_of_game}" is not a valid choice.'
        exp_res = {"type_of_game": [msg]}

        # Type of game is invalid
        res = self.client.post("/api/game-session/", self.game_session_data)
        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_delete_successful(self):
        """
        Tests DELETE method with successful data. Game session should be
        deleted.
        """
        self.client.force_login(self.teacher)

        self.client.post("/api/game-session/", self.game_session_data)
        self.assertTrue(self.redis.hgetall("games"))

        res = self.client.delete(f"/api/game-session/{self.class_group.id}/")
        self.assertEqual(res.status_code, 204)
        self.assertFalse(self.redis.hgetall("games"))

    def test_delete_game_session_invalid_user_isnt_teacher(self):
        """
        Tests DELETE method with invalid data. Game session should not be
        deleted.
        """
        self.client.force_login(self.teacher)

        self.client.post("/api/game-session/", self.game_session_data)
        self.assertTrue(self.redis.hgetall("games"))

        self.class_group.teacher = None
        self.class_group.save()

        exp_res = {
            "class_group": {
                "teacher": [
                    "Only teacher of class group can delete game session."
                ]
            }
        }

        # Current user is not teacher of class group related with game session
        res = self.client.delete(f"/api/game-session/{self.class_group.id}/")
        self.assertEqual(res.status_code, 403)
        self.assertTrue(self.redis.hgetall("games"))
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_active_not_empty(self):
        """
        Tests active action. Id numbers of active game session should be
        returned.
        """
        self.client.force_login(self.teacher)
        self.client.post("/api/game-session/", self.game_session_data)

        self.client.force_login(self.student_1)
        res = self.client.get("/api/game-session/active/")
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            res.content.decode("utf-8"), [self.class_group.id]
        )

    def test_active_empty(self):
        """Tests active action. Empty list should be returned."""
        self.client.force_login(self.teacher)
        self.client.post("/api/game-session/", self.game_session_data)

        new_student = User.objects.create_user(username="marian")
        new_student.groups.add(self.student_group)

        # New student user isn't related with any active game sessions
        self.client.force_login(new_student)
        res = self.client.get("/api/game-session/active/")
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(res.content.decode("utf-8"), [])


class GameReportAPI(GameTestHelper, TestCase):
    def setUp(self):
        call_command("creategroups")

        self.language = Language.objects.create(
            name="German", flag=self.generate_image("German")
        )
        self.city = City.objects.create(**new_city_data)

        self.principal_data = dict(new_user_data)
        self.principal_data.pop("password_repeat")

        self.principal = User.objects.create_user(**self.principal_data)
        self.principal.groups.add(Group.objects.get(name="principals"))

        self.teacher_data = dict(teacher_data)

        self.teacher = User.objects.create_user(**self.teacher_data)
        self.teacher.groups.add(Group.objects.get(name="teachers"))

        self.student_data = dict(student_data)
        self.student = User.objects.create_user(**self.student_data)
        self.student.groups.add(Group.objects.get(name="students"))

        school = School.objects.create(
            name="University of Cambridge", city=self.city,
            principal=self.principal
        )

        self.class_group = ClassGroup.objects.create(
            school=school, name="test", teacher=self.teacher
        )
        self.class_group.students.add(self.student)

        now = datetime.now()

        self.game_session_finish_data = {
            "language": {
                "id": self.language.id,
                "name": self.language.name
            },
            "class_group": {
                "id": self.class_group.id,
                "name": self.class_group.name,
            },
            "results": [
                {
                    "id": self.student.id,
                    "score": 1000
                }
            ],
            "aborted": [],
            "started": (now - timedelta(minutes=10)).isoformat(),
            "finished": now.isoformat(),
            "type_of_game": TYPE_OF_GAMES[0]
        }

        self.report = GameReport.create_report(self.game_session_finish_data)

    def test_list_without_login(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/game-report/")
        self.assertEqual(res.status_code, 403)

    def test_retrieve_without_login(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/game-report/1/")
        self.assertEqual(res.status_code, 403)

    def test_list_with_valid_query_params(self):
        """
        Tests list action with correct data. Game reports should be returned.
        """
        self.client.force_login(self.principal)
        url = f"/api/game-report/?class_group__id={self.class_group.id}"

        exp_res = {
            "id": self.report.id,
            "class_group": self.class_group.id,
            "language": self.language.id,
            "type_of_game": GAMES_CHOICES[0][0],
            "finished": f"{self.game_session_finish_data['finished']}Z"
        }

        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(res.content.decode("utf-8"), [exp_res])

    def test_list_without_query_params(self):
        """Tests list action with invalid data. Error should be returned."""
        self.client.force_login(self.principal)
        exp_res = {"detail": "Query params are required in this request."}

        # Query parameters are required in list action
        res = self.client.get("/api/game-report/")
        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(res.content.decode("utf-8"), exp_res)

    def test_list_with_invalid_query_params(self):
        """Tests list action with invalid data. Error should be returned."""
        self.client.force_login(self.principal)
        url = "/api/game-report/?invalid_query__param=1"

        msg = "Only these query params are allowed in this request: " \
                            f"{', '.join(GAME_REPORT_ALLOWED_PARAMS)}."
        exp_res = {"detail": msg}

        # Only a few query params are allowed for list action
        res = self.client.get(url)
        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(res.content.decode("utf-8"), exp_res)

    def test_retrieve_as_principal_of_school(self):
        """
        Tests retrieve action with principal of school related with class group.
        Game report should be returned.
        """
        self.client.force_login(self.principal)
        self.check_valid_retrieve()

    def test_retrieve_as_teacher_of_class_group(self):
        """
        Tests retrieve action with teacher of class group. Game report should be
        returned.
        """
        self.client.force_login(self.teacher)
        self.check_valid_retrieve()

    def test_retrieve_as_student_of_game_session(self):
        """
        Tests retrieve action with student that has played the game. Game report
        should be returned.
        """
        self.client.force_login(self.teacher)
        self.check_valid_retrieve()

    def test_retrieve_as_user_unrelated_with_game_session_report(self):
        """
        Tests retrieve action with student that has played the game. Game report
        should be returned.
        """
        new_student = User.objects.create_user(username="marian")
        self.client.force_login(new_student)

        msg = "Current user needs to be teacher of class group related with " \
                "game report, principal of school which is related with that" \
                " class group or student that has played the game."
        exp_res = {"detail": msg}

        res = self.client.get(f"/api/game-report/{self.report.id}/")
        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(res.content.decode("utf-8"), exp_res)

    def check_valid_retrieve(self):
        """
        Checks if valid game response valid retrieve response has been returned.
        """
        res = self.client.get(f"/api/game-report/{self.report.id}/")
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            res.content.decode("utf-8"), self.expected_response
        )

    @property
    def expected_response(self):
        """Prepares valid retrieve game report response."""
        player_score = self.game_session_finish_data["results"][0]
        player_score["id"] = str(self.student.id)
        player_score["points"] = str(player_score["score"])
        del player_score["score"]

        return {
            "id": self.report.id,
            "class_group": {
                "id": self.class_group.id,
                "name": self.class_group.name,
            },
            "language": {
                "id": self.language.id,
                "name": self.language.name,
                "flag": f"http://testserver{self.language.flag.url}"
            },
            "type_of_game": GAMES_CHOICES[0][0],
            "players": [
                {
                    "id": self.student.id,
                    "username": self.student.username
                }
            ],
            "started": f"{self.game_session_finish_data['started']}Z",
            "finished": f"{self.game_session_finish_data['finished']}Z",
            "report_file": self.game_session_finish_data["results"]
        }


class GameSetupSeleniumTest(
        SeleniumViewHelper,
        GameTestHelper,
        InfoBlockHelper,
        StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()
        call_command("creategroups")
        self.city = City.objects.create(**new_city_data)

        self.principal_data = dict(new_user_data)
        self.principal_data.pop("password_repeat")
        self.principal = User.objects.create_user(**self.principal_data)

        self.teacher_data = dict(teacher_data)
        self.teacher = User.objects.create_user(**self.teacher_data)

        self.student_data = dict(student_data)
        self.student = User.objects.create_user(**self.student_data)

        account_data = [
            (self.principal, Group.objects.filter(name="principals").first()),
            (self.teacher, Group.objects.filter(name="teachers").first()),
            (self.student, Group.objects.filter(name="students").first()),
        ]

        for user, group in account_data:
            Profile.objects.create(
                user=user, city=self.city, birthday=date(1992, 10, 10)
            )
            user.groups.add(group)
            user.save()

        school = School.objects.create(
            name="University of Cambridge", city=self.city,
            principal=self.principal
        )

        self.class_group = ClassGroup.objects.create(
            school=school, name="test", teacher=self.teacher
        )
        self.class_group.students.add(self.student.id, self.principal.id)

        self.languages = [
            "English", "Polish", "Spanish", "French", "Italian", "Russian",
            "Turkish", "Portuguese", "Dutch", "Romanian", "Serbian", "German",
            "Croatian", "Albanian", "Danish", "Swedish"
        ]
        for lang in self.languages:
            new_lang = Language.objects.create(
                name=lang, flag=self.generate_image(lang)
            )

    def test_game_setup_as_not_teacher(self):
        """
        Tests game setup components as user who is not a teacher. User should
        be redirected.
        """
        self.login_user(self.principal_data)
        self.browser.get(f"{self.live_server_url}{reverse('main:home')}#game")
        time.sleep(2)
        self.assertNotIn("game", self.browser.current_url)

    def test_game_setup_as_teacher_without_class_group_id(self):
        """
        Tests game setup components as user who is not a teacher. Class group
        select, languages and game types should be displayed.
        """
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.live_server_url}{reverse('main:home')}#game")
        time.sleep(2)
        self.assertIn("game", self.browser.current_url)

        # Checking game setup heading
        setup_heading = self.browser.find_element_by_class_name(
            "game-setup-header__heading"
        )
        self.assertEqual("START A GAME FOR", setup_heading.text)

        # Class group select should be empty by default
        class_select = Select(self.browser.find_element_by_tag_name("select"))
        self.assertEqual(len(class_select.options), 2)
        null_option, class_option = class_select.options
        self.assertEqual(class_select.first_selected_option, null_option)
        self.assertEqual(null_option.get_attribute("value"), "")
        self.assertEqual(null_option.text, "---")
        self.assertEqual(
            class_option.get_attribute("value"), str(self.class_group.id)
        )
        self.assertEqual(class_option.text, self.class_group.name)

        # Checking type of game and language headings
        lang_heading, game_heading = self.browser.find_elements_by_class_name(
            "game-setup-heading"
        )

        self.assertEqual("Choose a language of this session", lang_heading.text)
        self.assertEqual("Choose a game", game_heading.text)

        # English language shouldn't be included in languages section
        self.assertEqual(
            len(self.browser.find_elements_by_class_name("game-setup-flag")),
            len(self.languages) - 1
        )

        game_types_detail = {
            "match": {
                "text": "MATCH WORDS",
                "description": "Connect english word with its proper translation."
            },
            "choose": {
                "text": "CHOOSE ANSWER",
                "description": "Choose proper translation of the word or phrase."
            },
            "charades": {
                "text": "CHARADES",
                "description": "Guess the word or phrase by looking on the picture."
            },
            "enter": {
                "text": "ENTER THE TEXT",
                "description": "Type a proper translation of the text."
            }
        }

        proper_info = []

        # Checking game type blocks have proper game name and description
        for game_type in TYPE_OF_GAMES:
            game_block = self.browser.find_element_by_class_name(
                f"game-setup-type--{game_type}"
            )
            game_name = game_block.find_element_by_class_name(
                "game-setup-type__part__heading"
            )
            game_description = game_block.find_element_by_class_name(
                "game-setup-type__part__description"
            )
            exp_game_data = game_types_detail[game_type]
            hover = ActionChains(self.browser).move_to_element(game_block)
            hover.perform()
            time.sleep(2)

            proper_info.append(
                game_name.text == exp_game_data["text"]
                and game_description.text == exp_game_data["description"]
            )

        self.assertTrue(all(proper_info))

        self.assertEqual(
            self.browser.find_element_by_class_name(
                "index-forms__button--game-setup"
            ).text,
            "START GAME SESSION"
        )

    def test_game_setup_as_teacher_with_class_group_id(self):
        """
        Tests game setup components as user who is not a teacher. Class group
        select field should be filled with class group passed in query params.
        """
        self.login_user(self.teacher_data)
        game_url = f"{self.live_server_url}{reverse('main:home')}#game/" \
                                                f"{self.class_group.id}"
        self.browser.get(game_url)
        time.sleep(2)

        # Class group shoudn't be empty by default
        class_select = Select(self.browser.find_element_by_tag_name("select"))
        self.assertEqual(
            class_select.first_selected_option.text, self.class_group.name
        )
        self.assertEqual(
            class_select.first_selected_option.get_attribute("value"),
            str(self.class_group.id)
        )

    def test_game_setup_flags(self):
        """Tests game setup flags (languages) behaviour."""
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.live_server_url}{reverse('main:home')}#game")
        time.sleep(2)

        # There is no active language (flag) yet
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name(
                "game-setup-flag--active"
            )
        )

        flags = self.browser.find_elements_by_class_name("game-setup-flag")

        # First language is not active
        self.assertNotIn(
            "game-setup-flag--active", flags[0].get_attribute("class")
        )
        self.assertNotIn(
            "game-setup-flag__img--active",
            flags[0].find_element_by_tag_name("div").get_attribute("class")
        )

        flags[0].click()
        time.sleep(2)

        # First language is active after click
        self.assertIn(
            "game-setup-flag--active", flags[0].get_attribute("class")
        )
        self.assertIn(
            "game-setup-flag__img--active",
            flags[0].find_element_by_tag_name("div").get_attribute("class")
        )

        # Second language is not active
        self.assertNotIn(
            "game-setup-flag--active", flags[1].get_attribute("class")
        )
        self.assertNotIn(
            "game-setup-flag__img--active",
            flags[1].find_element_by_tag_name("div").get_attribute("class")
        )

        flags[1].click()
        time.sleep(2)

        # After click, second language is active
        self.assertIn(
            "game-setup-flag--active", flags[1].get_attribute("class")
        )
        self.assertIn(
            "game-setup-flag__img--active",
            flags[1].find_element_by_tag_name("div").get_attribute("class")
        )

        # After click second language, first language is not active anymore
        self.assertNotIn(
            "game-setup-flag--active", flags[0].get_attribute("class")
        )
        self.assertNotIn(
            "game-setup-flag__img--active",
            flags[0].find_element_by_tag_name("div").get_attribute("class")
        )

    def test_game_setup_game_types(self):
        """Tests game types behaviour."""
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.live_server_url}{reverse('main:home')}#game")
        time.sleep(2)

        # There is no active game type yet
        self.assertRaises(
            NoSuchElementException,
            lambda: self.browser.find_element_by_class_name(
                "game-setup-type--active"
            )
        )

        # First game type is not active
        game_types = self.browser.find_elements_by_class_name(
            "game-setup-type"
        )
        self.assertNotIn(
            "game-setup-typ--active", game_types[0].get_attribute("class")
        )

        game_types[0].click()
        time.sleep(2)

        # After click, first game type is active but second is still not active
        self.assertIn(
            "game-setup-type--active", game_types[0].get_attribute("class")
        )
        self.assertNotIn(
            "game-setup-type--active", game_types[1].get_attribute("class")
        )

        game_types[1].click()
        time.sleep(2)

        # After click, first game type is not active but second is active now
        self.assertIn(
            "game-setup-type--active", game_types[1].get_attribute("class")
        )
        self.assertNotIn(
            "game-setup-type--active", game_types[0].get_attribute("class")
        )

    def test_game_setup_game_init_successful(self):
        """Tests successful game session creating on game setup page."""
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.live_server_url}{reverse('main:home')}#game")
        time.sleep(2)

        submit_button = self.browser.find_element_by_class_name(
            "index-forms__button--game-setup"
        )

        # Without choosing class group, language and game type, game session
        # won't be initialized
        submit_button.click()
        self.assertNotIn("play", self.browser.current_url)

        # Choosing class group
        class_group_select = Select(
            self.browser.find_element_by_tag_name("select")
        )
        class_group_select.select_by_index(1)
        time.sleep(2)

        # Language and game type are still not chosen
        submit_button.click()
        self.assertNotIn("play", self.browser.current_url)

        # Choosing language
        self.browser.find_element_by_class_name("game-setup-flag").click()
        time.sleep(2)

        # Game type is empty, so game session is not created
        submit_button.click()
        self.assertNotIn("play", self.browser.current_url)

        # Choosing game type
        self.browser.find_element_by_class_name("game-setup-type").click()
        time.sleep(2)

        # Game session has been created
        submit_button.click()
        self.assertIn("play", self.browser.current_url)

        r = redis.StrictRedis()
        self.assertTrue(r.hgetall("games"))

    def test_game_setup_game_init_invalid_class_group(self):
        """Tests invalid game session creating on game setup page."""
        self.login_user(self.teacher_data)
        self.browser.get(f"{self.live_server_url}{reverse('main:home')}#game")
        time.sleep(2)

        submit_button = self.browser.find_element_by_class_name(
            "index-forms__button--game-setup"
        )

        # Class group with that id doesn't exist
        class_group_select = Select(
            self.browser.find_element_by_tag_name("select")
        )
        self.browser.execute_script(
            f"return arguments[0].setAttribute('value', '123456')",
            class_group_select.options[1]
        )
        class_group_select.select_by_index(1)
        time.sleep(2)

        for el in ["flag", "type"]:
            self.browser.find_element_by_class_name(f"game-setup-{el}").click()

        time.sleep(2)

        submit_button.click()
        time.sleep(5)

        self.check_error_info_block("Class group with that id doesn't exist.")

    def login_user(self, user_data):
        self.browser.get(self.live_server_url + reverse("authorization:login"))

        for name in ["username", "password"]:
            field = self.browser.find_element_by_css_selector(f"[name={name}]")
            field.send_keys(user_data[name])

        self.browser.find_element_by_tag_name("button").click()
        time.sleep(2)
