import json
import redis
import random
from datetime import datetime
from channels.generic.websocket import AsyncWebsocketConsumer
from authorization.serializers import UserGameSessionSerializer
from .games import GameManager


class GameConsumer(AsyncWebsocketConsumer):
    """
    Websocket consumer class responsible for starting, handling and finishing
    games.
    """
    async def connect(self):
        self.game_id = self.scope["url_route"]["kwargs"]["game_id"]
        self.game_group_name = f"game_{self.game_id}"
        self.game_handler = None

        await self.accept()

        # Get dictionary with info about game session
        self.redis = redis.StrictRedis()
        game_session = self.get_game_session()

        # If game session exists, check if user is allowed to play a game or
        # is a teacher of class group related with game session
        if game_session:
            await self.join_user_to_game_channel(game_session)
        else:
            msg = "There is no game session related with this class group."
            await self.raise_game_error(msg)

    async def join_user_to_game_channel(self, game_session):
        """
        Joins user to game channel if user is teacher or user is student of
        class group related with game session.

        Arg:
            game_session: dictionary with info about current game session
        """
        # Students and teacher of class group can join to game channel only if
        # game session hasn't started yet
        if game_session["status"] == "start" and self.is_allowed(game_session):
            game_manager = GameManager(game_session["type_of_game"], self)
            self.game_handler = game_manager.get_game_handler()

            # Invoke method responsible for allowing student to play
            self.add_student_to_players(game_session)

            await self.channel_layer.group_add(
                self.game_group_name,
                self.channel_name
            )

            await self.channel_layer.group_send(
                self.game_group_name,
                {
                    "type": "game_join",
                    "message": game_session
                }
            )

        else:
            msg = "Game has already started or you aren't allowed to join it."
            await self.raise_game_error(msg)

    def is_allowed(self, game_session):
        """
        Checks if user is teacher or student of class group related with game
        session.

        Arg:
            game_session: dictionary with info about current game session
        Returns:
            Boolean that indicates if user is allowed to be added to game
            channel
        """
        return (
            self.scope["user"].id == game_session["teacher"] or
            self.scope["user"].id in game_session["allowed_users"]
        )

    def add_student_to_players(self, game_session):
        """Adds student to players list of game session.

        Arg:
            game_session: dictionary with info about current game session
        """
        if not self.scope["user"].id == game_session["teacher"]:
            player_data = UserGameSessionSerializer(self.scope["user"]).data

            game_session["joined"][str(self.scope["user"].id)] = player_data
            self.redis.hset("games", self.game_id, game_session)

    async def raise_game_error(self, error_message):
        """Sends message with error message.

        Arg:
            error_message: error text to raise
        """
        await self.send(text_data=json.dumps({
            "type": "game_error",
            "game_data": {"detail": error_message}
        }))

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.game_group_name,
            self.channel_name
        )

        game_session = self.get_game_session()

        # Remove current user from game session if game session exists
        if game_session and self.game_handler:
            await self.remove_user_from_game_channel(game_session)

    async def remove_user_from_game_channel(self, game_session):
        """
        Removes current user from game channel if current user has joined to
        game already.

        Arg:
            game_session: dictionary with info about current game session
        """
        if (game_session["status"] == "start" and
            str(self.scope["user"].id) in game_session["joined"]):
            # If game session hasn't started yet, remove from game session
            # stored info about user and make user leave game channel
            game_session["joined"].pop(str(self.scope["user"].id))
            self.redis.hset("games", self.game_id, game_session)

            await self.channel_layer.group_send(
                self.game_group_name,
                {
                    "type": "game_join",
                    "message": game_session
                }
            )

        elif (game_session["status"] == "in_progress" and
                str(self.scope["user"].id) not in game_session["finished"]):
            # Otherwise, make user leave game channel but don't remove any user
            # info from game session (game report purposes)
            game_session["disconnected"].append(str(self.scope["user"].id))
            self.redis.hset("games", self.game_id, game_session)

            # Invoke method responsible for finishing game session if there is
            # no active player any more
            await self.game_handler.check_if_finished(game_session)

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        game_session = self.get_game_session()

        if (game_session["status"] == "start" and
            len(game_session["joined"]) >= 2 and
            game_session["teacher"] == self.scope["user"].id):
            # Prepare game session data for playing game
            game_session["status"] = "in_progress"
            game_session["started"] = datetime.now().isoformat()
            game_session["scores"] = {j: [] for j in game_session["joined"]}
            game_session["finished"] = []
            game_session["disconnected"] = []

            self.game_handler.start_game(game_session)

            await self.channel_layer.group_send(
                self.game_group_name,
                {
                    "type": "game_started",
                    "message": game_session
                }
            )

        if (game_session["status"] == "in_progress" and
            game_session["teacher"] != self.scope["user"].id):
            # Invoke game handler method responsible for checking if answer
            # was valid
            await self.game_handler.verify_answer(text_data_json, game_session)

    async def game_join(self, event):
        message = event["message"]
        game_join_keys = [
            "class_group", "joined", "language", "status", "teacher",
            "type_of_game"
        ]
        game_data = {key: message[key] for key in game_join_keys}

        await self.send(text_data=json.dumps({
            "type": event["type"],
            "game_data": game_data
        }))

    async def game_started(self, event):
        message = event["message"]
        game_data = self.game_handler.get_game_start_data(message)

        await self.send(text_data=json.dumps({
            "type": "game_started",
            "game_data": game_data
        }))

    async def game_updated(self, event):
        message = event["message"]

        await self.send(text_data=json.dumps({
            "type": "game_update",
            "game_data": message
        }))

    async def game_answered(self, event):
        message = event["message"]

        await self.send(text_data=json.dumps({
            "type": "game_message",
            "game_data": message
        }))

    async def game_finished(self, event):
        message = event["message"]

        await self.send(text_data=json.dumps({
            "type": event["type"],
            "game_data": message
        }))

    def get_game_session(self):
        """Gets data about current game session.

        Returns:
            Dictionary with info about current game session
        """
        game_session = self.redis.hget("games", self.game_id)

        if game_session:
            return json.loads(game_session.decode("utf8").replace("'", '"'))

    def finish_game(self, game_session):
        """Prepares dictionary with summary info about finished game session.

        Arg:
            game_session: dictionary with summary info of ended game session
        Return
            Dictionary with info about ended game session
        """
        game_session_finish = {
            "status": "finish",
            "finished": datetime.now().isoformat(),
            "results": [],
            "aborted": []
        }

        for key in ["language", "class_group", "started", "type_of_game"]:
            game_session_finish[key] = game_session[key]

        self.game_handler.finish_game(game_session, game_session_finish)
        self.redis.hdel("games", self.game_id)

        return game_session_finish
