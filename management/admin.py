from django.contrib import admin
from django.contrib.auth.models import User
from authorization.models import School, ClassGroup
from .models import Invitation


@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ("name", "city", "logo", "principal")

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # Only principal users are allowed to be a principal of school
        if db_field.name == "principal":
            kwargs["queryset"] = User.objects.filter(
                groups__name="principals"
            )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def city(self, obj):
        return f"{obj.city.name}, {obj.city.country_short}"


@admin.register(ClassGroup)
class ClassGroupAdmin(admin.ModelAdmin):
    list_display = ("name", "school", "teacher")

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # Only teacher users are allowed to be a teacher of class group
        if db_field.name == "teacher":
            kwargs["queryset"] = User.objects.filter(
                groups__name="teachers"
            )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # Only student users are allowed to be students of class group
        if db_field.name == "students":
            kwargs["queryset"] = User.objects.filter(groups__name="students")
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def school(self, obj):
        city = obj.school.city
        return f"{obj.school.name} - {city.name}, {city.country_short}"


@admin.register(Invitation)
class InvitationAdmin(admin.ModelAdmin):
    list_display = ("class_group", "student")

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # Only student users are allowed to be invited to class group
        if db_field.name == "student":
            kwargs["queryset"] = User.objects.filter(groups__name="students")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def class_group(self, obj):
        return f"{obj.name}, {obj.school.name}"
