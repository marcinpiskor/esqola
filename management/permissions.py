from rest_framework import permissions
from authorization.permissions import HasAllowedParams

SCHOOL_ALLOWED_PARAMS = {
    "name", "city__name", "principal__username" "principal__first_name",
    "principal__last_name"
}

class HasSchoolAllowedQueryParams(HasAllowedParams):
    """Permission that checks if school allowed query params has been passed."""
    allowed_params = SCHOOL_ALLOWED_PARAMS
    message = "Only these query params are allowed in this request: " \
                                f"{', '.join(SCHOOL_ALLOWED_PARAMS)}."


class IsNotPrincipalOfOtherSchool(permissions.BasePermission):
    """
    Permission that checks if current user is not already principal of other
    school.
    """
    message = "Current user is already a principal."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return not hasattr(request.user, "school")


class IsPrincipal(permissions.BasePermission):
    """Permission that checks if current user is principal of school."""
    message = "Current user needs to be a principal of this school."

    def has_object_permission(self, request, view, obj):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return obj.principal == request.user


class CanAddSchool(permissions.BasePermission):
    """
    Permission that checks if current user is allowed to create School object.
    """
    message = "'Can add school' permission is required."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return request.user.has_perm("authorization.add_school")


class CanChangeSchool(permissions.BasePermission):
    """
    Permission that checks if current user is allowed to update School object
    data.
    """
    message = "'Can change school' permission is required."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return request.user.has_perm("authorization.change_school")


class CanDeleteSchool(permissions.BasePermission):
    """
    Permission that checks if current user is allowed to remove School object.
    """
    message = "'Can delete school' permission is required."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return request.user.has_perm("authorization.delete_school")


class IsRelatedWithClassGroup(permissions.BasePermission):
    """
    Permission that checks if current user is a principal of school related
    with class group, teacher of class group or student of class group.
    """
    message = "Current user has to be a teacher of class group, a student" \
                                " of class group or a principal of school" \
                                " related with class group."

    def has_object_permission(self, request, view, obj):
        return (
            obj.teacher == request.user
            or obj.school.principal == request.user
            or obj.students.filter(id=request.user.id).count() > 0
        )


class IsPrincipalOfSchoolRelatedWithClassGroup(permissions.BasePermission):
    """
    Permission that checks if current user is a principal of school related
    with class group.
    """
    message = "Current user has to be a principal of school related with" \
                                                            " class group."

    def has_object_permission(self, request, view, obj):
        return obj.school.principal == request.user


class CanAddClassGroup(permissions.BasePermission):
    """
    Permission that checks if current user is allowed to create Class group
    object.
    """
    message = "'Can add class group' permission is required."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return request.user.has_perm("authorization.add_classgroup")


class CanChangeClassGroup(permissions.BasePermission):
    """
    Permission that checks if current user is allowed to update Class group
    object data.
    """
    message = "'Can change class group' permission is required."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return request.user.has_perm("authorization.change_classgroup")


class CanDeleteClassGroup(permissions.BasePermission):
    """
    Permission that checks if current user is allowed to delete Class group
    object.
    """
    message = "'Can delete class group' permission is required."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return request.user.has_perm("authorization.delete_classgroup")


class IsRelatedWithInvitation(permissions.BasePermission):
    """
    Permission that checks if current user is an invited user or teacher of
    class group that is inviting.
    """
    message = "Current needs to be an invited student or teacher of " \
                                        "class group that is inviting."

    def has_object_permission(self, request, view, obj):
        return (
            obj.student == request.user
            or obj.class_group.teacher == request.user
        )


class CanAddStudentsToClassGroup(permissions.BasePermission):
    """
    Permission that checks if current user is allowed to add students to Class
    group object.
    """
    message = "'Can add students to class group' permission is required."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return request.user.has_perm(
                "authorization.add_students_to_classgroup"
            )


class CanDeleteStudentsFromClassGroup(permissions.BasePermission):
    """
    Permission that checks if current user is allowed to delete students from
    Class group object.
    """
    message = "'Can delete students from class group' permission is required."

    def has_permission(self, request, view):
        if request.method in ["GET", "OPTIONS", "HEAD"]:
            return True
        else:
            return request.user.has_perm(
                "authorization.delete_students_from_classgroup"
            )


class IsInvitedUser(permissions.BasePermission):
    """Permission that checks if current user is an invited user."""
    message = "Only invited user can accept/reject an invitation."

    def has_object_permission(self, request, view, obj):
        return obj.student == request.user
