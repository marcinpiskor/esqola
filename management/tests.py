import io
from PIL import Image
from urllib.parse import urlencode
from django.test import TestCase
from django.core.management import call_command
from django.contrib.auth.models import User, Group
from authorization.utils import basic_user_data, new_user_data, new_city_data
from authorization.models import City, Profile, School, ClassGroup
from .utils import principal_data, teacher_data, student_data
from .permissions import SCHOOL_ALLOWED_PARAMS
from .models import Invitation


class SchoolAPITest(TestCase):
    def setUp(self):
        call_command("creategroups")
        self.city = City.objects.create(**new_city_data)

        user_data = dict(new_user_data)
        user_data.pop("password_repeat")

        self.user = User.objects.create_user(**user_data)
        self.user.groups.add(Group.objects.get(name="principals"))
        self.user.save()

        self.admin = User.objects.create_user(**basic_user_data)

        self.school = School.objects.create(
            name="University of Cambridge", city=self.city, principal=self.user
        )

    def test_list_without_user(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/school/?name=University")
        self.assertEqual(res.status_code, 403)

    def test_retrieve_without_user(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        res = self.client.get(f"/api/school/{self.user.id}/")
        self.assertEqual(res.status_code, 403)

    def test_post_without_user(self):
        """
        Tests POST method without user logged in. Forbidden error expected.
        """
        data = {
            "name": "Università di Bologna",
            "city.place_id": "ChIJvT-116N6WkcR5H4X8lxkuB0"
        }

        res = self.client.post("/api/school/", data, format="multipart")
        self.assertEqual(res.status_code, 403)

    def test_put_without_user(self):
        """Tests PUT method without user logged in. Forbidden error expected."""
        data = {
            "name": "Hogward University",
            "city.place_id": "ChIJ8UNwBh-9oRQR3Y1mdkU1Nic"
        }

        res = self.client.put(f"/api/school/{self.user.id}/", data)
        self.assertEqual(res.status_code, 403)

    def test_patch_without_user(self):
        """
        Tests PATCH method without user logged in. Forbidden error expected.
        """
        data = {"name": "Universidad"}

        res = self.client.patch(f"/api/school/{self.user.id}/", data=data)
        self.assertEqual(res.status_code, 403)

    def test_delete_without_user(self):
        """
        Tests DELETE method without user logged in. Forbidden error expected.
        """
        res = self.client.delete(f"/api/school/{self.user.id}/")
        self.assertEqual(res.status_code, 403)

    def test_list_with_valid_normal_query_param(self):
        """Tests list action valid query param. Schools should be returned"""
        self.client.force_login(self.user)

        query_param = f"name={self.school.name}"
        res = self.client.get(f"/api/school/?{query_param}")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            [self.expected_correct_response("list")]
        )

    def test_list_with_valid_nested_query_param(self):
        """
        Tests list action valid nested field query param. Schools should be
        returned.
        """
        self.client.force_login(self.user)

        query_param = f"city__name={self.school.city.name}"
        res = self.client.get(f"/api/school/?{query_param}")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            [self.expected_correct_response("list")]
        )

    def test_list_without_query_params(self):
        """Tests list action without query params. Error should be returned."""
        self.client.force_login(self.user)

        res = self.client.get("/api/school/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "Query params are required in this request."}
        )

    def test_list_with_invalid_query_param(self):
        """
        Tests list action with invalid query param. Error should be returned.
        """
        self.client.force_login(self.user)

        err = "Only these query params are allowed in this request: " \
                                f"{', '.join(SCHOOL_ALLOWED_PARAMS)}."

        res = self.client.get("/api/school/?birthday=1990-21-21")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"), {"detail": err}
        )

    def test_retrieve_with_own_school_as_principal(self):
        """
        Tests retrieve action of school that user is principal. Detail school
        info should be returned.
        """
        self.client.force_login(self.user)

        # User is related with Esqola principal group and is principal of that
        # school
        res = self.client.get(f"/api/school/{self.school.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            self.expected_correct_response("detail")
        )

    def test_retrieve_with_own_school_without_role(self):
        """
        Tests retrieve action of school that user is principal. Detail school
        info should be returned.
        """
        self.clear_current_user_groups()
        self.client.force_login(self.user)

        # User is not related with Esqola principal group but user is principal
        # of that school
        res = self.client.get(f"/api/school/{self.school.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            self.expected_correct_response("detail")
        )

    def test_retrieve_without_own_school_as_principal(self):
        """
        Tests retrieve action of school that user isn't principal. Detail school
        info should be returned.
        """
        self.clear_current_user_school()
        self.client.force_login(self.user)

        # User is related with Esqola principal group but user is not principal
        # of that school
        res = self.client.get(f"/api/school/{self.school.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            self.expected_correct_response("detail")
        )

    def test_get_detail_without_own_school_without_role(self):
        """
        Tests retrieve action of school that user isn't principal. Detail school
        info should be returned.
        """
        self.clear_current_user()
        self.client.force_login(self.user)

        # User is not related with Esqola principal group but user is not
        # principal of that school
        res = self.client.get(f"/api/school/{self.school.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            self.expected_correct_response("detail")
        )

    def test_correct_post_with_city_that_already_exists(self):
        """
        Tests POST method of school with correct data. School object should be
        created without new City object.
        """
        # There is a city with that place id in database so new City object
        # shouldn't be created
        self.check_correct_post(self.city.place_id, self.assertEqual)

    def test_correct_post_with_new_city(self):
        """
        Tests POST method of school with correct data. School object shouldn't
        be created without new City object.
        """
        # There is no city with that place id in database so new City object
        # should be created
        self.check_correct_post(
            "ChIJW-T2Wt7Gt4kRKl2I1CJFUsI", self.assertGreater
        )

    def test_invalid_post_without_permissions(self):
        """
        Tests POST method of school without permissions. Error should be
        returned.
        """
        self.clear_current_user_groups()
        self.client.force_login(self.user)

        # Current user is no related with principal group anymore
        res = self.client.post("/api/school/", {}, format="multipart")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "'Can add school' permission is required."}
        )

    def test_invalid_post_as_principal_of_school_already(self):
        """
        Tests POST method of school as user who is a principal already. Error
        should be returned.
        """
        self.client.force_login(self.user)

        data = {
            "name": "University of Oxford",
            "city.place_id": "ChIJW-T2Wt7Gt4kRKl2I1CJFUsI"
        }

        # Current user is already a principal of other school
        res = self.client.post("/api/school/", data, format="multipart")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "Current user is already a principal."}
        )

    def test_invalid_post_with_invalid_city(self):
        """
        Tests POST method of school with invalid data. Error should be returned.
        """
        self.clear_current_user_school()
        self.client.force_login(self.user)

        # City with that place id doesn't exist
        data = {"name": self.school.name, "city.place_id": "abrakadabra"}

        res = self.client.post("/api/school/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"city": {"place_id": ["City doesn't exist."]}}
        )

    def test_invalid_post_with_the_same_school_data(self):
        """
        Tests POST method of school with invalid data. Error should be returned.
        """
        self.clear_current_user_school()
        self.client.force_login(self.user)

        # School with that name already exists in that city
        data = {
            "name": self.school.name,
            "city.place_id": self.school.city.place_id
        }
        exp_error =  {
            "school": {
                "name": ["School with that name already exists in that city."]
            }
        }

        res = self.client.post("/api/school/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_error)

    def test_correct_put(self):
        """
        Tests PUT method of school with correct data. School object should be
        updated.
        """
        self.client.force_login(self.user)

        data = {
            "name": self.school.name,
            "city.place_id": "ChIJW-T2Wt7Gt4kRKl2I1CJFUsI"
        }

        res = self.client.put(
            f"/api/school/{self.school.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            self.expected_correct_response("detail")
        )

    def test_invalid_put_without_permissions(self):
        """
        Tests PUT method of school without permissions. Error should be raised.
        """
        self.clear_current_user_groups()
        self.client.force_login(self.user)

        data = {
            "name": self.school.name,
            "city.place_id": "ChIJW-T2Wt7Gt4kRKl2I1CJFUsI"
        }

        # Current user is no related with principal group anymore
        res = self.client.put(
            f"/api/school/{self.school.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "'Can change school' permission is required."}
        )

    def test_invalid_put_as_non_owner(self):
        """
        Tests PUT method of school as no principal. Error should be raised.
        """
        self.clear_current_user_school()
        self.client.force_login(self.user)

        data = {
            "name": self.school.name,
            "city.place_id": "ChIJW-T2Wt7Gt4kRKl2I1CJFUsI"
        }

        # Current user is no principal of that school anymore
        res = self.client.put(
            f"/api/school/{self.school.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "Current user needs to be a principal of this school."}
        )

    def test_correct_patch(self):
        """
        Tests PATCH method of school with correct data. School object should be
        updated.
        """
        self.client.force_login(self.user)

        data = {"name": "Harvard University"}
        exp_res = self.expected_correct_response("detail")
        exp_res["name"] = data["name"]

        res = self.client.patch(
            f"/api/school/{self.school.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_patch_without_permissions(self):
        """
        Tests PATCH method of school without permissions. Error should be
        raised.
        """
        self.clear_current_user_groups()
        self.client.force_login(self.user)

        data = {"name": "Harvard University"}

        # Current user is no related with principal group anymore
        res = self.client.patch(
            f"/api/school/{self.school.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "'Can change school' permission is required."}
        )

    def test_invalid_patch_as_non_owner(self):
        """
        Tests PATCH method of school as no principal. Error should be raised.
        """
        self.clear_current_user_school()
        self.client.force_login(self.user)

        data = {"name": "Harvard University"}

        # Current user is no principal of that school anymore
        res = self.client.patch(
            f"/api/school/{self.school.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "Current user needs to be a principal of this school."}
        )

    def test_correct_delete(self):
        """
        Tests DELETE method of school with correct data. School object should be
        deleted.
        """
        before_school_amount = School.objects.count()
        self.client.force_login(self.user)

        res = self.client.delete(f"/api/school/{self.school.id}/")

        self.assertEqual(res.status_code, 204)
        self.assertGreater(before_school_amount, School.objects.count())

    def test_invalid_delete_without_permissions(self):
        """
        Tests DELETE method of school without permissions. Error should be
        raised.
        """
        self.clear_current_user_groups()
        self.client.force_login(self.user)

        # Current user is no related with principal group anymore
        res = self.client.delete(f"/api/school/{self.school.id}/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "'Can delete school' permission is required."}
        )

    def test_invalid_delete_as_non_owner(self):
        """
        Tests DELETE method of school without permissions. Error should be
        raised.
        """
        self.clear_current_user_school()
        self.client.force_login(self.user)

        # Current user is no principal of that school anymore
        res = self.client.delete(f"/api/school/{self.school.id}/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            {"detail": "Current user needs to be a principal of this school."}
        )

    def check_correct_post(self, place_id, assert_method):
        """Checks correct POST method of school view set.

        Args:
            place_id: place id of city desired to be related with school
            assert_method: assert equal or not equal method
        """
        self.clear_current_user_school()
        self.client.force_login(self.user)

        before_city_amount = City.objects.count()
        before_school_amount = School.objects.count()

        data = {
            "name": "University of Oxford",
            "city.place_id": place_id
        }

        res = self.client.post("/api/school/", data, format="multipart")

        assert_method(City.objects.count(), before_city_amount)
        self.assertGreater(School.objects.count(), before_school_amount)

        self.assertEqual(res.status_code, 201)
        self.assertJSONEqual(
            str(res.content, encoding="utf-8"),
            self.expected_correct_response("detail", School.objects.last())
        )

    def clear_current_user(self):
        """Invokes methods responsible for clearing current user data."""
        self.clear_current_user_groups()
        self.clear_current_user_school()

    def clear_current_user_groups(self):
        """Clears current user groups attribute."""
        self.user.groups.set([])
        self.user.save()

    def clear_current_user_school(self):
        """Clears current user school attribute."""
        self.school.principal = self.admin
        self.school.save()

    def expected_correct_response(self, action, obj=None):
        """
        Prepares dictionary with expected successful school view set response.

        Args:
            action: boolean that indicates what type of request occured (list
            or detailed)
            obj: optional School object
        Returns
            Expected correct school view set response
        """
        if not obj:
            self.school.refresh_from_db()
            obj = self.school

        return {
            "id": obj.id,
            "name": obj.name,
            "classes": [],
            "logo": None,
            "city": self.get_serialized_city(obj.city),
            "principal": self.get_serialized_user(action, obj)
        }

    def get_serialized_user(self, action, obj):
        """
        Prepares dictionary with expected serialized school principal data.

        Args:
            action: boolean that indicates what type of data are expected
            obj: School principal User object
        Returns:
            Expected serialized school principal data
        """
        if action == "list":
            return obj.principal.id
        else:
            fields = ["id", "email", "username", "first_name", "last_name"]
            data = {f: getattr(obj.principal, f) for f in fields}
            data["profile"] = None
            data["school"] = obj.id

            return data

    def get_serialized_city(self, obj):
        """
        Prepares dictionary with expected serialized school city data.

        Args:
            obj: City object related with school
        Returns:
            Expected serialized school city data
        """
        fields = [
            "place_id", "name", "region", "country", "country_short",
            "latitude", "longitude"
        ]

        return {f: getattr(obj, f) for f in fields}


class ClassGroupAPITest(TestCase):
    def setUp(self):
        call_command("creategroups")
        self.city = City.objects.create(**new_city_data)

        self.principal_data_1 = dict(new_user_data)
        self.principal_data_1.pop("password_repeat")
        self.principal_1 = User.objects.create_user(**self.principal_data_1)

        self.principal_data_2 = dict(principal_data)
        self.principal_2 = User.objects.create_user(**self.principal_data_2)

        for user in [self.principal_1, self.principal_2]:
            user.groups.add(Group.objects.get(name="principals"))

        self.teacher_data = dict(teacher_data)

        self.teacher = User.objects.create_user(**self.teacher_data)
        self.teacher.groups.add(Group.objects.get(name="teachers"))

        self.student_data = dict(student_data)

        self.student = User.objects.create_user(**self.student_data)
        self.student.groups.add(Group.objects.get(name="students"))

        self.school_1 = School.objects.create(
            name="University of Cambridge", city=self.city,
            principal=self.principal_1
        )

        self.school_2 = School.objects.create(
            name="University of Oxford", city=self.city,
            principal=self.principal_2
        )

        for school in [self.school_1, self.school_2]:
            class_group = ClassGroup.objects.create(
                school=school, name="test", teacher=self.teacher
            )
            class_group.students.add(self.student)

    def test_list_without_user(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/class-group/")
        self.assertEqual(res.status_code, 403)

    def test_retrieve_without_user(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        class_group_id = self.school_1.classes.first().id
        res = self.client.get(f"/api/class-group/{class_group_id}/")
        self.assertEqual(res.status_code, 403)

    def test_post_without_user(self):
        """
        Tests POST method without user logged in. Forbidden error expected.
        """
        data = {"name": "8th grade"}
        res = self.client.post(f"/api/class-group/", data)
        self.assertEqual(res.status_code, 403)

    def test_put_without_user(self):
        """Tests PUT method without user logged in. Forbidden error expected."""
        data = {
            "name": "Argentinian students",
            "city.place_id": "ChIJz-fn0PHGPUcRcrIsc6p0wI0"
        }
        class_group_id = self.school_1.classes.first().id
        res = self.client.put(f"/api/class-group/{class_group_id}/", data)
        self.assertEqual(res.status_code, 403)

    def test_patch_without_user(self):
        """
        Tests PATCH method without user logged in. Forbidden error expected.
        """
        data = {"name": "Teachers"}
        class_group_id = self.school_1.classes.first().id
        res = self.client.patch(f"/api/class-group/{class_group_id}/", data)
        self.assertEqual(res.status_code, 403)

    def test_delete_without_user(self):
        """
        Tests DELETE method without user logged in. Forbidden error expected.
        """
        class_group_id = self.school_1.classes.first().id
        res = self.client.delete(f"/api/class-group/{class_group_id}/")
        self.assertEqual(res.status_code, 403)

    def test_delete_students_without_user(self):
        """
        Tests delete students action without user logged in. Forbidden error
        expected.
        """
        data = {"students_to_remove": self.student.id}
        class_group_id = self.school_1.classes.first().id
        res = self.client.delete(f"/api/class-group/{class_group_id}/", data)
        self.assertEqual(res.status_code, 403)

    def test_list_as_principal_of_school_related_with_class_group(self):
        """Tests list action as principal. Class groups should be returned."""
        self.client.force_login(self.principal_1)

        # Class groups related with school that current user is principal are
        # expected
        res = self.client.get("/api/class-group/")

        exp_res = self.get_class_group_exp_response(
            self.school_1.classes.first(), "list"
        )

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), [exp_res])

    def test_list_as_teacher_of_class_group(self):
        """Tests list action as principal. Class groups should be returned."""
        self.client.force_login(self.teacher)

        # Class groups which current user is teacher of are expected
        res = self.client.get("/api/class-group/")
        exp_res = []

        for school in [self.school_1, self.school_2]:
            exp_res.append(
                self.get_class_group_exp_response(
                    school.classes.first(), "list"
                )
            )

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_list_as_student_of_class_group(self):
        """Tests list action as principal. Class groups should be returned."""
        self.client.force_login(self.student)

        # Class groups which current user is student of are expected
        res = self.client.get("/api/class-group/")
        exp_res = []

        for school in [self.school_1, self.school_2]:
            exp_res.append(
                self.get_class_group_exp_response(
                    school.classes.first(), "list"
                )
            )

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_list_with_query_param(self):
        """Tests list action as principal. Class groups should be returned."""
        self.client.force_login(self.student)

        class_group_obj = self.school_1.classes.first()
        query_param = f"?id__in={class_group_obj.id}"

        # Class groups related with current user with id passed in query param
        # are expected
        res = self.client.get(f"/api/class-group/{query_param}")
        exp_res = self.get_class_group_exp_response(class_group_obj, "list")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), [exp_res])

    def test_retrieve_as_principal_of_school_related_with_class_group(self):
        """
        Tests retrieve action as principal. Class group should be returned.
        """
        self.client.force_login(self.principal_1)

        class_group_obj = self.principal_1.school.classes.first()
        exp_res = self.get_class_group_exp_response(class_group_obj, "detail")

        # Class group related with school that current user is principal is
        # expected
        res = self.client.get(f"/api/class-group/{class_group_obj.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_retrieve_as_teacher_of_class_group(self):
        """Tests retrieve action as teacher. Class group should be returned."""
        self.client.force_login(self.teacher)

        class_group_obj = self.teacher.teacher_classes.first()
        exp_res = self.get_class_group_exp_response(class_group_obj, "detail")

        # Class group which current user is teacher of is expected
        res = self.client.get(f"/api/class-group/{class_group_obj.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_retrieve_as_student_of_class_group(self):
        """Tests retrieve action as teacher. Class group should be returned."""
        self.client.force_login(self.student)

        class_group_obj = self.student.student_classes.first()
        exp_res = self.get_class_group_exp_response(class_group_obj, "detail")

        # Class group which current user is student of is expected
        res = self.client.get(f"/api/class-group/{class_group_obj.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_retrieve_as_user_unrelated_with_class_group(self):
        """
        Tests retrieve action as unrelated user. Forbidden error is expected.
        """
        self.client.force_login(self.principal_2)

        class_group_id = self.principal_1.school.classes.first().id
        msg = "Current user has to be a teacher of class group, a student" \
                                " of class group or a principal of school" \
                                " related with class group."
        exp_res = {"detail": msg}

        # Current user is not related with this class group
        res = self.client.get(f"/api/class-group/{class_group_id}/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_post_with_correct_data(self):
        """
        Tests POST method with correct data. New class group object is expected.
        """
        self.client.force_login(self.principal_1)
        before_class_group_amount = ClassGroup.objects.count()

        data = {"name": "Foreign students", "teacher.id": self.teacher.id}

        # New class group object has been created
        res = self.client.post(f"/api/class-group/", data, format="multipart")
        exp_res = self.get_class_group_exp_response(
            ClassGroup.objects.last(), "detail"
        )
        exp_res["name"] = data["name"]
        exp_res["students"] = []

        self.assertEqual(res.status_code, 201)
        self.assertGreater(
            ClassGroup.objects.count(), before_class_group_amount
        )

        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_with_name_that_already_exists(self):
        """Tests POST method with invalid data. Error should be returned."""
        self.client.force_login(self.principal_1)

        class_group_obj = self.principal_1.school.classes.first()
        data = {"name": class_group_obj.name, "teacher.id": self.teacher.id}

        msg = "Class group with that name already exists in that school."
        exp_res = {"name": [msg]}

        # Class group name is already used in that school
        res = self.client.post(f"/api/class-group/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_with_user_that_is_not_a_teacher(self):
        """Tests POST method with invalid data. Error should be returned."""
        self.client.force_login(self.principal_1)

        data = {"name": "Foreign students", "teacher.id": self.student.id}
        exp_res = {
            "teacher": {
                "id": ["Teacher user with that id doesn't exist."]
            }
        }

        # Passed teacher id is id of user who is not a teacher
        res = self.client.post(f"/api/class-group/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_without_permissions_as_teacher(self):
        """Tests POST method as unallowed user. Error should be returned."""
        self.client.force_login(self.teacher)

        data = {"name": "Foreign students", "teacher.id": self.teacher.id}
        exp_res = {"detail": "'Can add class group' permission is required."}

        # Only principal user is allowed to create Class group object
        res = self.client.post(f"/api/class-group/", data, format="multipart")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_without_permissions_as_student(self):
        """Tests POST method as unallowed user. Error should be returned."""
        self.client.force_login(self.student)

        data = {"name": "Foreign students", "teacher.id": self.teacher.id}
        exp_res = {"detail": "'Can add class group' permission is required."}

        # Only principal user is allowed to create Class group object
        res = self.client.post(f"/api/class-group/", data, format="multipart")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_correct_put(self):
        """
        Tests PUT method with correct data. Class group object should be
        updated.
        """
        self.client.force_login(self.principal_1)
        class_group_obj = self.principal_1.school.classes.first()

        data = {"name": "Students from abroad", "teacher.id": self.teacher.id}
        exp_res = self.get_class_group_exp_response(class_group_obj, "detail")
        exp_res["name"] = data["name"]

        # Class group object has been updated
        res = self.client.put(
            f"/api/class-group/{class_group_obj.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_put_without_permissions_as_teacher(self):
        """Tests PUT method as unallowed user. Error should be returned."""
        self.client.force_login(self.teacher)
        class_group_obj = self.principal_1.school.classes.first()

        data = {
            "name": "Students from abroad", "teacher.id": self.teacher.id
        }
        exp_res = {
            "detail": "'Can change class group' permission is required."
        }

        # Only principal user is allowed to update Class group object
        res = self.client.put(
            f"/api/class-group/{class_group_obj.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_put_without_permissions_as_student(self):
        """Tests PUT method as unallowed user. Error should be returned."""
        self.client.force_login(self.student)
        class_group_obj = self.principal_1.school.classes.first()

        data = {
            "name": "Students from abroad", "teacher.id": self.teacher.id
        }
        exp_res = {
            "detail": "'Can change class group' permission is required."
        }

        # Only principal user is allowed to update Class group object
        res = self.client.put(
            f"/api/class-group/{class_group_obj.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_put_as_principal_of_other_school(self):
        """Tests PUT method as unallowed user. Error should be returned."""
        self.client.force_login(self.principal_2)

        class_group_obj = self.principal_1.school.classes.first()
        data = {
            "name": "Students from abroad", "teacher.id": self.teacher.id
        }

        msg = "Current user has to be a principal of school related with" \
                                                            " class group."
        exp_res = {"detail": msg}

        # Current user is not principal of that school
        res = self.client.put(
            f"/api/class-group/{class_group_obj.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_correct_patch(self):
        """
        Tests PATCH method with correct data. Class group object should be
        updated.
        """
        self.client.force_login(self.principal_1)

        class_group_obj = self.principal_1.school.classes.first()
        data = {"teacher.id": 0}

        exp_res = self.get_class_group_exp_response(class_group_obj, "detail")
        exp_res["teacher"] = None

        # Class group object has been updated
        res = self.client.patch(
            f"/api/class-group/{class_group_obj.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_patch_without_permissions_as_teacher(self):
        """Tests PATCH method as unallowed user. Error should be returned."""
        self.client.force_login(self.teacher)
        class_group_obj = self.principal_1.school.classes.first()

        data = {"name": "Students from abroad"}
        exp_res = {
            "detail": "'Can change class group' permission is required."
        }

        # Only principal user is allowed to update Class group object
        res = self.client.patch(
            f"/api/class-group/{class_group_obj.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_patch_without_permissions_as_student(self):
        """Tests PATCH method as unallowed user. Error should be returned."""
        self.client.force_login(self.student)
        class_group_obj = self.principal_1.school.classes.first()

        data = {"name": "Students from abroad"}
        exp_res = {
            "detail": "'Can change class group' permission is required."
        }

        # Only principal user is allowed to update Class group object
        res = self.client.patch(
            f"/api/class-group/{class_group_obj.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_patch_as_principal_of_other_school(self):
        """Tests PATCH method as unallowed user. Error should be returned."""
        self.client.force_login(self.principal_2)
        class_group_obj = self.principal_1.school.classes.first()

        data = {"name": "Students from abroad"}
        msg = "Current user has to be a principal of school related with" \
                                                            " class group."
        exp_res = {"detail": msg}

        # Current user is not principal of that school
        res = self.client.patch(
            f"/api/class-group/{class_group_obj.id}/", urlencode(data),
            content_type="application/x-www-form-urlencoded"
        )

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_correct_delete(self):
        """
        Tests DELETE method of school with correct data. Class group object
        should be deleted.
        """
        self.client.force_login(self.principal_1)
        class_group_obj = self.principal_1.school.classes.first()

        before_class_group_amount = ClassGroup.objects.count()

        res = self.client.delete(f"/api/class-group/{class_group_obj.id}/")
        self.assertEqual(res.status_code, 204)
        self.assertGreater(
            before_class_group_amount, ClassGroup.objects.count()
        )

    def test_invalid_delete_without_permissions_as_teacher(self):
        """Tests DELETE method as unallowed user. Error should be returned."""
        self.client.force_login(self.teacher)
        class_group_obj = self.principal_1.school.classes.first()

        before_class_group_amount = ClassGroup.objects.count()
        exp_res = {
            "detail": "'Can delete class group' permission is required."
        }

        # Only principal user is allowed to delete Class group object
        res = self.client.delete(f"/api/class-group/{class_group_obj.id}/")

        self.assertEqual(res.status_code, 403)
        self.assertEqual(
            before_class_group_amount, ClassGroup.objects.count()
        )
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_delete_without_permissions_as_student(self):
        """Tests DELETE method as unallowed user. Error should be returned."""
        self.client.force_login(self.student)
        class_group_obj = self.principal_1.school.classes.first()

        before_class_group_amount = ClassGroup.objects.count()
        exp_res = {
            "detail": "'Can delete class group' permission is required."
        }

        # Only principal user is allowed to delete Class group object
        res = self.client.delete(f"/api/class-group/{class_group_obj.id}/")

        self.assertEqual(res.status_code, 403)
        self.assertEqual(
            before_class_group_amount, ClassGroup.objects.count()
        )
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_delete_as_principal_of_other_school(self):
        """
        Tests DELETE method as principal of other school. Error should be
        returned.
        """
        self.client.force_login(self.principal_2)
        class_group_obj = self.principal_1.school.classes.first()

        before_class_group_amount = ClassGroup.objects.count()
        msg = "Current user has to be a principal of school related with" \
                                                            " class group."
        exp_res = {"detail": msg}

        # Current user is not principal of that school
        res = self.client.delete(f"/api/class-group/{class_group_obj.id}/")

        self.assertEqual(res.status_code, 403)
        self.assertEqual(
            before_class_group_amount, ClassGroup.objects.count()
        )
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_correct_delete_students(self):
        """
        Tests delete students action with correct data. Students should be
        removed.
        """
        self.client.force_login(self.teacher)
        class_group_obj = self.school_1.classes.first()

        before_students_amount = class_group_obj.students.count()
        students = ",".join(
            [str(s.id) for s in class_group_obj.students.all()]
        )
        data = {"students": students}
        url = f"/api/class-group/{class_group_obj.id}/delete-students/"

        # Students have been removed from class group
        res = self.client.post(url, data)

        self.assertEqual(res.status_code, 200)
        self.assertGreater(
            before_students_amount, class_group_obj.students.count()
        )

    def test_delete_students_with_students_who_is_not_in_class_group(self):
        """
        Tests delete students action with invalid data. Error shouldn't be
        returned.
        """
        self.client.force_login(self.teacher)
        class_group_obj = self.principal_1.school.classes.first()

        before_students_amount = class_group_obj.students.count()
        data = {"students": "12345678"}
        url = f"/api/class-group/{class_group_obj.id}/delete-students/"

        # Passed students aren't students of class group but error shouldn't be
        # returned
        res = self.client.post(url, data)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(
            before_students_amount, class_group_obj.students.count()
        )

    def test_invalid_delete_students_with_empty_data(self):
        """
        Tests delete students action with invalid data. Error should be
        returned.
        """
        self.client.force_login(self.teacher)

        class_group_obj = self.principal_1.school.classes.first()
        url = f"/api/class-group/{class_group_obj.id}/delete-students/"
        exp_res = {
            "students": "At least id of one user to delete is required."
        }

        # Students field is required
        res = self.client.post(url, {})

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_delete_students_with_empty_students(self):
        """
        Tests delete students action with invalid data. Error shouldn't be
        returned.
        """
        self.client.force_login(self.teacher)

        class_group_obj = self.principal_1.school.classes.first()
        url = f"/api/class-group/{class_group_obj.id}/delete-students/"
        exp_res = {
            "students": "At least id of one user to delete is required."
        }

        # Students field can't be empty
        res = self.client.post(url, {"students": ""})

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_delete_students_with_invalid_students(self):
        """
        Tests delete students action with invalid data. Error shouldn't be
        returned.
        """
        self.client.force_login(self.teacher)

        class_group_obj = self.principal_1.school.classes.first()
        before_students_amount = class_group_obj.students.count()

        url = f"/api/class-group/{class_group_obj.id}/delete-students/"
        exp_res = {
            "students": "At least id of one user to delete is required."
        }

        # Students field doesn't contain any id number
        res = self.client.post(url, {"students": "Mamma mia"})

        self.assertEqual(res.status_code, 200)
        self.assertEqual(
            before_students_amount, class_group_obj.students.count()
        )

    def test_invalid_delete_students_as_not_teacher_but_related(self):
        """
        Tests delete students action as not teacher. Error should be returned.
        """
        self.client.force_login(self.principal_1)
        class_group_obj = self.principal_1.school.classes.first()

        url = f"/api/class-group/{class_group_obj.id}/delete-students/"
        data = {"students": "1, 2, 3"}
        msg = "'Can delete students from class group' permission is required."
        exp_res = {'detail': msg}

        # Only teacher user is allowed to delete students from class group
        res = self.client.post(url, data)

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_delete_students_as_non_related_with_class_group(self):
        """
        Tests delete students action as not teacher. Error should be returned.
        """
        self.client.force_login(self.teacher)

        class_group_obj = self.school_2.classes.first()
        class_group_obj.teacher = None
        class_group_obj.save()

        url = f"/api/class-group/{class_group_obj.id}/delete-students/"
        data = {"students": "1, 2, 3"}
        msg = "Current user has to be a teacher of class group, a student" \
                                " of class group or a principal of school" \
                                " related with class group."
        exp_res = {"detail": msg}

        # Current user is not a teacher of this class group
        res = self.client.post(url, data)

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def get_class_group_exp_response(self, class_group_obj, type):
        """Prepares expected class group view set response.

        Args:
            class_group_obj: expected serialize Class group object
            type: boolean that indicates what type of serialization is
            expected (basic or detail)
        Returns:
            Dictionary with expected class group view set data
        """
        for data in [self.teacher_data, self.student_data]:
            if "password" in data:
                data.pop("password")

        exp_res = {
            "id": class_group_obj.id,
            "name": "test",
            "school": class_group_obj.school.id,
            "teacher": {
                **self.teacher_data,
                "id": self.teacher.id,
                "profile": None,
                "school": None,
            }
        }

        if type == "detail":
            exp_res["invited_users"] = []
            exp_res["students"] = [
                {
                    **self.student_data,
                    "id": self.student.id,
                    "profile": None,
                    "school": None
                }
            ]

        return exp_res


class InvitationAPITest(TestCase):
    def setUp(self):
        call_command("creategroups")
        self.city = City.objects.create(**new_city_data)

        self.principal_data = dict(new_user_data)
        self.principal_data.pop("password_repeat")

        self.principal = User.objects.create_user(**self.principal_data)
        self.principal.groups.add(Group.objects.get(name="principals"))

        self.teacher_data = dict(teacher_data)

        self.teacher = User.objects.create_user(**self.teacher_data)
        self.teacher.groups.add(Group.objects.get(name="teachers"))

        self.student_data = dict(student_data)

        self.student = User.objects.create_user(**self.student_data)
        self.student.groups.add(Group.objects.get(name="students"))

        self.school = School.objects.create(
            name="Stanford University", city=self.city,
            principal=self.principal
        )

        self.class_group = ClassGroup.objects.create(
            name="test", teacher=self.teacher, school=self.school
        )

    def test_list_without_user(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/invitation/")
        self.assertEqual(res.status_code, 403)

    def test_retrieve_without_user(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        invitation_obj_id = self.create_test_invitation().id

        res = self.client.get(f"/api/invitation/{invitation_obj_id}/")
        self.assertEqual(res.status_code, 403)

    def test_post_without_user(self):
        """
        Tests POST method action without user logged in. Forbidden error
        expected.
        """
        data = {
            "class_group.id": self.class_group.id,
            "student.id": self.student.id,
        }

        res = self.client.post(f"/api/invitation/", data)
        self.assertEqual(res.status_code, 403)

    def test_delete_without_user(self):
        """
        Tests DELETE method without user logged in. Forbidden error expected.
        """
        invitation_obj_id = self.create_test_invitation().id

        res = self.client.delete(f"/api/invitation/{invitation_obj_id}/")
        self.assertEqual(res.status_code, 403)

    def test_accept_without_user(self):
        """
        Tests accept action without user logged in. Forbidden error expected.
        """
        invitation_obj_id = self.create_test_invitation().id

        res = self.client.get(f"/api/invitation/{invitation_obj_id}/accept/")
        self.assertEqual(res.status_code, 403)

    def test_reject_without_user(self):
        """
        Tests reject action without user logged in. Forbidden error expected.
        """
        invitation_obj_id = self.create_test_invitation().id

        res = self.client.get(f"/api/invitation/{invitation_obj_id}/reject/")
        self.assertEqual(res.status_code, 403)

    def test_list_as_teacher_of_class_group_that_invited_user(self):
        """Tests list action as teacher. No invitations should be returned."""
        self.client.force_login(self.teacher)
        self.create_test_invitation()

        res = self.client.get("/api/invitation/")
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), [])

    def test_list_as_user_that_has_been_invited(self):
        """Tests list action as invited user. Invitations should be returned."""
        self.client.force_login(self.student)
        self.create_test_invitation()

        exp_res = self.get_invitation_expected_response()

        # Current user has been invited by class groups so invitation object
        # should be returned
        res = self.client.get("/api/invitation/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), [exp_res])

    def test_list_as_user_non_related_with_invitation(self):
        """
        Tests list action as non related user. No invitations should be
        returned.
        """
        self.client.force_login(self.teacher)
        self.create_test_invitation()

        # Current user is not related with created invitation
        res = self.client.get("/api/invitation/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), [])

    def test_retrieve_as_teacher_of_class_group_that_invited_user(self):
        """Tests retrieve action as teacher. Invitation should be returned."""
        self.client.force_login(self.teacher)
        invitation = self.create_test_invitation()

        exp_res = self.get_invitation_expected_response()

        # Current user is teacher of class group that has invited student, so
        # invitation object should be returned
        res = self.client.get(f"/api/invitation/{invitation.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_retrieve_as_user_that_has_been_invited(self):
        """Tests retrieve action as user. Invitation should be returned."""
        self.client.force_login(self.student)
        invitation = self.create_test_invitation()

        exp_res = self.get_invitation_expected_response()

        # Current user is student invited by class group, so invitation object
        # should be returned
        res = self.client.get(f"/api/invitation/{invitation.id}/")
        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_retrieve_as_user_non_related_with_invitation(self):
        """
        Tests retrieve action as non related user. No invitations should be
        returned.
        """
        self.client.force_login(self.principal)
        invitation = self.create_test_invitation()

        msg = "Current needs to be an invited student or teacher of " \
                                        "class group that is inviting."
        exp_res = {"detail": msg}

        # Current user is not related with created invitation
        res = self.client.get(f"/api/invitation/{invitation.id}/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_correct_post(self):
        """
        Tests POST method with correct data. New invitation object should be
        created.
        """
        self.client.force_login(self.teacher)

        before_invitation_amount = Invitation.objects.count()
        data = {
            "class_group.id": self.class_group.id,
            "student.id": self.student.id,
        }
        exp_res = self.get_invitation_expected_response()

        # New invitation object has been created
        res = self.client.post("/api/invitation/", data, format="multipart")

        self.assertEqual(res.status_code, 201)
        self.assertGreater(
            Invitation.objects.count(), before_invitation_amount
        )
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_with_the_same_invitation_data(self):
        """Tests POST method with invalid data. Error should be returned."""
        self.client.force_login(self.teacher)
        self.create_test_invitation()

        data = {
            "class_group.id": self.class_group.id,
            "student.id": self.student.id,
        }
        exp_res = {
            "student": [
                "This user has been already invited to this class group."
            ]
        }

        # User has been already invited by this class group
        res = self.client.post(f"/api/invitation/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_with_student_id_that_doesnt_exist(self):
        """Test POST method with invalid data. Error should be returned."""
        self.client.force_login(self.teacher)

        data = {
            "class_group.id": self.class_group.id,
            "student.id": 123456,
        }
        exp_res = {
            "student": {"id": ["Student user with that id doesn't exist."]}
        }

        # Student user with that id doesn't exist
        res = self.client.post(f"/api/invitation/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_with_student_user_that_is_not_a_student(self):
        """Tests POST method with invalid data. Error should be returned."""
        self.client.force_login(self.teacher)

        data = {
            "class_group.id": self.class_group.id,
            "student.id": self.principal.id,
        }
        exp_res = {
            "student": {"id": ["Student user with that id doesn't exist."]}
        }

        # User is not a student
        res = self.client.post(f"/api/invitation/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_with_class_group_that_doesnt_exist(self):
        """Tests POST method with invalid data. Error should be returned."""
        self.client.force_login(self.teacher)

        data = {
            "class_group.id": 1234567,
            "student.id": self.student.id,
        }
        msg = "Class group with current user as teacher and with that id " \
                                                            "doesn't exist."
        exp_res = {"class_group": {"id": [msg]}}

        # Class group doesn't exist
        res = self.client.post(f"/api/invitation/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_with_class_group_that_is_not_related(self):
        """Tests POST method with invalid data. Error should be returned."""
        self.client.force_login(self.teacher)

        new_teacher = User.objects.create(username="el_professor_sergio")
        new_class_group = ClassGroup.objects.create(
            name="Spanish", teacher=new_teacher, school=self.school
        )

        data = {
            "class_group.id": new_class_group.id,
            "student.id": self.student.id,
        }
        msg = "Class group with current user as teacher and with that id " \
                                                            "doesn't exist."
        exp_res = {"class_group": {"id": [msg]}}

        # Current user is not a teacher of class group
        res = self.client.post(f"/api/invitation/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_post_with_user_that_already_is_a_student(self):
        """Tests POST method with invalid data. Error should be returned."""
        self.client.force_login(self.teacher)
        self.class_group.students.add(self.student)

        data = {
            "class_group.id": self.class_group.id,
            "student.id": self.student.id,
        }
        exp_res = {
            "student": ["This user already is a student of this class group."]
        }

        # User is already a student of this class group
        res = self.client.post(f"/api/invitation/", data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)


    def test_invalid_post_without_permissions(self):
        """Tests POST method with invalid data. Error should be returned."""
        self.client.force_login(self.principal)
        self.class_group.students.add(self.student)

        data = {
            "class_group.id": self.class_group.id,
            "student.id": self.student.id,
        }
        msg = "'Can add students to class group' permission is required."
        exp_res = {"detail": msg}

        # Only teacher user can send invitation to student
        res = self.client.post(f"/api/invitation/", data, format="multipart")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_correct_delete_as_teacher_of_class_group(self):
        """
        Tests DELETE method with correct data. Invitation should be deleted.
        """
        self.client.force_login(self.teacher)
        invitation = self.create_test_invitation()

        before_invitation_amount = Invitation.objects.count()

        # Current user is teacher of class group that invited student, so
        # invitation object has been deleted
        res = self.client.delete(f"/api/invitation/{invitation.id}/")

        self.assertEqual(res.status_code, 204)
        self.assertGreater(
            before_invitation_amount, Invitation.objects.count()
        )

    def test_correct_delete_as_invited_user(self):
        """
        Tests DELETE method with correct data. Invitation should be deleted.
        """
        self.client.force_login(self.student)
        invitation = self.create_test_invitation()

        before_invitation_amount = Invitation.objects.count()

        # Current user is invited student, so invitation object has been
        # deleted
        res = self.client.delete(f"/api/invitation/{invitation.id}/")

        self.assertEqual(res.status_code, 204)
        self.assertGreater(
            before_invitation_amount, Invitation.objects.count()
        )

    def test_invalid_delete_as_unrelated_user(self):
        """
        Tests DELETE method with invalid data. Invitation should not be deleted.
        """
        self.client.force_login(self.principal)
        invitation = self.create_test_invitation()

        msg = "Current needs to be an invited student or teacher of " \
                                        "class group that is inviting."
        exp_res = {"detail": msg}

        # Current user is not related with invitation, so invitation has not
        # been deleted
        res = self.client.delete(f"/api/invitation/{invitation.id}/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_correct_accept_invitation(self):
        """
        Tests accept action with correct data. Invitation should be deleted.
        """
        self.client.force_login(self.student)
        invitation = self.create_test_invitation()

        before_invitation_amount = Invitation.objects.count()
        before_students_amount = self.class_group.students.count()

        # Current user is invited student, so invitation has been deleted and
        # current user has been added to class group students
        res = self.client.get(f"/api/invitation/{invitation.id}/accept/")

        self.assertEqual(res.status_code, 200)
        self.assertGreater(
            self.class_group.students.count(), before_students_amount
        )
        self.assertGreater(
            before_invitation_amount, Invitation.objects.count()
        )

    def test_invalid_accept_invitation_as_teacher_of_inviting_class_group(self):
        """
        Tests accept action with invalid data. Invitation should not be deleted.
        """
        self.client.force_login(self.teacher)
        invitation = self.create_test_invitation()

        exp_res = {
            "detail": "Only invited user can accept/reject an invitation."
        }

        # Current user is teacher of class group that has invited student, but
        # invitation hasn't been deleted and current user hasn't been added to
        # class group students
        res = self.client.get(f"/api/invitation/{invitation.id}/accept/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_accept_invitation_as_unrelated_with_invitation(self):
        """
        Tests accept action with invalid data. Invitation should not be deleted.
        """
        self.client.force_login(self.principal)
        invitation = self.create_test_invitation()

        exp_res = {
            "detail": "Only invited user can accept/reject an invitation."
        }

        # Current user is not related with invitation, so invitation hasn't
        # been deleted and current user hasn't been added to class group
        # students
        res = self.client.get(f"/api/invitation/{invitation.id}/accept/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_accept_invitation_doesnt_exist(self):
        """
        Tests accept action with invalid data. Invitation should not be deleted.
        """
        self.client.force_login(self.student)
        exp_res = {"detail": "Not found."}

        # Invitation with that id doesn't exist
        res = self.client.get("/api/invitation/123456/accept/")

        self.assertEqual(res.status_code, 404)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_correct_reject_invitation(self):
        """
        Tests reject action with correct data. Invitation should be deleted.
        """
        self.client.force_login(self.student)
        invitation = self.create_test_invitation()

        before_invitation_amount = Invitation.objects.count()
        before_students_amount = self.class_group.students.count()

        # Current user is teacher of class group that invited student, so
        # invitation object has been rejected (deleted) but user hasn't been
        # added to class group students
        res = self.client.get(f"/api/invitation/{invitation.id}/reject/")

        self.assertEqual(res.status_code, 200)
        self.assertEqual(
            self.class_group.students.count(), before_students_amount
        )
        self.assertGreater(
            before_invitation_amount, Invitation.objects.count()
        )

    def test_invalid_reject_invitation_as_teacher_of_inviting_class_group(self):
        """
        Tests reject action with invalid data. Invitation shouldn't be deleted.
        """
        self.client.force_login(self.teacher)
        invitation = self.create_test_invitation()

        exp_res = {
            "detail": "Only invited user can accept/reject an invitation."
        }

        # Current user is teacher of class group that has invited student, but
        # invitation hasn't been rejected (deleted)
        res = self.client.get(f"/api/invitation/{invitation.id}/reject/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_reject_invitation_as_unrelated_with_invitation(self):
        """
        Tests reject action with invalid data. Invitation shouldn't be deleted.
        """
        self.client.force_login(self.teacher)
        invitation = self.create_test_invitation()

        exp_res = {
            "detail": "Only invited user can accept/reject an invitation."
        }

        # Current user is not related with invitation, so invitation hasn't
        # been deleted
        res = self.client.get(f"/api/invitation/{invitation.id}/reject/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def test_invalid_reject_invitation_doesnt_exist(self):
        """
        Tests reject action with invalid data. Invitation shouldn't be deleted.
        """
        self.client.force_login(self.student)
        exp_res = {"detail": "Not found."}

        # Invitation with that id doesn't exist
        res = self.client.get("/api/invitation/123456/reject/")

        self.assertEqual(res.status_code, 404)
        self.assertJSONEqual(str(res.content, encoding="utf-8"), exp_res)

    def create_test_invitation(self):
        """Creates Invitation object for testing purposes."""
        return Invitation.objects.create(
            class_group=self.class_group, student=self.student
        )

    def get_invitation_expected_response(self):
        """Prepares expected invitation view set response.

        Returns:
            Dictionary with expected invitation view set data
        """
        self.student_data.pop("password")

        return {
            "id": 1,
            "class_group": {
                "id": self.class_group.id,
                "name": self.class_group.name,
                "school": {
                    "id": self.school.id,
                    "name": self.school.name,
                    "logo": None,
                    "city": new_city_data,
                    "principal": self.principal.id,
                    "classes": [self.class_group.id]
                },
                "teacher": self.teacher.id
            },
            "student": {
                **self.student_data,
                "id": self.student.id,
                "profile": None,
                "school": None
            }
        }
