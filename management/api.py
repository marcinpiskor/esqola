import re
from rest_framework import status, viewsets, mixins
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q
from authorization.utils import QueryParamsMixin
from authorization.models import School, ClassGroup
from authorization.permissions import HasQueryParams
from .serializers import (
    SchoolListSerializer,
    SchoolDetailSerializer,
    ClassGroupListSerializer,
    ClassGroupDetailSerializer,
    InvitationSerializer
)
from .permissions import (
    SCHOOL_ALLOWED_PARAMS,
    HasSchoolAllowedQueryParams,
    IsNotPrincipalOfOtherSchool,
    IsPrincipal,
    CanAddSchool,
    CanChangeSchool,
    CanDeleteSchool,
    IsRelatedWithClassGroup,
    CanAddClassGroup,
    CanChangeClassGroup,
    CanDeleteClassGroup,
    IsPrincipalOfSchoolRelatedWithClassGroup,
    IsRelatedWithInvitation,
    CanAddStudentsToClassGroup,
    CanDeleteStudentsFromClassGroup,
    IsInvitedUser
)
from .models import Invitation


class SchoolViewSet(
        QueryParamsMixin,
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.CreateModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet
    ):
    """Viewset that handles getting, creating, updating and deleting school
    objects.

    Attributes:
        model: Django model for checking query paramaters
        str_query_params: string School model fields allowed to be query params
    """
    model = School
    str_query_params = list(SCHOOL_ALLOWED_PARAMS)

    def get_serializer_class(self):
        if self.action == "list":
            return SchoolListSerializer
        return SchoolDetailSerializer

    def get_permissions(self):
        if self.action is None or self.action == "retrieve":
            permissions = (IsAuthenticated, )
        if self.action == "list":
            permissions = (
                IsAuthenticated, HasQueryParams, HasSchoolAllowedQueryParams
            )
        if self.action == "create":
            permissions = (
                IsAuthenticated, CanAddSchool, IsNotPrincipalOfOtherSchool
            )
        if self.action == "update" or self.action == "partial_update":
            permissions = (IsAuthenticated, CanChangeSchool, IsPrincipal)
        if self.action == "destroy":
            permissions = (IsAuthenticated, CanDeleteSchool, IsPrincipal)

        return [permission() for permission in permissions]


class ClassGroupViewSet(
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.CreateModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet
    ):
    """
    Viewset that handles getting, creating, updating and deleting class
    group objects.
    """
    def get_queryset(self):
        if self.action == "list":
            current = self.request.user

            return ClassGroup.objects.filter(
                (Q(teacher=current)
                | Q(school__principal=current)
                | Q(students=current)),
                **self.get_query_params()
            ).distinct()
        else:
            return ClassGroup.objects.all()

    def get_query_params(self):
        """Prepares filter query dictionary with id of class groups to fetch.

        Returns:
            Dictionary with optional id of class groups to get.
        """
        kwargs = {}

        if "id__in" in self.request.query_params:
            kwargs["id__in"] = self.request.query_params["id__in"].split(",")

        return kwargs

    def get_serializer_class(self):
        if self.action == "list":
            return ClassGroupListSerializer
        return ClassGroupDetailSerializer

    def get_permissions(self):
        if self.action == "list" or self.action is None:
            permissions = (IsAuthenticated,)
        if self.action == "retrieve":
            permissions = (IsAuthenticated, IsRelatedWithClassGroup)
        if self.action == "create":
            permissions = (IsAuthenticated, CanAddClassGroup)
        if self.action == "update" or self.action == "partial_update":
            permissions = (
                IsAuthenticated, CanChangeClassGroup,
                IsPrincipalOfSchoolRelatedWithClassGroup
            )
        if self.action == "destroy":
            permissions = (
                IsAuthenticated, CanDeleteClassGroup,
                IsPrincipalOfSchoolRelatedWithClassGroup
            )
        if self.action == "delete_students":
            permissions = (
                IsAuthenticated, IsRelatedWithClassGroup,
                CanDeleteStudentsFromClassGroup
            )

        return [permission() for permission in permissions]

    @detail_route(methods=["POST"], url_path="delete-students")
    def delete_students(self, request, pk=None):
        """Action that handles deleting students from class group.

        Args:
            request: DRF request object
            pk: primary key of Class group object
        Returns:
            Response with blank message
        """
        students_to_remove = request.data.get("students")


        if not students_to_remove:
            return Response(
                {"students": "At least id of one user to delete is required."},
                status.HTTP_400_BAD_REQUEST
            )

        students_to_remove = list(re.findall(r"[0-9]+", students_to_remove))

        class_group = self.get_object()
        class_group.students.remove(*students_to_remove)

        return Response("")


class InvitationViewSet(
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.CreateModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet
    ):
    """Viewset that handles getting, creating, and deleting invitation objects.

    Attributes:
        serializer_class: contains data about which class group is inviting
    """
    serializer_class = InvitationSerializer

    def get_queryset(self):
        if self.action == "list":
            return self.request.user.invitations.all()
        else:
            return Invitation.objects.all()

    def get_permissions(self):
        if self.action == "list" or self.action is None:
            permissions = (IsAuthenticated,)
        if self.action == "create":
            permissions = (IsAuthenticated, CanAddStudentsToClassGroup)
        if self.action == "retrieve" or self.action == "destroy":
            permissions = (IsAuthenticated, IsRelatedWithInvitation)
        if self.action == "accept" or self.action == "reject":
            permissions = (IsAuthenticated, IsInvitedUser)

        return [permission() for permission in permissions]

    @detail_route(methods=["GET"])
    def accept(self, request, pk=None):
        """Action that handles invoking Invitation model method responsible for
        accepting (adding student to class group) invitation.

        Args:
            request: DRF request object
            pk: primary key of Invitation object
        Returns:
            Response with blank message
        """
        invitation = self.get_object()
        invitation.accept()

        return Response("")

    @detail_route(methods=["GET"])
    def reject(self, request, pk=None):
        """Action that handles invoking Invitation model method responsible for
        rejecting (only removing Invitation object) invitation.

        Args:
            request: DRF request object
            pk: primary key of Invitation object
        Returns:
            Response with blank message
        """
        invitation = self.get_object()
        invitation.reject()

        return Response("")
