import re
from django.contrib.auth.models import User
from rest_framework import serializers
from authorization.serializers import (
    CityProfileSerializer,
    UserListSerializer,
    UserDetailSerializer,
    UserClassGroupSerializer,
    UserInvitationSerializer
)
from authorization.models import School, ClassGroup, City
from .models import Invitation


class InvitationClassGroupSerializer(serializers.ModelSerializer):
    """Invitation model serializer for objects related with Class group object.

    Attribute:
        student: detail User model serializer (read-only)
    """
    student = UserDetailSerializer(read_only=True)

    class Meta:
        model = Invitation
        fields = ("id", "student")


class ClassGroupListSerializer(serializers.ModelSerializer):
    """Class group model serializer for list action.

    Attribute:
        teacher: basic User model serializer
    """
    teacher = UserListSerializer(read_only=True)

    class Meta:
        model = ClassGroup
        fields = ("id", "name", "school", "teacher")
        read_only_fields = ("id", "school")


class ClassGroupDetailSerializer(serializers.ModelSerializer):
    """Class group model serializer for retrieve action.

    Attribute:
        students: detail User model serializer
        invited_users: Invitation model serialize for class group invitations
        teacher: basic User model serializer
    """
    students = UserDetailSerializer(many=True, read_only=True)
    invited_users = InvitationClassGroupSerializer(many=True, read_only=True)
    teacher = UserClassGroupSerializer()

    class Meta:
        model = ClassGroup
        fields = (
            "id", "name", "school", "teacher", "students", "invited_users"
        )
        read_only_fields = ("id", "school", "students", "invited_users")

    def validate_name(self, value):
        """Validates if class group with that name already exists in school.

        Arg:
            value: class group name
        Returns:
            Formatted (removed undesired whitespaces) class group name
        Raises:
            Validation error with message that class group with that name
            already exists in school of principal
        """
        value = re.sub(r"[ ]+", " ", value.strip())
        request = self.context["request"]

        # Check if exists Class group object related with principal school with
        # the same name
        if (
            (request.method == "POST"
                and request.user.school.classes.filter(name=value).count() > 0
            ) or
            (self.instance
                and request.user.school.classes.filter(name=value).exclude(
                    id=self.instance.id
                ).count() > 0
            )):
            raise serializers.ValidationError(
                "Class group with that name already exists in that school."
            )

        return value

    def validate_teacher(self, value):
        """Validates if teacher with that id exists.

        Arg:
            value: dictionary with id of desired teacher
        Returns:
            None value or teacher user object
        Raises:
            Validation error with message that teacher user object with passed
            id doesn't exist
        """
        teacher_id = value.get("id")

        if teacher_id is None or teacher_id == 0:
            return None
        else:
            teacher = User.objects.filter(
                id=value.get("id"), groups__name="teachers"
            )

            if teacher.count() > 0:
                return teacher.first()

            msg = "Teacher user with that id doesn't exist."

            raise serializers.ValidationError({"id": [msg]})

    def create(self, validated_data):
        validated_data["school"] = self.context["request"].user.school
        return ClassGroup.objects.create(**validated_data)

    def update(self, instance, validated_data):
        for attr_name, value in validated_data.items():
            setattr(instance, attr_name, value)

        instance.save()
        return instance


class SchoolAutocompleteSerializer(serializers.ModelSerializer):
    """School model serializer for autocomplete action."""
    class Meta:
        model = School
        fields = ("id", "name")
        read_only_fields = ("id", "name")


class SchoolListSerializer(serializers.ModelSerializer):
    """School model serializer for list action.

    Attribute:
        city: City model serializer for objects related with Profile
    """
    city = CityProfileSerializer()

    class Meta:
        model = School
        fields = ("id", "name", "logo", "city", "principal", "classes")
        read_only_fields = ("id", "principal", "classes")


class SchoolDetailSerializer(SchoolListSerializer):
    """School model serializer for retrieve action.

    Attributes:
        city: detail User model serializer
        classes: basic Class group model serializer
    """
    principal = UserDetailSerializer(read_only=True)
    classes = ClassGroupListSerializer(many=True, read_only=True)

    def validate_name(self, value):
        """Removes undesired whitespaces from school name.

        Arg:
            value: school name
        Returns:
            Formatted school name
        """
        return re.sub(r"[ ]+", " ", value.strip())

    def validate(self, validated_data):
        city_id = self.get_city_id(validated_data.get("city"))
        name = validated_data.get("name")

        # Checks if school with that name already exists in city
        if (name and
            School.objects.filter(
                city__place_id=city_id, name__iexact=name
            ).count() > 0):
            msg = "School with that name already exists in that city."
            raise serializers.ValidationError({"school": {"name": [msg]}})

        validated_data["principal"] = self.context["request"].user

        return validated_data

    def create(self, validated_data):
        self.prepare_city_object(validated_data)
        new_school = School.objects.create(**validated_data)

        return new_school

    def update(self, instance, validated_data):
        self.prepare_city_object(validated_data)

        for attr_name, value in validated_data.items():
            setattr(instance, attr_name, value)

        instance.save()
        return instance

    def prepare_city_object(self, validated_data):
        """Creates new City object if city with that data doesn't exist yet.

        Arg:
            validated_data: school detail serializer data
        """
        city_data = validated_data.get("city")

        if city_data and not isinstance(city_data, City):
            validated_data["city"] = City.objects.create(**city_data)

    def get_city_id(self, city_data):
        """Gets city id based on current city data.

        Arg:
            city_data: validated school city data
        Returns:
            School city place id
        """
        if isinstance(city_data, City):
            return city_data.place_id
        elif city_data:
            return city_data["place_id"]


class ClassGroupInvitationSerializer(serializers.ModelSerializer):
    """Class group model serializer for objects related with Invitation model.

    Attributes:
        id: id of Class group object
        scholl: basic School model serializer
    """
    id = serializers.IntegerField()
    school = SchoolListSerializer(read_only=True)

    class Meta:
        model = ClassGroup
        fields = ("id", "name", "school", "teacher")
        read_only_fields = ("id", "name", "school", "teacher")


class ClassGroupGameSessionSerializer(serializers.ModelSerializer):
    """"Class group model serializer for game session serializer."""
    class Meta:
        model = ClassGroup
        fields = ("id", "name")


class InvitationSerializer(serializers.ModelSerializer):
    """Invitation model serializer.

    Attributes:
        class group: Class group model serializer for objects related with
        invitation.
        student: User model serializer for objects related with invitation
    """
    class_group = ClassGroupInvitationSerializer()
    student = UserInvitationSerializer()

    class Meta:
        model = ClassGroup
        fields = ("id", "class_group", "student")

    def validate_class_group(self, value):
        """Validates if class group object with that id exists.

        Arg:
            value: dictionary with Class group object id
        Returns:
            Class group object
        Raises:
            Validation error with message that class group object with that id
            doesn't exist.
        """
        name = "Class group with current user as teacher and"
        return self.check_if_obj_exists(
            ClassGroup, name, value.get("id"),
            teacher=self.context["request"].user
        )

    def validate_student(self, value):
        """Validates if student User object with that id exists.

        Arg:
            value: dictionary with student User object id
        Returns:
            Student user object
        Raises:
            Validation error with message that student User object with that id
            doesn't exist.
        """
        return self.check_if_obj_exists(
            User, "Student user", value.get("id"),
            groups__name="students"
        )

    def check_if_obj_exists(self, model, name, value, **kwargs):
        """Checks if object with passed id exists.

        Args:
            model: model of object to check
            name: error message if object doesn't exist
            value: id of object to check
        Returns:
            Existing object
        Raises:
            Validation error with message that object with that id doesn't
            exist.
        """
        query = model.objects.filter(id=value, **kwargs)

        if query.count() > 0:
            return query.first()

        raise serializers.ValidationError(
            {"id": [f"{name} with that id doesn't exist."]}
        )

    def validate(self, validated_data):
        if validated_data["class_group"].students.filter(
                id=validated_data["student"].id
            ).count() > 0:
            # Checks if user is student of class group
            msg = "This user already is a student of this class group."
            raise serializers.ValidationError({"student": [msg]})

        if Invitation.objects.filter(**validated_data).count() > 0:
            # Checks if class group already invited this user
            msg = "This user has been already invited to this class group."
            raise serializers.ValidationError({"student": [msg]})

        return validated_data

    def create(self, validated_data):
        return Invitation.objects.create(**validated_data)
