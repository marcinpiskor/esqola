from django.db import models
from django.contrib.auth.models import User
from authorization.models import ClassGroup

class Invitation(models.Model):
    class_group = models.ForeignKey(
        ClassGroup,
        on_delete=models.CASCADE,
        related_name="invited_users"
    )
    student = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="invitations"
    )

    def accept(self):
        """
        Adds invited user (student) to class group and deletes invitation.
        """
        self.class_group.students.add(self.student)
        self.delete()

    def reject(self):
        "Deletes invitation."
        self.delete()

    def __str__(self):
        return f"{self.student.username} - {self.class_group.name}"
