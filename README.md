
# Esqola

### Overview
- - -

Esqola is an e-learning platform for English speakers, that allows them to learn new languages by playing educational games.                                                                                       


### Used technologies
- - -
##### Back-end
* Python
* Django
* Django Rest Framework
* Channels
* Redis
##### Front-end
* HTML
* CSS
* SASS
* JavaScript
* React
* Google Maps API

### Games
- - -
#### Match the words
![Match](https://media.giphy.com/media/69sKIu0DVMb22uUOsi/giphy.gif)

Connect English word with its proper translation in foreign language.

#### Choose the answer

![Filter](https://media.giphy.com/media/5jWIqSEtzegSGjI4RR/giphy.gif)

Choose a proper English translation of the foreign word/phrase.

#### Charades

![Charades](https://media.giphy.com/media/paMrii0cFP3mpfcavK/giphy.gif)

Players have to guess what foreign word/phrase has been drawn by one of them.

#### Enter the text
![Enter](https://media.giphy.com/media/2vjpaB9E5ysTV2W5zd/giphy.gif)

Type English translation of foreign word or phrase.

### Presentation
---
####  Authorization
* [Welcome page](https://media.giphy.com/media/1dPi0JQDdsSTAIEnUx/giphy.gif)
* [Signing in](https://media.giphy.com/media/5nvGSX4BVb1345B5y8/giphy.gif)
* [Signing up](https://media.giphy.com/media/X6uTEzr4TBN2KleYic/giphy.gif)

#### Main
##### *Customization*
* [Customization start](https://media.giphy.com/media/1isLftMbFaVpcArvAJ/giphy.gif)
* [Customization process](https://media.giphy.com/media/ZylAaFcXTGVv8zQeMH/giphy.gif)
##### *School*

* [Creating new school](https://media.giphy.com/media/kFTSyyL1NhMPV4HboN/giphy.gif)
* [Creating new class group](https://media.giphy.com/media/BzyYnfjqbvQZvWli63/giphy.gif)
* [Ordering class groups](https://media.giphy.com/media/5YfbIP7AvVP4GfVroa/giphy.gif)
* [Managing class group](https://media.giphy.com/media/9DgaqfBXq4FdQE8Tl7/giphy.gif)

##### *Games*
* [Setting up the game session](https://media.giphy.com/media/tHVOQSKu3StRLClq4h/giphy.gif)
* [Joining to the game session](https://media.giphy.com/media/1woCUUKsB6pnFvR7fl/giphy.gif)
* [Starting the game session](https://media.giphy.com/media/5b5wzHr0iZvAckSvmH/giphy.gif)

##### *Dashboard*
* [Discussion tab](https://media.giphy.com/media/fGIiWzVqrqiqmU7tqW/giphy.gif)
* [Students tab](https://media.giphy.com/media/9M3egCfID9FkgQ8n6B/giphy.gif)
* [Game reports tab](https://media.giphy.com/media/18OfEU66vybSNmYJIm/giphy.gif)
##### *User*
* [Chatting with user](https://media.giphy.com/media/woZ09VIjo9Ejmm1iQW/giphy.gif)

##### *Cockpit*
* [Accepting class groups invitations](https://media.giphy.com/media/324XH7B4Qeg42ZYX4w/giphy.gif)

##### *Settings*
* [Updating user and profile data](https://media.giphy.com/media/nmRfdNArUYVc4BVRLA/giphy.gif)

##### *Search*
* [Searching users and schools](https://media.giphy.com/media/25PvN20Qkc5EnMsJfT/giphy.gif)

##### *RWD*
* [Navigation menu on mobile devices](https://media.giphy.com/media/4NcIbdM9pOTjeolFMP/giphy.gif)
* [Game setup language scroll and pages layout on mobile devices](https://media.giphy.com/media/8gZJID66CLBRxqekAj/giphy.gif)

### How to set up a project
- - -
* Obtain and set environmental variables that contain Google Maps API key and secret key.
* Bundle stylesheet and front-end app files.
```
   cd directory-with-project-files/static/js
   npm install
   npm run build
```

* Install and run Redis server.
* Execute all commands presented below.

```
    cd directory-with-project-files
    pip install -r requirements.txt
    python manage.py makemigrations --settings=esqola.settings.<settings-file>
    python manage.py migrate --settings=esqola.settings.<settings-file>
    python manage.py creategroups --settings=esqola.settings.<settings-file>
    python manage.py runserver --settings=esqola.settings.<settings-file>
```
### Credits
- - -
Flags collection by Freepik - https://www.flaticon.com/packs/flags-collection

Essential Set icons by Smashicons - https://www.flaticon.com/packs/essential-set-2


Use of all photos based on Pexels license.
