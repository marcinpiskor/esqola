import json
import urllib.request
from django.contrib.auth.models import User
from django.db import models, transaction
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist


class City(models.Model):
    """Stores basic info about location of school and user.

    Attributes:
        place_id: Google Places place id
        name: name of city
        region: region in which city is placed
        country: country in which city is placed
        country: two-character country code
        latitude: latitude of city
        longitude: longitude of city
    """
    place_id = models.CharField(
        max_length=128, primary_key=True, db_index=True
    )
    name = models.CharField(max_length=128, db_index=True)
    region = models.CharField(max_length=128, null=True, blank=True)
    country = models.CharField(max_length=128)
    country_short = models.CharField(max_length=2)
    latitude = models.FloatField()
    longitude = models.FloatField()

    class Meta:
        verbose_name_plural = "cities"

    @staticmethod
    def check_if_exists(place_id):
        """Makes Google Maps API request in order to check if city exists.

        Arg:
            place_id: Google Places place id of city to check
        Returns:
            Tuple with boolean that indicates if city exists and City object or
            data necessary to create City object
        Raises:
            Exception with error message that City doesn't exist
        """
        try:
            return True, City.objects.get(place_id=place_id)
        except ObjectDoesNotExist:
            response = {}
            api_key = settings.GOOGLE_MAPS_API_KEY
            url = f"https://maps.googleapis.com/maps/api/place/details/json"
            query_params = f"?placeid={place_id}&key={api_key}"

            try:
                with urllib.request.urlopen(f"{url}{query_params}") as f:
                    response.update(json.loads(f.read().decode("utf-8")))

                if (response["status"] != "OK" or
                    "locality" not in response["result"]["types"]):
                    # Only cities ('locality' type) are allowed
                    raise Exception()
            except:
                raise Exception("City doesn't exist.")

            return False, City.prepare_city_data(response["result"])

    @staticmethod
    def prepare_city_data(city_data):
        """Prepares dictionary with new city data based on fetched info.

        Arg:
            city_data: city data fetched from Google Places API
        Returns:
            Dictionary with required info for new City object (name, lat etc.)
        Raises:
            Exception with error message that City doesn't exist
        """
        properties_to_get = {
            "name": ("locality", "long_name"),
            "region": ("administrative_area_level_1", "long_name"),
            "country": ("country", "long_name"),
            "country_short": ("country", "short_name")
        }

        result = {
            "place_id": city_data["place_id"],
            "latitude": city_data["geometry"]["location"]["lat"],
            "longitude": city_data["geometry"]["location"]["lng"],
        }

        for name, property_data in properties_to_get.items():
            value = City.get_city_property(city_data, *property_data)
            result[name] = value

        return result

    @staticmethod
    def get_city_property(city_data, name, length):
        """Gets desired city property.

        Args:
            city_data: city data fetched from Google Places API
            name: name of city property
            length: desired length of city property (short_name or long_name)
        """
        addresses = city_data["address_components"]

        try:
            return next(el[length] for el in addresses if name in el["types"])
        except StopIteration:
            if name == "administrative_area_level_1":
                return ""

            raise Exception("City doesn't exist.")

    def __str__(self):
        fields = [self.name, self.region, self.country]
        return ", ".join([f for f in fields if f is not None])


class Profile(models.Model):
    """Stores additional info about user.

    Attributes:
        user: user related with profile
        city: current location of user
        photo: image file with photo of user
        birthday: user birthday
        points: amount of points gained during playing the games
    """
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name="profile"
    )
    city = models.ForeignKey(
        City,
        on_delete=models.SET_NULL,
        related_name="users",
        blank=True,
        null=True
    )
    photo = models.ImageField(upload_to="avatars/", blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    points = models.IntegerField(default=0)

    def __str__(self):
        return f"@{self.user.username} profile"

    @property
    def role(self):
        """Returns one of Esqola's group related with user"""
        group_names = [g["name"] for g in groups]
        return self.user.groups.filter(name__in=group_names).first()

    @role.setter
    def role(self, new_role):
        """Sets user role if user isn't already related with any groups.

        Arg:
            new_role: Group object
        """
        group_names = [g["name"] for g in groups]

        if (new_role.name in group_names and
            self.user.groups.filter(name__in=group_names).count() == 0):
            # Add user to Esqola group
            self.user.groups.add(new_role)


    @property
    def is_activated(self):
        """Checks if user is activated by checking if fields aren't blank."""
        required_fields = ["birthday", "role", "city"]
        return all([getattr(self, field) for field in required_fields])

    @staticmethod
    def update_scores(game_players):
        """Updates score points field for users that has taken part in game.

        Arg:
            game_players: dictionary with players id's as keys and players
            scores' as values.
        """
        scores = {}

        for player in game_players:
            if player["score"] > 0:
                scores[player["id"]] = player["score"]

        if len(scores) > 0:
            kwargs = {"user__id__in": scores}
            players = Profile.objects.filter(**kwargs).select_related("user")

            with transaction.atomic():
                for player in players:
                    player.points += scores[player.user.id]
                    player.save()


class School(models.Model):
    """Stores data about school.

    Attributes:
        name: name of school
        logo: image file with logo of school
        city: City object related with school
        principal: User object of principal of school
    """
    name = models.CharField(max_length=128, db_index=True)
    logo = models.ImageField(upload_to="school_logos/", blank=True, null=True)
    city = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        related_name="schools",
    )
    principal = models.OneToOneField(
        User,
        on_delete=models.SET_NULL,
        related_name="school",
        blank=True,
        null=True
    )

    def __str__(self):
        return f"{self.name} - {self.city.name}"


class ClassGroup(models.Model):
    """Stores data about class group.

    Attributes:
        name: name of class group
        school: School object related with class group
        teacher: User object of teacher of class group
        students: User objects of students of class group
    """
    name = models.CharField(max_length=128, db_index=True)
    school = models.ForeignKey(
        School,
        on_delete=models.CASCADE,
        related_name="classes"
    )
    teacher = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="teacher_classes",
        blank=True,
        null=True
    )
    students = models.ManyToManyField(User, related_name="student_classes")

    def __str__(self):
        return f"{self.name} - {self.school.name}"

groups = [
    {
        "name": "principals",
        "permissions": [
            {
                "codename": "add_school",
                "name": "Can add school",
                "model": School
            },
            {
                "codename": "change_school",
                "name": "Can change school",
                "model": School
            },
            {
                "codename": "delete_school",
                "name": "Can delete school",
                "model": School
            },
            {
                "codename": "add_classgroup",
                "name": "Can add class group",
                "model": ClassGroup
            },
            {
                "codename": "change_classgroup",
                "name": "Can change class group",
                "model": ClassGroup
            },
            {
                "codename": "delete_classgroup",
                "name": "Can delete class group",
                "model": ClassGroup
            }
        ]
    },
    {
        "name": "teachers",
        "permissions": [
            {
                "codename": "add_students_to_classgroup",
                "name": "Can add students to class group",
                "model": ClassGroup
            },
            {
                "codename": "delete_students_from_classgroup",
                "name": "Can delete students from class group",
                "model": ClassGroup
            },
        ]
    },
    {
        "name": "students",
        "permissions": []
    }
]
