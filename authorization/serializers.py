import re
from datetime import datetime
from rest_framework import serializers
from django.db.models import Q
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from chat.models import Message
from .models import Profile, City, ClassGroup
from .forms import RegisterForm
from .utils import user_model_fields_data, PASSWORD_ATTRIBUTES

PASSWORD_STYLE = {"input_type": "password"}

class GroupSerializer(serializers.ModelSerializer):
    """Group model serializer for read-only purposes."""
    class Meta:
        model = Group
        fields = ("name", "id")
        read_only_fields = ("name",)


class GroupProfileSerializer(GroupSerializer):
    """Group model serializer for objects related with Profile objects.

    Attribute:
        id: id of Group object related/desired to be related with Profile
    """
    id = serializers.IntegerField()

    def validate(self, value):
        """Validates if Group object exists.

        Arg:
            value: dictionary with id of Group object to check
        Raises:
            Validation error with message that role (group) doesn't exist
        """
        try:
            return Group.objects.get(id=value["id"])
        except ObjectDoesNotExist as err:
            msg = "Role doesn't exist."
            raise serializers.ValidationError({"id": [msg]})


class CityProfileSerializer(serializers.Serializer):
    """City model serializer for objects related with Profile objects.

    Attributes:
        place_id: Google Places place id
        name: name of city
        region: region in which city is placed
        country: country in which city is placed
        country: two-character country code
        latitude: latitude of city
        longitude: longitude of city
    """
    place_id = serializers.CharField()
    name = serializers.CharField(required=False)
    region = serializers.CharField(required=False)
    country = serializers.CharField(required=False)
    country_short = serializers.CharField(max_length=2, required=False)
    latitude = serializers.FloatField(required=False)
    longitude = serializers.FloatField(required=False)

    def validate(self, value):
        """Invokes City model static method that checks if city exists.

        Args:
            value: dictionary with passed city data
        Returns:
            City object or dictionary with data necessary to create City
        Raises:
            Validation error with message that city doesn't exist.
        """
        try:
            exists, city_info = City.check_if_exists(value["place_id"])
            return city_info
        except Exception as err:
            raise serializers.ValidationError({"place_id": err.args[0]})


class ProfileSerializer(serializers.ModelSerializer):
    """City model serializer for serializers related with User.

    Attributes:
        city: City model serializer for objects related with Profile
        role: Group model serializer for objects related with User
    """
    city = CityProfileSerializer()
    role = GroupProfileSerializer()

    class Meta:
        model = Profile
        fields = (
            "birthday", "points", "city", "photo", "role", "is_activated"
        )
        read_only_fields = ("points", "is_activated")

    def validate_birthday(self, value):
        """Validates if birthday is not empty and has proper value.

        Arg:
            value: passed birthday value
        Returns:
            Passed birthday value
        Raises:
            Validation error with message that birthday value is invalid
        """
        today = datetime.now().date()

        if value is None or value > today:
            raise serializers.ValidationError(["Invalid birthday."])

        return value


class ClassGroupCurrentUserSerializer(serializers.ModelSerializer):
    """Class group model serializer for current user serializer."""
    class Meta:
        model = ClassGroup
        fields = ("id", "name", "school")
        read_only_fields = ("id", "name", "school")


class CurrentUserSerializer(serializers.ModelSerializer):
    """User model serializer for current user object.

    Attributes:
        profile: Profile serializer for User model serializers
        student_classes: Class group serializer for user class group objects
        teacher_classes: Class group serializer for user class group objects
    """
    profile = ProfileSerializer()
    student_classes = ClassGroupCurrentUserSerializer(
        read_only=True, many=True
    )
    teacher_classes = ClassGroupCurrentUserSerializer(
        read_only=True, many=True
    )

    class Meta:
        model = User
        fields = (
            "id", "username", "email", "first_name", "last_name", "profile",
            "school", "teacher_classes", "student_classes"
        )
        read_only_fields = (
            "id", "profile", "school", "teacher_classes", "student_classes"
        )

    def validate_email(self, value):
        """Validates if user with passed e-mail value already exists.

        Arg:
            value: passed e-mail address
        Returns:
            Passed e-mail address
        Raises:
            Validation error with message that e-mail isn't available
        """
        user_id = self.context["request"].user.id

        if User.objects.exclude(id=user_id).filter(email=value).count() > 0:
            msg = "A user with that e-mail address already exists."
            raise serializers.ValidationError([msg])

        return value

    def validate_username(self, value):
        """Validates if user with passed username value already exists.

        Arg:
            value: passed username value
        Returns:
            Passed username value
        Raises:
            Validation error with message that username isn't available
        """
        RegisterForm.validate_length_and_pattern(
            "username", value, user_model_fields_data,
            serializers.ValidationError
        )

        user_id = self.context["request"].user.id

        if User.objects.exclude(id=user_id).filter(username=value).count() > 0:
            msg = "A user with that username already exists."
            raise serializers.ValidationError([msg])

        return value

    def validate_first_name(self, value):
        """Validates if passed first name value is valid.

        Arg:
            value: passed first name value
        Returns:
            Passed first name value
        Raises:
            Validation error with message that first name is invalid
        """
        RegisterForm.validate_length_and_pattern(
            "first_name", value, user_model_fields_data,
            serializers.ValidationError
        )

        return value.strip().capitalize()

    def validate_last_name(self, value):
        """Validates if passed last name value is valid.

        Arg:
            value: passed last name value
        Returns:
            Passed last name value
        Raises:
            Validation error with message that last name is invalid
        """
        RegisterForm.validate_length_and_pattern(
            "last_name", value, user_model_fields_data,
            serializers.ValidationError
        )
        last_name = re.sub(r"[  ]+", " ", value.strip())

        return last_name.title()

    def update(self, instance, validated_data):
        """Updates current user object and Profile object related with it.

        Args:
            instance: current user object
            validated_data: dictionary with validated data
        Returns:
            Updated current user object
        """
        profile_info = validated_data.get("profile")

        if profile_info:
            city_data = profile_info.get("city")

            if city_data and not isinstance(city_data, City):
                new_city = City.objects.create(**city_data)
                profile_info["city"] = new_city

        user = self.update_nested_objects(
            instance, validated_data, ["profile"]
        )

        return user

    def update_nested_objects(self, instance, validated_data, nested=[]):
        """Recursive method responsible for setting user/profile attributes.

        Args:
            instance: current user object
            validated_data: dictionary with validated data
            nested_fields: list with attribute names of nested objects
        Returns:
            Updated current user object
        """
        for attr, val in validated_data.items():
            if attr in nested:
                self.update_nested_objects(getattr(instance, attr), val)
            else:
                setattr(instance, attr, val)

        instance.save()

        return instance


class CurrentUserPasswordSerializer(serializers.Serializer):
    """Current user password serializer for changing password.

    Attributes:
        old_password: password field for current password
        new_password: password field for new password
    """
    old_password = serializers.CharField(style=PASSWORD_STYLE)
    new_password = serializers.CharField(
        min_length=PASSWORD_ATTRIBUTES["minlength"], style=PASSWORD_STYLE
    )

    def validate_old_password(self, value):
        """Validates if passed old password value is valid.

        Arg:
            value: passed old password value
        Returns:
            Passed old password value
        Raises:
            Validation error with message that old password is invalid
        """
        if self.context["request"].user.check_password(value):
            return value
        raise serializers.ValidationError(["Old password isn't correct."])

    def validate_new_password(self, value):
        """Validates if new password value matches the pattern.

        Arg:
            value: passed new password value
        Returns:
            Passed new password value
        Raises:
            Validation error with message that new password is invalid
        """
        if re.match(PASSWORD_ATTRIBUTES["pattern"], value):
            return value
        raise serializers.ValidationError([PASSWORD_ATTRIBUTES["title"]])

    def save(self):
        """Sets current user new password."""
        new_password = self.validated_data["new_password"]
        self.context["request"].user.set_password(new_password)
        self.context["request"].user.save()


class CityDetailSerializer(serializers.ModelSerializer):
    """City model serializer for read-only purposes."""
    class Meta:
        model = City
        fields = (
            "place_id", "name", "region", "country", "country_short",
            "latitude", "longitude"
        )
        read_only_fields = (
            "name", "region", "country", "country_short", "latitude",
            "longitude"
        )


class ProfileDetailSerializer(serializers.ModelSerializer):
    """Profile model serializer for read-only purposes.

    Attributes:
        city: read-only City model serializer
        role: read-only Group model serializer
    """
    city = CityDetailSerializer()
    role = GroupSerializer()

    class Meta:
        model = Profile
        fields = ("birthday", "points", "city", "photo", "role")
        read_only_fields = ("birthday", "points", "city", "photo", "role")


class ProfilePostSerializer(serializers.ModelSerializer):
    """Profile model serializer for user serializer for post serializer."""
    class Meta:
        model = Profile
        fields = ("photo",)
        read_only_fields = ("photo",)


class ProfileConversationSerializer(serializers.ModelSerializer):
    """Profile model serializer for user conversation serializer.

    Attribute:
        last_active: serializer method field (last message date)
    """
    last_active = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ("photo", "last_active")
        read_only_fields = ("photo", "last_active")

    def get_last_active(self, obj):
        """Gets date of last message that has been sent to current user.

        Arg:
            obj: Profile object
        """
        current_user = self.context["request"].user
        last_msg = Message.objects.filter(
            Q(to=obj.user, author=current_user) |
            Q(to=current_user, author=obj.user)
        ).first()

        return last_msg.created


class UserListSerializer(serializers.ModelSerializer):
    """User model serializer for list action of user view set."""
    class Meta:
        model = User
        fields = (
            "id", "username", "email", "first_name", "last_name", "profile",
            "school"
        )
        read_only_fields = (
            "username", "email", "first_name", "last_name", "profile",
            "school"
        )


class UserDetailSerializer(UserListSerializer):
    """User model serializer for retrieve action of user view set.

    Attribute:
        profile: Profile serializer with all profile fields
    """
    profile = ProfileDetailSerializer(read_only=True)


class UserPostSerializer(UserListSerializer):
    """User model serializer for user related with Post.

    Attribute:
        profile: Profile serializer only with photo field
    """
    profile = ProfilePostSerializer(read_only=True)


class UserConversationSerializer(UserListSerializer):
    """User model serializer for user related with conversations action.

    Attribute:
        profile: Profile serializer only with photo and last active field
    """
    profile = ProfileConversationSerializer(read_only=True)


class UserClassGroupSerializer(UserDetailSerializer):
    """User model serializer for user related with class group (teacher).

    Attribute:
        id: integer field with id of teacher of class group
    """
    id = serializers.IntegerField(initial=0, allow_null=True)


class UserInvitationSerializer(UserDetailSerializer):
    """User model serializer for user related with invitation (student).

    Attribute:
        id: integer field with id of student invited to class group
    """
    id = serializers.IntegerField()


class UserAutocompleteSerializer(serializers.ModelSerializer):
    """User model serializer for autocomplete action of search view set."""
    class Meta:
        model = User
        fields = ("id", "username", "first_name", "last_name")
        read_only_fields = ("id", "username", "first_name", "last_name")


class UserGameSessionSerializer(serializers.ModelSerializer):
    """User model serializer for game session serializer."""
    class Meta:
        model = User
        fields = ("id", "username")
        read_only_fields = ("id", "username")
