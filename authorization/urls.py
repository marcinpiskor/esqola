from django.urls import path
from .views import IndexView, LoginView, RegisterView, LogoutView

app_name = "authorization"
urlpatterns = [
    path('', view=IndexView.as_view(), name="index"),
    path('login', view=LoginView.as_view(), name="login"),
    path('logout', view=LogoutView.as_view(), name="logout"),
    path('register', view=RegisterView.as_view(), name="register"),
]
