from django.core.management.base import BaseCommand
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from authorization.models import groups


class Command(BaseCommand):
    """Command responsible for creating basic Esqola app user groups."""
    help = "Creates basic Esqola user groups - principals, teachers, students."

    def handle(self, *args, **kwargs):
        # Create group if it doesn't exist and create permissions if group
        # object has been just created.
        for group in groups:
            new_group, created = Group.objects.get_or_create(
                name=group["name"]
            )

            if created:
                for perm in group["permissions"]:
                    perm = dict(perm)

                    ct = ContentType.objects.get_for_model(perm.pop("model"))
                    new_perm, created = Permission.objects.get_or_create(
                        **perm, content_type=ct
                    )

                    new_group.permissions.add(new_perm)

        self.stdout.write(self.style.SUCCESS("All groups have been created."))
