import re
from django import forms
from django.contrib.auth.models import User
from .utils import user_model_fields_data, PASSWORD_ATTRIBUTES


DEFAULT_ATTRIBUTES = {"class": "form-field"}

class LoginForm(forms.Form):
    """Form that provides fields necessary to authenticate user.

    Attributes:
        username: username field of user to authenticate
        password: password field of user to authenticate
    """
    username = forms.CharField(
        label="Username", required=True,
        widget=forms.TextInput(
            {"id": "id_login_username", **DEFAULT_ATTRIBUTES}
        )
    )
    password = forms.CharField(
        label="Password", required=True,
        widget=forms.PasswordInput(
            {"id": "id_login_password", **DEFAULT_ATTRIBUTES}
        )
    )


class RegisterForm(forms.ModelForm):
    """Form that provides fields necessary to create new user.

    Attributes:
        password: password field of new user
        password_repeat: repeat password field of new user
    """
    password = forms.CharField(
        label="Password", required=True,
        widget=forms.PasswordInput(
            {**PASSWORD_ATTRIBUTES, **DEFAULT_ATTRIBUTES}
        )
    )
    password_repeat = forms.CharField(
        label="Repeat password", required=True,
        widget=forms.PasswordInput(
            {**PASSWORD_ATTRIBUTES, **DEFAULT_ATTRIBUTES}
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Provide attributes for fields that has been create automatically
        # based on meta
        for field_name in user_model_fields_data:
            widget, label, field_attrs = user_model_fields_data[field_name]
            self.fields[field_name].required = True
            self.fields[field_name].label = label
            self.fields[field_name].widget = widget(
                {**DEFAULT_ATTRIBUTES, **field_attrs}
            )

    class Meta:
        model = User
        fields = ("email", "username", "first_name", "last_name")

    def clean(self):
        """Checks if password and password repeat field have the same value."""
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_repeat = cleaned_data.get("password_repeat")

        if password and password_repeat and password != password_repeat:
            self.add_error("password", "Passwords are not the same.")

    def clean_email(self):
        """Checks if user with passed e-mail value already exists.

        Returns:
            E-mail address value passed to form
        Raises:
            Validation error with message that e-mail address is not available
        """
        email = self.cleaned_data["email"]

        if User.objects.filter(email=email).count() > 0:
            raise forms.ValidationError("E-mail address is already used.")

        return email

    def clean_username(self):
        """Checks if user with passed username value already exists.

        Returns:
            Username value passed to form
        Raises:
            Validation error with message that username is not available
        """
        username = self.cleaned_data["username"]
        self.validate_length_and_pattern(
            "username", username, user_model_fields_data, forms.ValidationError
        )

        if User.objects.filter(username=username).count() > 0:
            raise forms.ValidationError("Username is already used.")

        return username

    def clean_first_name(self):
        """Checks if passed first name value matches proper pattern and length.

        Returns:
            First name value passed to form
        """
        first_name = self.cleaned_data["first_name"]
        self.validate_length_and_pattern(
            "first_name", first_name, user_model_fields_data,
            forms.ValidationError
        )

        return first_name.strip().capitalize()

    def clean_last_name(self):
        """Checks if passed last name value matches proper pattern and length.

        Returns:
            Last name value passed to form
        """
        last_name = self.cleaned_data["last_name"]
        self.validate_length_and_pattern(
            "last_name", last_name, user_model_fields_data,
            forms.ValidationError
        )
        last_name = re.sub(r"[  ]+", " ", last_name.strip())

        return last_name.title()

    def clean_password(self):
        """Checks if passed password value matches proper pattern and length.

        Returns:
            Password value passed to form
        """
        password = self.cleaned_data["password"]
        password_data = {"password": (0, "Password", PASSWORD_ATTRIBUTES)}

        self.validate_length_and_pattern(
            "password", password, password_data, forms.ValidationError
        )

        return password

    @staticmethod
    def validate_length_and_pattern(name, value, field_info, error):
        """Validates length of field value and checks if value matches pattern.

        Args:
            name: name of field
            value: value of field
            field_info: tuple with info about particular field (field type,
            label text and field attributes)
            error: Exception class to raise if error has occured
        Raises:
            Django form/DRF serializer validation error raised when value has
            invalid length or doesn't match the pattern.
        """
        # Getting field attributes
        field_data = field_info[name][-1]

        minlen = field_data["minlength"]
        maxlen = field_data.get("maxlength")
        pattern = field_data["pattern"]

        if len(value) < minlen or (maxlen and len(value) > maxlen):
            err = RegisterForm.prepare_len_error(name, minlen, maxlen)
            raise error(err)
        elif not re.match(pattern, value):
            raise error(field_data["title"])

    @staticmethod
    def prepare_len_error(field_name, minlen, maxlen):
        """Prepares field invalid length error message.

        Args:
            field_name: name of field
            minlen: required minimum length of field value
            maxlen: required maximum length of field value
        """
        field_name = field_name.capitalize().replace("_", " ")
        err = f"{field_name} needs to have at least {minlen} "

        if maxlen is not None and maxlen > 0:
            err += f"and at most {maxlen} "

        err += "characters."

        return err
