from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User, Group
from management.serializers import (
    SchoolListSerializer,
    SchoolAutocompleteSerializer
)
from .models import School, groups
from .utils import QueryParamsMixin
from .permissions import (
    HasQueryParams,
    HasUserAllowedQueryParams,
    HasSearchQuery
)
from .serializers import (
    CurrentUserSerializer,
    CurrentUserPasswordSerializer,
    UserListSerializer,
    UserDetailSerializer,
    UserAutocompleteSerializer,
    GroupSerializer
)


class CurrentUserViewSet(
        mixins.ListModelMixin, mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin, viewsets.GenericViewSet):
    """Viewset that handles getting and updating data of current user.

    Attributes:
        permission_classes: only for authenticated users
    """
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)

    def get_serializer_class(self):
        if self.action == "change_password":
            return CurrentUserPasswordSerializer
        else:
            return CurrentUserSerializer

    @list_route(methods=["POST"], url_path="change-password")
    def change_password(self, request, *args, **kwargs):
        """Action that handles changing password of current user.

        Args:
            request: DRF request object with old and new password data
        Returns:
            Response with success message or error/s
        """
        context = {"request": request}
        serializer = self.get_serializer_class()
        serializer = serializer(data=request.data, context=context)

        if serializer.is_valid():
            serializer.save()
            msg = {"password": "Password has been changed."}
            return Response(msg, status.HTTP_201_CREATED)

        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class UserViewSet(QueryParamsMixin, viewsets.ReadOnlyModelViewSet):
    """Viewset that handles getting info about users.

    Attributes:
        model: Django model for checking query paramaters
        str_query_params: string User model fields allowed to be query params
    """
    model = User
    str_query_params = [
        "username", "first_name", "last_name", "profile__city__name",
        "groups__name"
    ]

    def get_serializer_class(self):
        if self.action == "list":
            return UserListSerializer
        return UserDetailSerializer

    def get_permissions(self):
        if self.action == "list":
            permissions = (
                IsAuthenticated, HasQueryParams, HasUserAllowedQueryParams
            )
        else:
            permissions = (IsAuthenticated,)

        return [permission() for permission in permissions]


class GroupViewSet(viewsets.ReadOnlyModelViewSet):
    """Viewset that handles getting info about groups.

    Attributes:
        serializer_class: read-only serializer
        permission_classes: only for authenticated users
    """
    queryset = Group.objects.filter(name__in=[g["name"] for g in groups])
    serializer_class = GroupSerializer
    permission_classes = (IsAuthenticated, )


class SearchViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """Viewset that handles searching users and schools based on phrase.

    Attributes:
        permission_classes: authenticated user and search query param are
        required
    """
    permission_classes = (IsAuthenticated, HasSearchQuery)

    def list(self, request, *args, **kwargs):
        serializers_data = {
            "users": UserDetailSerializer,
            "schools": SchoolListSerializer
        }

        return self.search_users_and_schools(request, serializers_data)

    @list_route(methods=["GET"])
    def autocomplete(self, request, *args, **kwargs):
        """Action that provides simplified query results (names only).

        Args:
            request: DRF request object
        Returns:
            Response with serialized search user and school queries data
        """
        serializers_data = {
            "users": UserAutocompleteSerializer,
            "schools": SchoolAutocompleteSerializer
        }

        return self.search_users_and_schools(request, serializers_data, 10)

    def search_users_and_schools(self, request, serializers_data, limit=0):
        """
        Prepares user and school querysets and handles serializing those data.

        Args:
            request: DRF request object
            serializers_data: dictionary that contains "users" and "schools"
            keys and model serializer values
            limit: amount of searched users and schools to serialize
        Returns:
            Response with serialized search user and school queries data
        """
        query_text = request.query_params.get("query").strip()

        data = {
            "users": self.get_user_queryset(query_text),
            "schools": School.objects.filter(name__istartswith=query_text)
        }

        if limit > 0:
            for key in data:
                data[key] = data[key][:limit]

        for key, value in data.items():
            serializer = serializers_data[key]
            data[key] = serializer(value, many=True).data

        return Response(data, status.HTTP_200_OK)

    @staticmethod
    def get_user_queryset(query_text):
        """Provides proper user queryset based on query phrase content.

        Argument:
            query_text: search query phrase
        Returns:
            Proper user queryset
        """
        queryset_data = query_text.split(" ")

        # Filter user objects by first and last name fields if query text
        # contains more than one word
        if len(queryset_data) > 1:
            first_name = queryset_data[0]
            last_name = " ".join(queryset_data[1:])

            return User.objects.filter(
                first_name__iexact=first_name,
                last_name__istartswith=last_name
            )
        # Otherwise, filter user objects by username field
        else:
            return User.objects.filter(username__istartswith=query_text)
