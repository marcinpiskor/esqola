from django.views import View
from django.views.generic import TemplateView, FormView
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.shortcuts import redirect
from .forms import LoginForm, RegisterForm
from .models import Profile
from .utils import LoginRedirectMixin


class IndexView(LoginRedirectMixin, TemplateView):
    """Template view with basic info about site and login/register forms."""
    template_name = "authorization/index.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["login_form"] = LoginForm
        context["register_form"] = RegisterForm

        return context


class LoginView(LoginRedirectMixin, FormView):
    """Form view that handles logging user in."""
    template_name = "authorization/login.html"
    form_class = LoginForm
    success_url = reverse_lazy("main:home")

    def form_valid(self, form):
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        user = authenticate(username=username, password=password)

        if user is not None:
            login(self.request, user)
            return super().form_valid(form)
        else:
            login_msg = "Username and/or password incorrect. Try again."
            messages.error(self.request, login_msg)
            return redirect("authorization:login")


class RegisterView(LoginRedirectMixin, FormView):
    """Form view that handles creating new user."""
    template_name = "authorization/register.html"
    form_class = RegisterForm
    success_url = reverse_lazy("main:home")

    def form_valid(self, form):
        # Delete 'password_repeat' field from data, because User model doesn't
        # have field with that name.
        del form.cleaned_data["password_repeat"]

        try:
            new_user = User.objects.create_user(**form.cleaned_data)
            new_profile = Profile.objects.create(user=new_user)

            success_msg = "Account has been successfully created."
            messages.success(self.request, success_msg)
        except Exception:
            error_msg = "Oops, something went wrong. Try again later."
            messages.error(self.request, error_msg)

        return super().form_valid(form)


class LogoutView(View):
    """View that handles logging user out."""
    def get(self, request):
        if request.user.is_authenticated:
            logout(request)
            logout_msg = "Your account has been logged out."
            messages.success(self.request, logout_msg)

        return redirect(reverse_lazy("authorization:login"))


def handler404(request):
    return redirect(reverse("authorization:index"))
