import io
import re
import time
from datetime import date, datetime
from PIL import Image
from rest_framework.test import APITestCase
from selenium.webdriver.common.keys import Keys
from django.urls import reverse
from django.conf import settings
from django.test import TestCase
from django.core.management import call_command
from django.contrib.auth.models import User, Group, Permission
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from .utils import (
    AuthSeleniumViewHelper,
    user_model_fields_data,
    PASSWORD_ATTRIBUTES,
    new_user_data,
    basic_user_data,
    update_user_data,
    new_city_data,
    UserTestHelper
)
from .permissions import USER_ALLOWED_PARAMS
from .models import Profile, City, School, groups


class AuthorizationCommandsTestCase(TestCase):
    def setUp(self):
        self.args = []
        self.kwargs = {}

    def test_create_groups_command_groups_amount(self):
        """Tests if Esqola groups have been created."""
        before_command = Group.objects.count()
        call_command("creategroups", *self.args, **self.kwargs)

        self.assertGreater(Group.objects.count(), before_command)

    def test_create_groups_command_permissions_amount(self):
        """Tests if permission related with Esqola groups have been created."""
        before_command = Permission.objects.count()
        call_command("creategroups", *self.args, **self.kwargs)

        self.assertGreater(Permission.objects.count(), before_command)

    def test_create_groups_command_groups_content(self):
        """Tests what Esqola groups have been created."""
        call_command("creategroups", *self.args, **self.kwargs)
        required_groups = [g["name"] for g in groups]

        self.assertEqual(
            Group.objects.filter(name__in=required_groups).count(),
            len(required_groups)
        )

    def test_create_groups_command_permissions_content(self):
        """
        Tests what permission related with Esqola groups have been created.
        """
        call_command("creategroups", *self.args, **self.kwargs)
        required_permissions = []

        # Preparing list of permissions expected to be created
        for group in groups:
            for permission in group["permissions"]:
                required_permissions.append(permission["codename"])

        kwargs = {"codename__in": required_permissions}

        self.assertEqual(
            Permission.objects.filter(**kwargs).count(),
            len(required_permissions)
        )


class IndexViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(**basic_user_data)

    def test_view_url_exists_by_location_with_user(self):
        """Tests visiting index view by location with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get("")
        # Redirects to home view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("main:home"))

    def test_view_url_exists_by_name_with_user(self):
        """Tests visiting index view by name with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get(reverse("authorization:index"))
        # Redirects to home view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("main:home"))

    def test_view_url_exists_by_location_without_user(self):
        """Tests visiting index view by location without user logged in."""
        res = self.client.get("")
        self.assertEqual(res.status_code, 200)

    def test_view_url_exists_by_name_without_user(self):
        """Tests visiting index view by name without user logged in."""
        res = self.client.get(reverse("authorization:index"))
        self.assertEqual(res.status_code, 200)

    def test_view_uses_correct_template(self):
        """Tests if index view uses proper template."""
        res = self.client.get(reverse("authorization:index"))
        self.assertTemplateUsed(res, "authorization/index.html")

    def test_view_template_content(self):
        """Tests if index view contains expected content."""
        res = self.client.get(reverse("authorization:index"))
        self.assertContains(res, '<h1 class="logo__title">Esqola</h1>')


class IndexViewSeleniumTest(AuthSeleniumViewHelper, StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.browser.get(self.live_server_url + reverse("authorization:index"))

    def test_logo(self):
        """Tests if logo animation working and size of logo is changing."""
        logo_wrapper = self.browser.find_element_by_class_name("logo-owl")

        self.assertEqual(logo_wrapper.value_of_css_property("width"), "73px")
        time.sleep(5)
        self.assertEqual(logo_wrapper.value_of_css_property("width"), "37px")

    def test_devices_images(self):
        """Tests if devices animation working and devices are visible."""
        device_imgs = self.browser.find_elements_by_class_name(
            "devices__image"
        )

        devices_are_hidden = []

        for img in device_imgs:
            devices_are_hidden.extend([
                img.value_of_css_property("opacity") == "0",
                img.value_of_css_property("transform") == "none"
            ])

        self.assertTrue(all(devices_are_hidden))

        time.sleep(10)

        devices_are_visible = []
        visible_transform = "matrix(1, 0, 0, 1, 0, 0)"

        for img in device_imgs:
            devices_are_visible.extend([
                img.value_of_css_property("opacity") == "1",
                img.value_of_css_property("transform") == visible_transform
            ])

        self.assertTrue(all(devices_are_visible))

    def test_game_example_buttons(self):
        """Tests game example buttons behaviour on click."""
        example_name = "game-example__language"
        lang_1, lang_2 = self.browser.find_elements_by_class_name(example_name)

        self.check_game_buttons(lang_1, lang_2)
        self.check_game_buttons(lang_2, lang_1)

    def test_game_example_success(self):
        """Tests game example success alert."""
        example_name = "game-example__language"
        success_name = "game-example__success"
        visible_name = f"{success_name}--visible"

        lang_1, lang_2 = self.browser.find_elements_by_class_name(
          example_name
        )
        success_block = self.browser.find_element_by_class_name(success_name)

        # Success block is invisible by default
        self.assertEqual(
            success_block.value_of_css_property("visibility"), "hidden"
        )
        self.assertNotIn(visible_name, success_block.get_attribute("class"))

        lang_1_words = lang_1.find_elements_by_class_name("game-word")
        lang_2_words = lang_2.find_elements_by_class_name("game-word")

        # Finish the game example by matching each question with each answer
        for word in lang_1_words:
            i = 0

            while "game-word--correct" not in word.get_attribute("class"):
                word.click()
                lang_2_words[i].click()
                i += 1

            # Remove used answer from possible answers
            lang_2_words.pop(i-1)

        time.sleep(3)

        # All game buttons should be unavailable to click
        unactive_buttons_query = ".game-word:not(.game-word--correct)"
        unactive_buttons = self.browser.find_elements_by_css_selector(
            unactive_buttons_query
        )
        self.assertEqual(len(unactive_buttons), 0)

        success_text_class = "game-example__success--info"

        self.assertEqual(
            self.browser.find_element_by_class_name(success_text_class).text,
            "Good job, keep it up!"
        )
        self.assertIn(visible_name, success_block.get_attribute("class"))

        # Game duration should be displayed
        score = self.browser.find_element_by_id("score").text
        self.assertTrue(re.match(r"[0-9]{1,}.[0-9]{3}s", score))

    def test_forms_behaviour(self):
        """Tests forms fields behaviour and active field wrapper handling."""
        active_class = "form-field__wrapper--active"
        form_fields = self.browser.find_elements_by_class_name("form-field")
        body = self.browser.find_element_by_tag_name("body")

        all_fields_working = []

        for field in form_fields:
            field_parent = self.browser.execute_script(
                "return arguments[0].parentNode", field
            )

            all_fields_working.append(
                active_class not in field_parent.get_attribute("class")
            )
            field.click()

            # Field after focus is active
            all_fields_working.append(
                active_class in field_parent.get_attribute("class")
            )

            # Unfocus active field
            body.click()

            # Field after blur is unactive
            all_fields_working.append(
                active_class not in field_parent.get_attribute("class")
            )

        self.assertTrue(all_fields_working)

    def test_login_form_redirect(self):
        """
        Tests if after sending login form with invalid data, user is redirect
        to login page.
        """
        self.check_form_redirect("login")

    def test_register_form_redirect(self):
        """
        Tests if after sending register form with invalid data, user is redirect
        to register page.
        """
        self.check_form_redirect("register")

    def check_game_buttons(self, primary_lang, secondary_lang):
        """Checks game buttons reaction on click.

        Args:
            primary_lang: wrapper for words of primary language
            secondary_lang: wrapper for words of secondary language
        """
        primary_buttons = primary_lang.find_elements_by_class_name("game-word")

        primary_but_1 = primary_buttons[0]
        primary_but_2 = primary_buttons[1]

        # Clicked game button of primary should be marked (change to red color)
        self.assertEqual(
            primary_but_1.value_of_css_property("color"), "rgb(255, 255, 255)"
        )
        primary_but_1.click()
        self.check_game_button_marked(primary_but_1)

        # Clicked game button of primary lang should be marked (red color)
        # Previous game button of primary lang should be unmarked (white color)
        primary_but_2.click()
        self.check_game_button_marked(primary_but_2)
        self.check_game_button_not_marked(primary_but_1)

        secondary_but = secondary_lang.find_element_by_class_name("game-word")

        # After word match, buttons from both languages should be unmarked
        secondary_but.click()
        self.check_game_button_not_marked(secondary_but)
        self.check_game_button_not_marked(primary_but_2)

    def check_game_button_marked(self, button):
        """Checks if game button is marked.

        Arg:
            button: button element to check
        """
        self.assertIn("game-word--marked", button.get_attribute("class"))
        self.assertEqual(
            button.value_of_css_property("color"), "rgb(181, 64, 64)"
        )

    def check_game_button_not_marked(self, button):
        """Checks if game button is unmarked.

        Arg:
            button: button element to check
        """
        self.assertNotIn("game-word--marked", button.get_attribute("class"))
        self.assertEqual(
            button.value_of_css_property("color"), "rgb(255, 255, 255)"
        )

    def check_form_redirect(self, name):
        """Checks if form redirects on form page with errors.

        Arg:
            name: name of form to check
        """
        form = self.browser.find_element_by_css_selector(
            f"[action='/{name}']"
        )
        form_fields = form.find_elements_by_tag_name(
            "input:not([type=hidden])"
        )

        # Fill form with invalid data.
        for field in form_fields:
            if field.get_attribute("type") in ["email", "password"]:
                value = "this@1S.wrong"
                if field.get_attribute("name") == "password_repeat":
                    value += "a"

                field.send_keys(value)
            else:
                field.send_keys("thisiswrong")

        field.send_keys(Keys.RETURN)
        time.sleep(2)

        # Redirected to form view
        slash_search = re.search(
            r"localhost:[0-9]+/", self.browser.current_url
        )
        url = self.browser.current_url[slash_search.span()[-1]-1:]
        self.assertEqual(url, reverse(f"authorization:{name}"))


class LoginViewTest(TestCase):
    def setUp(self):
        self.user_data = dict(basic_user_data)
        self.user = User.objects.create_user(**basic_user_data)
        self.error_msg = "Username and/or password incorrect. Try again."

    def test_view_url_exists_by_location_with_user(self):
        """Tests visiting login view by location with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get("/login")
        # Redirects to home view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("main:home"))

    def test_view_url_exists_by_name_with_user(self):
        """Tests visiting login view by name with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get(reverse("authorization:login"))
        # Redirects to home view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("main:home"))

    def test_view_url_exists_by_location_without_user(self):
        """Tests visiting login view by location without user logged in."""
        res = self.client.get("/login")
        self.assertEqual(res.status_code, 200)

    def test_view_url_exists_by_name_without_user(self):
        """Tests visiting login view by name without user logged in."""
        res = self.client.get(reverse("authorization:login"))
        self.assertEqual(res.status_code, 200)

    def test_view_uses_correct_template(self):
        """Tests if login view uses proper template."""
        res = self.client.get(reverse("authorization:login"))
        self.assertTemplateUsed(res, "authorization/login.html")

    def test_view_template_content(self):
        """Tests if index view contains expected content."""
        res = self.client.get(reverse("authorization:login"))
        self.assertContains(res, "Sign in")

    def test_post_correct_data(self):
        """Tests POST method of login view. User should be authenticated."""
        res = self.client.post(
            reverse("authorization:login"), self.user_data, follow=True
        )
        self.assertRedirects(res, reverse("main:home"))

    def test_post_incorrect_username(self):
        """Tests POST method of login view. User shouldn't be authenticated."""
        # Passing invalid username
        self.user_data["username"] = "totally_invalid_username"
        res = self.client.post(
            reverse("authorization:login"), self.user_data, follow=True
        )
        self.assertContains(res, self.error_msg)

    def test_post_incorrect_password(self):
        """Tests POST method of login view. User shouldn't be authenticated."""
        # Passing invalid password
        self.user_data["password"] = "tota11y_invalid_pasword!@@@"
        res = self.client.post(
            reverse("authorization:login"), self.user_data, follow=True
        )
        self.assertContains(res, self.error_msg)


class LoginViewSeleniumTest(AuthSeleniumViewHelper, StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.user_data = dict(basic_user_data)
        self.user = User.objects.create_user(**self.user_data)

        self.url = f"{self.live_server_url}{reverse('authorization:login')}"
        self.browser.get(self.url)

    def test_login_correct_data(self):
        """Tests login user with correct data. User should be authenticated."""
        self.send_form()
        # Success, redirected to home view.
        exp_url = self.live_server_url + reverse("main:home")
        self.assertTrue(self.browser.current_url.startswith(exp_url))

    def test_login_incorrect_username(self):
        """
        Tests login user with invalid data. User shouldn't be authenticated.
        """
        self.user_data["username"] = "incorrect_username"
        self.check_error_message()

    def test_login_incorrect_password(self):
        """
        Tests login user with invalid data. User shouldn't be authenticated.
        """
        self.user_data["password"] = "incorrect_password"
        self.check_error_message()

    def check_error_message(self):
        """Checks if failed auth error message has been displayed."""
        self.send_form()
        # URL hasn't changed
        self.assertEqual(self.url, self.browser.current_url)

        msg = self.browser.find_element_by_class_name("auth-message")
        exp_error = "Username and/or password incorrect. Try again."

        self.assertIn("auth-message--error", msg.get_attribute("class"))
        self.assertEqual(msg.text, exp_error)


class RegisterViewTest(UserTestHelper, TestCase):
    def setUp(self):
        self.user_data = dict(new_user_data)
        self.user = User.objects.create_user(
            email="admin@admin.admin", **basic_user_data
        )

    def test_view_url_exists_by_location_with_user(self):
        """Tests visiting register view by location with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get("/register")
        # Redirects to home view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("main:home"))

    def test_view_url_exists_by_name_with_user(self):
        """Tests visiting register view by name with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get(reverse("authorization:register"))
        # Redirects to home view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("main:home"))

    def test_view_url_exists_by_location_without_user(self):
        """Tests visiting register view by location without user logged in."""
        res = self.client.get("/register")
        self.assertEqual(res.status_code, 200)

    def test_view_url_exists_by_name_without_user(self):
        """Tests visiting register view by name without user logged in."""
        res = self.client.get(reverse("authorization:register"))
        self.assertEqual(res.status_code, 200)

    def test_view_uses_correct_template(self):
        """Tests if register view uses proper template."""
        res = self.client.get(reverse("authorization:register"))
        self.assertTemplateUsed(res, "authorization/register.html")

    def test_view_template_content(self):
        """Tests if index view contains expected content."""
        res = self.client.get(reverse("authorization:register"))
        self.assertContains(res, "Sign up")

    def test_post_correct_data_redirect(self):
        """Tests POST method of register view. User should be created."""
        res = self.client.post(
            reverse("authorization:register"), self.user_data, follow=True
        )
        url = f"{reverse('authorization:login')}?next={reverse('main:home')}"
        # Success, redirected to login view
        self.assertRedirects(res, url)
        self.assertContains(res, "Account has been successfully created.")

    def test_post_correct_data_new_user(self):
        """Tests POST method of register view. User should be created."""
        before_user_amount = User.objects.count()
        res = self.client.post(
            reverse("authorization:register"), self.user_data, follow=True
        )
        # New user has been created
        self.assertGreater(User.objects.count(), before_user_amount)

    def test_post_correct_data_user_fields(self):
        """Tests POST method of register view. User should be created."""
        self.client.post(reverse("authorization:register"), self.user_data)

        del self.user_data["password_repeat"]
        password = self.user_data.pop("password")

        user = User.objects.filter(**self.user_data).first()
        # Proper password has been set
        self.assertTrue(user.check_password(password))

    def test_post_used_email(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This e-mail is already related with different user
        value = self.user.email
        error = "E-mail address is already used."
        self.check_incorrect_value("email", value, error)

    def test_post_incorrect_email(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This e-mail doesn't match proper e-mail address pattern
        value = "this_is_incorrect_email_address"
        error = "Enter a valid email address."
        self.check_incorrect_value("email", value, error)

    def test_post_used_username(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This username is already related with different user
        value = self.user.username
        error = "Username is already used."
        self.check_incorrect_value("username", value, error)

    def test_post_incorrect_username(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This username doesn't match proper username pattern
        error = user_model_fields_data["username"][-1]["title"]
        self.check_incorrect_value("username", "1234567", error)

    def test_post_too_short_username(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This username is too short
        self.check_incorrect_length("username", min, "form")

    def test_post_too_long_username(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This username is too long
        self.check_incorrect_length("username", max, "default")

    def test_post_incorrect_first_name(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This first name doesn't match proper first name pattern
        value = "John -$%^"
        error = user_model_fields_data["first_name"][-1]["title"]
        self.check_incorrect_value("first_name", value, error)

    def test_post_too_short_first_name(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This first name is too short
        self.check_incorrect_length("first_name", min, "form")

    def test_post_too_long_first_name(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This first name is too long
        self.check_incorrect_length("first_name", max, "default")

    def test_post_incorrect_last_name(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This last name doesn't match proper last name pattern
        error = user_model_fields_data["last_name"][-1]["title"]
        self.check_incorrect_value("last_name", "Smith ___ &&&", error)

    def test_post_too_short_last_name(self):
        """Test POST method of register view. User shouldn't be created."""
        # This last name is too short
        self.check_incorrect_length("last_name", min, "form")

    def test_post_too_long_last_name(self):
        """Tests POST method of register view. User shouldn't be created."""
        # This last name is too long
        self.check_incorrect_length("last_name", max, "default")

    def test_post_incorrect_password(self):
        """Test POST method of register view. User shouldn't be created."""
        # Generate password that doesn't match proper password pattern
        value = ["a" for x in range(PASSWORD_ATTRIBUTES["minlength"])]
        value = "".join(value)
        error = PASSWORD_ATTRIBUTES["title"]
        self.check_incorrect_value("password", value, error)

    def test_post_too_short_password(self):
        """Tests POST method of register view. User shouldn't be created."""
        minlen = PASSWORD_ATTRIBUTES["minlength"]
        # Generate too short password
        chars = ["a", "A", "1"]
        value = "".join([chars[i%3] for i in range(minlen-1)])
        error = f"Password needs to have at least {minlen} characters."

        self.check_incorrect_value("password", value, error)

    def test_post_unequal_password_fields(self):
        """Tests POST method of register view. User shouldn't be created."""
        # Password value and repeat password value are not equal
        value = "passwords_areNOTeqvea1"
        error = "Passwords are not the same."
        self.check_incorrect_value("password_repeat", value, error)

    def test_post_empty_fields(self):
        """Tests POST method of register view. User shouldn't be created."""
        # Set each field to empty value
        for key in self.user_data:
            self.user_data[key] = ""

        res = self.client.post(
            reverse("authorization:register"), self.user_data
        )

        # Each field has displayed error label
        field_errors = re.findall("This field is required.", str(res.content))
        self.assertEqual(len(self.user_data), len(field_errors))


class RegisterViewSeleniumTest(
        AuthSeleniumViewHelper, StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.user_data = dict(new_user_data)
        url = f"{self.live_server_url}{reverse('authorization:register')}"
        self.browser.get(url)

    def test_all_fields_required(self):
        """Tests if register form fields are all required."""
        send_button = self.browser.find_element_by_tag_name("button")

        user_fields = list(self.user_data.items())
        last_field_name, last_field_value = user_fields.pop()

        # Not every field was filled, no redirected
        for name, value in user_fields:
            self.check_if_redirects(send_button, name, value, self.assertEqual)

        # Success, last field filled and redirected to login view
        self.check_if_redirects(
            send_button, last_field_name, last_field_value, self.assertNotEqual
        )

    def check_if_redirects(self, button, name, value, method):
        """Checks if user after send the registration form was redirected.

        Args:
            button: submit form button element
            name: name of last field to fill
            value: value of last field to fill
            method: assert method (equal or not equal)
        """
        field = self.browser.find_element_by_css_selector(f"[name={name}]")
        field.send_keys(value)

        register_url = self.browser.current_url

        button.click()
        time.sleep(3)
        method(register_url, self.browser.current_url)


class LogoutViewTest(TestCase):
    def setUp(self):
        self.user_data = dict(basic_user_data)
        self.user = User.objects.create_user(**self.user_data)
        self.success_msg = "Your account has been logged out."

    def test_view_url_redirect_by_location_with_user(self):
        """Tests visiting logout view by location with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get("/logout")
        # Redirect to login view after logout
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, "/login")

    def test_view_url_msg_by_location_with_user(self):
        """Tests logout view message content by location with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get("/logout", follow=True)
        # Redirect to login view after logout and display success logout msg
        self.assertEqual(res.status_code, 200)
        self.assertContains(res, self.success_msg)

    def test_view_url_redirect_by_name_with_user(self):
        """Tests visiting logout view by name with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get(reverse("authorization:logout"))
        # Redirect to login view after logout
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("authorization:login"))

    def test_view_url_msg_by_name_with_user(self):
        """Tests logout view message content by name with user logged in."""
        self.client.force_login(self.user)
        res = self.client.get("/logout", follow=True)
        # Redirect to login view after logout and display success logout msg
        self.assertEqual(res.status_code, 200)
        self.assertContains(res, self.success_msg)

    def test_view_url_redirect_by_location_without_user(self):
        """Tests visiting logout view by location without user logged in."""
        res = self.client.get("/logout")
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("authorization:login"))
        # Redirect to login view without logout
        res = self.client.get("/logout", follow=True)
        self.assertNotContains(res, self.success_msg)

    def test_view_url_msg_by_location_without_user(self):
        """Tests logout view message by location without user logged in."""
        res = self.client.get("/logout", follow=True)
        self.assertEqual(res.status_code, 200)
        self.assertNotContains(res, self.success_msg)

    def test_view_url_redirect_by_name_without_user(self):
        """Tests visiting logout view by location without user logged in."""
        res = self.client.get(reverse("authorization:logout"))
        # Redirect to login view
        self.assertEqual(res.status_code, 302)
        self.assertRedirects(res, reverse("authorization:login"))

    def test_view_url_msg_by_name_without_user(self):
        """Tests logout view message by location without user logged in."""
        res = self.client.get(reverse("authorization:logout"), follow=True)
        # Redirect to login view without displaying success logout msg
        self.assertEqual(res.status_code, 200)
        self.assertNotContains(res, self.success_msg)


class CurrentUserAPI(UserTestHelper, APITestCase):
    def setUp(self):
        call_command("creategroups")

        self.user_data = dict(new_user_data)
        self.user_data.pop("password_repeat")
        self.user = User.objects.create_user(**self.user_data)
        self.user_data.pop("password")

        self.update_data = {
            **update_user_data,
            "profile.birthday": "1982-06-22",
            "profile.photo": self.generate_image(),
            "profile.city.place_id": new_city_data["place_id"],
            "profile.role.id": Group.objects.first().id
        }
        self.profile = Profile.objects.create(user=self.user)

        for data in [self.user_data, self.update_data]:
            data["id"] = self.user.id

        self.user_data["school"] = None

    def test_list_without_user(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/current-user/")
        self.assertEqual(res.status_code, 403)

    def test_retrieve_without_user(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        res = self.client.get(f"/api/current-user/{self.user.id}/")
        self.assertEqual(res.status_code, 403)

    def test_put_without_user(self):
        """Tests PUT method without user logged in. Forbidden error expected."""
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )
        self.assertEqual(res.status_code, 403)

    def test_patch_without_user(self):
        """
        Tests PATCH method without user logged in. Forbidden error expected.
        """
        res = self.client.patch(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )
        self.assertEqual(res.status_code, 403)

    def test_list_without_profile(self):
        """Tests list action without profile data filled."""
        self.client.force_login(self.user)
        self.empty_profile()

        # User data with profile attributes equal to null should be returned
        res = self.client.get("/api/current-user/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"), [self.user_data]
        )

    def test_retrieve_without_profile(self):
        """Tests retrieve action without profile data filled."""
        self.client.force_login(self.user)
        self.empty_profile()

        # User data with profile attributes equal to null should be returned
        res = self.client.get(f"/api/current-user/{self.user.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"), self.user_data
        )

    def test_list_with_profile(self):
        """Tests list action with profile data filled and logged in user."""
        self.client.force_login(self.user)
        self.activated_profile()

        # User data with profile attributes should be returned
        res = self.client.get("/api/current-user/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"), [self.user_data]
        )

    def test_retrieve_with_profile(self):
        """Tests retrieve action with profile data filled."""
        self.client.force_login(self.user)
        self.activated_profile()

        # User data with profile attributes should be returned
        res = self.client.get(f"/api/current-user/{self.user.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"), self.user_data
        )

    def test_correct_put_with_new_city(self):
        """Tests PUT method with correct data. New city should be created."""
        self.client.force_login(self.user)
        before_city_amount = City.objects.count()

        # Current user profile should be update and new City object created
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 200)
        self.assertGreater(City.objects.count(), before_city_amount)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"), self.expected_correct_response
        )

    def test_correct_put_with_already_city(self):
        """Tests PUT method with correct data. New city shouldn't be created."""
        self.client.force_login(self.user)

        City.objects.create(**new_city_data)
        before_city_amount = City.objects.count()

        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 200)
        self.assertEqual(City.objects.count(), before_city_amount)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"), self.expected_correct_response
        )

    def test_correct_put_with_current_user_email(self):
        """
        Tests PUT method with correct data. E-mail error shouldn't ocurred.
        """
        self.client.force_login(self.user)

        # This e-mail is used by current user
        self.update_data["email"] = self.user.email

        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 200)

    def test_put_with_invalid_email(self):
        """Tests PUT method with invalid e-mail. E-mail error should ocurred."""
        self.client.force_login(self.user)

        # This e-mail doesn't match proper e-mail pattern
        self.update_data["email"] = "this is invalid"
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"email": ["Enter a valid email address."]}
        )

    def test_put_with_used_email(self):
        """Tests PUT method with invalid data. E-mail error should ocurred."""
        self.client.force_login(self.user)
        admin = User.objects.create(username="admin", email="admin@admin.com")

        # This e-mail is unavailable
        self.update_data["email"] = admin.email
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"email": ["A user with that e-mail address already exists."]}
        )

    def test_correct_put_with_current_user_username(self):
        """
        Tests PUT method with correct data. Username error shouldn't be
        returned.
        """
        self.client.force_login(self.user)

        # This username is used by current user
        self.update_data["username"] = self.user.username
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 200)

    def test_put_with_invalid_username(self):
        """
        Tests PUT method with invalid data. Username error should be returned.
        """
        self.client.force_login(self.user)
        # This username doesn't match proper username pattern
        self.check_incorrect_value(
            "username", "1234567",
            user_model_fields_data["username"][-1]["title"]
        )

    def test_put_with_username_too_short(self):
        """
        Tests PUT method with invalid data. Username error should be returned.
        """
        # This username is too short
        self.client.force_login(self.user)
        self.check_incorrect_length("username", min, "form")

    def test_put_with_username_too_long(self):
        """
        Tests PUT method with invalid data. Username error should be returned.
        """
        # This username is too long
        self.client.force_login(self.user)
        self.check_incorrect_length("username", max, "api")

    def test_put_with_invalid_first_name(self):
        """
        Tests PUT method with invalid data. First name error should be returned.
        """
        self.client.force_login(self.user)
        # This first name doesn't match proper first name pattern
        value = "John -$%^"
        self.check_incorrect_value(
            "first_name", value,
            user_model_fields_data["first_name"][-1]["title"]
        )

    def test_put_with_first_name_too_short(self):
        """
        Tests PUT method with invalid data. First name error should be returned.
        """
        self.client.force_login(self.user)
        # This first name is too short
        self.check_incorrect_length("first_name", min, "form")

    def test_put_with_first_name_too_long(self):
        """
        Tests PUT method with invalid data. First name error should be returned.
        """
        self.client.force_login(self.user)
        # This first name is too long
        self.check_incorrect_length("first_name", max, "api")

    def test_put_with_invalid_last_name(self):
        """
        Tests PUT method with invalid data. Last name error should be returned.
        """
        self.client.force_login(self.user)
        # This last name doesn't match proper first name pattern
        self.check_incorrect_value(
            "last_name", "Van-Brockhorst",
            user_model_fields_data["last_name"][-1]["title"]
        )

    def test_put_with_last_name_too_short(self):
        """
        Tests PUT method with invalid data. Last name error should be returned.
        """
        self.client.force_login(self.user)
        # This last name is too short
        self.check_incorrect_length("last_name", min, "form")

    def test_put_with_first_name_too_long(self):
        """
        Tests PUT method with invalid data. Last name error should be returned.
        """
        self.client.force_login(self.user)
        # This last name is too long
        self.check_incorrect_length("last_name", max, "api")

    def test_put_with_invalid_city_place_id(self):
        """Tests PUT method with invalid data. City error should be returned."""
        self.client.force_login(self.user)

        # City with that place id doesn't exist
        self.update_data["profile.city.place_id"] = "invalid place id"
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"profile": {"city": {"place_id": ["City doesn't exist."]}}}
        )

    def test_put_with_invalid_role_id(self):
        """Tests PUT method with invalid data. Role error should be returned."""
        self.client.force_login(self.user)

        # Group objects with that id doesn't exist
        self.update_data["profile.role.id"] = 12345
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"profile": {"role": {"id": ["Role doesn't exist."]}}
        })

    def test_put_with_invalid_birthday(self):
        """Tests PUT method with invalid data. Birthday should be returned."""
        self.client.force_login(self.user)

        msg = "Date has wrong format. Use one of these formats instead: " \
                                                        "YYYY[-MM[-DD]]."
        exp_res = {"profile": {"birthday": [msg]}}

        # Month part of date is too big
        self.update_data["profile.birthday"] = "1989-20-10"
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_put_with_too_big_birthday(self):
        """Tests PUT method with invalid data. Birthday should be returned."""
        self.client.force_login(self.user)

        # Date is bigger than today's date
        self.update_data["profile.birthday"] = f"{datetime.now().year+1}-10-13"
        res = self.client.put(f"/api/current-user/{self.user.id}/",
                                    self.update_data, format="multipart")

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"profile": {"birthday": ["Invalid birthday."]}}
        )

    def test_patch_field(self):
        """
        Tests PATCH method with correct data. Profile data should be updated.
        """
        self.client.force_login(self.user)
        value = {"username": "iamnewusername"}

        res = self.client.patch(f"/api/current-user/{self.user.id}/", value)
        self.assertContains(res, value["username"])

    def test_change_password_correct_response(self):
        """
        Tests change password action with correct data. Password should be
        changed.
        """
        self.client.force_login(self.user)
        value = {
            "old_password": new_user_data["password"],
            "new_password": "IAmTheNewPassword123"
        }

        res = self.client.post(f"/api/current-user/change-password/", value)

        self.assertEqual(res.status_code, 201)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"password": "Password has been changed."}
        )

        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password(value["new_password"]))

    def test_change_password_invalid_old_password(self):
        """
        Tests change username action with invalid data. Password shouldn't be
        changed.
        """
        self.client.force_login(self.user)
        # Old password is invalid
        value = {
            "old_password": "invalid  old___ paswrod",
            "new_password": "IAmTheNewPassword123"
        }

        res = self.client.post(f"/api/current-user/change-password/", value)

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"old_password": ["Old password isn't correct."]}
        )

        self.user.refresh_from_db()
        self.assertFalse(self.user.check_password(value["new_password"]))

    def test_change_password_invalid_new_password(self):
        """
        Tests change username action with invalid data. Password shouldn't be
        changed.
        """
        self.client.force_login(self.user)
        # New password is invalid
        value = {
            "old_password": new_user_data["password"],
            "new_password": "invalid   new___pasword"
        }

        err_msg = "Password needs to have at least one small/big char " \
                                                            "and number."

        res = self.client.post(f"/api/current-user/change-password/", value)

        self.assertEqual(res.status_code, 400)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"), {"new_password": [err_msg]}
        )

        self.user.refresh_from_db()
        self.assertFalse(self.user.check_password(value["new_password"]))

    def empty_profile(self):
        """Fills profile expected value with empty data."""
        self.user_data["student_classes"] = []
        self.user_data["teacher_classes"] = []

        self.user_data["profile"] = {
            "birthday": None,
            "points": 0,
            "city": None,
            "photo": None,
            "role": None,
            "is_activated": False
        }

    def activated_profile(self):
        """Fills profile expected value with valid data."""
        self.profile.city = City.objects.create(**new_city_data)
        self.profile.birthday = date(1990, 12, 12)
        self.profile.role = Group.objects.first()

        self.profile.save()

        self.user_data["student_classes"] = []
        self.user_data["teacher_classes"] = []

        self.user_data["profile"] = {
            "birthday": self.profile.birthday.isoformat(),
            "points": 0,
            "city": new_city_data,
            "photo": None,
            "role": {
                "id": self.profile.role.id,
                "name": self.profile.role.name
            },
            "is_activated": True
        }

    def check_incorrect_value(self, field, value, error):
        """Makes invalid PUT request and checks if error has been returned.

        Args:
            field: field to set invalid data
            value: invalid value of field
            error: text of error expected to be returned
        """
        self.update_data[field] = value
        res = self.client.put(
            f"/api/current-user/{self.user.id}/", self.update_data,
            format="multipart"
        )
        self.assertEqual(res.status_code, 400)

        exp_error = {}
        exp_error[field] = [error]
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_error)

    @property
    def expected_correct_response(self):
        """Prepares expected current user correct response.

        Returns:
            Dictionary that contains expected current user response data
        """
        self.profile.refresh_from_db()

        return {
            **update_user_data,
            "id": self.user.id,
            "profile": {
                "birthday": self.update_data["profile.birthday"],
                "points": 0,
                "city": new_city_data,
                "photo": "http://testserver/media/" + self.profile.photo.name,
                "role": {
                    "id": Group.objects.first().id,
                    "name": Group.objects.first().name
                },
                "is_activated": True
            },
            "school": None,
            "student_classes": [],
            "teacher_classes": [],
        }

    def generate_image(self):
        """Generates profile photo image file.

        Returns:
            Generated image file
        """
        file = io.BytesIO()
        image = Image.new("RGBA", size=(100, 100), color=(155, 0, 0))
        image.save(file, "png")

        file.name = f"{settings.SECRET_KEY}.png"
        file.seek(0)

        return file


class UserAPI(UserTestHelper, APITestCase):
    def setUp(self):
        call_command("creategroups")

        self.user_data_1 = dict(new_user_data)
        [self.user_data_1.pop(f) for f in ["password", "password_repeat"]]

        self.user_data_2 = {
            "email": "margaret@matcher.com",
            "username": "maggie_m",
            "first_name": "Margaret",
            "last_name": "Matcher",
        }

        for data in [self.user_data_1, self.user_data_2]:
            user = User.objects.create_user(**data)
            profile = Profile.objects.create(user=user)
            profile.role = Group.objects.first()

            data["id"] = user.id

    def test_list_without_user(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        query_param = f"first_name={self.user_data_2['first_name']}"
        res = self.client.get(f"/api/user/?{query_param}")

        self.assertEqual(res.status_code, 403)

    def test_retrieve_without_user(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        res = self.client.get(f"/api/user/{User.objects.last().id}/")
        self.assertEqual(res.status_code, 403)

    def test_list_with_valid_normal_query_param(self):
        """
        Tests list action with allowed query param. Users should be returned.
        """
        user = User.objects.first()
        self.client.force_login(user)

        exp_res = {
            **self.user_data_1,
            "profile": user.profile.id,
            "school": None
        }

        query_param = f"username={user.username}"
        res = self.client.get(f"/api/user/?{query_param}")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), [exp_res])

    def test_list_with_valid_nested_query_param(self):
        """
        Tests list action with allowed nested query param. Users should be
        returned.
        """
        users = User.objects.all()
        self.client.force_login(users[0])
        query_param = f"groups__name={users[0].groups.first().name}"

        exp_res = []

        for i, data in enumerate([self.user_data_1, self.user_data_2]):
            exp_res.append({
                **data,
                "profile": users[i].profile.id,
                "school": None
            })

        res = self.client.get(f"/api/user/?{query_param}")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_list_without_query_params(self):
        """Tests list action without query param. Error should be returned."""
        self.client.force_login(User.objects.first())

        res = self.client.get("/api/user/")
        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf8") ,
            {"detail": "Query params are required in this request."}
        )

    def test_list_with_invalid_query_param(self):
        """
        Tests list action without unallowed query param. Error should be
        returned.
        """
        self.client.force_login(User.objects.first())

        res = self.client.get("/api/user/?birthday=1990-21-21")
        self.assertEqual(res.status_code, 403)
        err = "Only these query params are allowed in this request: " \
                                    f"{', '.join(USER_ALLOWED_PARAMS)}."
        self.assertJSONEqual(
            str(res.content, encoding="utf8"), {"detail": err}
        )

    def test_retrieve(self):
        """Tests retrieve action. User should be returned."""
        self.client.force_login(User.objects.first())
        user_to_retrieve = User.objects.last()

        res = self.client.get(f"/api/user/{user_to_retrieve.id}/")
        self.assertEqual(res.status_code, 200)

        user_2_role = user_to_retrieve.groups.first()

        exp_res = {
            **self.user_data_2,
            "profile": {
                "birthday": None,
                "city": None,
                "photo": None,
                "points": 0,
                "role": {
                    "id": user_2_role.id,
                    "name": user_2_role.name
                }
            },
            "school": None,
        }

        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)


class GroupsAPI(APITestCase):
    def setUp(self):
        call_command("creategroups")
        self.user = User.objects.create_user(**basic_user_data)
        self.esqola_names = [g["name"] for g in groups]

    def test_list_correct(self):
        """
        Tests list action with user logged in. Groups should be returned.
        """
        self.client.force_login(self.user)
        esqola_groups = Group.objects.filter(name__in=self.esqola_names).all()

        exp_res = []

        for group in esqola_groups:
            exp_res.append({"id": group.id, "name": group.name})

        res = self.client.get("/api/group/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_list_without_user(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/group/")
        self.assertEqual(res.status_code, 403)

    def test_retrieve_correct(self):
        """
        Tests retrieve action with user logged in. Group should be returned.
        """
        self.client.force_login(self.user)
        esqola_group = Group.objects.filter(name__in=self.esqola_names).first()

        exp_res = {"id": esqola_group.id, "name": esqola_group.name}

        res = self.client.get(f"/api/group/{esqola_group.id}/")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(str(res.content, encoding="utf8"), exp_res)

    def test_retrieve_without_user(self):
        """
        Tests retrieve action without user logged in. Forbidden error expected.
        """
        esqola_group = Group.objects.filter(name__in=self.esqola_names).last()

        res = self.client.get(f"/api/group/{esqola_group.id}/")
        self.assertEqual(res.status_code, 403)


class SearchAPI(APITestCase):
    def setUp(self):
        self.empty_profile = {
            "birthday": None,
            "points": 0,
            "city": None,
            "photo": None,
            "role": None
        }

        self.user_data_1 = dict(new_user_data)
        [self.user_data_1.pop(f) for f in ["password", "password_repeat"]]

        self.user_data_2 = dict(self.user_data_1)
        self.user_data_2["username"] = f"{self.user_data_2['username']}_123"

        for user_data in [self.user_data_1, self.user_data_2]:
            user = User.objects.create_user(**user_data)
            Profile.objects.create(user=user)

        self.city = City.objects.create(**new_city_data)
        self.school = School.objects.create(
            name=f"{self.user_data_1['username']} university", city=self.city
        )

    def test_list_with_one_word_query(self):
        """
        Tests list action with correct query param. Users and school should be
        returned.
        """
        user = User.objects.first()
        self.client.force_login(user)

        # Query is a single word
        res = self.client.get(f"/api/search/?query={user.username}")
        self.assertEqual(res.status_code, 200)

        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {
                "users": self.expected_search_users_schools,
                "schools": [
                    {
                        "id": 1,
                        "name": self.school.name,
                        "logo": None,
                        "city": new_city_data,
                        "principal": None,
                        "classes": []
                    }
                ]
            }
        )

    def test_list_with_phrase_query(self):
        """
        Tests list action with correct query param. Users should be returned.
        """
        user = User.objects.first()
        self.client.force_login(user)

        # Query has more than one word
        query = f"{user.first_name} {user.last_name}"
        res = self.client.get(f"/api/search/?query={query}")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"users": self.expected_search_users_schools, "schools": []}
        )

    def test_autocomplete_with_one_word_query(self):
        """
        Tests autocomplete action with correct query param. Users and school
        should be returned.
        """
        user = User.objects.first()
        self.client.force_login(user)

        # Query is a single word
        url = f"/api/search/autocomplete/?query={user.username}"
        res = self.client.get(url)

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {
                "users": self.expected_autocomplete_users,
                "schools": [{"id": self.school.id, "name": self.school.name}]
            }
        )

    def test_autocomplete_with_phrase_query(self):
        """
        Tests autocomplete action with correct query param. Users should be
        returned.
        """
        user = User.objects.first()
        self.client.force_login(user)

        # Query has more than one word
        query = f"{user.first_name} {user.last_name}"
        res = self.client.get(f"/api/search/autocomplete/?query={query}")

        self.assertEqual(res.status_code, 200)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"users": self.expected_autocomplete_users, "schools": []}
        )

    def test_list_without_user(self):
        """
        Tests autocomplete action without user logged in. Forbidden error
        expected.
        """
        res = self.client.get("/api/search/?query=abc")
        self.assertEqual(res.status_code, 403)

    def test_list_without_query(self):
        """
        Tests autocomplete action without user logged in. Forbidden error
        expected.
        """
        self.client.force_login(User.objects.first())
        # Search query param is required
        res = self.client.get("/api/search/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"detail": "Search query param is required."}
        )

    def test_autocomplete_without_user(self):
        """
        Tests list action without user logged in. Forbidden error expected.
        """
        res = self.client.get("/api/search/autocomplete/?query=abc")
        self.assertEqual(res.status_code, 403)

    def test_autocomplete_without_query(self):
        """Tests list action without query param. Forbidden error expected."""
        self.client.force_login(User.objects.first())

        # Search query param is required
        res = self.client.get("/api/search/autocomplete/")

        self.assertEqual(res.status_code, 403)
        self.assertJSONEqual(
            str(res.content, encoding="utf8"),
            {"detail": "Search query param is required."}
        )

    @property
    def expected_search_users_schools(self):
        """Prepares expected search users and schools correct response.

        Returns:
            Dictionary that contains expected users and schools response data
        """
        return [
            {
                **self.user_data_1,
                "id": 1,
                "profile": self.empty_profile,
                "school": None
            },
            {
                **self.user_data_2,
                "id": 2,
                "profile": self.empty_profile,
                "school": None
            }
        ]

    @property
    def expected_autocomplete_users(self):
        """Prepares expected autocomplete users and schools correct response.

        Returns:
            Dictionary that contains expected autocomplete users and schools
            response data
        """
        fields = ["id", "username", "first_name", "last_name"]
        result = []

        for user in User.objects.all():
            result.append({field: getattr(user, field) for field in fields})

        return result


class CityModelTestCase(TestCase):
    def test_check_if_exists_method_city_already_exists(self):
        """
        Tests check if city exists method. Already created city should be
        returned.
        """
        city = City.objects.create(**new_city_data)
        created, city_info = City.check_if_exists(city.place_id)

        # City object with that place id already exists
        self.assertTrue(created)
        self.assertTrue(isinstance(city_info, City))

    def test_check_if_exists_method_new_city(self):
        """
        Tests check if city exists method. Dictionary with info about new city
        should be returned.
        """
        created, city_info = City.check_if_exists(new_city_data["place_id"])

        # City object with that place id doesn't exist yet
        self.assertFalse(created)
        self.assertTrue(isinstance(city_info, dict))

    def test_check_if_exists_method_city_doesnt_exist(self):
        """Tests check if city exists method. Exception should be raised."""
        # There is no city with that place id in Google Places
        self.assertRaises(
            Exception,
            lambda: City.check_if_exists("this_city_doesnt_exist123")
        )


class ProfileModelTestCase(TestCase):
    def setUp(self):
        call_command("creategroups")
        self.user = User.objects.create(**basic_user_data)
        self.profile = Profile.objects.create(user=self.user)

    def test_role_property_getter_no_group(self):
        """Tests role property getter. None value should be returned."""
        self.assertIsNone(self.profile.role)

    def test_role_property_getter_no_esqola_group(self):
        """Tests role property getter. None value should be returned."""
        # This group was not created by 'creategroups' command
        new_group = Group.objects.create(name="Super new group")
        self.user.groups.add(new_group)
        self.assertIsNone(self.profile.role)

    def test_role_property_getter_esqola_group(self):
        """Tests role property getter. Group object should be returned."""
        # This group was created by 'creategroups' command
        esqola_group = Group.objects.first()
        self.profile.role = esqola_group
        self.assertEqual(self.profile.role, esqola_group)

    def test_role_property_setter_no_group(self):
        """Tests role property setter. Role should be set."""
        self.assertIsNone(self.profile.role)
        # This group was created by 'creategroups' command
        self.profile.role = Group.objects.first()
        self.assertTrue(isinstance(self.profile.role, Group))

    def test_role_property_setter_no_esqola_group(self):
        """Tests role property setter. Role shouldn't be set."""
        self.assertIsNone(self.profile.role)
        # This group wasn't created by 'creategroups' command
        new_group = Group.objects.create(name="Super new group")
        self.profile.role = new_group
        self.assertIsNone(self.profile.role)

    def test_role_property_setter_already_set_group(self):
        """Tests role property setter. Role shouldn't be set."""
        esqola_groups = Group.objects.all()
        self.profile.role = esqola_groups[0]
        # Role can be set only once
        self.profile.role = esqola_groups[1]
        self.assertEqual(self.profile.role, esqola_groups[0])

    def test_is_activated_property_no_activated(self):
        """Tests is activated property. False value should be returned."""
        self.assertFalse(self.profile.is_activated)

    def test_is_activated_property_activated(self):
        """Tests is activated property. True value should be returned."""
        self.assertFalse(self.profile.is_activated)

        # City, birthday and role fields are required to activate the profile
        self.profile.city = City.objects.create(**new_city_data)
        self.assertFalse(self.profile.is_activated)

        self.profile.birthday = date(2000, 10, 10)
        self.assertFalse(self.profile.is_activated)

        self.profile.role = Group.objects.first()
        self.assertTrue(self.profile.is_activated)

    def test_update_scores_method(self):
        """Tests update scores method. Amount of points should be updated."""
        user_data = dict(new_user_data)
        user_data.pop("password_repeat")

        user_2 = User.objects.create_user(**user_data)
        profile_2 = Profile.objects.create(user=user_2)

        user_1_points_before = self.profile.points
        user_2_points_before = profile_2.points

        scores = [
            {"id": self.user.id, "score": 1000},
            {"id": user_2.id, "score": 800},
        ]

        Profile.update_scores(scores)

        [p.refresh_from_db() for p in [self.profile, profile_2]]

        self.assertEqual(
            self.profile.points - user_1_points_before, scores[0]["score"]
        )
        self.assertEqual(
            profile_2.points - user_2_points_before, scores[1]["score"]
        )
