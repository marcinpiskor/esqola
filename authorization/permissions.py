from rest_framework import permissions


USER_ALLOWED_PARAMS = {
    "username", "first_name", "last_name", "school", "profile__city__name",
    "profile__birthday", "profile__points", "groups__name"
}

class HasQueryParams(permissions.BasePermission):
    """Permission that checks if any query paramater has been passed."""
    message = "Query params are required in this request."

    def has_permission(self, request, view):
        if len(request.query_params) > 0:
            return True
        else:
            return False


class HasAllowedParams(permissions.BasePermission):
    """
    Parent class for permission responsible for checking if allowed params has
    been passed
    """
    def has_permission(self, request, view):
        passed_params = set(request.query_params.keys())
        return len(passed_params) == len(self.allowed_params&passed_params)


class HasUserAllowedQueryParams(HasAllowedParams):
    """Permission that checks if user allowed query params has been passed."""
    allowed_params = USER_ALLOWED_PARAMS
    message = "Only these query params are allowed in this request: " \
                                    f"{', '.join(USER_ALLOWED_PARAMS)}."


class HasSearchQuery(permissions.BasePermission):
    """Permission that checks if search query params has been passed."""
    message = "Search query param is required."

    def has_permission(self, request, view):
        query_text = request.query_params.get("query")

        if query_text and len(query_text.strip()) > 1:
            return True
        else:
            return False
