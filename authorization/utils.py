import re
import time
from selenium import webdriver
from django import forms
from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect


PASSWORD_ATTRIBUTES = {
    "pattern": "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$",
    "title": "Password needs to have at least one small/big char and number.",
    "minlength": 6
}
user_model_fields_data = {
    "email": (forms.EmailInput, "E-mail address", {}),
    "username": (
        forms.TextInput,
        "Username",
        {
            "minlength": 4,
            "maxlength": 150,
            "pattern": "^(?=.*[a-zA-Z]).*$",
            "title": "Username needs to contain at least one character."
        }
    ),
    "first_name": (
        forms.TextInput,
        "First name",
        {
            "minlength": 2,
            "maxlength": 30,
            "pattern": "^[a-zA-Z]+$",
            "title": "First name need to contain only characters."
        }
    ),
    "last_name": (
        forms.TextInput,
        "Last name",
        {
            "minlength": 2,
            "maxlength": 150,
            "pattern": "^[a-zA-Z ]+$",
            "title": "Last name needs to contain characters and spaces."
        }
    ),
}
password = "SuperHardPassword123"
basic_user_data = {
    "username": "admin",
    "password": "adminadmin123"
}
new_user_data = {
    "email": "adam@smith.com",
    "username": "iamadam",
    "first_name": "Adam",
    "last_name": "Smith",
    "password": password,
    "password_repeat": password
}
update_user_data = {
    "email": "john@smith.com",
    "username": "johnsmithandwesson",
    "first_name": "John",
    "last_name": "Smith",
}
new_city_data = {
    "place_id": "ChIJz-fn0PHGPUcRcrIsc6p0wI0",
    "name": "Gorlice",
    "region": "Lesser Poland Voivodeship",
    "country": "Poland",
    "country_short": "PL",
    "latitude": 49.6546159,
    "longitude": 21.1596321
}


class SeleniumViewHelper:
    def setUp(self):
        super().setUp()
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_element_size(self, value, exp_value, **kwargs):
        """Invokes method responsible for validating pixel size of element.

        Args:
            value: string value to check in px
            exp_value: expected value (float or int)
        """
        if isinstance(exp_value, list):
            for i, size in enumerate(value.split()):
                self.validate_pixel_size(size, exp_value[i], **kwargs)
        else:
            self.validate_pixel_size(value, exp_value, **kwargs)

    def validate_pixel_size(self, value, exp_value, tolerance=0):
        """Extracts element size in pixels and compares with expected value.

        Args:
            value: string value to check in px
            exp_value: expected value (float or int)
        """
        if "." in value:
            pattern = r"[0-9]+.[0-9]+"
        else:
            pattern = r"[0-9]+"

        value = float(re.search(pattern, value).group())
        self.assertLessEqual(abs(value - exp_value), tolerance)

    def send_form(self):
        """Fills form with user data and sends it."""
        for name, value in self.user_data.items():
            selector = f"[name={name}]"
            field = self.browser.find_element_by_css_selector(selector)
            field.send_keys(value)

        self.browser.find_element_by_tag_name("button").click()
        time.sleep(2)


class AuthSeleniumViewHelper(SeleniumViewHelper):
    def check_form_media_queries(self):
        """Checks changing size of form after window resizing."""
        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=1000, height=600)

        section = self.browser.find_element_by_class_name("auth-section")
        form = self.browser.find_element_by_class_name("auth-form")

        self.check_element_size(
            section.value_of_css_property("padding-top"), 16
        )
        self.check_element_size(
            section.value_of_css_property("padding-right"), 80
        )

        self.check_element_size(
            form.value_of_css_property("padding-top"), 0
        )
        self.check_element_size(
            form.value_of_css_property("padding-right"), 210, tolerance=15
        )

        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=680, height=600)

        self.check_element_size(
            form.value_of_css_property("padding-top"), 0
        )
        self.check_element_size(
            form.value_of_css_property("padding-right"), 78, tolerance=15
        )

        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(width=500, height=600)

        self.check_element_size(
            form.value_of_css_property("padding-top"), 0
        )
        self.check_element_size(
            form.value_of_css_property("padding-right"), 0
        )

        self.check_element_size(
            form.value_of_css_property("padding-top"), 0
        )
        self.check_element_size(
            form.value_of_css_property("padding-right"), 0
        )


class UserTestHelper:
    def check_incorrect_length(self, field, func, len_type):
        """Checks if error about incorrect length has been displayed.

        Args:
            field: name of field to check
            func: min or max function
            len_type: type of length error
        """
        minlen, maxlen = self.get_min_max_field_len(field)
        # Get invalid length (too short or too long)
        value_len = func(minlen, maxlen) + func(1, -1)
        # Generate string based on invalid length
        value = "".join(["a" for i in range(value_len)])

        # Prepare expected error message
        if len_type == "default":
            error = "Ensure this value has at most {0} characters (it has {1})."
            error = error.format(maxlen, value_len)
        elif len_type == "api":
            error = "Ensure this field has no more than {0} characters."
            error = error.format(maxlen)
        elif len_type == "form":
            field_error_name = " ".join(field.split("_")).capitalize()
            error = "{0} needs to have at least {1} and at most {2} characters."
            error = error.format(field_error_name, minlen, maxlen)

        self.check_incorrect_value(field, value, error)

    def get_min_max_field_len(self, field):
        """Gets info about min and max length of particular user

        Arg:
            field: name of field to get info about
        Returns
            Tuple with min length as first and max length as second element
        """
        data = user_model_fields_data[field][-1]
        return data["minlength"], data.get("maxlength")

    def check_incorrect_value(self, field, value, error):
        """Makes invalid POST request and checks if error has been displayed.

        Args:
            field: field to set invalid data
            value: invalid value of field
            error: text of error expected to be displayed
        """
        self.user_data[field] = value
        url = reverse("authorization:register")
        res = self.client.post(url, self.user_data)

        self.assertContains(res, error)


class LoginRedirectMixin:
    """Mixin that provides method which redirects authenticated user to home."""
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse_lazy("main:home"))

        return super().dispatch(request, *args, **kwargs)


class QueryParamsMixin:
    """
    Mixin class that provides methods that help list action results filtering.
    """
    def get_queryset(self):
        """Returns proper queryset dependently on amount of query params."""
        if len(self.request.query_params) > 0:
            return self.model.objects.filter(**self.get_query_params())
        else:
            return self.model.objects.all()

    def get_query_params(self, clear_pagination=False):
        """Prepares dictionary with data for filter query.

        Arg:
            clear_pagination: bool that indicates if remove query params
            related with pagination
        Returns:
            Dictionary with data for filter query
        """
        data = {}
        query_params_names = list(self.request.query_params.keys())

        if clear_pagination:
            for key in ["limit", "offset"]:
                if key in query_params_names:
                    query_params_names.remove(key)

        for name in query_params_names:
            value = self.request.query_params[name]

            if name in self.str_query_params:
                name += "__istartswith"

            if isinstance(value, list):
                value = value[0]

            data[name] = value

        return data
