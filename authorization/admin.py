from django.contrib import admin
from .models import Profile, City


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ("place_id", "name", "region", "country")


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    fields = ("user", "city", "birthday", "points", "photo")
    list_display = ("username", "city", "birthday", "points")

    def username(self, obj):
        return obj.user.username

    def city(self, obj):
        return obj.city.name, obj.city.country_short
